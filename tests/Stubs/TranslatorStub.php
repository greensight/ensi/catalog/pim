<?php

namespace Tests\Stubs;

use Illuminate\Contracts\Translation\Translator;

/**
 * TranslatorStub - заглушка для переводчика в модульных тестах.
 * @package Tests\Unit
 */
class TranslatorStub implements Translator
{
    private string $locale = 'ru';

    /**
     * @inheritDoc
     */
    public function get($key, array $replace = [], $locale = null)
    {
        return $key;
    }

    /**
     * @inheritDoc
     */
    public function choice($key, $number, array $replace = [], $locale = null)
    {
        return $key;
    }

    /**
     * @inheritDoc
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @inheritDoc
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
}
