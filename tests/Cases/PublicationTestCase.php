<?php

namespace Tests\Cases;

use App\Domain\Products\Publication\Editor;
use Illuminate\Support\Collection;
use Mockery;

trait PublicationTestCase
{
    protected function mockEditor(?array $methods = null, array $reviews = [], array $verifies = []): Editor
    {
        $allows = [
            'update' => null,
            'commit' => null,
            'rollback' => null,
            'review' => new Collection($reviews),
            'verify' => new Collection($verifies),
        ];

        if ($methods !== null) {
            $allows = array_intersect_key($allows, array_flip($methods));
        }

        return Mockery::mock(Editor::class, $allows);
    }
}
