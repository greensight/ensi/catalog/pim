<?php

namespace Tests;

use App\Support\Extensions\OptionalMacros;
use Carbon\CarbonImmutable;
use Illuminate\Config\Repository;
use Illuminate\Support\DateFactory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Optional;
use Illuminate\Validation\Factory;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase as BaseUnitTestCase;
use Symfony\Component\VarDumper\VarDumper;
use Tests\Stubs\TranslatorStub;

class UnitTestCase extends BaseUnitTestCase
{
    use MockeryPHPUnitIntegration;

    private static bool $init = false;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::initApplication();
    }

    protected function setUp(): void
    {
        parent::setUp();

        app()->instance('config', new Repository());
    }

    public static function initApplication(): void
    {
        if (self::$init) {
            return;
        }

        VarDumper::setHandler(null);
        DateFactory::useClass(CarbonImmutable::class);

        Optional::mixin(new OptionalMacros());

        with(app(), function ($container) {
            $translator = new TranslatorStub();
            $container->instance('translator', $translator);

            Validator::swap(new Factory($translator, $container));
        });

        self::$init = true;
    }
}
