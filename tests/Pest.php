<?php

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Testing\Fakes\QueueFake;
use Illuminate\Testing\TestResponse;

use function Pest\Laravel\post;

use PHPUnit\Framework\Assert;
use Spatie\QueueableAction\ActionJob;

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

//uses(Tests\IntegrationTestCase::class)->in('Integration');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

//expect()->extend('toBeOne', function () {
//    return $this->toBe(1);
//});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

/**
 * @param class-string $className
 */
function handleBeforeCommit(string $className): void
{
    $instance = resolve($className);
    $instance->afterCommit = false;

    test()->instance($className, $instance);
}

function checkQueueFaked(): void
{
    Assert::assertTrue(Queue::getFacadeRoot() instanceof QueueFake, 'Queue was not faked. Use `Queue::fake()`.');
}

function assertActionQueued(string $className, ?int $count = null): void
{
    checkQueueFaked();

    $pushedCount = Queue::pushed(config('queuableaction.job_class'))
        ->filter(fn (ActionJob $job) => $job->displayName() === $className)
        ->count();

    $count === null
        ? Assert::assertGreaterThan(0, $pushedCount)
        : Assert::assertEquals($count, $pushedCount);
}

function assertActionDoesntQueued(string $className): void
{
    assertActionQueued($className, 0);
}

function postFile(string $uri, UploadedFile $file, string $field = 'file'): TestResponse
{
    return post($uri, [$field => $file], ['Content-Type' => 'multipart/form-data']);
}
