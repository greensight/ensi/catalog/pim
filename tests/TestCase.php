<?php

namespace Tests;

use App\Domain\Kafka\Actions\Send\SendKafkaMessageAction;
use Ensi\LaravelTestFactories\WithFakerProviderTestCase;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Mockery\MockInterface;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFakerProviderTestCase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->mockKafka();
    }

    /**
     * Много где отправка в кафку идёт через обсервер, так что мокать её вручную везде будет очень оверхедно
     * Поэтому мокаем по умолчанию, а по необходимости используем forgetMockKafka или перезатираем мок
     * @return void
     */
    protected function mockKafka(): void
    {
        $this->mock(SendKafkaMessageAction::class)->allows('execute');
    }

    /**
     * Откатывает действие метода mockKafka
     * @return void
     */
    protected function forgetMockKafka(): void
    {
        $this->forgetMock(SendKafkaMessageAction::class);
    }

    protected function mockQueryBuilder(): MockInterface|Builder
    {
        return $this->mock(Builder::class);
    }
}
