ProductGroupReadOnlyProperties:
  type: object
  properties:
    id:
      type: integer
      example: 220
    products_count:
      type: integer
      example: 220
    created_at:
      description: Create time
      format: date-time
      type: string
      example: "2021-07-03T11:57:28.000000Z"
    updated_at:
      description: Update time
      format: date-time
      type: string
      example: "2021-07-03T11:57:28.000000Z"
  required:
    - id
    - created_at
    - updated_at

ProductGroupFillableProperties:
  type: object
  properties:
    category_id:
      type: integer
      example: 100
    main_product_id:
      type: integer
      description: ID of a product to be used as a cover
      nullable: true
      example: 5
    name:
      type: string
      example: "Shoes"
    is_active:
      type: boolean
      example: true

ProductGroupFillableRequiredProperties:
  type: object
  required:
    - category_id
    - name
    - is_active

ProductGroupResponseRequiredProperties:
  type: object
  required:
    - category_id
    - main_product_id
    - name
    - is_active

ProductGroupProduct:
  type: object
  properties:
    id:
      type: integer
      description: Link ID
      example: 220
    product_group_id:
      type: integer
      example: 5
    product_id:
      type: integer
      example: 5
    created_at:
      description: Create time
      format: date-time
      type: string
      example: "2021-07-03T11:57:28.000000Z"
    updated_at:
      description: Update time
      format: date-time
      type: string
      example: "2021-07-03T11:57:28.000000Z"
  required:
    - id
    - product_group_id
    - product_id
    - created_at
    - updated_at

ProductGroupIncludes:
  type: object
  properties:
    category:
      $ref: '../../categories/schemas/categories.yaml#/Category'
    main_product:
      $ref: './products.yaml#/ProductDraft'
    products:
      type: array
      items:
        $ref: './products.yaml#/ProductDraft'
    product_links:
      type: array
      items:
        $ref: '#/ProductGroupProduct'

ProductGroup:
  allOf:
    - $ref: '#/ProductGroupReadOnlyProperties'
    - $ref: '#/ProductGroupFillableProperties'
    - $ref: '#/ProductGroupIncludes'
    - $ref: '#/ProductGroupFillableRequiredProperties'

ProductGroupsBindResult:
  type: object
  properties:
    product_group:
      $ref: '#/ProductGroup'
    product_errors:
      type: array
      items:
        type: integer
        description: ID of a product that doesn't have all gluing properties filled
        example: 1
  required:
    - product_group
    - product_errors

CreateProductGroupRequest:
  allOf:
    - $ref: '#/ProductGroupFillableProperties'
    - $ref: '#/ProductGroupFillableRequiredProperties'

ReplaceProductGroupRequest:
  allOf:
    - $ref: '#/ProductGroupFillableProperties'
    - $ref: '#/ProductGroupFillableRequiredProperties'

PatchProductGroupRequest:
  allOf:
    - $ref: '#/ProductGroupFillableProperties'

ProductGroupResponse:
  type: object
  properties:
    data:
      $ref: '#/ProductGroup'
    meta:
      type: object
  required:
    - data

ProductGroupsBindResponse:
  type: object
  properties:
    data:
      $ref: '#/ProductGroupsBindResult'
    meta:
      type: object
  required:
    - data

SearchProductGroupsRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'
    filter:
      type: object

SearchProductGroupsResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/ProductGroup'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta

BindProductGroupProductsRequest:
  type: object
  properties:
    replace:
      type: boolean
      default: false
      description: If true, all gluing links not specified in the request will be deleted
      example: true
    products:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            example: 111
          vendor_code:
            type: string
            description: Article
            example: "123466"
          barcode:
            type: string
            description: EAN
            example: "1003912039"
