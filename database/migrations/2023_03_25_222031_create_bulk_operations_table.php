<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_operations', function (Blueprint $table) {
            $table->id();

            $table->string('action');
            $table->string('entity');
            $table->jsonb('ids');
            $table->unsignedTinyInteger('status');
            $table->string('error_message')->nullable();
            $table->string('created_by')->nullable();

            $table->timestamps(6);
        });

        Schema::create('bulk_operation_errors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('operation_id')->constrained('bulk_operations')->cascadeOnDelete();
            $table->unsignedBigInteger('entity_id');
            $table->string('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulk_operation_errors');
        Schema::dropIfExists('bulk_operations');
    }
};
