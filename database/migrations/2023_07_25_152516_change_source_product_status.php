<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropConstrainedForeignId('status_id');
            $table->dropConstrainedForeignId('system_status_id');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreignId('status_id')->nullable(true)->constrained('status_settings');
            $table->unsignedTinyInteger('system_status_id')->nullable(true);
        });

        Schema::table('published_products', function (Blueprint $table) {
            $table->dropConstrainedForeignId('status_id');
            $table->string('allow_publish_comment')->nullable(true);
        });
        Schema::table('published_products', function (Blueprint $table) {
            $table->foreignId('status_id')->nullable(true)->constrained('status_settings');
        });

        Schema::dropIfExists('product_statuses');
    }

    public function down(): void
    {
        $now = Date::now();
        $defaultStatusId = 1;
        $defaultStatusName = 'Новый';

        Schema::create('product_statuses', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('product_statuses')->insert([
            'id' => $defaultStatusId,
            'name' => $defaultStatusName,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        Schema::table('products', function (Blueprint $table) use ($defaultStatusId) {
            $table->dropConstrainedForeignId('status_id');
            $table->dropColumn('system_status_id');
        });
        Schema::table('products', function (Blueprint $table) use ($defaultStatusId) {
            $table->foreignId('status_id')->default($defaultStatusId)->constrained('product_statuses');
            $table->foreignId('system_status_id')->default($defaultStatusId)->constrained('product_statuses');
        });

        Schema::table('published_products', function (Blueprint $table) use ($defaultStatusId) {
            $table->dropConstrainedForeignId('status_id');
        });
        Schema::table('published_products', function (Blueprint $table) use ($defaultStatusId) {
            $table->dropColumn('allow_publish_comment');
            $table->foreignId('status_id')->default($defaultStatusId)->constrained('product_statuses');
        });
    }
};
