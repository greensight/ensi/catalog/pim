<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Staudenmeir\LaravelMigrationViews\Facades\Schema as ViewsSchema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('actual_category_properties', function (Blueprint $table) {
            $table->boolean('is_gluing')->default(false);
        });

        $this->createNewActualPropertiesView();
        $this->createNewBoundPropertiesView();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->createOldActualPropertiesView();
        $this->createOldBoundPropertiesView();

        Schema::table('actual_category_properties', function (Blueprint $table) {
            $table->dropColumn('is_gluing');
        });
    }

    private function createNewActualPropertiesView(): void
    {
        ViewsSchema::dropViewIfExists('v_actual_category_properties');

        $query = DB::table('actual_category_properties', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'links.is_gluing as is_gluing',
                'links.is_inherited as is_inherited',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
                'properties.is_common',
                'properties.is_system',
                'properties.is_moderated',
            );

        ViewsSchema::createOrReplaceView('v_actual_category_properties', $query);
    }

    private function createNewBoundPropertiesView(): void
    {
        ViewsSchema::dropViewIfExists('v_bound_category_properties');

        $query = DB::table('category_property_links', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'links.is_gluing as is_gluing',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
                'properties.is_common',
                'properties.is_system',
                'properties.is_moderated',
            )
            ->selectRaw('false as is_inherited');

        ViewsSchema::createOrReplaceView('v_bound_category_properties', $query);
    }

    private function createOldActualPropertiesView(): void
    {
        ViewsSchema::dropViewIfExists('v_actual_category_properties');

        $query = DB::table('category_property_links', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
                'properties.is_common',
                'properties.is_system',
                'properties.is_moderated',
            )
            ->selectRaw('false as is_inherited');

        ViewsSchema::createOrReplaceView('v_actual_category_properties', $query);
    }

    private function createOldBoundPropertiesView(): void
    {
        ViewsSchema::dropViewIfExists('v_bound_category_properties');

        $query = DB::table('category_property_links', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
                'properties.is_common',
                'properties.is_system',
                'properties.is_moderated',
            )
            ->selectRaw('false as is_inherited');

        ViewsSchema::createOrReplaceView('v_bound_category_properties', $query);
    }
};
