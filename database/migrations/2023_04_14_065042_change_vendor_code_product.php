<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('products')->whereNull('vendor_code')
            ->chunkById(500, function (Collection $products) {
                foreach ($products as $product) {
                    DB::table('products')
                        ->where('id', $product->id)
                        ->update(['vendor_code' => '']);
                }
            });
        Schema::table('products', function (Blueprint $table) {
            $table->string('vendor_code')->nullable(false)->change();
        });

        DB::table('published_products')->whereNull('vendor_code')
            ->chunkById(500, function (Collection $products) {
                foreach ($products as $product) {
                    DB::table('published_products')
                        ->where('id', $product->id)
                        ->update(['vendor_code' => '']);
                }
            });
        Schema::table('published_products', function (Blueprint $table) {
            $table->string('vendor_code')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('vendor_code')->nullable()->change();
        });

        Schema::table('published_products', function (Blueprint $table) {
            $table->string('vendor_code')->nullable()->change();
        });
    }
};
