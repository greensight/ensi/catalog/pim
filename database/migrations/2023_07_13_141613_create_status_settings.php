<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('status_settings', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name');
            $table->string('color')->nullable(true);
            $table->unsignedTinyInteger('type');
            $table->boolean('is_active');
            $table->boolean('is_publication');
            $table->json('events')->nullable(true);
            $table->timestamps(6);
        });

        Schema::create('status_links', function (Blueprint $table) {
            $table->id();
            $table->foreignId('last_status_id')->constrained('status_settings')->cascadeOnDelete();
            $table->foreignId('new_status_id')->constrained('status_settings')->cascadeOnDelete();
            $table->timestamps(6);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('status_links');
        Schema::dropIfExists('status_settings');
    }
};
