<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Класс миграции для обновления продуктовой модели в соответствии с требованиями.
 *
 * Class RefineProductModel
 */
class RefineProductModel extends Migration
{
    /** @var string[] Поля таблицы products, которые следует удалить */
    public const PRODUCT_COLUMNS_TO_DROP = [
        'how_to_video', 'approval_status', 'approval_status_comment',
        'production', 'production_comment', 'explosive',
        'has_battery', 'description_video',
        'how_to', 'gas', 'need_special_case',
        'need_special_store', 'fragile', 'days_to_return',
    ];

    /** @var string[] Новые поля таблицы products; данный массив используется в методе down */
    public const PRODUCT_COLUMNS_NEW = [
        'ingredients', 'weight_gross', 'calories',
        'proteins', 'fats', 'carbohydrates',
        'storage_area', 'shelf_life', 'additional_info',
        'vat_rate', 'external_id', 'storage_address',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createManufacturersTable();
        $this->createCountriesTable();

        $this->createProductTypesTable();
        $this->addProductTypes();

        $this->dropDeprecatedProductsColumns();
        $this->addNewProductsColumns();
        $this->addNewOffersColumns();
    }

    /**
     * Удаляет неактуальные поля товаров.
     *
     * @return void
     */
    protected function dropDeprecatedProductsColumns(): void
    {
        Schema::table('products', function (Blueprint $table) {
            foreach (static::PRODUCT_COLUMNS_TO_DROP as $productColumnToDrop) {
                $table->dropColumn($productColumnToDrop);
            }
        });
    }

    /**
     * Создает таблицу производителей товаров.
     *
     * @return void
     */
    protected function createManufacturersTable(): void
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });
    }

    /**
     * Создает таблицу стран-производителей товаров.
     *
     * @return void
     */
    protected function createCountriesTable(): void
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });
    }

    /**
     * Создает таблицу продуктовых типов ("штучный", "весовой", "фасованный").
     *
     * @return void
     */
    protected function createProductTypesTable(): void
    {
        Schema::create('product_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });
    }

    /**
     * Заполняет таблицу продуктовых типов базовыми значениями.
     *
     * @return void
     */
    protected function addProductTypes(): void
    {
        DB::table('product_types')->insert(['code' => 'per_item', 'name' => 'штучный']);
        DB::table('product_types')->insert(['code' => 'per_kilo', 'name' => 'весовой']);
        DB::table('product_types')->insert(['code' => 'per_package', 'name' => 'фасованный']);
    }

    /**
     * Добавляет новые поля товаров, специфичные для проекта.
     *
     * @return void
     */
    protected function addNewProductsColumns(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('ingredients')->nullable();
            $table->decimal('weight_gross', 18, 4)->default(0);
            $table->integer('calories')->nullable();
            $table->decimal('proteins')->nullable();
            $table->decimal('fats')->nullable();
            $table->decimal('carbohydrates')->nullable();
            $table->text('storage_area')->nullable();
            $table->text('shelf_life')->nullable();
            $table->text('additional_info')->nullable();
            $table->integer('vat_rate')->nullable();
            $table->string('external_id')->unique()->nullable(false);
            $table->string('storage_address')->nullable();

            $table->bigInteger('manufacturer_id')->unsigned()->nullable();
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->bigInteger('product_type_id')->unsigned()->nullable();

            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('product_type_id')->references('id')->on('product_types');
        });
    }

    /**
     * Добавляет поле адреса хранения товара в магазине в таблицу офферов.
     *
     * @return void
     */
    protected function addNewOffersColumns(): void
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->char('storage_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // удаляем созданные этой миграцией поля
        Schema::table('products', function (Blueprint $table) {
            foreach (static::PRODUCT_COLUMNS_NEW as $newColumn) {
                $table->dropColumn($newColumn);
            }

            $table->dropForeign(['manufacturer_id']);
            $table->dropColumn('manufacturer_id');

            $table->dropForeign(['country_id']);
            $table->dropColumn('country_id');

            $table->dropForeign(['product_type_id']);
            $table->dropColumn('product_type_id');
        });

        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('storage_address');
        });

        // удаляем созданные этой миграцией таблицы
        Schema::dropIfExists('manufacturers');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('product_types');

        // создаем удалённые ранее поля
        Schema::table('products', function (Blueprint $table) {
            $table->string('how_to_video')->nullable();
            $table->tinyInteger('approval_status')->unsigned()->default(1);
            $table->text('approval_status_comment')->nullable();
            $table->tinyInteger('production')->unsigned()->default(1);
            $table->text('production_comment')->nullable();
            $table->char('segment')->nullable();
            $table->boolean('explosive')->default(false);
            $table->boolean('has_battery')->default(false);
            $table->boolean('gas')->default(false);
            $table->string('description_video')->nullable();
            $table->text('how_to')->nullable();
            $table->text('need_special_case')->nullable();
            $table->text('need_special_store')->nullable();
            $table->boolean('fragile')->default(false);
            $table->integer('days_to_return')->nullable();
        });
    }
}
