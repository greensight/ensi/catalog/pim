<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixFilesType extends Migration
{
    public function up()
    {
        DB::table('categories')->update(['file' => null]);
        DB::table('brands')->update(['file' => null]);
        DB::table('product_tips')->update(['file' => null]);
        DB::table('product_images')->delete();
        Schema::table('categories', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
        });
        Schema::table('product_images', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
        });
        Schema::table('product_tips', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
        });
    }

    public function down()
    {
        DB::table('categories')->update(['file' => null]);
        DB::table('brands')->update(['file' => null]);
        DB::table('product_tips')->update(['file' => null]);
        DB::table('product_images')->delete();
        DB::statement('alter table categories alter column file type jsonb using file::jsonb');
        DB::statement('alter table brands alter column file type jsonb using file::jsonb');
        DB::statement('alter table product_images alter column file type jsonb using file::jsonb');
        DB::statement('alter table product_tips alter column file type jsonb using file::jsonb');
    }
}
