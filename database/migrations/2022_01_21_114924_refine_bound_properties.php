<?php

use Illuminate\Database\Migrations\Migration;
use Staudenmeir\LaravelMigrationViews\Facades\Schema as ViewsSchema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        ViewsSchema::dropViewIfExists('category_properties_view');

        $this->createNewActualPropertiesView();
        $this->createBoundPropertiesView();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        ViewsSchema::dropViewIfExists('v_bound_category_properties');
        ViewsSchema::dropViewIfExists('v_actual_category_properties');

        $this->createOldActualPropertiesView();
    }

    private function createBoundPropertiesView(): void
    {
        $query = DB::table('category_property_links', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
                'properties.is_common',
            )
            ->selectRaw('false as is_inherited');

        ViewsSchema::createOrReplaceView('v_bound_category_properties', $query);
    }

    private function createNewActualPropertiesView(): void
    {
        $query = DB::table('actual_category_properties', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'links.is_inherited as is_inherited',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
                'properties.is_common',
            );

        ViewsSchema::createOrReplaceView('v_actual_category_properties', $query);
    }

    private function createOldActualPropertiesView(): void
    {
        $query = DB::table('actual_category_properties', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'links.is_inherited as is_inherited',
                'properties.code',
                'properties.name',
                'properties.display_name',
                'properties.type',
                'properties.hint_value',
                'properties.hint_value_name',
                'properties.is_active',
                'properties.is_multiple',
                'properties.is_filterable',
                'properties.is_public',
                'properties.has_directory',
            );

        ViewsSchema::createOrReplaceView('category_properties_view', $query);
    }
};
