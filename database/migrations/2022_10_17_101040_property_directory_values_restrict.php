<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('published_property_values', function (Blueprint $table) {
            $table->dropForeign(['directory_value_id']);

            $table
                ->foreign('directory_value_id')
                ->references('id')
                ->on('property_directory_values')
                ->restrictOnDelete();

            $table->index(['directory_value_id']);
        });

        Schema::table('product_property_values', function (Blueprint $table) {
            $table->dropForeign(['directory_value_id']);

            $table
                ->foreign('directory_value_id')
                ->references('id')
                ->on('property_directory_values')
                ->restrictOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table('published_property_values', function (Blueprint $table) {
            $table->dropIndex(['directory_value_id']);
            $table->dropForeign(['directory_value_id']);

            $table
                ->foreign('directory_value_id')
                ->references('id')
                ->on('property_directory_values')
                ->cascadeOnDelete();
        });

        Schema::table('product_property_values', function (Blueprint $table) {
            $table->dropForeign(['directory_value_id']);

            $table
                ->foreign('directory_value_id')
                ->references('id')
                ->on('property_directory_values')
                ->cascadeOnDelete();
        });
    }
};
