<?php

use App\Domain\Classifiers\Enums\ProductFlag;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductEnums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('product_statuses', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('product_flags', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
            $table->timestamps();
        });

        $this->fillData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('product_flags');
        Schema::dropIfExists('product_statuses');
    }

    private function fillData(): void
    {
        foreach ($this->getProductFlags() as $id => $name) {
            $now = Date::now();
            DB::table('product_flags')->insert([
                'id' => $id,
                'name' => $name,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }

        foreach ($this->getProductStatuses() as $id => $name) {
            $now = Date::now();
            DB::table('product_statuses')->insert([
                'id' => $id,
                'name' => $name,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }

    private function getProductFlags(): array
    {
        return [
            ProductFlag::MODERATION->value => 'На модерации',
            ProductFlag::CONTENT->value => 'Производство контента',
            ProductFlag::NOT_SENT_FOR_MODERATION->value => 'Изменения не были отправлены на модерацию',
            ProductFlag::NOT_AGREED->value => 'Изменения не были согласованы',
            ProductFlag::REQUIRED_ATTRIBUTES->value => 'Не заполнены обязательные атрибуты',
            ProductFlag::REQUIRED_MASTER_DATA->value => 'Не заполнены мастер-данные',
            ProductFlag::IN_SALE->value => 'В продаже',
            ProductFlag::NOT_SALE->value => 'Не в продаже',
        ];
    }

    private function getProductStatuses(): array
    {
        return [
            1 => 'Новый',
            2 => 'Согласован',
            3 => 'Отклонен',
            4 => 'Заблокирован',
            5 => 'Снят с продажи',
            6 => 'Удален',
        ];
    }
}
