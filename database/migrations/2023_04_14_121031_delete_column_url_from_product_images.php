<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('product_images', function (Blueprint $table) {
            $table->dropColumn('url');
        });

        Schema::table('published_images', function (Blueprint $table) {
            $table->dropColumn('url');
        });

        Schema::table('published_products', function (Blueprint $table) {
            $table->dropColumn('main_image_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('product_images', function (Blueprint $table) {
            $table->string('url')->nullable();
        });

        Schema::table('published_images', function (Blueprint $table) {
            $table->string('url')->nullable();
        });

        Schema::table('published_products', function (Blueprint $table) {
            $table->string('main_image_url')->nullable();
        });
    }
};
