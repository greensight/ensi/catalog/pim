<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('product_import_warnings', function (Blueprint $table) {
            $table->id();
            $table->timestamps(6);

            $table->foreignId('import_id')->constrained('product_imports');

            $table->string('vendor_code');
            $table->unsignedTinyInteger('import_type');
            $table->string('message');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('product_import_warnings');
    }
};
