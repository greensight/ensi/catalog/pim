<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeProductFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('segment');
            $table->renameColumn('video', 'how_to_video');
            $table->string('description_video')->nullable();
            $table->text('how_to')->nullable();
        });

        Schema::create('product_tips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned();
            $table->jsonb('file')->nullable();
            $table->text('description');
            $table->integer('sort')->default(0);

            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_tips');
        Schema::table('products', function (Blueprint $table) {
            $table->char('segment');
            $table->renameColumn('how_to_video', 'video');
            $table->dropColumn('description_video');
            $table->dropColumn('how_to');
        });
    }
}
