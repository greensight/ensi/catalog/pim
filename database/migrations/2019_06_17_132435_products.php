<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

/**
 * Class Products
 */
class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->jsonb('file')->nullable();

            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->string('code');
            $table->jsonb('file')->nullable();

            $table->unsignedBigInteger(NestedSet::LFT)->default(0);
            $table->unsignedBigInteger(NestedSet::RGT)->default(0);
            $table->unsignedBigInteger(NestedSet::PARENT_ID)->nullable();

            $table->index(NestedSet::getDefaultColumns());

            $table->timestamps();

            $table->foreign(NestedSet::PARENT_ID)->references('id')->on('categories');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->bigInteger('brand_id')->unsigned()->nullable();
            $table->bigInteger('category_id')->unsigned()->nullable();

            $table->tinyInteger('type')->unsigned()->default(1);
            $table->string('barcode')->nullable();
            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->string('video')->nullable();
            $table->tinyInteger('approval_status')->unsigned()->default(1);
            $table->text('approval_status_comment')->nullable();

            $table->tinyInteger('archive')->unsigned()->default(0);
            $table->text('archive_comment')->nullable();
            $table->dateTime('archive_date')->nullable();

            $table->tinyInteger('production')->unsigned()->default(1);
            $table->text('production_comment')->nullable();

            $table->decimal('weight', 18, 4)->default(0);
            $table->decimal('width', 18, 4)->default(0);
            $table->decimal('height', 18, 4)->default(0);
            $table->decimal('length', 18, 4)->default(0);
            $table->char('segment')->nullable();

            $table->boolean('explosive')->default(false);
            $table->boolean('has_battery')->default(false);

            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('products');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('category_id')->references('id')->on('categories');
        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('code')->unsigned();
            $table->jsonb('file')->nullable();
            $table->bigInteger('product_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned();

            $table->integer('merchant_id')->unsigned();
            $table->string('external_id');
            $table->tinyInteger('sale_status')->unsigned()->default(1);

            $table->timestamps();

            $table->unique(['product_id', 'merchant_id']);
        });

        Schema::create('offer_certs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('offer_id')->unsigned();
            $table->string('name')->nullable();
            $table->date('date_end')->nullable();
            $table->jsonb('file')->nullable();

            $table->timestamps();

            $table->foreign('offer_id')->references('id')->on('offers');
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 500);
            $table->string('display_name', 500);
            $table->string('code', 255);
            $table->string('type', 20);
            $table->boolean('is_filterable')->default(false);
            $table->boolean('is_multiple')->default(false);

            $table->timestamps();
        });

        Schema::create('property_directory_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();

            $table->string('name', 1000);
            $table->string('code');

            $table->timestamps();
        });

        Schema::create('category_property_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();

            $table->boolean('is_required')->default(false);

            $table->timestamps();

            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('category_id')->references('id')->on('categories');
        });

        Schema::create('product_property_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();

            $table->text('value_string')->nullable();
            $table->bigInteger('value_integer')->nullable();
            $table->double('value_double')->nullable();
            $table->dateTime('value_datetime')->nullable();
            $table->bigInteger('value_directory')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('value_directory')->references('id')->on('property_directory_values');
        });

        Schema::create('history', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->tinyInteger('type')->unsigned();
            $table->jsonb('data')->nullable();

            $table->string('entity');
            $table->bigInteger('entity_id')->unsigned();

            $table->timestamps();
        });

        Schema::create('history_main_entity', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('history_id')->unsigned();
            $table->string('main_entity');
            $table->bigInteger('main_entity_id')->unsigned();

            $table->timestamps();

            $table->foreign('history_id')->references('id')->on('history');
        });

        Schema::create('index_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->bigInteger('object_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('index_requests');
        Schema::dropIfExists('history_main_entity');
        Schema::dropIfExists('history');
        Schema::dropIfExists('category_property_links');
        Schema::dropIfExists('product_property_values');
        Schema::dropIfExists('property_directory_values');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('offer_certs');
        Schema::dropIfExists('offers');
        Schema::dropIfExists('product_images');
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('brands');
    }
}
