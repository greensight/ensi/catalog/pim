<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->boolean('is_active')->default(false);
            $table->string('logo_url')->nullable();
            $table->renameColumn('file', 'logo_file');

            $table->index(['code']);
            $table->index(['name']);
            $table->index(['is_active', 'id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->dropIndex(['code']);
            $table->dropIndex(['name']);
            $table->dropIndex(['is_active', 'id']);

            $table->renameColumn('logo_file', 'file');
            $table->dropColumn(['is_active', 'logo_url']);
        });
    }
};
