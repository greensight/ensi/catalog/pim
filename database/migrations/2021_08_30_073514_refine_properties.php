<?php

use App\Domain\Classifiers\Enums\PropertyType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefineProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('is_color');

            $table->boolean('is_active')->default(true);
            $table->boolean('is_system')->default(false);
            $table->boolean('is_common')->default(false);
            $table->boolean('is_required')->default(false);
            $table->boolean('is_public')->default(false);
            $table->boolean('has_directory')->default(false);

            $table->string('hint_value')->nullable();
            $table->string('hint_value_name')->nullable();

            $table->unique(['code']);
            $table->index(['name']);
            $table->index(['display_name']);
            $table->index(['type']);
            $table->index(['is_common', 'is_active']);
        });

        Schema::table('property_directory_values', function (Blueprint $table) {
            $table->string('name', 1000)->nullable()->change();
            $table->string('code')->nullable()->change();

            $table->string('value', 1000)->nullable();
            $table->string('type', 20)->default(PropertyType::STRING->value);

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');

            $table->index(['property_id', 'value']);
            $table->index(['code']);
        });

        DB::statement('UPDATE property_directory_values SET value=name');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('UPDATE property_directory_values SET name=value WHERE name is NULL');

        Schema::table('property_directory_values', function (Blueprint $table) {
            $table->dropForeign(['property_id']);
            $table->dropIndex(['property_id', 'value']);
            $table->dropIndex(['code']);

            $table->dropColumn(['value', 'type']);
            $table->string('code')->change();
            $table->string('name', 1000)->change();
        });

        Schema::table('properties', function (Blueprint $table) {
            $table->boolean('is_color')->default(false);

            $table->dropColumn([
                'is_required',
                'is_common',
                'is_system',
                'is_active',
                'is_public',
                'has_directory',
                'hint_value',
                'hint_value_name',
            ]);

            $table->dropUnique(['code']);
            $table->dropIndex(['name']);
            $table->dropIndex(['display_name']);
            $table->dropIndex(['type']);
        });
    }
}
