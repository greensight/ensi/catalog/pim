<?php

use App\Domain\Classifiers\Enums\ProductType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('published_products', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->timestamps(6);

            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();

            $table->foreignId('category_id')->constrained('categories');
            $table->foreignId('brand_id')->nullable()->constrained('brands');

            $table->string('external_id')->nullable();
            $table->integer('type')->default(ProductType::PIECE->value);

            $table->foreignId('status_id')->constrained('product_statuses');
            $table->string('status_comment')->nullable();
            $table->boolean('allow_publish')->default(false);

            $table->string('barcode')->nullable();
            $table->string('vendor_code')->nullable();
            $table->boolean('is_adult')->nullable();

            $table->decimal('weight', 18, 4)->nullable();
            $table->decimal('weight_gross', 18, 4)->nullable();
            $table->decimal('width', 18, 4)->nullable();
            $table->decimal('height', 18, 4)->nullable();
            $table->decimal('length', 18, 4)->nullable();

            $table->string('main_image')->nullable();

            $table->foreign('id')
                ->references('id')
                ->on('products')
                ->cascadeOnDelete();

            $table->index(['code']);
            $table->index(['name']);
            $table->index(['barcode']);
            $table->index(['vendor_code']);

            $table->index(['category_id']);
            $table->index(['brand_id']);
            $table->index(['external_id']);
            $table->index(['status_id', 'allow_publish']);

            $table->index(['created_at']);
            $table->index(['updated_at']);
        });

        Schema::create('published_images', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->timestamps(6);

            $table->foreignId('product_id')->constrained('published_products')->cascadeOnDelete();

            $table->integer('sort')->default(0);
            $table->string('file')->nullable();
            $table->string('url')->nullable();
            $table->string('name')->nullable();

            $table->foreign('id')
                ->references('id')
                ->on('product_images')
                ->cascadeOnDelete();

            $table->index(['product_id', 'sort']);
        });

        Schema::create('published_property_values', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->timestamps(6);
            $table->softDeletes('deleted_at', 6);

            $table->foreignId('product_id')->constrained('published_products')->cascadeOnDelete();
            $table->foreignId('property_id')->constrained('properties')->cascadeOnDelete();
            $table->foreignId('directory_value_id')
                ->nullable()
                ->constrained('property_directory_values')
                ->cascadeOnDelete();

            $table->string('type', 20)->nullable();
            $table->string('value', 1000)->nullable();
            $table->string('name', 1000)->nullable();

            $table->foreign('id')
                ->references('id')
                ->on('product_property_values')
                ->cascadeOnDelete();

            $table->index(['product_id', 'deleted_at']);
        });

        Schema::create('product_metrics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('products')->cascadeOnDelete();

            $table->string('category', 50);
            $table->integer('total')->default(0);
            $table->integer('filled')->default(0);
            $table->integer('errors')->default(0);
            $table->decimal('ratio')->default(0.0);

            $table->index(['product_id', 'category']);
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreignId('system_status_id')->nullable()->constrained('product_statuses');
            $table->string('system_status_comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['system_status_id', 'system_status_comment']);
        });

        Schema::dropIfExists('product_metrics');
        Schema::dropIfExists('published_property_values');
        Schema::dropIfExists('published_images');
        Schema::dropIfExists('published_products');
    }
};
