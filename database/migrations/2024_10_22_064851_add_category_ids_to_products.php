<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('category_product_links', function (Blueprint $table) {
            $table->id();

            $table->foreignId('category_id')->constrained('categories');
            $table->foreignId('product_id')->constrained('products');

            $table->timestamps(6);

            $table->unique(['category_id', 'product_id']);

            $table->index(['category_id']);
            $table->index(['product_id']);
        });

        DB::table('products')->select(['id', 'category_id'])->chunkById(100, function (Collection $products) {
            foreach ($products as $product) {
                DB::table('category_product_links')->insert([
                    'product_id' => $product->id,
                    'category_id' => $product->category_id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable()->change();
        });
        Schema::table('published_products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable()->change();
        });
    }

    public function down(): void
    {
        DB::table('category_product_links')->select(['id', 'product_id', 'category_id'])->chunkById(100, function (Collection $productLinks) {
            foreach ($productLinks as $productLink) {
                DB::table('products')
                    ->where('id', $productLink->product_id)
                    ->whereNull('category_id')
                    ->update(['category_id' => $productLink->category_id]);
            }
        });

        DB::table('category_product_links')->select(['id', 'product_id', 'category_id'])->chunkById(100, function (Collection $productLinks) {
            foreach ($productLinks as $productLink) {
                DB::table('published_products')
                    ->where('id', $productLink->product_id)
                    ->whereNull('category_id')
                    ->update(['category_id' => $productLink->category_id]);
            }
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable(false)->change();
        });
        Schema::table('published_products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable(false)->change();
        });

        // drop pivot table
        Schema::dropIfExists('category_product_links');
    }
};
