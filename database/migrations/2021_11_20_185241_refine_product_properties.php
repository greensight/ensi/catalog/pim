<?php

use App\Domain\Classifiers\Enums\PropertyType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefineProductProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('product_property_values', function (Blueprint $table) {
            $table->string('value', 1000)->nullable();
            $table->string('name', 1000)->nullable();

            $table->foreignId('directory_value_id')
                ->nullable()
                ->constrained('property_directory_values')
                ->onDelete('cascade');

            $table->softDeletes('deleted_at', 6);

            $table->index(['directory_value_id']);
        });

        $this->convertValuesUp();

        Schema::table('product_property_values', function (Blueprint $table) {
            $table->dropColumn([
                'value_string',
                'value_integer',
                'value_double',
                'value_datetime',
                'value_directory',
            ]);

            $table->string('value', 1000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('product_property_values', function (Blueprint $table) {
            $table->text('value_string')->nullable();
            $table->bigInteger('value_integer')->nullable();
            $table->double('value_double')->nullable();
            $table->dateTime('value_datetime')->nullable();
            $table->bigInteger('value_directory')->unsigned()->nullable();
        });

        $this->convertValuesDown();

        Schema::table('product_property_values', function (Blueprint $table) {
            $table->dropColumn(['value', 'name', 'directory_value_id']);
            $table->dropSoftDeletes();
        });
    }

    private function convertValuesUp(): void
    {
        $query = DB::table('product_property_values');

        foreach ($query->lazyById() as $record) {
            $type = $record->type;
            $directoryId = null;

            $value = match ($type) {
                PropertyType::BOOLEAN->value => (string)($record->value_integer == 1),
                PropertyType::INTEGER->value => (string)$record->value_integer,
                PropertyType::DOUBLE->value => (string)$record->value_double,
                PropertyType::DATETIME->value => (string)$record->value_datetime,
                'directory' => (string)$record->value_directory,
                default => $record->value_string,
            };

            if ($type === 'directory') {
                $directoryId = (int)$record->value_directory;
                $type = PropertyType::STRING->value;
            }

            DB::table('product_property_values')
                ->where('id', $record->id)
                ->update([
                    'value' => $value,
                    'type' => $type,
                    'directory_value_id' => $directoryId,
                ]);
        }
    }

    private function convertValuesDown(): void
    {
        $query = DB::table('product_property_values');

        foreach ($query->lazyById() as $record) {
            $type = $record->type;
            $fields = [];

            switch ($type) {
                case PropertyType::STRING->value:
                    $fields['value_string'] = $record->value;

                    break;
                case PropertyType::DATETIME->value:
                    $fields['value_datetime'] = $record->value;

                    break;
                case PropertyType::BOOLEAN->value:
                case PropertyType::INTEGER->value:
                    $fields['value_integer'] = (int)$record->value;

                    break;
                case PropertyType::DOUBLE->value:
                    $fields['value_double'] = (float)$record->value;

                    break;
            }

            if ($record->directory_value_id) {
                $fields = [];
                $fields['value_directory'] = $record->directory_value_id;
                $fields['type'] = 'directory';
            }

            DB::table('product_property_values')
                ->where('id', $record->id)
                ->update($fields);
        }
    }
}
