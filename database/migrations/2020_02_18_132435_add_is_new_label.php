<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddIsNewLabel
 */
class AddIsNewLabel extends Migration
{
    public const PRODUCTS_TABLE = 'products';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::PRODUCTS_TABLE, function (Blueprint $table) {
            $table->boolean('is_new')->default(false)->after('has_battery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::PRODUCTS_TABLE, function (Blueprint $table) {
            $table->dropColumn('is_new');
        });
    }
}
