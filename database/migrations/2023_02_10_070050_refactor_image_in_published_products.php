<?php

use Ensi\LaravelEnsiFilesystem\EnsiStorageFacade;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public const TABLE_NAME = 'published_products';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->string('main_image_file')->nullable();
                $table->string('main_image_url')->nullable();
            });

            $baseUrl = Storage::disk(EnsiStorageFacade::publicDiskName())->url('/');
            DB::table(self::TABLE_NAME)->whereNotNull('main_image')
                ->chunkById(500, function (Collection $products) use ($baseUrl) {
                    foreach ($products as $product) {
                        if (filter_var($product->main_image, FILTER_VALIDATE_URL)) {
                            if (str_starts_with($product->main_image, $baseUrl)) {
                                $this->updateMainImageFile($product->id, str_replace($baseUrl, '', $product->main_image));
                            } else {
                                $this->updateMainImageUrl($product->id, $product->main_image);
                            }
                        } else {
                            $this->updateMainImageFile($product->id, $product->main_image);
                        }
                    }
                });

            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->dropColumn('main_image');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->string('main_image')->nullable();
            });

            DB::table(self::TABLE_NAME)->whereRaw('main_image_file IS NOT NULL OR main_image_url IS NOT NULL')
                ->chunkById(500, function (Collection $products) {
                    foreach ($products as $product) {
                        if ($product->main_image_url) {
                            $this->updateMainImage($product->id, $product->main_image_url);
                        } elseif ($product->main_image_file) {
                            $this->updateMainImage($product->id, $product->main_image_file);
                        }
                    }
                });

            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->dropColumn('main_image_file');
                $table->dropColumn('main_image_url');
            });
        });
    }

    protected function updateMainImageUrl(int $id, string $mainImageUrl): void
    {
        $this->updateColumn($id, ['main_image_url' => $mainImageUrl]);
    }

    protected function updateMainImageFile(int $id, string $mainImageFile): void
    {
        $this->updateColumn($id, ['main_image_file' => $mainImageFile]);
    }

    protected function updateMainImage(int $id, string $mainImage): void
    {
        $this->updateColumn($id, ['main_image' => $mainImage]);
    }

    protected function updateColumn(int $id, array $column): void
    {
        DB::table(self::TABLE_NAME)
            ->where('id', $id)
            ->update($column);
    }
};
