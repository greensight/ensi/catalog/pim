<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;
use Staudenmeir\LaravelMigrationViews\Facades\Schema as ViewsSchema;

class RefineCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn(['file', 'icon']);

            $table->boolean('is_active')->default(false);
            $table->boolean('is_inherits_properties')->default(true);

            $table->boolean('is_real_active')->default(false);
            $table->jsonb('full_code')->nullable();
            $table->timestamp('actualized_at', 6)->nullable();

            $table->unique(['code']);
            $table->index([NestedSet::PARENT_ID]);
        });

        Schema::table('category_property_links', function (Blueprint $table) {
            $table->unique(['category_id', 'property_id']);
        });

        Schema::create('actual_category_properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->constrained('categories');
            $table->foreignId('property_id')->constrained('properties');

            $table->boolean('is_required');
            $table->boolean('is_inherited');
            $table->boolean('is_common');

            $table->timestamps(6);

            $table->unique(['category_id', 'property_id']);
            $table->index(['property_id']);
        });

        $this->createCategoryPropertiesView();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ViewsSchema::dropViewIfExists('category_properties_view');
        Schema::dropIfExists('actual_category_properties');

        Schema::table('category_property_links', function (Blueprint $table) {
            $table->dropUnique(['category_id', 'property_id']);
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropIndex([NestedSet::PARENT_ID]);
            $table->dropUnique(['code']);

            $table->dropColumn([
                'is_active',
                'is_inherits_properties',
                'is_real_active',
                'full_code',
                'actualized_at',
            ]);

            $table->string('file')->nullable();
            $table->string('icon')->nullable();
        });
    }

    private function createCategoryPropertiesView(): void
    {
        $query = DB::table('actual_category_properties', 'links')
            ->join('properties', 'links.property_id', '=', 'properties.id')
            ->select(
                'links.id as id',
                'links.property_id as property_id',
                'links.category_id as category_id',
                'links.is_required as is_required',
                'links.is_inherited as is_inherited',
                'properties.code',
                'properties.name',
                'display_name',
                'type',
                'hint_value',
                'hint_value_name',
                'is_active',
                'is_multiple',
                'is_filterable',
                'is_public',
                'has_directory',
            );

        ViewsSchema::createOrReplaceView('category_properties_view', $query);
    }
}
