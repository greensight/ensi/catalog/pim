<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('product_import_warnings', function (Blueprint $table) {
            $table->string('vendor_code')->nullable(true)->change();
        });
    }

    public function down(): void
    {
        DB::table('product_import_warnings')
            ->where('vendor_code', '=', null)
            ->update(['vendor_code' => 'Не заполнено']);

        Schema::table('product_import_warnings', function (Blueprint $table) {
            $table->string('vendor_code')->nullable(false)->change();
        });
    }
};
