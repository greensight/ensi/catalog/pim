<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('property_variant_group');
        Schema::dropIfExists('variant_groups');

        Schema::create('product_groups', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->foreignId('category_id')->constrained();
            $table->unsignedBigInteger('main_product_id')->nullable();

            $table->timestamps(6);

            $table->foreign('main_product_id')->references('id')->on('products')->cascadeOnDelete();
        });

        Schema::create('product_group_product', function (Blueprint $table) {
            $table->id();

            $table->foreignId('product_group_id')->constrained()->cascadeOnDelete();
            $table->foreignId('product_id')->constrained()->cascadeOnDelete();

            $table->timestamps(6);

            $table->unique(['product_group_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_group_product');
        Schema::dropIfExists('product_groups');

        Schema::create('variant_groups', function (Blueprint $table) {
            $table->id();
            $table->json('attribute_ids');
            $table->json('product_ids');
        });

        Schema::create('property_variant_group', function (Blueprint $table) {
            $table->foreignId('variant_group_id')->constrained();
            $table->foreignId('property_id')->constrained();
        });
    }
};
