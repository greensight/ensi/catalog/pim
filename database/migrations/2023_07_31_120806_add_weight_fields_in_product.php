<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->tinyInteger('uom')->nullable();
            $table->tinyInteger('tariffing_volume')->nullable();
            $table->decimal('order_step', 18, 4)->nullable();
            $table->decimal('order_minvol', 18, 4)->nullable();
            $table->decimal('picking_weight_deviation', 18, 4)->nullable();
        });

        Schema::table('published_products', function (Blueprint $table) {
            $table->tinyInteger('uom')->nullable();
            $table->tinyInteger('tariffing_volume')->nullable();
            $table->decimal('order_step', 18, 4)->nullable();
            $table->decimal('order_minvol', 18, 4)->nullable();
            $table->decimal('picking_weight_deviation', 18, 4)->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('uom');
            $table->dropColumn('tariffing_volume');
            $table->dropColumn('order_step');
            $table->dropColumn('order_minvol');
            $table->dropColumn('picking_weight_deviation');
        });

        Schema::table('published_products', function (Blueprint $table) {
            $table->dropColumn('uom');
            $table->dropColumn('tariffing_volume');
            $table->dropColumn('order_step');
            $table->dropColumn('order_minvol');
            $table->dropColumn('picking_weight_deviation');
        });
    }
};
