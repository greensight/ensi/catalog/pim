<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('product_imports', function (Blueprint $table) {
            $table->id();
            $table->timestamps(6);

            $table->unsignedTinyInteger('type');
            $table->unsignedTinyInteger('status');
            $table->string('file');
            $table->unsignedInteger('chunks_count')->nullable();
            $table->unsignedInteger('chunks_finished')->default(0);

            $table->index(['type']);
            $table->index(['status']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('product_imports');
    }
};
