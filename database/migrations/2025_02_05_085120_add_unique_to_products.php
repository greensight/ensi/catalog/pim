<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        $fields = ['vendor_code', 'barcode', 'external_id'];
        foreach ($fields as $field) {
            $this->fixDuplicates($field);
        }

        Schema::table('products', function (Blueprint $table) {
            $table->unique(['vendor_code']);
            $table->unique(['barcode']);
            $table->unique(['external_id']);
        });
    }

    private function fixDuplicates(string $field): void
    {
        $tables = ['products', 'published_products'];

        foreach ($tables as $table) {
            DB::update("UPDATE $table
                SET $field = concat($field, '-', id)
                WHERE $field IN (SELECT $field FROM $table WHERE $field IS NOT NULL GROUP BY $field HAVING COUNT(*) > 1)");
        }
    }

    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropUnique(['vendor_code']);
            $table->dropUnique(['barcode']);
            $table->dropUnique(['external_id']);
        });
    }
};
