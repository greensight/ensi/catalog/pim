<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductVariants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
            $table->dropColumn('parent_id');
            $table->dropColumn('type');
        });

        Schema::create('variant_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsigned();
        });

        Schema::create('product_variant_group', function (Blueprint $table) {
            $table->bigInteger('variant_group_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();

            $table->foreign('variant_group_id')->references('id')->on('variant_groups');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->tinyInteger('type')->unsigned()->nullable();

            $table->foreign('parent_id')->references('id')->on('products');
        });

        Schema::dropIfExists('product_variant_group');
        Schema::dropIfExists('variant_groups');
    }
}
