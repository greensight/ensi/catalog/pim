<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::dropIfExists('product_tips');
        Schema::dropIfExists('index_requests');
        Schema::dropIfExists('history_main_entity');
        Schema::dropIfExists('history');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::create('product_tips', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('product_id');
            $table->jsonb('file')->nullable();
            $table->text('description');
            $table->integer('sort')->default(0);

            $table->timestamps(6);

            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('index_requests', function (Blueprint $table) {
            $table->id('id');
            $table->string('type');
            $table->unsignedBigInteger('object_id');

            $table->boolean('strike')->default(false);
            $table->boolean('delete')->default(false);

            $table->timestamps(6);
        });

        Schema::create('history', function (Blueprint $table) {
            $table->id('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->tinyInteger('type')->unsigned();
            $table->jsonb('data')->nullable();

            $table->string('entity');
            $table->unsignedBigInteger('entity_id');

            $table->timestamps(6);
        });

        Schema::create('history_main_entity', function (Blueprint $table) {
            $table->id('id');

            $table->unsignedBigInteger('history_id');
            $table->string('main_entity');
            $table->unsignedBigInteger('main_entity_id');

            $table->timestamps(6);

            $table->foreign('history_id')->references('id')->on('history');
        });
    }
};
