<?php

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Models\ProductFieldSettings;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'product_fields';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string('code', 100)->unique();
            $table->unsignedInteger('edit_mask')->default(0);

            $table->string('name');
            $table->boolean('is_moderated')->default(false);
            $table->boolean('is_required')->default(false);
            $table->string('metrics_category', 100)->nullable();

            $table->timestamps(6);
        });

        $this->fill();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }

    private function fill(): void
    {
        $this->insertFieldsSettings('code', 'Код');
        $this->insertFieldsSettings('external_id', 'Внешний ID');
        $this->insertFieldsSettings('allow_publish', 'Публикация разрешена', true);
        $this->insertFieldsSettings('status_id', 'Статус', true);
        $this->insertFieldsSettings('status_comment', 'Комментарий к статусу');

        $this->insertMandatory('name', 'Название');
        $this->insertMandatory('vendor_code', 'Артикул');
        $this->insertMandatory('category_id', 'Категория');
        $this->insertMandatory('type', 'Тип товара');

        $this->insertCustomizable('barcode', 'Штрихкод');
        $this->insertCustomizable('brand_id', 'Бренд');
        $this->insertCustomizable('is_adult', 'Товар 18+');
        $this->insertCustomizable('weight', 'Вес нетто');
        $this->insertCustomizable('weight_gross', 'Вес брутто');
        $this->insertCustomizable('width', 'Ширина');
        $this->insertCustomizable('height', 'Высота');
        $this->insertCustomizable('length', 'Длина');

        $this->insertCustomizable('description', 'Описание', MetricsCategory::CONTENT);
        $this->insertCustomizable('images', 'Изображения', MetricsCategory::CONTENT);
    }

    private function insertMandatory(string $code, string $name): void
    {
        $this->insertFieldsSettings($code, $name, true, ProductFieldSettings::EDIT_MODERATED, MetricsCategory::MASTER);
    }

    private function insertCustomizable(string $code, string $name, MetricsCategory $category = MetricsCategory::MASTER): void
    {
        $mask = ProductFieldSettings::EDIT_REQUIRED | ProductFieldSettings::EDIT_MODERATED;

        $this->insertFieldsSettings($code, $name, false, $mask, $category);
    }

    private function insertFieldsSettings(
        string $code,
        string $name,
        bool $isRequired = false,
        int $mask = 0,
        ?MetricsCategory $category = null
    ): void {
        $now = Date::now();

        DB::table(self::TABLE_NAME)->insert([
            'code' => $code,
            'name' => $name,
            'edit_mask' => $mask | ProductFieldSettings::EDIT_NAME,
            'is_required' => $isRequired,
            'is_moderated' => false,
            'metrics_category' => $category,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
};
