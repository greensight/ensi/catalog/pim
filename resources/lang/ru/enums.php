<?php

use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationActionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationEntityEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationStatusEnum;

return [
    BulkOperationActionTypeEnum::class => [
        BulkOperationActionTypeEnum::PATCH->value => 'Изменение атрибутов',
    ],

    BulkOperationEntityEnum::class => [
        BulkOperationEntityEnum::PRODUCT->value => 'Товары',
    ],

    BulkOperationStatusEnum::class => [
        BulkOperationStatusEnum::NEW->value => 'Новая',
        BulkOperationStatusEnum::IN_PROGRESS->value => 'В процессе',
        BulkOperationStatusEnum::COMPLETED->value => 'Завершено',
        BulkOperationStatusEnum::ERROR->value => 'Ошибка',
    ],
];
