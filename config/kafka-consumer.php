<?php

use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;
use Ensi\LaravelMetrics\Kafka\KafkaMetricsMiddleware;

return [
    /*
    | Optional, defaults to empty array.
    | Array of global middleware fully qualified class names.
    */
    'global_middleware' => [ RdKafkaConsumerMiddleware::class, KafkaMetricsMiddleware::class ],
    'stop_signals' => [SIGTERM, SIGINT],

    'processors' => [
        //[
        //    'topic' => 'offers',
        //    'consumer' => 'default',
        //    'type' => 'action',
        //    'class' => ListenOfferAction::class,
        //    'queue' => false,
        //    'consume_timeout' => 5000,
        //],
    ],
];
