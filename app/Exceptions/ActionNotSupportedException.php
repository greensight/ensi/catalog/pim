<?php

namespace App\Exceptions;

use RuntimeException;

class ActionNotSupportedException extends RuntimeException
{
}
