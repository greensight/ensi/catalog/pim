<?php

namespace App\Exceptions;

use RuntimeException;

class MassOperationException extends RuntimeException
{
    protected $invalidTargets;

    public function __construct(array $invalidTargets, ?string $message = null)
    {
        parent::__construct($message ?? 'Не все объекты были обработаны', 500);

        $this->invalidTargets = $invalidTargets;
    }

    public function getInvalidTargets(): array
    {
        return $this->invalidTargets;
    }
}
