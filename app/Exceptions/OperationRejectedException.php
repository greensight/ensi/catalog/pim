<?php

namespace App\Exceptions;

use RuntimeException;

class OperationRejectedException extends RuntimeException
{
    //
}
