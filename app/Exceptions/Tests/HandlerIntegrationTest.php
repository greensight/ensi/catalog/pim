<?php

use App\Domain\Classifiers\Models\Brand;
use App\Exceptions\Handler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('support', 'integration');

function renderApiError(Exception $exception): TestResponse
{
    $handler = new Handler(app());
    $request = new Request(server: ['HTTP_ACCEPT' => 'application/json', 'REQUEST_URI' => 'api/v1/test']);

    $symphonyResponse = $handler->render($request, $exception);

    return new TestResponse(new Response(
        $symphonyResponse->getContent(),
        $symphonyResponse->getStatusCode(),
        $symphonyResponse->headers->all()
    ));
}

test('model not found', function () {
    $exception = (new ModelNotFoundException())->setModel(Brand::class, [100]);

    renderApiError($exception)
        ->assertStatus(404)
        ->assertJsonPath('errors.0.code', 'NotFoundHttpException');
});

test('http exception', function () {
    $exception = new AccessDeniedHttpException();

    renderApiError($exception)
        ->assertStatus(403)
        ->assertJsonPath('errors.0.code', 'AccessDeniedHttpException');
});

test('IllegalOperationException', function () {
    $exception = new \App\Exceptions\IllegalOperationException();

    renderApiError($exception)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', 'IllegalOperationException');
});
