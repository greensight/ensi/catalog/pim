<?php

namespace App\Exceptions;

use RuntimeException;

class DownloadFileException extends RuntimeException
{
}
