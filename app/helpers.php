<?php

use Illuminate\Support\Arr;

/**
 * Возвращает первое непустое значение.
 *
 * @param mixed ...$values
 * @return mixed
 */
function coalesce(...$values): mixed
{
    foreach ($values as $value) {
        if (!blank($value)) {
            return $value;
        }
    }

    return null;
}

/**
 * Создает массив с ключами $keys. Значения ищутся по ключу в массиве $values.
 * Если значение не найдено, то подставляется $defaultValue.
 *
 * @param array $keys
 * @param array $values
 * @param mixed $defaultValue
 * @return array
 */
function data_combine_assoc(array $keys, array $values, mixed $defaultValue = null): array
{
    return array_replace(
        array_fill_keys($keys, $defaultValue),
        Arr::only($values, $keys)
    );
}

function parse_bool(mixed $value): bool
{
    return filter_var($value, FILTER_VALIDATE_BOOL);
}

function is_absolute_url(?string $source): bool
{
    return $source === null
        ? false
        : preg_match('/^(http|https):\/\/[^\s]*$/i', $source);
}
