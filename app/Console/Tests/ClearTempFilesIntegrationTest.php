<?php

use App\Domain\Support\Models\TempFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('console', 'integration');

test('delete temp files success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $tempFile = new TempFile();
    $tempFile->path = '/001/010/image_30.png';
    $tempFile->created_at = Date::now()->subHours(48);
    $tempFile->save();

    $disk = Storage::disk($fs->publicDiskName());
    $disk->write($tempFile->path, '123');

    artisan('files:clear')
        ->doesntExpectOutput('Ошибка')
        ->assertExitCode(0);

    assertModelMissing($tempFile);
    $disk->assertMissing($tempFile->path);
});

test('skip recent files', function () {
    $tempFile = new TempFile();
    $tempFile->path = '/001/010/image_40.png';
    $tempFile->created_at = Date::now()->subMinutes(5);
    $tempFile->save();

    artisan('files:clear')
        ->doesntExpectOutput('Ошибка')
        ->assertExitCode(0);

    assertDatabaseHas($tempFile->getTable(), ['id' => $tempFile->id]);
});
