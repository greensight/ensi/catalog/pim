<?php

namespace App\Console\Commands;

use App\Domain\Support\Models\TempFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

class ClearTempFilesCommand extends Command
{
    /* Время хранения временных файлов в часах */
    public const STORAGE_TIME = 24;

    protected $signature = 'files:clear';
    protected $description = 'Clear temporary (preloaded) files';

    public function handle(EnsiFilesystemManager $fs): int
    {
        $disk = Storage::disk($fs->publicDiskName());

        $timestamp = Date::now()->subHours(self::STORAGE_TIME);
        $records = TempFile::where('created_at', '<', $timestamp)->lazyById();

        foreach ($records as $record) {
            try {
                $this->deleteFile($disk, $record);
            } catch (Exception $e) {
                $this->error("Ошибка при удалении файла {$record->path}: {$e->getMessage()}");
            }
        }

        return 0;
    }

    private function deleteFile(FilesystemAdapter $disk, TempFile $tempFile): void
    {
        if ($disk->exists($tempFile->path)) {
            $disk->delete($tempFile->path);
        }

        $tempFile->delete();
    }
}
