<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Действие массовой операции:
 * * `patch` - Изменение атрибутов
 */
enum BulkOperationActionTypeEnum: string
{
    /** Изменение атрибутов */
    case PATCH = 'patch';
}
