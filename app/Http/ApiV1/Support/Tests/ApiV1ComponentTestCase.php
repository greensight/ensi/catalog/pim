<?php

namespace App\Http\ApiV1\Support\Tests;

use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;

use function public_path;

use Tests\ComponentTestCase;

class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }
}
