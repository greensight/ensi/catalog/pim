<?php

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Http\ApiV1\OpenApiGenerated\Enums\MetricsCategoryEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductFlagEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductSystemStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PropertyTypeEnum;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

test('PropertyTypeEnum has the same elements as PropertyType', function ($value) {
    expect(PropertyType::from($value))->not->toBeNull();
})->with(array_column(PropertyTypeEnum::cases(), 'value'));

test('ProductTypeEnum has the same elements as ProductType', function ($value) {
    expect(ProductType::tryFrom($value))->not->toBeNull();
})->with(array_column(ProductTypeEnum::cases(), 'value'));

test('ProductSystemStatusEnum has the same elements as ProductStatus', function ($value) {
    expect(ProductSystemStatus::tryFrom($value))->not->toBeNull();
})->with(array_column(ProductSystemStatusEnum::cases(), 'value'));

test('MetricsCategoryEnum has the same elements as MetricsCategory', function ($value) {
    expect(MetricsCategory::tryFrom($value))->not->toBeNull();
})->with(array_column(MetricsCategoryEnum::cases(), 'value'));

test('ProductFlagEnum has the same elements as ProductFlag', function ($value) {
    expect(ProductFlag::tryFrom($value))->not->toBeNull();
})->with(array_column(ProductFlagEnum::cases(), 'value'));
