<?php

namespace App\Http\ApiV1\Support\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersIgnoreProperty implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $properties = is_array($value) ? $value : [$value];

        $query->whereNotIn($property, $properties);
    }
}
