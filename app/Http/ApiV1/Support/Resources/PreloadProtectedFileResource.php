<?php

namespace App\Http\ApiV1\Support\Resources;

use App\Domain\Support\Models\TempFile;

/**
 * @mixin TempFile
 */
class PreloadProtectedFileResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'preload_file_id' => $this->id,
            'file' => $this->mapProtectedFileToResponse($this->path),
        ];
    }
}
