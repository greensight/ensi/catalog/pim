<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Http\UploadedFile;

class PreloadFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:10240'],
        ];
    }

    public function image(): UploadedFile
    {
        return $this->file('file');
    }
}
