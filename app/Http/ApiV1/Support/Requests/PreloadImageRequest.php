<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

class PreloadImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'image',
                'max:10240',
                Rule::dimensions()
                    ->minWidth(20)
                    ->minHeight(20)
                    ->maxWidth(3840)
                    ->maxHeight(2160),
            ],
        ];
    }

    public function image(): UploadedFile
    {
        return $this->file('file');
    }
}
