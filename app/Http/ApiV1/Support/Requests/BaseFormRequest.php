<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Redirector;

abstract class BaseFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public static function createFromArray(array $requestData, bool $validate = false, ?Application $app = null): static
    {
        $app = $app ?? app();
        $request = new static($requestData);

        $request->setContainer($app)
            ->setRedirector($app->make(Redirector::class));

        if ($validate) {
            $request->validateResolved();
        }

        return $request;
    }

    public function getId(): int
    {
        return (int) $this->route('id');
    }

    public static function nestedRules(string $prefix, array $rules, array $mainRule = ['required']): array
    {
        return collect($rules)
            ->mapWithKeys(function ($rules, $key) use ($prefix) {
                $requiredKey = array_search("required", $rules);
                if ($requiredKey !== false) {
                    $rules[$requiredKey] = "required_with:$prefix";
                }

                return ["{$prefix}.{$key}" => $rules];
            })
            ->put($prefix, array_merge($mainRule, ['array']))
            ->all();
    }
}
