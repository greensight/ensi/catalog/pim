<?php

namespace App\Http\ApiV1\Modules\Support\Controllers;

use App\Http\ApiV1\Modules\Support\Queries\TempFilesQuery;
use App\Http\ApiV1\Modules\Support\Resources\TempFilesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class TempFilesController
{
    public function search(TempFilesQuery $query, PageBuilderFactory $builderFactory): Responsable
    {
        return TempFilesResource::collectPage(
            $builderFactory->fromQuery($query)->build()
        );
    }
}
