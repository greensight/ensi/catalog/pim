<?php

namespace App\Http\ApiV1\Modules\Support\Resources;

use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin TempFile
 */
class TempFilesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'path' => $this->path,
            'created_at' => $this->created_at,
        ];
    }
}
