<?php

namespace App\Http\ApiV1\Modules\Support\Queries;

use App\Domain\Support\Models\TempFile;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TempFilesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(TempFile::query());

        $this->allowedSorts(['id', 'path', 'created_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('path'),
            ...DateFilter::make('created_at')->lte()->gte(),
        ]);
    }
}
