<?php

use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'support');

test('POST /api/v1/support/temp-files:search 200', function () {
    $models = TempFile::factory()
        ->count(5)
        ->create();
    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/support/temp-files:search', [
        'sort' => ['-id'],
    ])
        ->assertStatus(200)
        ->assertJsonCount($models->count(), 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
});

test('POST /api/v1/support/temp-files:search filter success', function (
    string $fieldKey,
    mixed $value = null,
    ?string $filterKey = null,
    mixed $filterValue = null,
) {
    /** @var TempFile $failedJob */
    $failedJob = TempFile::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/support/temp-files:search', ['filter' => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $failedJob->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $failedJob->id);
})->with([
    ['id'],
    ['path'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test('POST /api/v1/support/temp-files:search sort success', function (string $sort) {
    TempFile::factory()->create();
    postJson('/api/v1/support/temp-files:search', ['sort' => [$sort]])->assertOk();
})->with([
    'id', 'path', 'created_at',
]);
