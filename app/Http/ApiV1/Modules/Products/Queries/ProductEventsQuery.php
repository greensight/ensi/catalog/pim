<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Products\Models\ProductEvent;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductEventsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductEvent::query());

        $this->allowedIncludes(['product']);
        $this->allowedSorts(['id', 'event_id', 'created_at', 'updated_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('event_id'),
            AllowedFilter::exact('product_id'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
