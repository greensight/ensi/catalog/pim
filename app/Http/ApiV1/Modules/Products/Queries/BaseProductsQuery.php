<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Http\ApiV1\Support\Filters\FiltersIgnoreProperty;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\Includes\IncludeInterface;
use Spatie\QueryBuilder\QueryBuilder;

abstract class BaseProductsQuery extends QueryBuilder
{
    public function __construct(Builder|Relation $subject)
    {
        parent::__construct($subject);

        $this->allowedSorts(array_merge([
            'id',
            'name',
            'code',
            'created_at',
            'updated_at',
        ], $this->extraSorts()));
        $this->defaultSort('id');

        $this->allowedFilters(array_merge([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
            AllowedFilter::partial('barcode'),
            AllowedFilter::partial('vendor_code'),
            AllowedFilter::exact('allow_publish'),
            AllowedFilter::exact('is_adult'),
            AllowedFilter::exact('type'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),

            AllowedFilter::exact('code'),
            AllowedFilter::exact('status_id'),
            AllowedFilter::exact('category_id', 'categoryProductLinks.category_id'),
            AllowedFilter::exact('brand_id'),
            AllowedFilter::exact('external_id'),
            AllowedFilter::scope('has_product_groups'),
            AllowedFilter::scope('category_has_is_gluing'),
            AllowedFilter::scope('has_no_filled_required_attributes'),

            AllowedFilter::custom('ignore_id', new FiltersIgnoreProperty(), 'id'),
        ], $this->extraFilters()));

        $this->allowedIncludes(array_merge([
            'brand',
            'categories',
            'categories.properties',
            'images',
            AllowedInclude::custom('categories.hidden_properties', new class () implements IncludeInterface {
                public function __invoke(Builder $query, string $include): void
                {
                    $query->with(['categories.properties', 'categories.boundProperties', 'categories.allProperties']);
                }
            }),
            AllowedInclude::custom('attributes', new PropertyDirectoryValueInclude()),
            AllowedInclude::relationship('category_product_links', 'categoryProductLinks'),
            AllowedInclude::relationship('product_groups', 'productGroups'),
            AllowedInclude::relationship('product_groups.category', 'productGroups.category'),
            AllowedInclude::relationship('product_groups.main_product', 'productGroups.mainProduct'),
            AllowedInclude::relationship('product_groups.products', 'productGroups.products'),
            AllowedInclude::relationship('no_filled_required_attributes', 'flagRequiredValues'),
        ], $this->extraIncludes()));
    }

    protected function extraSorts(): array
    {
        return [];
    }

    protected function extraFilters(): array
    {
        return [];
    }

    protected function extraIncludes(): array
    {
        return [];
    }
}
