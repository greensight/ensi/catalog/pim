<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Products\Models\ProductGroup;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductGroupsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductGroup::query());

        $this->allowedSorts(['id', 'name', 'created_at', 'updated_at']);
        $this->defaultSort('id');
        $this->withCount('products');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('is_active'),
            ...StringFilter::make('name')->contain(),
            AllowedFilter::scope('product_name_like'),
            AllowedFilter::scope('product_barcode_like'),
            AllowedFilter::scope('product_vendor_code_like'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->allowedIncludes([
            AllowedInclude::relationship('category'),
            AllowedInclude::relationship('main_product', 'mainProduct'),
            AllowedInclude::relationship('main_product.images', 'mainProduct.images'),
            AllowedInclude::relationship('main_product.attributes', 'mainProduct.properties'),
            AllowedInclude::relationship('products'),
            AllowedInclude::relationship('product_links', 'productLinks'),
        ]);
    }
}
