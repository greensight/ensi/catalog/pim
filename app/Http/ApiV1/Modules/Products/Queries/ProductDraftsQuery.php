<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder;

class ProductDraftsQuery extends BaseProductsQuery
{
    public function __construct()
    {
        parent::__construct(Product::query());
        $this->hiddenDeleteProduct();
    }

    private function hiddenDeleteProduct(): void
    {
        $this->subject->where(fn (Builder $q) => $q
            ->whereNotIn('system_status_id', ProductSystemStatus::hiddenStatus())
            ->orWhereNull('system_status_id'));
    }
}
