<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Products\Models\PublishedProduct;

class PublishedProductsQuery extends BaseProductsQuery
{
    public function __construct()
    {
        parent::__construct(PublishedProduct::query());
    }
}
