<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Includes\IncludedRelationship;

class PropertyDirectoryValueInclude extends IncludedRelationship
{
    public function __invoke(Builder $query, string $relationship): void
    {
        parent::__invoke($query, 'properties.directoryValue');
    }
}
