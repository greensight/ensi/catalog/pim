<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Products\Actions\ProductGroups\Data\ProductGroupProductData;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;

class BindProductGroupProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'replace' => ['sometimes', 'boolean'],
            'products' => ['sometimes', 'array'],
            'products.*.id' => [
                'required_without_all:products.*.vendor_code,products.*.barcode',
                'integer',
                Rule::exists(Product::class, 'id'),
            ],

            'products.*.vendor_code' => [
                'prohibits:products.*.id,products.*.barcode',
                'string',
                Rule::exists(Product::class, 'vendor_code'),
            ],

            'products.*.barcode' => [
                'prohibits:products.*.id,products.*.vendor_code',
                'string',
                Rule::exists(Product::class, 'barcode'),
            ],
        ];
    }

    public function isReplace(): bool
    {
        return $this->validated()['replace'] ?? false;
    }

    /**
     * @return Collection<ProductGroupProductData>
     */
    public function products(): Collection
    {
        return collect($this->validated()['products'] ?? [])->map(function (array $product) {
            $productGroupProductData = new ProductGroupProductData();
            $productGroupProductData->id = $product['id'] ?? null;
            $productGroupProductData->vendorCode = $product['vendor_code'] ?? null;
            $productGroupProductData->barcode = $product['barcode'] ?? null;

            return $productGroupProductData;
        });
    }
}
