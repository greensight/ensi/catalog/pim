<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Property;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class DeleteProductAttributesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'property_ids' => ['required', 'array', 'min:1'],
            'property_ids.*' => ['integer', Rule::exists(Property::class, 'id')],
        ];
    }

    public function getPropertyIds(): array
    {
        return $this->validated('property_ids');
    }
}
