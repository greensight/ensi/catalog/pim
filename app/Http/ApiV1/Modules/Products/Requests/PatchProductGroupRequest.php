<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchProductGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'category_id' => ['sometimes', 'integer', Rule::exists(Category::class, 'id')],
            'name' => ['sometimes', 'string'],
            'is_active' => ['sometimes', 'boolean'],
            'main_product_id' => ['sometimes', 'nullable', 'integer', Rule::exists(Product::class, 'id')],
        ];
    }
}
