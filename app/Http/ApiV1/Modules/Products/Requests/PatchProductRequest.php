<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductUomEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatchProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $productId = (int) $this->route('id');

        return array_merge(
            self::baseRules(),
            self::uniqueRules($productId),
            self::relationRules(),
            PatchProductImagesRequest::itemRules($productId),
            PatchProductAttributesRequest::itemRules()
        );
    }

    public static function baseRules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'category_ids' => ['sometimes', 'required', 'array'],
            'category_ids.*' => ['integer', Rule::exists(Category::class, 'id')],
            'allow_publish' => ['sometimes', 'boolean'],
            'type' => ['sometimes', 'required', new Enum(ProductType::class)],
            'description' => ['sometimes', 'nullable', 'string'],

            'brand_id' => ['sometimes', 'nullable', Rule::exists(Brand::class, 'id')],

            'weight' => ['sometimes', 'nullable', 'numeric'],
            'weight_gross' => ['sometimes', 'nullable', 'numeric'],
            'length' => ['sometimes', 'nullable', 'numeric'],
            'width' => ['sometimes', 'nullable', 'numeric'],
            'height' => ['sometimes', 'nullable', 'numeric'],
            'is_adult' => ['sometimes', 'nullable', 'boolean'],
            'uom' => ['nullable', Rule::enum(ProductUomEnum::class)],
            'order_step' => ['nullable', 'numeric'],
            'order_minvol' => ['nullable', 'numeric'],
            'picking_weight_deviation' => ['nullable', 'numeric'],

            'status_id' => ['sometimes', 'required', 'integer'],
            'status_comment' => ['sometimes', 'nullable', 'string'],
        ];
    }

    public static function uniqueRules(int $productId): array
    {
        return [
            'barcode' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],
            'external_id' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],
            'code' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],
            'vendor_code' => [
                'sometimes',
                'required',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],
        ];
    }

    public static function relationRules(): array
    {
        return [
            'images' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];
    }
}
