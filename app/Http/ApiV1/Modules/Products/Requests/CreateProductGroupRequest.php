<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateProductGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'category_id' => ['required', 'integer', Rule::exists(Category::class, 'id')],
            'name' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
            'main_product_id' => ['sometimes', 'nullable', 'integer', Rule::exists(Product::class, 'id')],
        ];
    }
}
