<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassPatchProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $baseRules = [
            'ids' => ['required', 'array'],
            'ids.*' => ['required', 'integer'],
            'fields' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge(
            $baseRules,
            self::fieldsRules('fields'),
            PatchProductAttributesRequest::itemRules()
        );
    }

    public static function fieldsRules(string $prefix): array
    {
        $rules = PatchProductRequest::baseRules();

        return collect($rules)->mapWithKeys(function ($rule, $key) use ($prefix) {
            return ["{$prefix}.{$key}" => $rule];
        })->all();
    }

    public function getIds(): array
    {
        return $this->validated('ids');
    }

    public function getFields(): array
    {
        return $this->validated('fields', []);
    }

    public function getAttributes(): array
    {
        return $this->validated('attributes', []);
    }
}
