<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchProductAttributesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return array_merge(
            ['attributes' => ['present', 'array']],
            self::itemRules()
        );
    }

    public function attributeValues(): array
    {
        return $this->validated()['attributes'];
    }

    public static function itemRules(): array
    {
        return [
            'attributes.*.property_id' => [
                'required',
                Rule::exists(Property::class, 'id'),
            ],
            'attributes.*.name' => ['sometimes', 'string'],
            'attributes.*.mark_to_delete' => ['sometimes', 'bool'],

            'attributes.*.value' => [
                'required_without_all:attributes.*.directory_value_id,attributes.*.preload_file_id,attributes.*.mark_to_delete',
            ],
            'attributes.*.directory_value_id' => [
                'prohibits:attributes.*.value,attributes.*.preload_file_id,attributes.*.mark_to_delete',
                'integer',
                Rule::exists(PropertyDirectoryValue::class, 'id'),
            ],
            'attributes.*.preload_file_id' => [
                'prohibits:attributes.*.directory_value_id,attributes.*.value,attributes.*.mark_to_delete',
                'integer',
                Rule::exists(TempFile::class, 'id'),
            ],
        ];
    }
}
