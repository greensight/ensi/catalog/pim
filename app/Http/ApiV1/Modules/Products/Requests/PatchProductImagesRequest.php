<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchProductImagesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');

        return array_merge(
            ['images' => ['present', 'array']],
            self::itemRules($id)
        );
    }

    public function images(): array
    {
        return $this->validated('images');
    }

    public static function itemRules(?int $productId): array
    {
        $rules = [
            'images.*.name' => ['sometimes', 'nullable', 'string'],
            'images.*.sort' => ['sometimes', 'integer'],

            'images.*.preload_file_id' => [
                'required_without_all:images.*.id',
                'prohibits:images.*.id',
                'integer',
                Rule::exists(TempFile::class, 'id'),
            ],
        ];

        if ($productId !== null) {
            $rules['images.*.id'] = [
                'prohibits:images.*.preload_file_id',
                'integer',
                Rule::exists(ProductImage::class, 'id')->where('product_id', $productId),
            ];
        }

        return $rules;
    }
}
