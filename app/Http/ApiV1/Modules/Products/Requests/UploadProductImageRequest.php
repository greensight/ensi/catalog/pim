<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class UploadProductImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'preload_file_id' => [
                'required',
                'prohibits:url',
                'integer',
                Rule::exists(TempFile::class, 'id'),
            ],
            'name' => ['sometimes', 'nullable', 'string'],
            'sort' => ['sometimes', 'integer'],
        ];
    }
}
