<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassPatchProductsByQueryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $baseRules = [
            'filter' => ['sometimes', 'array'],
            'fields' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge(
            $baseRules,
            MassPatchProductsRequest::fieldsRules('fields'),
            PatchProductAttributesRequest::itemRules()
        );
    }

    public function getFields(): array
    {
        return $this->validated('fields', []);
    }

    public function getAttributes(): array
    {
        return $this->validated('attributes', []);
    }
}
