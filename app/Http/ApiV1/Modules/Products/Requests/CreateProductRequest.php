<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductUomEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $fieldRules = [
            'name' => ['required', 'string'],
            'type' => ['required', new Enum(ProductType::class)],
            'category_ids' => ['required', 'array'],
            'category_ids.*' => ['integer', Rule::exists(Category::class, 'id')],
            'allow_publish' => ['sometimes', 'boolean'],
            'description' => ['nullable', 'string'],

            'code' => [
                'nullable',
                'string',
                Rule::unique(Product::class),
            ],
            'vendor_code' => [
                'required',
                'string',
                Rule::unique(Product::class),
            ],
            'barcode' => [
                'nullable',
                'string',
                Rule::unique(Product::class),
            ],
            'external_id' => [
                'nullable',
                'string',
                Rule::unique(Product::class),
            ],

            'brand_id' => ['nullable', Rule::exists(Brand::class, 'id')],

            'weight' => ['nullable', 'numeric'],
            'weight_gross' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'is_adult' => ['nullable', 'boolean'],
            'uom' => ['nullable', Rule::enum(ProductUomEnum::class)],
            'order_step' => ['nullable', 'numeric'],
            'order_minvol' => ['nullable', 'numeric'],
            'picking_weight_deviation' => ['nullable', 'numeric'],

            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge($fieldRules, PatchProductImagesRequest::itemRules(null));
    }
}
