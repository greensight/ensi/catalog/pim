<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\BaseImage;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin BaseImage
 */
class ProductImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'sort' => $this->sort,
            'name' => $this->name,
            'file' => $this->mapPublicFileToResponse($this->file),
        ];
    }
}
