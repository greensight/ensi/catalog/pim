<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\ProductGroup;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductGroup
 */
class ProductGroupsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'main_product_id' => $this->main_product_id,
            'name' => $this->name,
            'is_active' => $this->is_active,
            'products_count' => $this->whenCounted('products', $this->products_count),

            'category' => CategoriesResource::make($this->whenLoaded('category')),
            'main_product' => ProductsResource::make($this->whenLoaded('mainProduct')),
            'products' => ProductsResource::collection($this->whenLoaded('products')),
            'product_links' => ProductGroupProductsResource::collection($this->whenLoaded('productLinks')),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
