<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\ProductPropertyValue;
use App\Http\ApiV1\Modules\Categories\Resources\PropertyValuesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductPropertyValue
 */
class ProductAttributesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'property_id' => $this->property_id,
            $this->merge(fn () => $this->makeValue()),
            'directory_value_id' => $this->when($this->isDirectory(), $this->directory_value_id),
        ];
    }

    private function makeValue(): PropertyValuesResource
    {
        $dirValue = $this->relationLoaded('directoryValue') ? $this->directoryValue : null;

        return new PropertyValuesResource(
            $dirValue !== null ? $dirValue : $this->resource
        );
    }
}
