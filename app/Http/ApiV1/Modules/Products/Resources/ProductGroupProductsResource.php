<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\ProductGroupProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductGroupProduct
 */
class ProductGroupProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_group_id' => $this->product_group_id,
            'product_id' => $this->product_id,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
