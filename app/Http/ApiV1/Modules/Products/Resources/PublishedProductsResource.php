<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\PublishedProduct;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesResource;
use App\Http\ApiV1\Modules\Categories\Resources\CategoryProductLinksResource;
use App\Http\ApiV1\Modules\Classifiers\Resources\BrandsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PublishedProduct
 */
class PublishedProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'external_id' => $this->external_id,
            'type' => $this->type,
            'status_id' => $this->status_id,
            'status_comment' => $this->status_comment,
            'allow_publish' => $this->allow_publish,
            'category_ids' => $this->getCategoryIds(),
            'brand_id' => $this->brand_id,

            'barcode' => $this->barcode,
            'vendor_code' => $this->vendor_code,
            'weight' => $this->weight,
            'weight_gross' => $this->weight_gross,
            'length' => $this->length,
            'height' => $this->height,
            'width' => $this->width,
            'is_adult' => $this->is_adult,

            'uom' => $this->uom,
            'tariffing_volume' => $this->tariffing_volume,
            'order_step' => $this->order_step,
            'order_minvol' => $this->order_minvol,
            'picking_weight_deviation' => $this->picking_weight_deviation,

            'main_image_file' => $this->mapPublicFileToResponse($this->main_image_file),

            'no_filled_required_attributes' => $this->whenLoaded(
                'flagRequiredValues',
                fn () => $this->flagRequiredValues->isNotEmpty()
            ),
            'brand' => BrandsResource::make($this->whenLoaded('brand')),
            'categories' => CategoriesResource::collection($this->whenLoaded('categories')),
            'images' => ProductImagesResource::collection($this->whenLoaded('images')),
            'attributes' => ProductAttributesResource::collection($this->whenLoaded('properties')),
            'product_groups' => ProductGroupsResource::collection($this->whenLoaded('productGroups')),
            'category_product_links' => CategoryProductLinksResource::collection($this->whenLoaded('categoryProductLinks')),
        ];
    }
}
