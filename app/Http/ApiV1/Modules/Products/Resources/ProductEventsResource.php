<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\ProductEvent;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductEvent
 */
class ProductEventsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'event_id' => $this->event_id,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'product' => ProductsResource::make($this->whenLoaded('product')),
        ];
    }
}
