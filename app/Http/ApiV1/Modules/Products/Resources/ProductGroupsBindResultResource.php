<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Actions\ProductGroups\Data\BindProductsResultData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin BindProductsResultData
 */
class ProductGroupsBindResultResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'product_group' => new ProductGroupsResource($this->productGroup),
            'product_errors' => $this->productErrors,
        ];
    }
}
