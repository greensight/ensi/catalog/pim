<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductFlagValue;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test('POST /api/v1/products/published:search 200', function () {
    $product = Product::factory()->create();

    postJson('/api/v1/products/published:search')
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'main_image_file']]])
        ->assertJsonPath('data.0.id', $product->id);
});

test('POST /api/v1/products/products search with flag required attributes true', function () {
    $product = Product::factory()->withFlags(ProductFlag::REQUIRED_ATTRIBUTES)->create();

    $request = [
        'filter' => ['id' => $product->id],
        'include' => ['no_filled_required_attributes'],
    ];

    postJson('/api/v1/products/products:search', $request)
        ->assertOk()
        ->assertJsonPath('data.0.id', $product->id)
        ->assertJsonPath('data.0.no_filled_required_attributes', true);

    assertDatabaseHas(
        ProductFlagValue::class,
        ['product_id' => $product->id, 'value' => ProductFlag::REQUIRED_ATTRIBUTES]
    );
});

test('POST /api/v1/products/products search with flag required attributes false', function () {
    $product = Product::factory()->create();

    $request = [
        'filter' => ['id' => $product->id],
        'include' => ['no_filled_required_attributes'],
    ];

    postJson('/api/v1/products/products:search', $request)
        ->assertOk()
        ->assertJsonPath('data.0.id', $product->id)
        ->assertJsonPath('data.0.no_filled_required_attributes', false);
});

test('POST /api/v1/products/published:search by ignore_id', function () {
    $product = Product::factory()->create();
    $productIgnore = Product::factory()->create();
    $request = [
        'filter' => [
            'ignore_id' => $productIgnore->id,
        ],
    ];

    postJson('/api/v1/products/published:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test('GET /api/v1/products/published/{id} 200', function () {
    $product = Product::factory()->create();

    getJson("/api/v1/products/published/{$product->id}")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'main_image_file']])
        ->assertJsonPath('data.id', $product->id);
});

test('GET /api/v1/products/published/{id} with includes success', function () {
    $product = Product::factory()->create();

    getJson("/api/v1/products/published/{$product->id}?include=images")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'images']]);
});

test('GET /api/v1/products/published/{id} missing 404', function () {
    getJson('/api/v1/products/published/100000')
        ->assertStatus(404);
});

test("POST /api/v1/products/published:search filter success", function (
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null,
) {
    /** @var Product $product */
    $product = Product::factory()->create($value ? [$fieldKey => $value] : []);
    Product::factory()->create();

    postJson("/api/v1/products/published:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $product->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
})->with([
    ['id'],
    ['brand_id'],
    ['name', 'name'],
    ['barcode', 'barcode'],
    ['vendor_code', 'vendor_code'],
    ['external_id', 'external_id'],
    ['allow_publish', true],
    ['is_adult', true],
    ['type', ProductType::PIECE->value],
    ['status_id', fn () => ProductStatusSetting::factory()->create(['id' => 1]), 'status_id', 1],
    ['code', 'code'],
]);

test("POST /api/v1/products/published:search filter by category success", function () {
    $categories = Category::factory()->count(2)->create();

    /** @var Product $product */
    $product = Product::factory()->inCategories($categories->all())->create();
    Product::factory()->create();

    postJson("/api/v1/products/published:search", ['filter' => [
        'category_id' => $categories->first()->id,
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test("POST /api/v1/products/published:search sort success", function (string $sort) {
    Product::factory()->create();
    postJson("/api/v1/products/published:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'name', 'code', 'created_at', 'updated_at',
]);

test('POST /api/v1/products/published:search include category properties', function () {
    $categoryWithProperties = Category::factory()->create();
    Property::factory()
        ->actual($categoryWithProperties)
        ->create();
    Property::factory()
        ->actual($categoryWithProperties)
        ->inactive()
        ->create();

    /** @var Product $product */
    $product = Product::factory()->inCategories([$categoryWithProperties])->create();

    $request = [
        'filter' => ['id' => $product->id],
        'include' => ['categories.properties', 'categories.hidden_properties', 'category_product_links'],
    ];

    postJson('/api/v1/products/published:search', $request)
        ->assertOk()
        ->assertJsonPath('data.0.id', $product->id)
        ->assertJsonStructure(['data' => [['id', 'name', 'category_product_links', 'categories' => [['properties', 'hidden_properties']]]]]);
});
