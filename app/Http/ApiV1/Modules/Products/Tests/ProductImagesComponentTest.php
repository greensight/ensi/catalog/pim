<?php

use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertSoftDeleted;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test('PATCH /api/v1/products/products/{id}/images fails 400', function (
    bool $image,
    bool $file,
    array $fields,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    $product = Product::factory()->create();

    if ($image) {
        $fields['id'] = ProductImage::factory()->owned($product)->create()->id;
    }

    if ($file) {
        $fields['preload_file_id'] = TempFile::create(['path' => '/path/to/image.png'])->id;
    }

    patchJson("/api/v1/products/products/{$product->id}/images", ['images' => [$fields]])
        ->assertStatus(400);
})->with([
    'new image without file' => [false, false, ['sort' => 10]],
    'sets file on existing image' => [true, true, ['name' => 'foo']],
    'missing preload file' => [false, false, ['preload_file_id' => 100_000]],
    'invalid url' => [false, false, ['url' => '100']],
], FakerProvider::$optionalDataset);

test('PUT /api/v1/products/products/{id}/images adds internal image', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $product = Product::factory()->create();

    $request = [
        'images' => [['preload_file_id' => TempFile::create(['path' => '/path/to/image.png'])->id]],
    ];

    putJson("/api/v1/products/products/{$product->id}/images", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'file', 'sort']]])
        ->assertJsonCount(1, 'data');

    assertDatabaseHas(ProductImage::class, ['product_id' => $product->id, 'file' => '/path/to/image.png']);
    assertDatabaseHas(PublishedImage::class, ['product_id' => $product->id, 'file' => '/path/to/image.png']);
});

test('PATCH /api/v1/products/products/{id}/images updates image', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $product = Product::factory()->create();
    $image = ProductImage::factory()->owned($product)->create();

    $request = [
        'images' => [['id' => $image->id, 'name' => 'foo', 'sort' => 50]],
    ];

    patchJson("/api/v1/products/products/{$product->id}/images", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment(['id' => $image->id, 'name' => 'foo', 'sort' => 50]);

    assertDatabaseHas($image->getTable(), ['id' => $image->id, 'name' => 'foo', 'sort' => 50]);
    assertDatabaseHas(PublishedImage::class, ['id' => $image->id, 'name' => 'foo', 'sort' => 50]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/products/products/{id}/images check event image', function () {
    Event::fake();

    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $product = Product::factory()->create();
    $image = ProductImage::factory()->owned($product)->create();

    $request = [
        'images' => [['id' => $image->id, 'name' => 'foo', 'sort' => 50]],
    ];

    patchJson("/api/v1/products/products/{$product->id}/images", $request)
        ->assertOk();

    Event::assertDispatched(
        ProductEvent::class,
        fn (ProductEvent $event) => $event->event === ProductEventEnum::IMAGES_UPLOADED
    );
});

test('POST /api/v1/products/products/{id}:upload-image preload file success', function () {
    $product = Product::factory()->create();

    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());
    $preloadFile = TempFile::create(['path' => '/path/to/image.png']);
    $request = ['preload_file_id' => $preloadFile->id, 'name' => 'foo', 'sort' => 50];
    postJson("/api/v1/products/products/{$product->id}:upload-image", $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'name', 'sort', 'file' => ['url']]]);

    assertDatabaseHas(ProductImage::class, ['product_id' => $product->id, 'file' => $preloadFile->path]);
    assertDatabaseHas(PublishedImage::class, ['product_id' => $product->id, 'file' => $preloadFile->path]);
});

test('POST /api/v1/products/products/{id}:upload-image check event image', function () {
    Event::fake();

    $product = Product::factory()->create();

    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $preloadFile = TempFile::create(['path' => '/path/to/image.png']);
    $request = ['preload_file_id' => $preloadFile->id];

    postJson("/api/v1/products/products/{$product->id}:upload-image", $request)
        ->assertStatus(201);

    Event::assertDispatched(
        ProductEvent::class,
        fn (ProductEvent $event) => $event->event === ProductEventEnum::IMAGES_UPLOADED
    );
});

test('POST /api/v1/products/products/{id}:upload-image fails 400', function () {
    $product = Product::factory()->create();

    $request = ['preload_file_id' => 123];

    postJson("/api/v1/products/products/{$product->id}:upload-image", $request)
        ->assertStatus(400);
});

test('POST /api/v1/products/products/{id}:delete-image success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $product = Product::factory()->create();
    $image = ProductImage::factory()->owned($product)->create();

    postJson("/api/v1/products/products/{$product->id}:delete-image", ['file_id' => $image->id])
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    assertSoftDeleted(ProductImage::class, ['product_id' => $product->id, 'id' => $image->id]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/products/products/{id}:delete-image fails 404', function () {
    $product = Product::factory()->create();

    postJson("/api/v1/products/products/{$product->id}:delete-image", ['file_id' => 123])
        ->assertStatus(404);
});
