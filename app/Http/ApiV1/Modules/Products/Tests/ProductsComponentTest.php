<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Common\Models\BulkOperation;
use App\Domain\Products\Actions\ProductGroups\BindProductsAction;
use App\Domain\Products\Actions\ProductGroups\Data\ProductGroupProductData;
use App\Domain\Products\Actions\ProductGroups\ForceDeleteProductFromProductGroupAction;
use App\Domain\Products\Actions\Products\PatchManyProductsByQueryAction;
use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Jobs\MassPatchProductsJob;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Domain\Products\Publication\Data\FlagSet;
use App\Domain\Support\Jobs\DistributeFileContentsJob;
use App\Domain\Support\Models\Model;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Modules\Products\Tests\Factories\ProductRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationActionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationEntityEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Assert;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test('POST /api/v1/products/products success', function () {
    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Model::setEventDispatcher($initialEvent);

    $category = Category::factory()->create();
    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->forCreating()
        ->make();

    $response = postJson('/api/v1/products/products', $request);

    unset($request['category_ids']);
    $id = $response->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'name', 'status_id']])
        ->assertJsonFragment($request)
        ->json('data.id');

    Event::assertDispatched(
        ProductEvent::class,
        fn (ProductEvent $event) => $event->event === ProductEventEnum::PRODUCT_CREATED
    );

    Event::assertDispatched(
        ProductEvent::class,
        fn (ProductEvent $event) => $event->event === ProductEventEnum::ADDED_DESCRIPTION
    );

    assertDatabaseHas(
        Product::class,
        ['name' => $request['name'], 'code' => $request['code']]
    );
    assertDatabaseHas(
        CategoryProductLink::class,
        ['category_id' => $category->id, 'product_id' => $id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $id, 'name' => $request['name'], 'code' => $request['code']]
    );
});

test('POST /api/v1/products/products success set tariffing_volume for weightProduct', function () {
    $category = Category::factory()->create();
    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->weightProduct()
        ->forCreating()
        ->make();

    $response = postJson('/api/v1/products/products', $request);

    unset($request['category_ids']);
    $id = $response->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'name', 'status_id']])
        ->assertJsonFragment($request)
        ->json('data.id');

    /** @var Product|null $product */
    $product = Product::query()->find($id);

    Assert::assertNotNull($product);
    Assert::assertNotNull($product->tariffing_volume);
});

test('POST /api/v1/products/products not unique field', function (string $field) {
    $category = Category::factory()->create();
    $existing = Product::factory()->create();
    CategoryProductLink::factory()->createFast($category, $existing);

    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->forCreating()
        ->make([$field => $existing->$field]);

    postJson('/api/v1/products/products', $request)
        ->assertStatus(400)
        ->assertJsonValidationErrors(['message' => str_replace('_', ' ', $field)], 'errors.0');
})->with(['code', 'vendor_code', 'barcode', 'external_id']);

test('PUT /api/v1/products/products/{id} success', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->make(['external_id' => $product->external_id, 'vendor_code' => 'foo']);

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'name' => $request['name']]);
    assertDatabaseHas(PublishedProduct::class, ['id' => $product->id, 'vendor_code' => 'foo']);
});

test('PUT /api/v1/products/products/{id} validation fails', function (array $except, array $extra, string $expected) {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->except($except)
        ->make($extra);

    $this->skipNextOpenApiRequestValidation()
        ->putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertStatus(400)
        ->assertJsonValidationErrors(['message' => $expected], 'errors.0');
})->with([
    'without type' => [['type'], [], 'type'],
    'images is null' => [[], ['images' => null], 'images'],
    'attributes is null' => [[], ['attributes' => null], 'attributes'],
]);

test('PUT /api/v1/products/products/{id} images', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->withImage()
        ->make();

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.images');

    assertDatabaseHas(PublishedImage::class, ['product_id' => $product->id]);
});

test('PUT /api/v1/products/products/{id} attributes', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create();

    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->withAttribute(['property_id' => $property->id, 'value' => 'foo'])
        ->make();

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.attributes')
        ->assertJsonFragment(['property_id' => $property->id, 'value' => 'foo']);

    assertDatabaseHas(PublishedPropertyValue::class, ['property_id' => $property->id, 'product_id' => $product->id]);
});

test('PATCH /api/v1/products/products/{id} success', function () {
    $product = Product::factory()->create();
    $brand = Brand::factory()->create();
    $request = [
        'name' => 'foo',
        'weight' => 220.25,
        'is_adult' => !$product->is_adult,
        'brand_id' => $brand->id,
    ];

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    assertDatabaseHas($product->getTable(), $request);
    assertDatabaseHas(PublishedProduct::class, ['id' => $product->id, 'name' => 'foo']);
});

test('PATCH /api/v1/products/products/{id} check next status 200', function () {
    /** @var ProductStatusSetting $currentProductStatus */
    $currentProductStatus = ProductStatusSetting::factory()->create();
    /** @var ProductStatusSetting $newProductStatus */
    $newProductStatus = ProductStatusSetting::factory()->activeManual()->create();
    ProductStatusLink::factory()->withStatuses($currentProductStatus, $newProductStatus)->create();

    $product = Product::factory()->create(['status_id' => $currentProductStatus->id]);

    $request = [
        'status_id' => $newProductStatus->id,
    ];

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    assertDatabaseHas($product->getTable(), $request);
});

test('PATCH /api/v1/products/products/{id} check next status 400', function (?ProductStatusSetting $newProductStatus) {
    /** @var ProductStatusSetting $currentProductStatus */
    $currentProductStatus = ProductStatusSetting::factory()->create();
    if ($newProductStatus) {
        ProductStatusLink::factory()->withStatuses($currentProductStatus, $newProductStatus)->create();
    }

    $product = Product::factory()->create(['status_id' => $currentProductStatus->id]);

    $request = [
        'status_id' => $newProductStatus?->id ?? $currentProductStatus->id + 1,
    ];

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertStatus(400);
})->with([
    'not active' => fn () => ProductStatusSetting::factory()->manual()->create(['is_active' => false]),
    'not manual' => fn () => ProductStatusSetting::factory()->activeAutomatic()->create(),
    'not status' => null,
]);

test('PATCH /api/v1/products/products/{id} check next status for previous 400', function () {
    /** @var ProductStatusSetting $currentProductStatus */
    $currentProductStatus = ProductStatusSetting::factory()->create();
    /** @var ProductStatusSetting $newProductStatus */
    $newProductStatus = ProductStatusSetting::factory()->activeManual()->create();
    ProductStatusLink::factory()->withStatuses($newProductStatus, $currentProductStatus)->create();

    $product = Product::factory()->create(['status_id' => $currentProductStatus->id]);

    $request = [
        'status_id' => $newProductStatus->id,
    ];

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/products/products/{id} change allow_publish success', function (bool $allowPublish) {
    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Model::setEventDispatcher($initialEvent);

    $product = Product::factory()->createQuietly(['allow_publish' => !$allowPublish]);

    $request = ['allow_publish' => $allowPublish];

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    $expectedEvent = $allowPublish ? ProductEventEnum::PRODUCT_ACTIVATION : ProductEventEnum::PRODUCT_DEACTIVATION;
    Event::assertDispatched(ProductEvent::class, fn (ProductEvent $event) => $event->event === $expectedEvent);

    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'allow_publish' => $allowPublish]);
})->with([true, false]);

test('PATCH /api/v1/products/products/{id} attributes and images', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create();

    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->withAttribute(['property_id' => $property->id, 'value' => 'foo'])
        ->withImage()
        ->make();

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.images')
        ->assertJsonCount(1, 'data.attributes');

    assertDatabaseHas(PublishedImage::class, ['product_id' => $product->id]);
});

test('DELETE /api/v1/products/products/{id} success', function () {
    $product = Product::factory()->create(['system_status_id' => null]);

    $this
        ->mock(ForceDeleteProductFromProductGroupAction::class)
        ->shouldReceive('execute')
        ->once()
        ->withArgs(fn (int $productId) => $productId === $product->id);

    deleteJson("/api/v1/products/products/{$product->id}")
        ->assertOk();

    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'status_id' => null, 'system_status_id' => ProductSystemStatus::DELETED]);
});

test('POST /api/v1/products/products:preload-image success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $file = UploadedFile::fake()->image('123.png', 300, 300);

    $result = postFile('/api/v1/products/products:preload-image', $file)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'preload_file_id',
                'file' => ['path', 'root_path', 'url'],
            ],
        ])
        ->json('data.preload_file_id');

    assertDatabaseHas(TempFile::class, ['id' => $result]);
});

test('GET /api/v1/products/products success', function () {
    $product = Product::factory()->create();

    getJson("/api/v1/products/products/{$product->id}?include=brand")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand' => ['name']]]);
});

test('POST /api/v1/products/products required only success', function () {
    $category = Category::factory()->create();
    $request = ProductRequestFactory::new()
        ->withCategories([$category->id])
        ->onlyRequired()->make();

    $id = postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'name', 'category_ids']])
        ->json('data.id');

    assertDatabaseHas(
        Product::class,
        ['id' => $id, 'status_id' => null, 'allow_publish' => $request['allow_publish']]
    );
});

test('POST /api/v1/products/products with images', function () {
    $category = Category::factory()->create();

    $request = ProductRequestFactory::new()->withCategories([$category->id])
        ->withImage()
        ->make();

    $id = postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonCount(1, 'data.images')
        ->json('data.id');

    assertDatabaseHas(PublishedImage::class, ['product_id' => $id]);
});

test('POST /api/v1/products/products with attributes', function () {
    $category = Category::factory()->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create();

    $request = ProductRequestFactory::new()
        ->withAttribute(['property_id' => $property->id, 'value' => 'foo'])
        ->make(['category_ids' => [$category->id]]);

    $id = postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonCount(1, 'data.attributes')
        ->json('data.id');

    assertDatabaseHas(ProductPropertyValue::class, ['product_id' => $id, 'property_id' => $property->id]);
});

test('PUT /api/v1/products/products/{id} required only attributes', function () {
    $product = Product::factory()->create();
    $category = Category::factory()->create();
    $request = ProductRequestFactory::new()->withCategories([$category->id])->onlyRequired()->make();

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk();

    assertDatabaseHas(CategoryProductLink::class, ['product_id' => $product->id, 'category_id' => $category->id]);
});

test('POST /api/v1/products/products:search has_product_groups', function (bool $has) {
    $categoryOne = Category::factory()->create();
    $categoryTwo = Category::factory()->create();

    $productOne = Product::factory()->inCategories([$categoryOne])->create();
    $productTwo = Product::factory()->inCategories([$categoryTwo])->create();

    $productGroup = ProductGroup::factory()->withCategoryId($categoryOne->id)->create();

    $productGroupProductData = new ProductGroupProductData();
    $productGroupProductData->id = $productOne->id;

    resolve(BindProductsAction::class)->execute(
        $productGroup->id,
        false,
        collect([$productGroupProductData])
    );
    $request = [
        'filter' => ['has_product_groups' => $has],
        'include' => ['product_groups'],
    ];

    postJson('/api/v1/products/products:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $has ? $productOne->id : $productTwo->id);
})->with([[true], [false]]);

test('POST /api/v1/products/products:mass-patch fields and attributes', function () {
    $count = 2;

    $category = Category::factory()->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create();
    $products = Product::factory()
        ->inCategories([$category])
        ->count($count)
        ->create(['is_adult' => false]);

    $ids = $products->pluck('id')->all();

    $request = [
        'ids' => $ids,
        'fields' => [
            'is_adult' => true,
        ],
        'attributes' => [
            [
                'property_id' => $property->id,
                'value' => 'foo',
            ],
        ],
    ];

    postJson("/api/v1/products/products:mass-patch", $request)
        ->assertOk()
        ->assertJsonCount($count, 'data.processed');

    assertEquals(
        $count,
        Product::query()
            ->whereIn('id', $ids)
            ->where('is_adult', true)
            ->count()
    );

    assertEquals(
        $count,
        ProductPropertyValue::query()
            ->whereIn('product_id', $ids)
            ->where('property_id', $property->id)
            ->where('value', 'foo')
            ->count()
    );

    assertDatabaseHas(BulkOperation::class, [
        'status' => BulkOperationStatusEnum::COMPLETED,
        'action' => BulkOperationActionTypeEnum::PATCH,
        'entity' => BulkOperationEntityEnum::PRODUCT,
    ]);
});

test('POST /api/v1/products/products:mass-patch ignore unique fields', function (string $field, string $value) {
    $count = 2;
    $products = Product::factory()
        ->count($count)
        ->create();

    $request = [
        'ids' => $products->pluck('id')->all(),
        'fields' => [
            $field => $value,
        ],
    ];

    // Request must be successful
    postJson("/api/v1/products/products:mass-patch", $request)
        ->assertOk()
        ->assertJsonCount($count, 'data.processed');

    // But product field must not be updated
    $products->each(function (Product $product) use ($field) {
        assertDatabaseHas(Product::class, ['id' => $product->id, $field => $product->$field]);
    });
})->with([
    ['code', 'product-code'],
    ['vendor_code', '000000'],
    ['barcode', '00000000000'],
    ['external_id', '1'],
]);

test('POST /api/v1/products/products:mass-patch preload file', function () {
    $category = Category::factory()->create();
    $property = Property::factory()
        ->withType(PropertyType::IMAGE)
        ->actual($category)
        ->create();
    $product = Product::factory()->create();
    CategoryProductLink::factory()->createFast($category, $product);

    $prototype = TempFile::create(['path' => '/path/to/image.png']);
    $request = [
        'ids' => [$product->id],
        'attributes' => [
            [
                'property_id' => $property->id,
                'preload_file_id' => $prototype->id,
            ],
        ],
    ];

    Queue::fake();

    postJson("/api/v1/products/products:mass-patch", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.processed');

    Queue::assertPushed(DistributeFileContentsJob::class);

    assertTrue(
        ProductPropertyValue::query()
            ->where('product_id', $product->id)
            ->where('property_id', $property->id)
            ->whereNot('value', $prototype->path)
            ->exists()
    );
    assertDatabaseHas(TempFile::class, ['id' => $prototype->id]);
});

test('POST /api/v1/products/products:mass-patch-by-query success', function () {
    $property = Property::factory()->create();
    $product = Product::factory()->create();

    $request = [
        'filter' => [
            'category_id' => $product->getCategoryIds()->first(),
        ],
        'fields' => [
            'is_adult' => true,
        ],
        'attributes' => [
            [
                'property_id' => $property->id,
                'value' => 'foo',
            ],
        ],
    ];

    Queue::fake();

    postJson("/api/v1/products/products:mass-patch-by-query", $request)
        ->assertOk()
        ->assertJsonPath('data', null);

    Queue::assertPushed(MassPatchProductsJob::class);

    assertDatabaseHas(BulkOperation::class, [
        'status' => BulkOperationStatusEnum::NEW,
        'action' => BulkOperationActionTypeEnum::PATCH,
        'entity' => BulkOperationEntityEnum::PRODUCT,
    ]);
});

test('POST /api/v1/products/products:mass-patch-by-query job pushed for each chunk', function () {
    /** @var ApiV1ComponentTestCase $this */
    $chunkSize = 2;

    $this->partialMock(PatchManyProductsByQueryAction::class)->allows([
        'getChunkSize' => $chunkSize,
    ]);
    Product::factory()->count(++$chunkSize)->create();

    $request = [
        'filter' => [],
        'fields' => [
            'is_adult' => false,
        ],
    ];

    Queue::fake();

    postJson("/api/v1/products/products:mass-patch-by-query", $request)
        ->assertOk()
        ->assertJsonPath('data', null);

    Queue::assertPushed(MassPatchProductsJob::class, 2);

    assertEquals(
        2,
        BulkOperation::query()
            ->where('status', BulkOperationStatusEnum::NEW)
            ->where('action', BulkOperationActionTypeEnum::PATCH)
            ->where('entity', BulkOperationEntityEnum::PRODUCT)
            ->count()
    );
});

test('POST /api/v1/products/products:search has_no_filled_required_attributes', function (bool $has) {
    $products = Product::factory()->count(2)->create();

    /** @var Product $product */
    $product = $products->first();
    $set = new FlagSet($product->id);
    $set->set(ProductFlag::REQUIRED_ATTRIBUTES);
    $set->save();

    $request = [
        'filter' => ['has_no_filled_required_attributes' => $has],
    ];

    postJson('/api/v1/products/products:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $has ? $product->id : $products->last()->id);
})->with([[true], [false]]);


test('GET /api/v1/products/products/{id} deleted 404', function () {
    $product = Product::factory()->deleted()->create();

    getJson("/api/v1/products/products/{$product->id}")
        ->assertStatus(404);
});

test('POST /api/v1/products/products:search excludes deleted', function () {
    $product = Product::factory()->create();
    Product::factory()->deleted()->create();

    postJson('/api/v1/products/products:search', [])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test("POST /api/v1/products/products:search filter success", function (
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null,
) {
    /** @var Product $product */
    $product = Product::factory()->create($value ? [$fieldKey => $value] : []);
    Product::factory()->create();

    postJson("/api/v1/products/products:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $product->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
})->with([
    ['id'],
    ['brand_id'],
    ['name'],
    ['barcode'],
    ['vendor_code'],
    ['external_id'],
    ['allow_publish'],
    ['is_adult'],
    ['type'],
    ['status_id', fn () => ProductStatusSetting::factory()->create(['id' => 1]), 'status_id', 1],
    ['code'],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/products/products:search filter by category success", function () {
    $categories = Category::factory()->count(2)->create();

    /** @var Product $product */
    $product = Product::factory()->inCategories($categories->all())->create();
    Product::factory()->create();

    postJson("/api/v1/products/products:search", ['filter' => [
        'category_id' => $categories->first()->id,
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test('POST /api/v1/products/products:search include category properties', function () {
    $categoryWithProperties = Category::factory()->create();
    Property::factory()
        ->actual($categoryWithProperties)
        ->create();
    Property::factory()
        ->actual($categoryWithProperties)
        ->inactive()
        ->create();

    /** @var Product $product */
    $product = Product::factory()->inCategories([$categoryWithProperties])->create();

    $request = [
        'filter' => ['id' => $product->id],
        'include' => ['categories.properties', 'categories.hidden_properties', 'category_product_links'],
    ];

    postJson('/api/v1/products/products:search', $request)
        ->assertOk()
        ->assertJsonPath('data.0.id', $product->id)
        ->assertJsonStructure(['data' => [['id', 'name', 'category_product_links', 'categories' => [['properties', 'hidden_properties']]]]]);
});
