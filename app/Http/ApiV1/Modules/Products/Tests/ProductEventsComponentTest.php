<?php

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductEvent;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'product-events');

test('POST /api/v1/products/product-events:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = ProductEvent::factory()
        ->count(5)
        ->createQuietly();
    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/products/product-events:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/products/product-events:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var ProductEvent $model */
    $model = ProductEvent::factory()->createQuietly($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/products/product-events:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['event_id'],
    ['product_id'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/products/product-events:search sort success", function (string $sort) {
    ProductEvent::factory()->createQuietly();

    postJson("/api/v1/products/product-events:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'event_id',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/products/product-events:search include success", function () {
    /** @var ProductEvent $model */
    $model = ProductEvent::factory()
        ->for(Product::factory(), 'product')
        ->createQuietly();

    postJson("/api/v1/products/product-events:search", ["include" => ['product']])
        ->assertStatus(200)
        ->assertJsonPath('data.0.product.id', $model->product->id);
});

test('POST /api/v1/products/product-events:search 400', function () {
    ProductEvent::factory()
        ->count(10)
        ->createQuietly();

    postJson('/api/v1/products/product-events:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
});
