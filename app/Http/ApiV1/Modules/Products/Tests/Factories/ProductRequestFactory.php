<?php

namespace App\Http\ApiV1\Modules\Products\Tests\Factories;

use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductUomEnum;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LaravelTestFactories\FactoryMissingValue;
use Illuminate\Support\Collection;
use Storage;

class ProductRequestFactory extends BaseApiFactory
{
    public ?int $statusId = null;
    public ?Collection $images = null;
    public ?Collection $attributes = null;

    protected function definition(): array
    {
        $type = $this->faker->randomEnum(ProductType::cases());
        $isWeigh = $type === ProductType::WEIGHT;

        return [
            'name' => $this->faker->sentence(),
            'description' => $this->faker->text,
            'type' => $this->faker->randomEnum(ProductType::cases()),
            'status_id' => $this->notNull($this->statusId),
            'status_comment' => $this->faker->optional()->sentence(),
            'allow_publish' => $this->faker->boolean,

            'external_id' => $this->faker->unique()->numerify('######'),
            'code' => $this->faker->unique()->slug,
            'barcode' => $this->faker->unique()->ean13(),
            'vendor_code' => $this->faker->unique()->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,

            'uom' => $this->faker->nullable($isWeigh)->randomEnum(ProductUomEnum::cases()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'picking_weight_deviation' => $this->faker->nullable($isWeigh)->randomFloat(2, 0, 100),

            'category_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), 1),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
            'attributes' => $this->whenNotNull($this->attributes, $this->attributes?->all()),
        ];
    }

    public function weightProduct(): static
    {
        return $this->state([
            'type' => ProductType::WEIGHT,
            'uom' => $this->faker->randomEnum(ProductUomEnum::cases()),
            'order_step' => $this->faker->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->randomFloat(2, 1, 100),
            'picking_weight_deviation' => $this->faker->nullable()->randomFloat(2, 0, 100),
        ]);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withCategories(array $categoryIds): self
    {
        return $this->state(['category_ids' => $categoryIds]);
    }

    public function withStatus(int $statusId): self
    {
        return $this->immutableSet('statusId', $statusId);
    }

    public function withImage(?int $preloadFileId = null): self
    {
        if (!$preloadFileId) {
            $fs = resolve(EnsiFilesystemManager::class);

            Storage::fake($fs->publicDiskName());

            $imagePath = EnsiFile::factory()->fileName('image_1024')
                ->fileExt('png')
                ->make()['path'];

            $preloadFileId = TempFile::create(['path' => $imagePath])->id;
        }

        $factory = ProductImageRequestFactory::new()->preloadFileId($preloadFileId);
        $images = is_null($this->images) ? collect([$factory]) : $this->images->push($factory);

        return $this->immutableSet('images', $images);
    }

    public function withAttribute(array $attribute): self
    {
        $attributes = collect($this->attributes ?? null);
        $attributes->push($attribute);

        return $this->immutableSet('attributes', $attributes);
    }

    public function forCreating(): self
    {
        return $this->except(['status_id', 'status_comment']);
    }

    public function onlyRequired(): self
    {
        return $this->only(['name', 'category_id', 'vendor_code', 'type', 'allow_publish']);
    }
}
