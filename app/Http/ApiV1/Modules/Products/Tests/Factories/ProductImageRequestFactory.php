<?php

namespace App\Http\ApiV1\Modules\Products\Tests\Factories;

use App\Domain\Support\Models\TempFile;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductImageRequestFactory extends BaseApiFactory
{
    public ?TempFile $file = null;
    public ?int $preload_file_id = null;

    protected function definition(): array
    {
        return [
            'sort' => $this->faker->numberBetween(1, 500),
            'name' => $this->faker->optional()->sentence(3),
            'file' => $this->notNull($this->file),
            'preload_file_id' => $this->notNull($this->preload_file_id),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function preloadFileId(int $preloadFileId): self
    {
        return $this->immutableSet('preload_file_id', $preloadFileId);
    }

    public function preload(): self
    {
        $fileSuffix = $this->faker->randomNumber();
        $path = "/folder/image_{$fileSuffix}.png";

        return $this->immutableSet('file', TempFile::create(['path' => $path]));
    }
}
