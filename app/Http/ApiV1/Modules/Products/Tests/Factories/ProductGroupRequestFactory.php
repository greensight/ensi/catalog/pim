<?php

namespace App\Http\ApiV1\Modules\Products\Tests\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Product;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductGroupRequestFactory extends BaseApiFactory
{
    public ?int $categoryId = null;
    public ?int $mainProductId = null;

    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'category_id' => $this->notNull($this->categoryId),
            'main_product_id' => $this->notNull($this->mainProductId),
            'is_active' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withCategoryId(?int $categoryId = null): self
    {
        if (!$categoryId) {
            $categoryId = Category::factory()->create()->id;
        }

        return $this->immutableSet('categoryId', $categoryId);
    }

    public function withMainProductId(?int $mainProductId = null): self
    {
        if (!$mainProductId) {
            $mainProductId = Product::factory()->create()->id;
        }

        return $this->immutableSet('mainProductId', $mainProductId);
    }
}
