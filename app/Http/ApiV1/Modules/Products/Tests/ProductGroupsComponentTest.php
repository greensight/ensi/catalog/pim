<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Http\ApiV1\Modules\Products\Tests\Factories\ProductGroupRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('products', 'component');

$productGroupFields = ['id', 'category_id', 'name', 'main_product_id', 'is_active'];

test('POST /api/v1/products/product-groups 201', function () use ($productGroupFields) {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ProductGroupRequestFactory::new()
        ->withCategoryId($category->id)
        ->withMainProductId($product->id)
        ->make();

    postJson('/api/v1/products/product-groups', $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => $productGroupFields]);

    assertDatabaseHas(
        ProductGroup::class,
        [
            'category_id' => $request['category_id'],
            'name' => $request['name'],
            'main_product_id' => $request['main_product_id'],
        ]
    );
});

test("POST /api/v1/products/product-groups 400 if not filled gluing property main product", function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    ActualCategoryProperty::factory()->forCategory($category)->create(['is_gluing' => true]);

    $request = ProductGroupRequestFactory::new()
        ->withCategoryId($category->id)
        ->withMainProductId($product->id)
        ->make();

    postJson('/api/v1/products/product-groups', $request)
        ->assertStatus(400);

    assertDatabaseMissing(
        ProductGroup::class,
        [
            'category_id' => $request['category_id'],
            'main_product_id' => $request['main_product_id'],
        ]
    );
});

test('POST /api/v1/products/product-groups 400', function () {
    $request = ProductGroupRequestFactory::new()->make();
    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/products/product-groups', $request)
        ->assertStatus(400);

    assertDatabaseMissing(
        ProductGroup::class,
        [
            'category_id' => null,
            'name' => $request['name'],
            'main_product_id' => null,
        ]
    );
});

test('GET /api/v1/products/product-groups/{id} 200', function () use ($productGroupFields) {
    $productGroup = ProductGroup::factory()->create();

    getJson("/api/v1/products/product-groups/$productGroup->id")
        ->assertOk()
        ->assertJsonStructure(['data' => $productGroupFields]);
});

test('GET /api/v1/products/product-groups/{id} product_count check', function () {
    $productGroup = ProductGroup::factory()->create();
    $product = Product::factory()->create();
    $productGroup->products()->sync([$product->id]);

    getJson("/api/v1/products/product-groups/$productGroup->id")
        ->assertOk()
        ->assertJsonPath('data.products_count', 1);
});

test('GET /api/v1/products/product-groups/{id} 404', function () {
    getJson('/api/v1/products/product-groups/999')
        ->assertStatus(404);
});

test('GET /api/v1/products/product-groups/{id} 200 include=category, main_product, products', function () use ($productGroupFields) {
    $countProducts = 5;
    $product = Product::factory()->create();
    $productGroup = ProductGroup::factory()
        ->withMainProductId($product->id)
        ->createWithProducts($countProducts)
        ->create();

    getJson("/api/v1/products/product-groups/$productGroup->id?include=category,main_product,products")
        ->assertOk()
        ->assertJsonStructure(['data' => $productGroupFields])
        ->assertJsonStructure(['data' => ['category' => ['id', 'name']]])
        ->assertJsonStructure(['data' => ['main_product' => ['id', 'name']]])
        ->assertJsonStructure(['data' => ['products' => [['id', 'name']]]])
        ->assertJsonCount($countProducts, 'data.products');
});

test('PUT /api/v1/products/product-groups/{id} 200', function () {
    $category = Category::factory()->create();
    $productGroup = ProductGroup::factory()->withCategoryId($category->id)->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ProductGroupRequestFactory::new()
        ->withCategoryId($category->id)
        ->withMainProductId($product->id)
        ->make();

    putJson("/api/v1/products/product-groups/$productGroup->id", $request)
        ->assertOk()
        ->assertJsonPath('data.main_product_id', $request['main_product_id']);

    assertDatabaseHas($productGroup->getTable(), ['id' => $productGroup->id, 'main_product_id' => $request['main_product_id']]);
});

test('PUT /api/v1/products/product-groups/{id} 400', function () {
    $productGroup = ProductGroup::factory()->create();
    $id = $productGroup->id;
    $request = ProductGroupRequestFactory::new()->make();
    $this->skipNextOpenApiRequestValidation();

    putJson("/api/v1/products/product-groups/$id", $request)
        ->assertStatus(400);

    assertDatabaseMissing(
        ProductGroup::class,
        [
            'id' => $id,
            'category_id' => null,
            'name' => $request['name'],
            'main_product_id' => null,
        ]
    );
});

test('PUT /api/v1/products/product-groups/{id} 404', function () {
    $request = ProductGroupRequestFactory::new()
        ->withCategoryId()
        ->withMainProductId()
        ->make();

    putJson('/api/v1/products/product-groups/999', $request)
        ->assertStatus(404);
});

test('DELETE /api/v1/products/product-groups/{id} 200', function () {
    $productGroup = ProductGroup::factory()->create();
    $id = $productGroup->id;

    deleteJson("/api/v1/products/product-groups/$id")
        ->assertOk();

    assertModelMissing($productGroup);
});

test('DELETE /api/v1/products/product-groups/{id} 404', function () {
    deleteJson('/api/v1/products/product-groups/999')
        ->assertStatus(404);
});

test('PATCH /api/v1/products/product-groups/{id} 200', function () {
    $productGroup = ProductGroup::factory()->create();
    $id = $productGroup->id;
    $request = ProductGroupRequestFactory::new()->withMainProductId()->only(['name'])->make();

    patchJson("/api/v1/products/product-groups/$id", $request)
        ->assertOk()
        ->assertJsonPath('data.name', $request['name']);

    assertDatabaseHas($productGroup->getTable(), ['id' => $id, 'name' => $request['name']]);
});

test('PATCH /api/v1/products/product-groups/{id} 400', function () {
    $productGroup = ProductGroup::factory()->create();
    $id = $productGroup->id;
    $request = ProductGroupRequestFactory::new()->make(['main_product_id' => 0]);
    $this->skipNextOpenApiRequestValidation();

    patchJson("/api/v1/products/product-groups/$id", $request)
        ->assertStatus(400);

    assertDatabaseMissing($productGroup->getTable(), ['id' => $id, 'main_product_id' => $request['main_product_id']]);
});

test('PATCH /api/v1/products/product-groups/{id} 404', function () {
    patchJson('/api/v1/products/product-groups/999')
        ->assertStatus(404);
});

test('POST /api/v1/products/product-groups/{id}:bind-products 200 by id', function () {
    Event::fake();

    $category = Category::factory()->create();
    $productGroup = ProductGroup::factory()->withCategoryId($category->id)->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ['products' => [['id' => $product->id]]];

    postJson("/api/v1/products/product-groups/$productGroup->id:bind-products", $request)
        ->assertOk()
        ->assertJsonPath('data.product_group.products.0.id', $product->id);

    Event::assertDispatched(
        ProductEvent::class,
        fn (ProductEvent $event) => $event->event === ProductEventEnum::ADDED_TO_PRODUCT_GROUPS
    );

    assertDatabaseHas(
        ProductGroupProduct::tableName(),
        [
            'product_id' => $product->id,
            'product_group_id' => $productGroup->id,
        ]
    );
});

test('POST /api/v1/products/product-groups/{id}:bind-products 200 by vendor_code', function () {
    $category = Category::factory()->create();
    $productGroup = ProductGroup::factory()->withCategoryId($category->id)->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ['products' => [['vendor_code' => $product->vendor_code]]];

    postJson("/api/v1/products/product-groups/$productGroup->id:bind-products", $request)
        ->assertOk()
        ->assertJsonPath('data.product_group.products.0.id', $product->id);

    assertDatabaseHas(
        ProductGroupProduct::tableName(),
        [
            'product_id' => $product->id,
            'product_group_id' => $productGroup->id,
        ]
    );
});

test('POST /api/v1/products/product-groups/{id}:bind-products 200 by barcode', function () {
    $category = Category::factory()->create();
    $productGroup = ProductGroup::factory()->withCategoryId($category->id)->create();
    $product = Product::factory()->inCategories([$category])->create();

    $request = ['products' => [['barcode' => $product->barcode]]];

    postJson("/api/v1/products/product-groups/$productGroup->id:bind-products", $request)
        ->assertOk()
        ->assertJsonPath('data.product_group.products.0.id', $product->id);

    assertDatabaseHas(
        ProductGroupProduct::tableName(),
        [
            'product_id' => $product->id,
            'product_group_id' => $productGroup->id,
        ]
    );
});

test('POST /api/v1/products/product-groups/{id}:bind-products 200 pass incorrect', function () {
    $productGroup = ProductGroup::factory()->create();
    $id = $productGroup->id;

    $products = Product::factory()->count(3)->create();
    $request = ['products' => $products->pluck('id')->map(function (int $id) {
        return ['id' => $id];
    })];

    postJson("/api/v1/products/product-groups/$id:bind-products", $request)
        ->assertOk()
        ->assertJsonCount(0, 'data.product_group.products');

    assertDatabaseMissing(
        ProductGroupProduct::tableName(),
        [
            'product_id' => $products->pluck('id')->all(),
            'product_group_id' => $id,
        ]
    );
});

test('POST /api/v1/products/product-groups/{id}:bind-products 200 with product errors', function () {
    $productGroup = ProductGroup::factory()->create();
    $category = $productGroup->category;
    $id = $productGroup->id;

    // Товар у которого заполнены ВСЕ свойства, участвующие в склейке
    $correctProduct = Product::factory()->inCategories([$category])->create();
    $link = ActualCategoryProperty::factory()->forCategory($category)->create(['is_gluing' => true]);
    ProductPropertyValue::factory()->forProduct($correctProduct)->createOne(['property_id' => $link->property_id]);

    // Товар у которого не заполнены свойства, участвующие в склейке
    $errorProduct = Product::factory()->inCategories([$category])->create();

    $request = ['products' => [['id' => $correctProduct->id], ['id' => $errorProduct->id]]];
    postJson("/api/v1/products/product-groups/$id:bind-products", $request)
        ->assertOk()
        ->assertJsonPath('data.product_group.products.0.id', $correctProduct->id)
        ->assertJsonPath('data.product_errors.0', $errorProduct->id);

    assertDatabaseHas(
        ProductGroupProduct::tableName(),
        [
            'product_id' => $correctProduct->id,
            'product_group_id' => $id,
        ]
    );

    assertDatabaseMissing(
        ProductGroupProduct::tableName(),
        [
            'product_id' => $errorProduct->id,
            'product_group_id' => $id,
        ]
    );
});

test('POST /api/v1/products/product-groups/{id}:bind-products 400', function () {
    $productGroup = ProductGroup::factory()->create();
    $request = ['products' => [['id' => 999]]];
    $this->skipNextOpenApiRequestValidation();

    postJson("/api/v1/products/product-groups/$productGroup->id:bind-products", $request)
        ->assertStatus(400);
});

test('POST /api/v1/products/product-groups/{id}:bind-products unlink main product 400', function () {
    /** @var Category $category */
    $category = Category::factory()->create();

    /** @var Product $mainProduct */
    $mainProduct = Product::factory()->inCategories([$category])->create();
    /** @var Product $secondProduct */
    $secondProduct = Product::factory()->inCategories([$category])->create();

    $productGroup = ProductGroup::factory()->create([
        'main_product_id' => $mainProduct->id,
    ]);
    ProductGroupProduct::factory()->for($productGroup)->for($mainProduct)->create();
    $request = ['products' => [['id' => $secondProduct->id]], 'replace' => true];

    postJson("/api/v1/products/product-groups/$productGroup->id:bind-products", $request)
        ->assertStatus(400);
});

test('POST /api/v1/products/product-groups/{id}:bind-products 404', function () {
    postJson('/api/v1/products/product-groups/999:bind-products')
        ->assertStatus(404);
});

test('POST /api/v1/products/product-groups:mass-delete 200', function () {
    $productGroups = ProductGroup::factory()->count(5)->create();

    postJson('/api/v1/products/product-groups:mass-delete', ['ids' => $productGroups->pluck('id')])
        ->assertStatus(200);

    foreach ($productGroups as $productGroup) {
        assertModelMissing($productGroup);
    }
});

test('POST /api/v1/products/product-groups:search 200 filter by category_id', function () {
    $productGroup = ProductGroup::factory()->create();
    $request = [
        'filter' => ['category_id' => $productGroup->category_id],
    ];

    postJson('/api/v1/products/product-groups:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $productGroup->id);
});

test('POST /api/v1/products/product-groups:search 200 filter by product_name_like', function () {
    $productGroup = ProductGroup::factory()->createWithProducts()->create();
    /** @var Product $product */
    $product = $productGroup->products->first();
    $request = [
        'filter' => ['product_name_like' => mb_substr($product->name, 0, mb_strlen($product->name) - 1)],
    ];

    postJson('/api/v1/products/product-groups:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $productGroup->id);
});

test('POST /api/v1/products/product-groups:search 200 filter by product_barcode_like', function () {
    $productGroup = ProductGroup::factory()->createWithProducts()->create();
    /** @var Product $product */
    $product = $productGroup->products->first();
    $request = [
        'filter' => ['product_barcode_like' => mb_substr($product->barcode, 0, mb_strlen($product->barcode) - 1)],
    ];

    postJson('/api/v1/products/product-groups:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $productGroup->id);
});

test('POST /api/v1/products/product-groups:search 200 filter by product_vendor_code_like', function () {
    $productGroup = ProductGroup::factory()->createWithProducts()->create();
    /** @var Product $product */
    $product = $productGroup->products->first();
    $request = [
        'filter' => ['product_vendor_code_like' => mb_substr($product->vendor_code, 0, mb_strlen($product->vendor_code) - 1)],
    ];

    postJson('/api/v1/products/product-groups:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $productGroup->id);
});

test('POST /api/v1/products/product-groups:search 400', function () {
    $request = [
        'filter' => ['main_product_id' => 1],
    ];
    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/products/product-groups:search', $request)
        ->assertStatus(400);
});

test('POST /api/v1/products/product-groups:search-one 200', function () {
    $productGroup = ProductGroup::factory()->create();
    $request = [
        'filter' => ['category_id' => $productGroup->category_id],
    ];

    postJson('/api/v1/products/product-groups:search-one', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $productGroup->id);
});

test('POST /api/v1/products/product-groups:search-one 400', function () {
    $request = [
        'filter' => ['main_product_id' => 1],
    ];
    $this->skipNextOpenApiRequestValidation();

    postJson('/api/v1/products/product-groups:search-one', $request)
        ->assertStatus(400);
});
