<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test(
    'PATCH /api/v1/products/products/{id}/attributes fails validation 400',
    function (bool $dir, bool $file, array $fields) {
        $product = Product::factory()->create();

        if (!array_key_exists('property_id', $fields)) {
            $fields['property_id'] = Property::factory()->create()->id;
        }
        if ($dir) {
            $fields['directory_value_id'] = PropertyDirectoryValue::factory()->create()->id;
        }
        if ($file) {
            $fields['preload_file_id'] = TempFile::create(['path' => '/path/to/image.png'])->id;
        }

        patchJson("/api/v1/products/products/{$product->id}/attributes", ['attributes' => [$fields]])
            ->assertStatus(400);
    }
)->with([
    'value and directory_value_id together' => [true, false, ['value' => 'foo']],
    'value and preload_file_id together' => [false, true, ['value' => 'foo']],
    'directory and file together' => [true, true, ['name' => 'bar']],
    'none are specified' => [false, false, ['name' => 'bar']],
    'missing property' => [false, false, ['property_id' => 100_000, 'value' => 'foo']],
    'missing directory value' => [false, false, ['directory_value_id' => 100_000]],
    'missing preload file' => [false, false, ['preload_file_id' => 100_000]],
]);

test('PATCH /api/v1/products/products/{id}/attributes check event', function (bool $requiredSend, $commonSend) {
    Event::fake();
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $requiredProperty = Property::factory()
        ->withType(PropertyType::STRING)
        ->required()
        ->actual($category)
        ->create();

    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->required(false)
        ->actual($category)
        ->create();

    $request['attributes'] = [];
    if ($requiredSend) {
        $request['attributes'][] = [
            'property_id' => $requiredProperty->id,
            'value' => 'string',
        ];
    }

    if ($commonSend) {
        $request['attributes'][] = [
            'property_id' => $property->id,
            'value' => 'string',
        ];
    }

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk();

    if ($requiredSend) {
        Event::assertDispatched(
            ProductEvent::class,
            fn (ProductEvent $event) => $event->event === ProductEventEnum::REQUIRED_PROPERTIES_FILLED
        );
    } else {
        Event::assertNotDispatched(
            ProductEvent::class,
            fn (ProductEvent $event) => $event->event === ProductEventEnum::REQUIRED_PROPERTIES_FILLED
        );
    }

    if ($commonSend) {
        Event::assertDispatched(
            ProductEvent::class,
            fn (ProductEvent $event) => $event->event === ProductEventEnum::OPTIONAL_PROPERTIES_FILLED
        );
    } else {
        Event::assertNotDispatched(
            ProductEvent::class,
            fn (ProductEvent $event) => $event->event === ProductEventEnum::OPTIONAL_PROPERTIES_FILLED
        );
    }
})->with([
    'not send' => [false, false],
    'required send' => [true, false],
    'all send' => [true, true],
]);

test('PATCH /api/v1/products/products/{id}/attributes preload file', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::IMAGE)
        ->actual($category)
        ->create();

    $request = ['attributes' => [
        [
            'property_id' => $property->id,
            'preload_file_id' => TempFile::create(['path' => '/products/01/02/image1.png'])->id,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonStructure(['data' => [['type', 'name', 'value', 'file']]])
        ->assertJsonPath('data.0.value', '/products/01/02/image1.png');
});

test('PATCH /api/v1/products/products/{id}/attributes external image', function () {
    $image = 'https://ya.ru/images?id=123';
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::IMAGE)
        ->actual($category)
        ->create();

    $request = ['attributes' => [
        [
            'property_id' => $property->id,
            'value' => $image,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.value', $image);
});

test('PATCH /api/v1/products/products/{id}/attributes native value', function (PropertyType $type) {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();

    $propValue = ProductPropertyValue::factory()
        ->forProduct($product)
        ->withType($type)
        ->actual()
        ->makeOne();

    $expectedValue = (string)$propValue->getValue();
    $request = ['attributes' => [
        [
            'property_id' => $propValue->property_id,
            'value' => $expectedValue,
        ],
    ]];

    patchJson("/api/v1/products/products/{$propValue->product_id}/attributes", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['property_id', 'value']]])
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.value', $expectedValue);

    assertDatabaseHas(PublishedPropertyValue::class, [
        'property_id' => $propValue->property_id,
        'product_id' => $propValue->product_id,
    ]);
})->with(PropertyType::cases());

test('PATCH /api/v1/products/products/{id}/attributes directory value', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()->directory()->actual($category)->create();
    $dirValue = PropertyDirectoryValue::factory()->forProperty($property)->create();

    $request = ['attributes' => [
        [
            'property_id' => $property->id,
            'directory_value_id' => $dirValue->id,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['property_id', 'value']]])
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment([
            'property_id' => $property->id,
            'directory_value_id' => $dirValue->id,
            'value' => (string)$dirValue->getValue(),
            'name' => $dirValue->name,
        ]);
});

test('PATCH /api/v1/products/products/{id}/attributes sets multiple values to attribute', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create(['is_multiple' => true]);

    $request = ['attributes' => [
        ['property_id' => $property->id, 'value' => 'foo'],
        ['property_id' => $property->id, 'value' => 'bar'],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(2, 'data');
});

test('PATCH /api/v1/products/products/{id}/attributes fails 400', function (array $options, array $values) {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create($options);

    $attributes = [];
    foreach ($values as $value) {
        $value['property_id'] = $property->id;

        if (array_key_exists('directory_value_id', $value)) {
            $value['directory_value_id'] = PropertyDirectoryValue::factory()->forProperty($property)->create()->id;
        }

        if (array_key_exists('preload_file_id', $value)) {
            $value['preload_file_id'] = TempFile::create(['path' => '/path/to/image.png'])->id;
        }

        $attributes[] = $value;
    }

    patchJson("/api/v1/products/products/{$product->id}/attributes", ['attributes' => $attributes])
        ->assertStatus(400);
})->with([
    'property does not support directory value' => [
        ['has_directory' => false],
        [['directory_value_id' => 0]],
    ],
    'property does not support native value' => [
        ['has_directory' => true],
        [['value' => 'foo']],
    ],
    'property does not support file' => [
        [],
        [['preload_file_id' => 0]],
    ],
    'property does not support multiple values' => [
        ['is_multiple' => false],
        [['value' => 'foo'], ['value' => 'bar']],
    ],
]);

test('PATCH /api/v1/products/products/{id}/attributes skips not specified attributes', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $existing = ProductPropertyValue::factory()->forProduct($product)->actual()->create();

    $request = ['attributes' => []];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data');

    assertDatabaseHas($existing->getTable(), ['id' => $existing->id, 'deleted_at' => null]);
});

test('PUT /api/v1/products/products/{id}/attributes clears not specified attributes', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $existing = ProductPropertyValue::factory()->forProduct($product)->actual()->create();

    $request = ['attributes' => []];

    putJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(0, 'data');

    assertDatabaseMissing(ProductPropertyValue::class, ['id' => $existing->id]);
});

test('POST /api/v1/products/products:common-attributes with props intersection', function () {
    $categoryOne = Category::factory()->create();
    $categoryTwo = Category::factory()->create();

    Product::factory()->inCategories([$categoryOne])->create();
    Product::factory()->inCategories([$categoryTwo])->create();

    [$propertyOne, $propertyMutual] = Property::factory()->count(2)->create();

    ActualCategoryProperty::factory()->createFast($categoryOne, $propertyOne);
    ActualCategoryProperty::factory()->createFast($categoryOne, $propertyMutual);
    ActualCategoryProperty::factory()->createFast($categoryTwo, $propertyMutual);

    $request = ['filter' => [
        'category_id' => [$categoryOne->id, $categoryTwo->id],
    ]];

    postJson("/api/v1/products/products:common-attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $propertyMutual->id);
});

test('POST /api/v1/products/products:common-attributes with common props', function () {
    [$productOne, $productTwo] = Product::factory()->count(2)->create();

    $propertyCommon = Property::factory()->common()->create();

    $request = ['filter' => [
        'category_id' => [$productOne->category_id, $productTwo->category_id],
    ]];

    postJson("/api/v1/products/products:common-attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $propertyCommon->id);
});

test('POST /api/v1/products/products:common-attributes required property in category', function (bool $requiredInCategory, bool $requiredProperty, $expectRequired) {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();

    $property = Property::factory()->create(['is_required' => $requiredProperty]);
    ActualCategoryProperty::factory()->createFast($category, $property, isRequired: $requiredInCategory);

    $request = ['filter' => [
        'id' => [$product->id],
    ]];

    postJson("/api/v1/products/products:common-attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $property->id)
        ->assertJsonPath('data.0.is_required', $expectRequired);
})->with([
    'category more important' => [true, false, true],
    'preservation of the original' => [false, true, true],
]);

test('DELETE /api/v1/products/products/{id}/attributes 200', function () {
    $product = Product::factory()->create();
    $deleteProperty = ProductPropertyValue::factory()->forProduct($product)->actual()->create();
    $otherProperty = ProductPropertyValue::factory()->forProduct($product)->actual()->create();

    $request = ['property_ids' => [$deleteProperty->property_id]];

    deleteJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonPath('data', null);

    assertModelExists($otherProperty);
    assertModelMissing($deleteProperty);
});

test('PATCH /api/v1/products/products/{id}/attributes mark to delete', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $deleteProperty = ProductPropertyValue::factory()->forProduct($product)->actual()->create();
    $otherProperty = ProductPropertyValue::factory()->forProduct($product)->actual()->create();

    $request = ['attributes' => [
        [
            'property_id' => $deleteProperty->property_id,
            'mark_to_delete' => true,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['property_id', 'value']]])
        ->assertJsonCount(1, 'data');

    assertDatabaseMissing(PublishedPropertyValue::class, [
        'property_id' => $deleteProperty->property_id,
        'product_id' => $deleteProperty->product_id,
    ]);

    assertDatabaseHas(PublishedPropertyValue::class, [
        'property_id' => $otherProperty->property_id,
        'product_id' => $otherProperty->product_id,
    ]);
});
