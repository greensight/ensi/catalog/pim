<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\PublishedProductsQuery;
use App\Http\ApiV1\Modules\Products\Resources\PublishedProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PublishedProductsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, PublishedProductsQuery $query): AnonymousResourceCollection
    {
        return PublishedProductsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, PublishedProductsQuery $query): PublishedProductsResource
    {
        return new PublishedProductsResource($query->findOrFail($id));
    }
}
