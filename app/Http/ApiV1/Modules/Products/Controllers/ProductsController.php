<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Domain\Products\Actions\Attributes\DeleteProductAttributesAction;
use App\Domain\Products\Actions\Attributes\PatchProductAttributesAction;
use App\Domain\Products\Actions\Attributes\ReplaceProductAttributesAction;
use App\Domain\Products\Actions\Images\DeleteProductImageAction;
use App\Domain\Products\Actions\Images\PatchProductImagesAction;
use App\Domain\Products\Actions\Images\ReplaceProductImagesAction;
use App\Domain\Products\Actions\Images\UploadProductImageAction;
use App\Domain\Products\Actions\Products\CalculateProductsCommonAttributesAction;
use App\Domain\Products\Actions\Products\CreateProductAction;
use App\Domain\Products\Actions\Products\DeleteManyProductsAction;
use App\Domain\Products\Actions\Products\DeleteProductAction;
use App\Domain\Products\Actions\Products\PatchManyProductsAction;
use App\Domain\Products\Actions\Products\PatchManyProductsByQueryAction;
use App\Domain\Products\Actions\Products\PatchProductAction;
use App\Domain\Products\Actions\Products\PreloadImageAction;
use App\Domain\Products\Actions\Products\ReplaceProductAction;
use App\Domain\Support\MassOperationResult;
use App\Http\ApiV1\Modules\Categories\Resources\PropertiesResource;
use App\Http\ApiV1\Modules\Products\Queries\ProductDraftsQuery;
use App\Http\ApiV1\Modules\Products\Requests\CreateProductRequest;
use App\Http\ApiV1\Modules\Products\Requests\DeleteProductAttributesRequest;
use App\Http\ApiV1\Modules\Products\Requests\DeleteProductImageRequest;
use App\Http\ApiV1\Modules\Products\Requests\MassPatchProductsByQueryRequest;
use App\Http\ApiV1\Modules\Products\Requests\MassPatchProductsRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchProductAttributesRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchProductImagesRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchProductRequest;
use App\Http\ApiV1\Modules\Products\Requests\ReplaceProductAttributesRequest;
use App\Http\ApiV1\Modules\Products\Requests\ReplaceProductImagesRequest;
use App\Http\ApiV1\Modules\Products\Requests\ReplaceProductRequest;
use App\Http\ApiV1\Modules\Products\Requests\UploadProductImageRequest;
use App\Http\ApiV1\Modules\Products\Resources\ProductAttributesResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductImagesResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Requests\PreloadImageRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\PreloadPublicFileResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductDraftsQuery $query): AnonymousResourceCollection
    {
        return ProductsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ProductDraftsQuery $query): ProductsResource
    {
        return new ProductsResource($query->findOrFail($id));
    }

    public function create(CreateProductRequest $request, CreateProductAction $action): ProductsResource
    {
        return new ProductsResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceProductRequest $request,
        ReplaceProductAction $action
    ): ProductsResource {
        return new ProductsResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchProductRequest $request, PatchProductAction $action): ProductsResource
    {
        return new ProductsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyProductsAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    public function massPatch(MassPatchProductsRequest $request, PatchManyProductsAction $action): MassOperationResult
    {
        return $action->execute($request->getIds(), $request->getFields(), $request->getAttributes());
    }

    public function massPatchByQuery(
        ProductDraftsQuery $query,
        MassPatchProductsByQueryRequest $request,
        PatchManyProductsByQueryAction $action
    ): EmptyResource {
        $action->execute($query->getEloquentBuilder(), $request->getFields(), $request->getAttributes());

        return new EmptyResource();
    }

    public function replaceAttributes(
        int $id,
        ReplaceProductAttributesRequest $request,
        ReplaceProductAttributesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function patchAttributes(
        int $id,
        PatchProductAttributesRequest $request,
        PatchProductAttributesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function deleteAttributes(
        int $id,
        DeleteProductAttributesRequest $request,
        DeleteProductAttributesAction $action
    ): Responsable {
        $action->execute($id, $request->getPropertyIds());

        return new EmptyResource();
    }

    public function commonAttributes(
        ProductDraftsQuery $query,
        CalculateProductsCommonAttributesAction $action
    ): AnonymousResourceCollection {
        return PropertiesResource::collection($action->execute($query->getEloquentBuilder()));
    }

    public function preloadImage(PreloadImageRequest $request, PreloadImageAction $action): PreloadPublicFileResource
    {
        return new PreloadPublicFileResource($action->execute($request->image()));
    }

    public function patchImages(
        int $id,
        PatchProductImagesRequest $request,
        PatchProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function replaceImages(
        int $id,
        ReplaceProductImagesRequest $request,
        ReplaceProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function uploadImage(
        int $id,
        UploadProductImageRequest $request,
        UploadProductImageAction $action
    ): ProductImagesResource {
        return new ProductImagesResource($action->execute($id, $request->validated()));
    }

    public function deleteImage(
        int $id,
        DeleteProductImageRequest $request,
        DeleteProductImageAction $action
    ): EmptyResource {
        $action->execute($id, $request->validated('file_id'));

        return new EmptyResource();
    }
}
