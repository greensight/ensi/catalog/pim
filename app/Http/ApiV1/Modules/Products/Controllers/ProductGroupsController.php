<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Domain\Products\Actions\ProductGroups\BindProductsAction;
use App\Domain\Products\Actions\ProductGroups\CreateProductGroupAction;
use App\Domain\Products\Actions\ProductGroups\DeleteManyProductGroupsAction;
use App\Domain\Products\Actions\ProductGroups\DeleteProductGroupAction;
use App\Domain\Products\Actions\ProductGroups\PatchProductGroupAction;
use App\Domain\Products\Actions\ProductGroups\ReplaceProductGroupAction;
use App\Domain\Support\MassOperationResult;
use App\Http\ApiV1\Modules\Products\Queries\ProductGroupsQuery;
use App\Http\ApiV1\Modules\Products\Requests\BindProductGroupProductsRequest;
use App\Http\ApiV1\Modules\Products\Requests\CreateProductGroupRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchProductGroupRequest;
use App\Http\ApiV1\Modules\Products\Requests\ReplaceProductGroupRequest;
use App\Http\ApiV1\Modules\Products\Resources\ProductGroupsBindResultResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductGroupsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductGroupsController
{
    public function create(CreateProductGroupRequest $request, CreateProductGroupAction $action): ProductGroupsResource
    {
        return ProductGroupsResource::make($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceProductGroupRequest $request,
        ReplaceProductGroupAction $action
    ): ProductGroupsResource {
        return ProductGroupsResource::make($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchProductGroupRequest $request,
        PatchProductGroupAction $action
    ): ProductGroupsResource {
        return ProductGroupsResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductGroupAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyProductGroupsAction $action): MassOperationResult
    {
        return $action->execute($request->getIds());
    }

    public function get(int $id, ProductGroupsQuery $query): ProductGroupsResource
    {
        return ProductGroupsResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductGroupsQuery $query): AnonymousResourceCollection
    {
        return ProductGroupsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(ProductGroupsQuery $query): ProductGroupsResource
    {
        return ProductGroupsResource::make($query->firstOrFail());
    }

    public function bindProducts(
        int $id,
        BindProductGroupProductsRequest $request,
        BindProductsAction $action
    ): ProductGroupsBindResultResource {
        return ProductGroupsBindResultResource::make(
            $action->enrichResult()->execute($id, $request->isReplace(), $request->products())
        );
    }
}
