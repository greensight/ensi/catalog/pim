<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\ProductEventsQuery;
use App\Http\ApiV1\Modules\Products\Resources\ProductEventsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ProductEventsController
{
    public function search(ProductEventsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ProductEventsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
