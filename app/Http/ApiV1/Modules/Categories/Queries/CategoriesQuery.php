<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\Category;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\Includes\IncludeInterface;
use Spatie\QueryBuilder\QueryBuilder;

class CategoriesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Category::query());

        $this->allowedSorts([
            'id',
            'name',
            'code',
            'parent_id',
            'is_active',
            'is_real_active',
            'is_inherits_properties',
            'created_at',
            'updated_at',
        ]);

        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('parent_id'),
            AllowedFilter::exact('is_active'),
            AllowedFilter::exact('is_real_active'),

            ...StringFilter::make('code')->contain(),
            ...StringFilter::make('name')->contain(),
            AllowedFilter::callback('is_root', function (Builder $query, $value) {
                if (!is_bool($value)) {
                    return;
                }

                if ($value === true) {
                    $query->whereIsRoot();
                } else {
                    /** @var Category $model */
                    $model = $query->getModel();
                    $query->whereNotNull($model->getParentIdName());
                }
            }),
            AllowedFilter::callback('exclude_id', function (Builder $query, $ids) {
                return is_array($ids)
                    ? $query->whereNotIn('id', $ids)
                    : $query->where('id', '!=', $ids);
            }),
            AllowedFilter::scope('has_is_gluing'),
        ]);

        $this->allowedIncludes([
            'properties',
            AllowedInclude::custom('hidden_properties', new class () implements IncludeInterface {
                public function __invoke(Builder $query, string $include)
                {
                    $query->with(['properties', 'boundProperties']);
                }
            }),
        ]);
    }
}
