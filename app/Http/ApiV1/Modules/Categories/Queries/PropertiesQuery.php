<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\Property;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PropertiesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Property::query());

        $this->allowedSorts(['id', 'created_at', 'updated_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('display_name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('is_active'),
            AllowedFilter::exact('is_public'),
            AllowedFilter::exact('is_filterable'),
            AllowedFilter::exact('is_multiple'),
            AllowedFilter::exact('is_system'),
            AllowedFilter::exact('is_common'),
            AllowedFilter::exact('is_required'),
            AllowedFilter::exact('has_directory'),
            AllowedFilter::exact('hint_value'),
            AllowedFilter::exact('hint_value_name'),

            ...StringFilter::make('name')->contain(),
            ...StringFilter::make('display_name')->contain(),
            ...StringFilter::make('code')->contain(),
            ...StringFilter::make('hint_value')->contain(),
            ...StringFilter::make('hint_value_name')->contain(),

            ...DateFilter::make('created_at')->exact()->lte()->gte(),
            ...DateFilter::make('updated_at')->exact()->lte()->gte(),

            ...ExtraFilter::nested('actualLinks', [
                AllowedFilter::exact('category_id'),
            ]),
        ]);

        $this->allowedIncludes(['directory']);
    }
}
