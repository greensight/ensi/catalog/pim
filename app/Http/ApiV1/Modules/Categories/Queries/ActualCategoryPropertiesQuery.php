<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\ActualCategoryProperty;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ActualCategoryPropertiesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ActualCategoryProperty::query());

        $this->allowedIncludes(['category', 'property']);
        $this->allowedSorts(['id', 'created_at', 'updated_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('property_id'),
            AllowedFilter::exact('is_required'),
            AllowedFilter::exact('is_gluing'),
            AllowedFilter::exact('is_inherited'),
            AllowedFilter::exact('is_common'),

            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),
        ]);
    }
}
