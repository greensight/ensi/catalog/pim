<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ActualCategoryProperty
 */
class ActualCategoryPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'property_id' => $this->property_id,
            'is_required' => $this->is_required,
            'is_gluing' => $this->is_gluing,
            'is_inherited' => $this->is_inherited,
            'is_common' => $this->is_common,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'category' => CategoriesResource::make($this->whenLoaded('category')),
            'property' => PropertiesResource::make($this->whenLoaded('property')),
        ];
    }
}
