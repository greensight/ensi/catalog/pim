<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\Property;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Property
 */
class PropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'display_name' => $this->display_name,
            'type' => $this->type,
            'hint_value' => $this->hint_value,
            'hint_value_name' => $this->hint_value_name,

            'is_system' => $this->is_system,
            'is_active' => $this->is_active,
            'is_multiple' => $this->is_multiple,
            'is_filterable' => $this->is_filterable,
            'is_public' => $this->is_public,
            'is_common' => $this->is_common,
            'is_required' => $this->is_required,
            'has_directory' => $this->has_directory,
            'is_moderated' => $this->is_moderated,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'directory' => $this->when(
                $this->isDirectoryLoaded(),
                fn () => AttributeDirectoryValuesResource::collection($this->directory)
            ),
        ];
    }

    private function isDirectoryLoaded(): bool
    {
        return $this->has_directory && $this->relationLoaded('directory');
    }
}
