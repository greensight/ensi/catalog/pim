<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\Category;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class CategoriesTreeResourceCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return $this->collection->keyBy('id')
            ->pipe(fn (Collection $source) => $this->buildTree($source))
            ->pipe(fn (Collection $source) => $this->mapCategories($source))
            ->toArray();
    }

    private function categoryToArray(Category $category): array
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'code' => $category->code,
            'children' => $this->when(
                $category->relationLoaded('children'),
                fn () => $this->mapCategories($category->children),
                []
            ),
        ];
    }

    private function mapCategories(Collection $categories): Collection
    {
        return $categories->map(
            fn (Category $category) => $this->categoryToArray($category)
        );
    }

    private function buildTree(Collection $source): Collection
    {
        $result = new Collection();

        /** @var Category $category */
        foreach ($source as $category) {
            if (!$category->parent_id || !$source->has($category->parent_id)) {
                $result->push($category);

                continue;
            }

            $parent = $source->get($category->parent_id);

            if (!$parent) {
                continue;
            }

            if (!$parent->relationLoaded('children')) {
                $parent->setRelation('children', new Collection());
            }

            $parent->children->push($category);
        }

        return $result;
    }
}
