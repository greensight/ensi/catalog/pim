<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\CategoryPropertyView;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CategoryPropertyView
 */
class CategoryPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'property_id' => $this->property_id,
            'name' => $this->name,
            'code' => $this->code,
            'display_name' => $this->display_name,
            'type' => $this->type,
            'hint_value' => $this->hint_value,
            'hint_value_name' => $this->hint_value_name,

            'is_active' => $this->is_active,
            'is_multiple' => $this->is_multiple,
            'is_filterable' => $this->is_filterable,
            'is_public' => $this->is_public,
            'has_directory' => $this->has_directory,

            'is_required' => $this->is_required,
            'is_inherited' => $this->is_inherited,
            'is_common' => $this->is_common,
            'is_gluing' => $this->is_gluing,
        ];
    }
}
