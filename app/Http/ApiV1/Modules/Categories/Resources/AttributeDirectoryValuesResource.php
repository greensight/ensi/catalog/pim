<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PropertyDirectoryValue
 */
class AttributeDirectoryValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getKey(),
            'code' => $this->code,
            $this->merge(new PropertyValuesResource($this->resource)),
        ];
    }
}
