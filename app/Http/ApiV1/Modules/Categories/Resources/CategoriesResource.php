<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\Category;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;

/**
 * @mixin Category
 */
class CategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'parent_id' => $this->parent_id,
            'is_inherits_properties' => $this->is_inherits_properties,
            'is_active' => $this->is_active,
            'is_real_active' => $this->is_real_active,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            $this->mergeWhen($this->relationLoaded('properties'), fn () => $this->properties()),
        ];
    }

    private function properties(): array
    {
        $properties = $this->properties;

        $result = [
            'properties' => CategoryPropertiesResource::collection($properties),
        ];

        if ($this->relationLoaded('boundProperties')) {
            $result['hidden_properties'] = $this->hiddenProperties($properties->keyBy('property_id'));
        }

        return $result;
    }

    private function hiddenProperties(Collection $actualProperties): AnonymousResourceCollection
    {
        $hidden = $this->allProperties->keyBy('property_id')
            ->diffKeys($actualProperties)
            ->values();

        return CategoryPropertiesResource::collection($hidden);
    }
}
