<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Models\CategoryProductLink;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CategoryProductLink */
class CategoryProductLinksResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
