<?php

namespace App\Http\ApiV1\Modules\Categories\Controllers;

use App\Domain\Categories\Actions\DirectoryValues\CreateDirectoriesValueAction;
use App\Domain\Categories\Actions\DirectoryValues\CreateDirectoryValueAction;
use App\Domain\Categories\Actions\DirectoryValues\DeleteDirectoryValueAction;
use App\Domain\Categories\Actions\DirectoryValues\PatchDirectoryValueAction;
use App\Domain\Categories\Actions\DirectoryValues\PreloadFileAction;
use App\Domain\Categories\Actions\DirectoryValues\ReplaceDirectoryValueAction;
use App\Http\ApiV1\Modules\Categories\Queries\DirectoryValuesQuery;
use App\Http\ApiV1\Modules\Categories\Requests\CreateDirectoryValueRequest;
use App\Http\ApiV1\Modules\Categories\Requests\CreateDirectoryValuesRequest;
use App\Http\ApiV1\Modules\Categories\Requests\PatchDirectoryValueRequest;
use App\Http\ApiV1\Modules\Categories\Requests\ReplaceDirectoryValueRequest;
use App\Http\ApiV1\Modules\Categories\Resources\DirectoryValuesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\PreloadFileRequest;
use App\Http\ApiV1\Support\Requests\PreloadImageRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\PreloadPublicFileResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DirectoryValuesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, DirectoryValuesQuery $query): AnonymousResourceCollection
    {
        return DirectoryValuesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, DirectoryValuesQuery $query): DirectoryValuesResource
    {
        return new DirectoryValuesResource($query->findOrFail($id));
    }

    public function create(
        int $propertyId,
        CreateDirectoryValueRequest $request,
        CreateDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($propertyId, $request->validated()));
    }

    public function massCreate(
        int $propertyId,
        CreateDirectoryValuesRequest $request,
        CreateDirectoriesValueAction $action
    ): AnonymousResourceCollection {
        return DirectoryValuesResource::collection($action->execute($propertyId, $request->validated()));
    }

    public function replace(
        int $id,
        ReplaceDirectoryValueRequest $request,
        ReplaceDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchDirectoryValueRequest $request,
        PatchDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDirectoryValueAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function preloadImage(PreloadImageRequest $request, PreloadFileAction $action): PreloadPublicFileResource
    {
        return new PreloadPublicFileResource($action->execute($request->image()));
    }

    public function preloadFile(PreloadFileRequest $request, PreloadFileAction $action): PreloadPublicFileResource
    {
        return new PreloadPublicFileResource($action->execute($request->file('file')));
    }
}
