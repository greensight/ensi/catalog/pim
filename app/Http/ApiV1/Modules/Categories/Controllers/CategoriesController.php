<?php

namespace App\Http\ApiV1\Modules\Categories\Controllers;

use App\Domain\Categories\Actions\Categories\ActualizeCategoriesAction;
use App\Domain\Categories\Actions\Categories\BindPropertiesAction;
use App\Domain\Categories\Actions\Categories\CreateCategoryAction;
use App\Domain\Categories\Actions\Categories\DeleteCategoryAction;
use App\Domain\Categories\Actions\Categories\DeleteManyCategoriesAction;
use App\Domain\Categories\Actions\Categories\PatchCategoryAction;
use App\Domain\Categories\Actions\Categories\ReplaceCategoryAction;
use App\Http\ApiV1\Modules\Categories\Queries\CategoriesQuery;
use App\Http\ApiV1\Modules\Categories\Queries\CategoriesTreeQuery;
use App\Http\ApiV1\Modules\Categories\Requests\BindCategoryPropertiesRequest;
use App\Http\ApiV1\Modules\Categories\Requests\CreateCategoryRequest;
use App\Http\ApiV1\Modules\Categories\Requests\PatchCategoryRequest;
use App\Http\ApiV1\Modules\Categories\Requests\ReplaceCategoryRequest;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesResource;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesTreeResourceCollection;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoriesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, CategoriesQuery $query): AnonymousResourceCollection
    {
        return CategoriesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, CategoriesQuery $query): CategoriesResource
    {
        return new CategoriesResource($query->findOrFail($id));
    }

    public function create(CreateCategoryRequest $request, CreateCategoryAction $action): CategoriesResource
    {
        return new CategoriesResource($action->refreshResult()->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceCategoryRequest $request,
        ReplaceCategoryAction $action
    ): CategoriesResource {
        return new CategoriesResource($action->refreshResult()->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchCategoryRequest $request, PatchCategoryAction $action): CategoriesResource
    {
        return new CategoriesResource($action->refreshResult()->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteCategoryAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyCategoriesAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    public function tree(CategoriesTreeQuery $query): CategoriesTreeResourceCollection
    {
        return new CategoriesTreeResourceCollection($query->get());
    }

    public function bindProperties(
        int $id,
        BindCategoryPropertiesRequest $request,
        BindPropertiesAction $action
    ): CategoriesResource {
        return new CategoriesResource(
            $action->enrichResult()->execute($id, $request->isReplace(), $request->properties())
        );
    }

    public function actualize(ActualizeCategoriesAction $action): EmptyResource
    {
        $action->execute();

        return new EmptyResource();
    }
}
