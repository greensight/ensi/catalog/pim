<?php

namespace App\Http\ApiV1\Modules\Categories\Controllers;

use App\Http\ApiV1\Modules\Categories\Queries\ActualCategoryPropertiesQuery;
use App\Http\ApiV1\Modules\Categories\Resources\ActualCategoryPropertiesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ActualCategoryPropertiesController
{
    public function search(ActualCategoryPropertiesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ActualCategoryPropertiesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
