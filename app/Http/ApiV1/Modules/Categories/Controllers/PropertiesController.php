<?php

namespace App\Http\ApiV1\Modules\Categories\Controllers;

use App\Domain\Categories\Actions\Properties\CreatePropertyAction;
use App\Domain\Categories\Actions\Properties\DeleteManyPropertiesAction;
use App\Domain\Categories\Actions\Properties\DeletePropertyAction;
use App\Domain\Categories\Actions\Properties\PatchPropertyAction;
use App\Domain\Categories\Actions\Properties\ReplacePropertyAction;
use App\Domain\Support\MassOperationResult;
use App\Exceptions\IllegalOperationException;
use App\Http\ApiV1\Modules\Categories\Queries\PropertiesQuery;
use App\Http\ApiV1\Modules\Categories\Requests\CreatePropertyRequest;
use App\Http\ApiV1\Modules\Categories\Requests\PatchPropertyRequest;
use App\Http\ApiV1\Modules\Categories\Requests\ReplacePropertyRequest;
use App\Http\ApiV1\Modules\Categories\Resources\PropertiesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PropertiesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, PropertiesQuery $query): AnonymousResourceCollection
    {
        return PropertiesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, PropertiesQuery $query): PropertiesResource
    {
        return new PropertiesResource($query->findOrFail($id));
    }

    public function create(CreatePropertyRequest $request, CreatePropertyAction $action): PropertiesResource
    {
        return new PropertiesResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplacePropertyRequest $request,
        ReplacePropertyAction $action
    ): PropertiesResource {
        return new PropertiesResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchPropertyRequest $request, PatchPropertyAction $action): PropertiesResource
    {
        return new PropertiesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeletePropertyAction $action): EmptyResource
    {
        try {
            $action->execute($id);
        } catch (IllegalOperationException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyPropertiesAction $action): MassOperationResult
    {
        return $action->execute($request->getIds());
    }
}
