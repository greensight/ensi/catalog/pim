<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertNotSoftDeleted;
use function Pest\Laravel\assertSoftDeleted;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component', 'complex');

test('show bound attribute', function () {
    $category = Category::factory()->createOne();
    $product = Product::factory()->inCategories([$category])->create();

    $property = Property::factory()
        ->withType(PropertyType::INTEGER)
        ->actual($category)
        ->createOne();

    $release = ProductPropertyValue::factory()
        ->forProduct($product)
        ->forProperty($property)
        ->published()
        ->createOne()
        ->release;

    $release->delete();

    $request = ['properties' => [['id' => $property->id, 'is_required' => false, 'is_gluing' => false]]];
    postJson("/api/v1/categories/categories/{$category->id}:bind-properties", $request)
        ->assertOk();

    assertNotSoftDeleted($release);
});

test('hide deactivated attribute', function () {
    $category = Category::factory()->createOne();
    $product = Product::factory()->inCategories([$category])->create();

    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->createOne();

    CategoryPropertyLink::factory()->createFast($category, $property);
    $release = ProductPropertyValue::factory()
        ->forProduct($product)
        ->forProperty($property)
        ->published()
        ->createOne()
        ->release;

    patchJson("/api/v1/categories/properties/{$property->id}", ['is_active' => false])
        ->assertOk();

    assertSoftDeleted($release);
});

test('hidden_properties inherits attribute', function () {
    $parent = Category::factory()->actualized()->createOne();
    $child = Category::factory()->withParent($parent)->actualized()->createOne();

    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($parent, isInherited: true)
        ->createOne();

    // Bind property (for parent category)
    $request = ['properties' => [['id' => $property->id, 'is_required' => false, 'is_gluing' => false]]];
    postJson("/api/v1/categories/categories/{$parent->id}:bind-properties", $request)
        ->assertOk();

    // Check inherits active attribute (for child category)
    getJson("/api/v1/categories/categories/$child->id?include=hidden_properties")
        ->assertStatus(200)
        ->assertJsonCount(0, 'data.hidden_properties')
        ->assertJsonCount(1, 'data.properties');

    // Disable property
    patchJson("/api/v1/categories/properties/{$property->id}", ['is_active' => false])
        ->assertOk();

    // Check inherits disabled attribute (for child category)
    getJson("/api/v1/categories/categories/$child->id?include=hidden_properties")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['hidden_properties' => [['property_id', 'is_required', 'name']]]])
        ->assertJsonPath('data.hidden_properties.0.property_id', $property->id)
        ->assertJsonCount(1, 'data.hidden_properties')
        ->assertJsonCount(0, 'data.properties');
});

test('show activated attribute', function () {
    $category = Category::factory()->createOne();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()
        ->withType(PropertyType::DOUBLE)
        ->inactive()
        ->actual($category)
        ->createOne();

    CategoryPropertyLink::factory()->createFast($category, $property);
    $release = ProductPropertyValue::factory()
        ->forProduct($product)
        ->forProperty($property)
        ->published()
        ->createOne()
        ->release;

    $release->delete();

    patchJson("/api/v1/categories/properties/{$property->id}", ['is_active' => true])
        ->assertOk();

    assertNotSoftDeleted($release);
});
