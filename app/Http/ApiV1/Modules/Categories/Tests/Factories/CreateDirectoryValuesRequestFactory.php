<?php

namespace App\Http\ApiV1\Modules\Categories\Tests\Factories;

use App\Domain\Classifiers\Enums\PropertyType;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateDirectoryValuesRequestFactory extends BaseApiFactory
{
    public ?int $id;
    public ?PropertyType $type = null;

    protected function definition(): array
    {
        $propertyId = $this->id ?? $this->faker->randomDigitNotZero();
        $type = $this->type ?? $this->faker->randomEnum(PropertyType::cases());
        $items = [];
        for ($i = 0; $i < $this->faker->randomDigitNotZero(); $i++) {
            $items[] = DirectoryValueFactory::new()->withType($type)->make(['property_id' => $propertyId]);
        }

        return [
            'items' => $items,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withPropertyId(int $id): self
    {
        return $this->immutableSet('property_id', $id);
    }

    public function withType(PropertyType $type): self
    {
        return $this->immutableSet('type', $type);
    }
}
