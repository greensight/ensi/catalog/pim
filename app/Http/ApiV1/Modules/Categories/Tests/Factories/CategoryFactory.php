<?php

namespace App\Http\ApiV1\Modules\Categories\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CategoryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->word,
            'code' => $this->faker->unique()->regexify('[A-Za-z0-9_]{30}'),
            'is_active' => true,
            'parent_id' => null,
            'is_inherits_properties' => true,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
