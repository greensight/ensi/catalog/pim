<?php

namespace App\Http\ApiV1\Modules\Categories\Tests\Factories;

use App\Domain\Classifiers\Enums\PropertyType;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Support\Facades\Date;

class DirectoryValueFactory extends BaseApiFactory
{
    public ?PropertyType $type = null;

    protected function definition(): array
    {
        $type = $this->type ?? $this->faker->randomElement(PropertyType::cases());

        return [
            'value' => $this->generateValue($type),
            'name' => $this->faker->sentence(3),
            'type' => $type->value,
            'code' => $this->faker->slug(2),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withType(PropertyType $type): self
    {
        return $this->immutableSet('type', $type);
    }

    private function generateValue(PropertyType $type): mixed
    {
        return (string) match ($type) {
            PropertyType::STRING => $this->faker->word,
            PropertyType::TEXT => $this->faker->text,
            PropertyType::BOOLEAN => $this->faker->boolean ? '1' : '0',
            PropertyType::COLOR => '#' . dechex($this->faker->numberBetween(0x100000, 0xFFFFFF)),
            PropertyType::DATETIME => Date::make($this->faker->dateTime)->toJSON(),
            PropertyType::DOUBLE => $this->faker->randomFloat(4),
            PropertyType::INTEGER => $this->faker->randomNumber(),
            PropertyType::IMAGE => EnsiFile::factory()->fileName($this->faker->word)->fileExt('jpg')->make()['path'],
            PropertyType::FILE => EnsiFile::factory()->fileName($this->faker->word)->fileExt('txt')->make()['path'],
        };
    }
}
