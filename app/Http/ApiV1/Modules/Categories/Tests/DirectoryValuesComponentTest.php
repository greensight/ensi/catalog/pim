<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Modules\Categories\Tests\Factories\CreateDirectoryValuesRequestFactory;
use App\Http\ApiV1\Modules\Categories\Tests\Factories\DirectoryValueFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\assertDatabaseCount;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('categories', 'component');

test('POST /api/v1/categories/properties/{id}:add-directory success', function (PropertyType $type) {
    $property = Property::factory()->create(['has_directory' => true, 'type' => $type->value]);
    $propertyId = $property->id;
    $request = DirectoryValueFactory::new()->withType($property->type)->make();

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'value', 'name', 'code', 'type']])
        ->assertJsonPath('data.value', (string)PropertyValue::make($type, $request['value']));

    assertDatabaseHas(PropertyDirectoryValue::class, ['property_id' => $propertyId]);
})->with(PropertyType::cases());

test('POST /api/v1/categories/properties/{id}:add-directory invalid value', function () {
    $property = Property::factory()->create(['has_directory' => true, 'type' => PropertyType::INTEGER]);
    $propertyId = $property->id;
    $request = DirectoryValueFactory::new()->withType(PropertyType::STRING)->make();

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', 'ValidationError');

    assertDatabaseMissing(PropertyDirectoryValue::class, ['property_id' => $propertyId]);
});

test('POST /api/v1/categories/properties/{id}:add-directory preloaded image', function () {
    $property = Property::factory()->create(['has_directory' => true, 'type' => PropertyType::IMAGE]);
    $propertyId = $property->id;
    $imagePath = EnsiFile::factory()->fileName('image_1024')->fileExt('png')->make()['path'];
    $tempFile = TempFile::create(['path' => $imagePath]);
    $request = DirectoryValueFactory::new()
        ->withType($property->type)
        ->except(['value'])
        ->make(['preload_file_id' => $tempFile->id]);

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(201)
        ->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'value',
                'file' => ['path', 'root_path', 'url'],
            ],
        ])
        ->assertJsonPath('data.value', $imagePath);

    assertModelMissing($tempFile);
});

test('POST /api/v1/categories/properties/{id}:add-directory preloaded file', function () {
    $property = Property::factory()->create(['has_directory' => true, 'type' => PropertyType::FILE]);
    $propertyId = $property->id;
    $filePath = EnsiFile::factory()->fileName('text_1024')->fileExt('txt')->make()['path'];
    $tempFile = TempFile::create(['path' => $filePath]);
    $request = DirectoryValueFactory::new()
        ->withType($property->type)
        ->except(['value'])
        ->make(['preload_file_id' => $tempFile->id]);

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(201)
        ->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'value',
                'file' => ['path', 'root_path', 'url'],
            ],
        ])
        ->assertJsonPath('data.value', $filePath);

    assertModelMissing($tempFile);
});

test('PUT /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::COLOR)->create();
    $request = DirectoryValueFactory::new()->withType(PropertyType::COLOR)->make();
    $id = $value->id;

    putJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200);
});

test('PUT /api/v1/categories/properties/directory/{id} not directory property', function () {
    $property = Property::factory()->withType(PropertyType::STRING)->create();
    $value = PropertyDirectoryValue::factory()->forProperty($property)->create();
    $request = DirectoryValueFactory::new()->withType($value->type)->make();
    $id = $value->id;

    putJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::COLOR)->create();
    $id = $value->id;
    $request = DirectoryValueFactory::new()->only(['name'])->make();

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.name', $request['name']);

    assertDatabaseHas($value->getTable(), ['id' => $id, 'name' => $request['name']]);
});

test('PATCH /api/v1/categories/properties/directory/{id} null value', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::INTEGER)->create(['value' => 90]);
    $id = $value->id;
    $request = ['value' => null];

    $this->skipNextOpenApiRequestValidation()
        ->patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(400);

    assertDatabaseHas($value->getTable(), ['id' => $id, 'value' => 90]);
});

test('PATCH /api/v1/categories/properties/directory/{id} preloaded image', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE)->create();
    $id = $value->id;
    $oldFile = $value->getValue()->native();

    $imagePath = EnsiFile::factory()->fileName('image_1024')->fileExt('png')->make()['path'];
    $tempFile = TempFile::create(['path' => $imagePath]);
    $request = ['preload_file_id' => $tempFile->id];

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.value', $tempFile->path);

    assertDatabaseHas(TempFile::class, ['path' => $oldFile]);
});

test('PATCH /api/v1/categories/properties/directory/{id} preloaded file', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE)->create();
    $id = $value->id;
    $oldFile = $value->getValue()->native();

    $filePath = EnsiFile::factory()->fileName('text_1024')->fileExt('txt')->make()['path'];
    $tempFile = TempFile::create(['path' => $filePath]);
    $request = ['preload_file_id' => $tempFile->id];

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.value', $tempFile->path);

    assertDatabaseHas(TempFile::class, ['path' => $oldFile]);
});

test('PATCH /api/v1/categories/properties/directory/{id} to external file', function () {
    $fileUrl = 'https://ya.ru/images/001.jpg';
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE)->create();
    $id = $value->id;
    $request = ['value' => $fileUrl];

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.value', $fileUrl);
});

test('DELETE /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::COLOR)->create();
    $id = $value->id;

    deleteJson("/api/v1/categories/properties/directory/$id")
        ->assertStatus(200);

    assertModelMissing($value);
});

test('DELETE /api/v1/categories/properties/directory/{id} used in products failed', function () {
    $propValue = ProductPropertyValue::factory()
        ->directory()
        ->actual()
        ->createOne();

    deleteJson("/api/v1/categories/properties/directory/{$propValue->directory_value_id}")
        ->assertStatus(400);

    assertModelExists($propValue);
    expect($propValue->directoryValue)->not->toBeNull();
});

test('GET /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::STRING)->create();
    $id = $value->id;

    getJson("/api/v1/categories/properties/directory/$id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['code', 'name', 'value', 'type']]);
});

test('POST /api/v1/categories/properties/directory:search success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::STRING)->count(2)->create()->last();
    $request = [
        'filter' => ['code' => $value->code],
    ];

    postJson('/api/v1/categories/properties/directory:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment(['code' => $value->code]);
});

test('POST /api/v1/categories/properties/directory:preload-image success', function () {
    $diskName = resolve(EnsiFilesystemManager::class)->publicDiskName();
    Storage::fake($diskName);

    $file = UploadedFile::fake()->image('123.png', 300, 300);

    $fileId = postFile('/api/v1/categories/properties/directory:preload-image', $file)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'preload_file_id',
                'file' => ['path', 'root_path', 'url'],
            ],
        ])
        ->json('data.preload_file_id');

    assertDatabaseHas(TempFile::class, ['id' => $fileId]);

    $tempFile = TempFile::find($fileId);

    /** @var \Illuminate\Filesystem\FilesystemAdapter */
    $disk = Storage::disk($diskName);
    $disk->assertExists($tempFile->path);
});

test('POST /api/v1/categories/properties/directory:preload-file success', function () {
    $diskName = resolve(EnsiFilesystemManager::class)->publicDiskName();
    Storage::fake($diskName);

    $file = UploadedFile::fake()->create('123.txt', kilobytes: 20);

    $fileId = postFile('/api/v1/categories/properties/directory:preload-file', $file)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'preload_file_id',
                'file' => ['path', 'root_path', 'url'],
            ],
        ])
        ->json('data.preload_file_id');

    assertDatabaseHas(TempFile::class, ['id' => $fileId]);

    $tempFile = TempFile::find($fileId);

    /** @var \Illuminate\Filesystem\FilesystemAdapter */
    $disk = Storage::disk($diskName);
    $disk->assertExists($tempFile->path);
});


test('POST /api/v1/categories/properties/{id}:add-directories success', function () {
    $property = Property::factory()->create(['has_directory' => true]);
    $propertyId = $property->id;
    $count = PropertyDirectoryValue::query()->count();
    $request = CreateDirectoryValuesRequestFactory::new()
        ->withPropertyId($propertyId)
        ->withType($property->type)
        ->make();

    postJson("/api/v1/categories/properties/$propertyId:add-directories", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.0.property_id', $propertyId)
        ->assertJsonPath('data.0.type', $property->type->value)
        ->assertJsonCount(count($request['items']), 'data');

    assertDatabaseHas(Property::class, ['id' => $propertyId]);
    assertDatabaseCount(PropertyDirectoryValue::class, count($request['items']) + $count);
    assertDatabaseHas(PropertyDirectoryValue::class, ['name' => $request['items'][0]['name']]);
});

test('POST /api/v1/categories/properties/{id}:add-directories invalid value', function () {
    $property = Property::factory()->create(['has_directory' => true, 'type' => PropertyType::INTEGER]);
    $propertyId = $property->id;
    $request = CreateDirectoryValuesRequestFactory::new()
        ->withPropertyId($propertyId)
        ->withType(PropertyType::STRING)
        ->make();

    postJson("/api/v1/categories/properties/$propertyId:add-directories", $request)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', 'ValidationError');

    assertDatabaseMissing(PropertyDirectoryValue::class, ['property_id' => $propertyId]);
});
