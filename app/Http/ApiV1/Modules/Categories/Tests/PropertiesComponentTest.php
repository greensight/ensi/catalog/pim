<?php

use App\Domain\Categories\Events\PropertyInvalidated;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Http\ApiV1\Modules\Categories\Tests\Factories\PropertyFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('categories', 'component');

beforeEach(function () {
    Event::fake([PropertyInvalidated::class]);
});

test('POST /api/v1/categories/properties success', function () {
    $request = PropertyFactory::new()->make();

    postJson('/api/v1/categories/properties', $request)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['id']])
        ->assertJsonFragment($request);

    assertDatabaseHas(Property::class, ['code' => $request['code'], 'is_system' => false]);
});

test('PATCH /api/v1/categories/properties/{id} success', function () {
    $property = Property::factory()->create();
    $request = ['code' => 'foo'];
    $id = $property->id;

    patchJson("/api/v1/categories/properties/$id", $request)
        ->assertStatus(200)
        ->assertJsonFragment($request);

    assertDatabaseHas($property->getTable(), ['id' => $id, 'code' => $request['code']]);
    Event::assertNotDispatched(PropertyInvalidated::class);
});

test('PATCH /api/v1/categories/properties/{id} 404', function () {
    $id = (Property::query()->max('id') ?? 0) + 1;

    patchJson("/api/v1/categories/properties/$id", ['code' => 'foo'])
        ->assertNotFound();
});

test('PATCH /api/v1/categories/properties/{id} system property success', function () {
    $property = Property::factory()->create(['is_system' => true, 'code' => 'property', 'is_active' => false]);
    $request = ['code' => 'foo', 'is_active' => true];
    $id = $property->id;

    patchJson("/api/v1/categories/properties/$id", $request)
        ->assertStatus(200);

    assertDatabaseHas($property->getTable(), ['id' => $id, 'code' => $property->code, 'is_active' => true]);
    Event::assertDispatched(PropertyInvalidated::class);
});

test('PUT /api/v1/categories/properties/{id} success', function () {
    $property = Property::factory()->create();
    $request = PropertyFactory::new()->make(['name' => 'New attribute name', 'is_active' => !$property->is_active]);
    $id = $property->id;

    putJson("/api/v1/categories/properties/$id", $request)
        ->assertStatus(200)
        ->assertJsonFragment($request);

    assertDatabaseHas($property->getTable(), ['id' => $id, 'name' => $request['name']]);
    Event::assertDispatched(PropertyInvalidated::class);
});

test('PUT /api/v1/categories/properties/{id} system property', function () {
    $property = Property::factory()->create(['is_system' => true, 'code' => 'property']);
    $request = PropertyFactory::new()->make(['code' => 'foo', 'is_required' => false]);
    $id = $property->id;

    putJson("/api/v1/categories/properties/$id", $request)
        ->assertStatus(200);

    assertDatabaseHas($property->getTable(), ['id' => $id, 'code' => $property->code]);
});

test('DELETE /api/v1/categories/properties/{id} success', function () {
    $property = Property::factory()->create();
    $id = $property->id;

    deleteJson("/api/v1/categories/properties/$id")
        ->assertStatus(200);

    assertDatabaseMissing($property->getTable(), ['id' => $id]);
});

test('DELETE /api/v1/categories/properties/{id} system property 403', function () {
    $property = Property::factory()->create(['is_system' => true]);
    $id = $property->id;

    deleteJson("/api/v1/categories/properties/$id")
        ->assertStatus(400);

    assertDatabaseHas($property->getTable(), ['id' => $id]);
});

test('DELETE /api/v1/categories/properties/{id} link product 403', function () {
    $property = Property::factory()->create(['is_system' => true]);
    ProductPropertyValue::factory()->forProperty($property)->create();
    $id = $property->id;

    deleteJson("/api/v1/categories/properties/$id")
        ->assertStatus(400);

    assertDatabaseHas($property->getTable(), ['id' => $id]);
});

test('DELETE /api/v1/categories/properties/{id} used in categories 403', function () {
    $property = Property::factory()->create(['is_system' => true]);
    CategoryPropertyLink::factory()->forProperty($property)->create();
    $id = $property->id;

    deleteJson("/api/v1/categories/properties/$id")
        ->assertStatus(400);

    assertDatabaseHas($property->getTable(), ['id' => $id]);
});

test('POST /api/v1/categories/properties:mass-delete removes allowed', function () {
    $systemProperty = Property::factory()->create(['is_system' => true]);
    $property = Property::factory()->create();

    $request = [
        'ids' => [$systemProperty->id, $property->id],
    ];

    postJson('/api/v1/categories/properties:mass-delete', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data.processed')
        ->assertJsonPath('data.errors.0.id', $systemProperty->id);

    assertDatabaseHas($systemProperty->getTable(), ['id' => $systemProperty->id]);
    assertDatabaseMissing($property->getTable(), ['id' => $property->id]);
});

test('GET /api/v1/categories/properties/{id} success', function () {
    $property = Property::factory()->create();
    $id = $property->id;

    getJson("/api/v1/categories/properties/$id")
        ->assertStatus(200)
        ->assertJsonFragment(['id' => $id, 'code' => $property->code]);
});

test('GET /api/v1/categories/properties/{id} with directory success', function () {
    $property = Property::factory()->create(['has_directory' => true]);
    $directory = PropertyDirectoryValue::factory()->forProperty($property)->create();
    $id = $property->id;

    getJson("/api/v1/categories/properties/$id?include=directory")
        ->assertStatus(200)
        ->assertJsonPath('data.directory.0.id', $directory->id);
});

test('POST /api/v1/categories/properties:search success', function () {
    $property = Property::factory()->count(2)->create()->last();
    $request = ['filter' => ['code' => $property->code]];

    postJson('/api/v1/categories/properties:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment(['code' => $property->code]);
});

test('POST /api/v1/categories/properties:search filters by category', function () {
    ActualCategoryProperty::factory()->create();

    $category = Category::factory()->create();
    $link = ActualCategoryProperty::factory()->forCategory($category)->create();
    $request = ['filter' => ['category_id' => $category->id]];

    postJson('/api/v1/categories/properties:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $link->property_id);
});
