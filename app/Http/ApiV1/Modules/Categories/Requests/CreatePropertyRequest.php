<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreatePropertyRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'type' => ['required', new Enum(PropertyType::class)],
            'display_name' => ['required', 'string'],
            'code' => [
                'nullable',
                'string',
                Rule::unique(Property::class, 'code')->ignore((int)$this->route('id')),
            ],
            'hint_value' => ['nullable', 'string'],
            'hint_value_name' => ['nullable', 'string'],

            'is_active' => ['boolean'],
            'is_multiple' => ['boolean'],
            'is_filterable' => ['boolean'],
            'is_public' => ['boolean'],
            'is_required' => ['sometimes', 'boolean'],
            'has_directory' => ['boolean'],
            'is_moderated' => ['boolean'],
        ];
    }
}
