<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Categories\Actions\Categories\Data\CategoryPropertyLinkData;
use App\Domain\Categories\Models\Property;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class BindCategoryPropertiesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'replace' => ['sometimes', 'boolean'],
            'properties' => ['sometimes', 'array'],
            'properties.*.id' => [
                'required',
                Rule::exists(Property::class, 'id')->where('is_common', 0),
            ],
            'properties.*.is_required' => ['required', 'boolean'],
            'properties.*.is_gluing' => ['required', 'boolean'],
        ];
    }

    public function isReplace(): bool
    {
        return $this->validated()['replace'] ?? false;
    }

    /**
     * @return CategoryPropertyLinkData[]
     */
    public function properties(): array
    {
        $source = $this->validated()['properties'] ?? [];

        return Arr::map(
            $source,
            fn (array $property) => new CategoryPropertyLinkData(
                $property['id'],
                $property['is_required'],
                $property['is_gluing'],
            )
        );
    }
}
