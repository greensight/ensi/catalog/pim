<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDirectoryValueRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'value' => ['sometimes', 'required'],
            'name' => ['sometimes', 'string'],
            'code' => ['sometimes', 'nullable', 'string'],
            'preload_file_id' => ['sometimes', 'integer'],
        ];
    }
}
