<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Categories\Models\Category;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $categoryId = $this->route('id');

        return [
            'name' => ['required', 'string'],
            'code' => [
                'nullable',
                'string',
                'regex:/^[a-z0-9_]+$/i',
                Rule::unique(Category::class, 'code')->ignore($categoryId),
            ],
            'parent_id' => ['nullable', 'integer', Rule::exists(Category::class, 'id')],
            'is_active' => ['required', 'boolean'],
            'is_inherits_properties' => ['required', 'boolean'],
        ];
    }
}
