<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateDirectoryValuesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'items' => ['required', 'array'],
            'items.*.value' => ['required_without:items.*.preload_file_id'],
            'items.*.name' => ['required', 'string'],
            'items.*.code' => ['nullable', 'string'],
            'items.*.preload_file_id' => ['sometimes', 'integer'],
        ];
    }
}
