<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateDirectoryValueRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'value' => ['required_without:preload_file_id'],
            'name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'preload_file_id' => ['sometimes', 'integer'],
        ];
    }
}
