<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Classifiers\Enums\PropertyType;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchPropertyRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'type' => ['sometimes', 'required', new Enum(PropertyType::class)],
            'display_name' => ['sometimes', 'required', 'string'],
            'code' => ['sometimes', 'nullable', 'string'],
            'hint_value' => ['sometimes', 'nullable', 'string'],
            'hint_value_name' => ['sometimes', 'nullable', 'string'],

            'is_multiple' => ['sometimes', 'boolean'],
            'is_filterable' => ['sometimes', 'boolean'],
            'is_public' => ['sometimes', 'boolean'],
            'is_active' => ['sometimes', 'boolean'],
            'is_required' => ['sometimes', 'boolean'],
            'has_directory' => ['sometimes', 'boolean'],
            'is_moderated' => ['sometimes', 'boolean'],
        ];
    }
}
