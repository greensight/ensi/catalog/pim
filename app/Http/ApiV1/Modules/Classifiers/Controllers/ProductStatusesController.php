<?php

namespace App\Http\ApiV1\Modules\Classifiers\Controllers;

use App\Domain\Classifiers\Actions\ProductStatuses\CreateProductStatusSettingAction;
use App\Domain\Classifiers\Actions\ProductStatuses\DeleteProductStatusSettingAction;
use App\Domain\Classifiers\Actions\ProductStatuses\GetNextProductStatusesAction;
use App\Domain\Classifiers\Actions\ProductStatuses\PatchProductStatusSettingAction;
use App\Domain\Classifiers\Actions\ProductStatuses\SetPreviousProductStatusesAction;
use App\Http\ApiV1\Modules\Classifiers\Queries\ProductStatusesQuery;
use App\Http\ApiV1\Modules\Classifiers\Requests\CreateProductStatusRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\NextProductStatusesRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\PatchProductStatusRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\SetPreviousProductStatusesRequest;
use App\Http\ApiV1\Modules\Classifiers\Resources\ProductStatusesDataResource;
use App\Http\ApiV1\Modules\Classifiers\Resources\ProductStatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ProductStatusesController
{
    public function get(int $id, ProductStatusesQuery $query): Responsable
    {
        return ProductStatusesResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductStatusesQuery $query): Responsable
    {
        return ProductStatusesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateProductStatusRequest $request, CreateProductStatusSettingAction $action): Responsable
    {
        return ProductStatusesResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchProductStatusRequest $request, PatchProductStatusSettingAction $action): Responsable
    {
        return ProductStatusesResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductStatusSettingAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function getNext(NextProductStatusesRequest $request, GetNextProductStatusesAction $action): Responsable
    {
        return ProductStatusesDataResource::collection($action->execute($request->getStatusId()));
    }

    public function setPrevious(int $id, SetPreviousProductStatusesRequest $request, SetPreviousProductStatusesAction $action): Responsable
    {
        return ProductStatusesResource::make($action->execute($id, $request->getIds()));
    }
}
