<?php

namespace App\Http\ApiV1\Modules\Classifiers\Controllers;

use App\Domain\Classifiers\Actions\ReplaceProductFlagAction;
use App\Http\ApiV1\Modules\Classifiers\Queries\ProductFlagsQuery;
use App\Http\ApiV1\Modules\Classifiers\Requests\ReplaceProductFlagRequest;
use App\Http\ApiV1\Modules\Classifiers\Resources\ProductFlagsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductFlagsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductFlagsQuery $query): AnonymousResourceCollection
    {
        return ProductFlagsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ProductFlagsQuery $query): ProductFlagsResource
    {
        return new ProductFlagsResource($query->findOrFail($id));
    }

    public function replace(
        int $id,
        ReplaceProductFlagRequest $request,
        ReplaceProductFlagAction $action
    ): ProductFlagsResource {
        return new ProductFlagsResource($action->execute($id, $request->validated()));
    }
}
