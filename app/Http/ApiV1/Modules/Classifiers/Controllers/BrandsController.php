<?php

namespace App\Http\ApiV1\Modules\Classifiers\Controllers;

use App\Domain\Classifiers\Actions\Brands\CreateBrandAction;
use App\Domain\Classifiers\Actions\Brands\DeleteBrandAction;
use App\Domain\Classifiers\Actions\Brands\DeleteBrandImageAction;
use App\Domain\Classifiers\Actions\Brands\DeleteManyBrandsAction;
use App\Domain\Classifiers\Actions\Brands\PatchBrandAction;
use App\Domain\Classifiers\Actions\Brands\PreloadBrandImageAction;
use App\Domain\Classifiers\Actions\Brands\ReplaceBrandAction;
use App\Domain\Classifiers\Actions\Brands\SaveBrandImageAction;
use App\Http\ApiV1\Modules\Classifiers\Queries\BrandsQuery;
use App\Http\ApiV1\Modules\Classifiers\Requests\CreateBrandRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\PatchBrandRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\PreloadBrandImageRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\ReplaceBrandRequest;
use App\Http\ApiV1\Modules\Classifiers\Requests\UploadBrandImageRequest;
use App\Http\ApiV1\Modules\Classifiers\Resources\BrandsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\PreloadPublicFileResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, BrandsQuery $query): AnonymousResourceCollection
    {
        return BrandsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, BrandsQuery $query): BrandsResource
    {
        return new BrandsResource($query->findOrFail($id));
    }

    public function create(CreateBrandRequest $request, CreateBrandAction $action): BrandsResource
    {
        return new BrandsResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceBrandRequest $request,
        ReplaceBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchBrandRequest $request, PatchBrandAction $action): BrandsResource
    {
        return new BrandsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteBrandAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyBrandsAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    public function preloadImage(PreloadBrandImageRequest $request, PreloadBrandImageAction $action): PreloadPublicFileResource
    {
        return new PreloadPublicFileResource($action->execute($request->file('file')));
    }

    public function uploadImage(int $id, UploadBrandImageRequest $request, SaveBrandImageAction $action): BrandsResource
    {
        return new BrandsResource($action->execute($id, $request->file('file')));
    }

    public function deleteImage(int $id, DeleteBrandImageAction $action): BrandsResource
    {
        return new BrandsResource($action->execute($id));
    }
}
