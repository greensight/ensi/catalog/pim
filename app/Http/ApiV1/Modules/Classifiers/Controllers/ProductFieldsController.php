<?php

namespace App\Http\ApiV1\Modules\Classifiers\Controllers;

use App\Domain\Classifiers\Actions\PatchProductFieldAction;
use App\Http\ApiV1\Modules\Classifiers\Queries\ProductFieldsQuery;
use App\Http\ApiV1\Modules\Classifiers\Requests\PatchProductFieldRequest;
use App\Http\ApiV1\Modules\Classifiers\Resources\ProductFieldsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductFieldsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductFieldsQuery $query): AnonymousResourceCollection
    {
        return ProductFieldsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ProductFieldsQuery $query): ProductFieldsResource
    {
        return new ProductFieldsResource($query->findOrFail($id));
    }

    public function patch(
        int $id,
        PatchProductFieldRequest $request,
        PatchProductFieldAction $action
    ): ProductFieldsResource {
        return new ProductFieldsResource($action->execute($id, $request->validated()));
    }
}
