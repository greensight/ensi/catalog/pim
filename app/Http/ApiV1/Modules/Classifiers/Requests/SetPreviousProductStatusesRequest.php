<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class SetPreviousProductStatusesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array'],
            'ids.*' => ['integer', Rule::exists(ProductStatusSetting::class, 'id')],
        ];
    }

    public function getIds(): array
    {
        return $this->input('ids');
    }
}
