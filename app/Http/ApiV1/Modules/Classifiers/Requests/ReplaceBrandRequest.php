<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceBrandRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:brands'],
            'is_active' => ['required', 'boolean'],
            'code' => ['nullable', 'string', 'unique:brands'],
            'description' => ['nullable', 'string'],
            'logo_url' => ['nullable', 'url'],
            'preload_file_id' => ['nullable', 'integer', 'min:1'],
        ];
    }
}
