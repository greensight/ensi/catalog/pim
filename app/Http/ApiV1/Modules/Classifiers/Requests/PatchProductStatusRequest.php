<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Data\EventConditionsData;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\RequiredIf;

class PatchProductStatusRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            ...BaseFormRequest::nestedRules(
                'events',
                EventConditionsData::rules(),
                ["nullable", new RequiredIf($this->type == ProductStatusTypeEnum::AUTOMATIC->value)]
            ),
            'is_active' => ['boolean'],
            'is_publication' => ['required', 'boolean',
                Rule::when($this->is_publication, [Rule::unique(ProductStatusSetting::class)->ignore($this->getId())]),
            ],
            'type' => ['integer', Rule::enum(ProductStatusTypeEnum::class)],
            'color' => ['nullable', 'string'],
            'name' => ['string'],
            'code' => ['nullable', 'string', Rule::unique(ProductStatusSetting::class)->ignore($this->getId())],
        ];
    }
}
