<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchBrandRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'unique:brands'],
            'is_active' => ['sometimes', 'required', 'boolean'],
            'code' => ['sometimes', 'nullable', 'string', 'unique:brands'],
            'description' => ['sometimes', 'nullable', 'string'],
            'logo_url' => ['sometimes', 'nullable', 'url'],
            'preload_file_id' => ['sometimes', 'integer', 'min:1'],
        ];
    }
}
