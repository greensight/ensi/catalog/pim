<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class NextProductStatusesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'integer', Rule::exists(ProductStatusSetting::class)],
        ];
    }

    public function getStatusId(): ?int
    {
        return $this->input('id');
    }
}
