<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchProductFieldRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'is_moderated' => ['sometimes', 'required', 'boolean'],
            'is_required' => ['sometimes', 'required', 'boolean'],
            'metrics_category' => ['sometimes', 'nullable', new Enum(MetricsCategory::class)],
        ];
    }
}
