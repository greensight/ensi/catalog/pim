<?php

namespace App\Http\ApiV1\Modules\Classifiers\Queries;

use App\Domain\Classifiers\Models\Brand;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BrandsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Brand::query());

        $this->allowedSorts([
            'id',
            'name',
            'code',
            'is_active',
            'created_at',
            'updated_at',
        ]);

        $this->defaultSort('-id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('is_active'),

            ...StringFilter::make('code')->contain(),
            ...StringFilter::make('name')->contain(),
            ...StringFilter::make('description')->contain(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
