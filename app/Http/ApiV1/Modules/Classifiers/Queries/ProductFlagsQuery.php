<?php

namespace App\Http\ApiV1\Modules\Classifiers\Queries;

use App\Domain\Classifiers\Models\ProductFlagSettings;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductFlagsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductFlagSettings::query());

        $this->allowedSorts(['id', 'name']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
        ]);
    }
}
