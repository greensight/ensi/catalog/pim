<?php

namespace App\Http\ApiV1\Modules\Classifiers\Queries;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductStatusesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductStatusSetting::query());

        $this->allowedSorts(['id', 'code', 'name', 'created_at', 'updated_at']);
        $this->defaultSort('id');

        $this->allowedIncludes([
            AllowedInclude::relationship('next_statuses', 'nextStatuses'),
            AllowedInclude::relationship('previous_statuses', 'previousStatuses'),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('name')->contain(),
            ...StringFilter::make('code')->contain(),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('is_active'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
