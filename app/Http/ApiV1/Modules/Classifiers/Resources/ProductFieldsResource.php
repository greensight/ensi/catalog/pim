<?php

namespace App\Http\ApiV1\Modules\Classifiers\Resources;

use App\Domain\Classifiers\Models\ProductFieldSettings;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductFieldSettings
 */
class ProductFieldsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'edit_mask' => $this->edit_mask,

            'name' => $this->name,
            'is_required' => $this->is_required,
            'is_moderated' => $this->is_moderated,
            'metrics_category' => $this->metrics_category,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
