<?php

namespace App\Http\ApiV1\Modules\Classifiers\Resources;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin ProductStatusSetting */
class ProductStatusesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'color' => $this->color,
            'type' => $this->type,
            'is_active' => $this->is_active,
            'is_publication' => $this->is_publication,
            'events' => $this->events,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'next_statuses' => self::collection($this->whenLoaded('nextStatuses')),
            'previous_statuses' => self::collection($this->whenLoaded('previousStatuses')),
        ];
    }
}
