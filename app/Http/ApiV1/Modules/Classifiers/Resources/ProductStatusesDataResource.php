<?php

namespace App\Http\ApiV1\Modules\Classifiers\Resources;

use App\Domain\Classifiers\Actions\ProductStatuses\Data\ProductStatusData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin ProductStatusData */
class ProductStatusesDataResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'available' => $this->available,
        ];
    }
}
