<?php

use App\Domain\Classifiers\Models\ProductFlagSettings;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductFlagEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('classifiers', 'component');

test('PUT /api/v1/classifiers/product-flags/{id} success', function () {
    $newName = 'Изменен';
    $request = ['name' => $newName];
    $id = ProductFlagEnum::NOT_SENT_FOR_MODERATION->value;

    putJson("/api/v1/classifiers/product-flags/$id", $request)
        ->assertOk()
        ->assertJsonPath('data.name', $newName);

    assertDatabaseHas(ProductFlagSettings::class, ['id' => $id, 'name' => $newName]);
});

test('GET /api/v1/classifiers/product-flags/{id} success', function () {
    $id = ProductFlagEnum::MODERATION->value;

    getJson("/api/v1/classifiers/product-flags/$id")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/classifiers/product-flags:search success', function () {
    $request = [
        'filter' => ['name' => 'мастер-данные'],
    ];

    postJson('/api/v1/classifiers/product-flags:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', ProductFlagEnum::REQUIRED_MASTER_DATA->value);
});
