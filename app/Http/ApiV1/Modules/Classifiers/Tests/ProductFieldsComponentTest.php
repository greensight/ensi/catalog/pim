<?php

use App\Domain\Classifiers\Models\ProductFieldSettings;
use App\Http\ApiV1\OpenApiGenerated\Enums\MetricsCategoryEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('classifiers', 'component');

test('POST /api/v1/classifiers/product-fields:search 200', function () {
    $request = [
        'filter' => ['name' => 'название'],
    ];
    postJson('/api/v1/classifiers/product-fields:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.code', 'name')
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'is_required', 'is_moderated']]]);
});

test('GET /api/v1/classifiers/product-fields/{id} 200', function () {
    $id = ProductFieldSettings::findByCode('width')->id;

    getJson("/api/v1/classifiers/product-fields/{$id}")
        ->assertStatus(200)
        ->assertJsonPath('data.code', 'width');
});

test('GET /api/v1/classifiers/product-fields/{id} 404', function () {
    getJson('/api/v1/classifiers/product-fields/100000')
        ->assertStatus(404);
});

test('PATCH /api/v1/classifiers/product-fields/{id} 200', function () {
    $id = ProductFieldSettings::findByCode('length')->id;

    $request = [
        'name' => 'foo',
        'is_required' => true,
        'is_moderated' => true,
    ];

    patchJson("/api/v1/classifiers/product-fields/{$id}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.name', 'foo');

    assertDatabaseHas(
        ProductFieldSettings::class,
        ['id' => $id, 'name' => 'foo', 'is_required' => true, 'is_moderated' => true]
    );
});

test('PATCH /api/v1/classifiers/product-fields/{id} ignores not editable attributes', function () {
    $id = ProductFieldSettings::findByCode('name')->id;

    $request = [
        'is_required' => false,
        'metrics_category' => MetricsCategoryEnum::ATTRIBUTES->value,
    ];

    patchJson("/api/v1/classifiers/product-fields/{$id}", $request)
        ->assertStatus(200);

    assertDatabaseHas(
        ProductFieldSettings::class,
        ['id' => $id, 'is_required' => true, 'metrics_category' => MetricsCategoryEnum::MASTER->value]
    );
});

test('PATCH /api/v1/classifiers/product-fields/{id} 400', function () {
    $id = ProductFieldSettings::findByCode('length')->id;

    $request = [
        'name' => '',
        'is_required' => null,
        'is_moderated' => '',
        'metrics_category' => 'none',
    ];

    $this->skipNextOpenApiRequestValidation()
        ->patchJson("/api/v1/classifiers/product-fields/{$id}", $request)
        ->assertStatus(400)
        ->assertJsonCount(4, 'errors');
});

test('PATCH /api/v1/classifiers/product-fields/{id} 404', function () {
    patchJson('/api/v1/classifiers/product-fields/100000', ['name' => 'foo'])
        ->assertStatus(404);
});
