<?php

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Support\Events\FileReleased;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Modules\Classifiers\Tests\Factories\BrandRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('classifiers', 'component');

test('POST /api/v1/classifiers/brands success', function () {
    $imagePath = EnsiFile::factory()->fileName('image_1024')
        ->fileExt('png')
        ->make()['path'];

    $request = BrandRequestFactory::new()->make([
        'preload_file_id' => TempFile::create(['path' => $imagePath])->id,
    ]);

    $id = postJson('/api/v1/classifiers/brands', $request)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'code', 'name', 'logo_url']])
        ->json('data.id');

    assertDatabaseHas(
        (new Brand())->getTable(),
        ['id' => $id, 'name' => $request['name']]
    );
});

test('PUT /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->create();
    $imagePath = EnsiFile::factory()->fileName('image_1024')
        ->fileExt('png')
        ->make()['path'];

    $request = BrandRequestFactory::new()->make([
        'preload_file_id' => TempFile::create(['path' => $imagePath])->id,
    ]);

    putJson("/api/v1/classifiers/brands/{$brand->id}", $request)
        ->assertOk()
        ->assertJsonFragment([
            'code' => $request['code'],
            'name' => $request['name'],
            'is_active' => $request['is_active'],
        ]);
});

test('PATCH /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->create();
    $request = ['name' => 'foo', 'logo_url' => 'https://images.com?img=12'];

    patchJson("/api/v1/classifiers/brands/{$brand->id}", $request)
        ->assertOk()
        ->assertJsonFragment(array_filter($request))
        ->assertJsonPath('data.logo_file', null);

    assertDatabaseHas(
        $brand->getTable(),
        ['id' => $brand->id, 'name' => 'foo', 'logo_file' => null, 'logo_url' => 'https://images.com?img=12']
    );
});

test('DELETE /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->create();

    deleteJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertOk();

    assertModelMissing($brand);
});

test('DELETE /api/v1/classifiers/brands/{id} with existing product', function () {
    $brand = Brand::factory()->create();
    Product::factory()->create(['brand_id' => $brand->id]);

    deleteJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertStatus(400);
});

test('DELETE /api/v1/classifiers/brands/{id} with existing published product', function () {
    $brand = Brand::factory()->create();
    $product = Product::factory()->create();

    $published = PublishedProduct::findOrFail($product->id);
    $published->brand_id = $brand->id;
    $published->save();

    deleteJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertStatus(400);
});

test('GET /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->create();

    getJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $brand->id);
});

test('POST /api/v1/classifiers/brands/{id}:upload-image success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $brand = Brand::factory()->withExternalLogo()->create();
    $file = UploadedFile::fake()->create('123.png', kilobytes: 20);

    postFile("/api/v1/classifiers/brands/{$brand->id}:upload-image", $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'logo_file' => ['url']]])
        ->assertJsonPath('logo_url', null);
});

test('POST /api/v1/classifiers/brands/{id}:delete-image success', function () {
    $brand = Brand::factory()->create();
    Event::fake(FileReleased::class);

    postJson("/api/v1/classifiers/brands/{$brand->id}:delete-image")
        ->assertOk()
        ->assertJsonPath('data.logo_file', null);

    Event::assertDispatched(FileReleased::class);
});

test('POST /api/v1/classifiers/brands/{id}:delete-image ignores external logo', function () {
    $brand = Brand::factory()->withExternalLogo()->create();

    postJson("/api/v1/classifiers/brands/{$brand->id}:delete-image")
        ->assertOk()
        ->assertJsonPath('data.logo_url', $brand->logo_url);
});

test('POST /api/v1/classifiers/brands:preload-image success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $file = UploadedFile::fake()->create('file.png', kilobytes: 20);

    postFile('/api/v1/classifiers/brands:preload-image', $file)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['preload_file_id', 'file']]);
});

test("POST /api/v1/classifiers/brands:search filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
) {
    /** @var Brand $brand */
    $brand = Brand::factory()->create($value ? [$fieldKey => $value] : []);
    Brand::factory()->create();

    postJson("/api/v1/classifiers/brands:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $brand->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $brand->id);
})->with([
    ['id', null, null, null],
    ['is_active', true, null, null],
    ['code', "123", 'code_like', "2"],
    ['name', "123", 'name_like', "2"],
    ['description', "123", 'description_like', "2"],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/classifiers/brands:search sort success", function (string $sort) {
    Brand::factory()->create();
    postJson("/api/v1/classifiers/brands:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'name', 'code', 'is_active', 'created_at', 'updated_at',
]);
