<?php

use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Modules\Classifiers\Tests\Factories\ProductStatusRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Arr;
use Illuminate\Testing\Assert;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/classifiers/product-statuses:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productStatuses = ProductStatusSetting::factory()
        ->count(10)
        ->sequence(
            ['type' => ProductStatusTypeEnum::AUTOMATIC->value],
            ['type' => ProductStatusTypeEnum::MANUAL->value],
        )
        ->create();
    postJson('/api/v1/classifiers/product-statuses:search', [
        "filter" => ["type" => ProductStatusTypeEnum::MANUAL->value],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $productStatuses->last()->id)
        ->assertJsonPath('data.0.type', ProductStatusTypeEnum::MANUAL->value);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/classifiers/product-statuses:search filter success', function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    /** @var ProductStatusSetting $productStatus */
    $productStatus = ProductStatusSetting::factory()->create($value ? [$fieldKey => $value] : []);
    ProductStatusSetting::factory()->create();

    postJson('/api/v1/classifiers/product-statuses:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $productStatus->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $productStatus->id);
})->with([
    ['id', null, null, null],
    ['name', 'Foo', 'name_like', 'o'],
    ['code', 'Foo', 'code_like', 'o'],
    ['type', ProductStatusTypeEnum::AUTOMATIC->value, 'type', [ProductStatusTypeEnum::AUTOMATIC->value]],
    ['is_active', true, 'is_active', true],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/classifiers/product-statuses:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    ProductStatusSetting::factory()->create();
    postJson("/api/v1/classifiers/product-statuses:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'code', 'name', 'created_at', 'updated_at',
])->with(FakerProvider::$optionalDataset);

test("POST /api/v1/classifiers/product-statuses:search include success", function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var ProductStatusSetting $currentStatus */
    $currentStatus = ProductStatusSetting::factory()->create();

    /** @var ProductStatusSetting $nextStatus */
    $nextStatus = ProductStatusSetting::factory()->create();
    /** @var ProductStatusSetting $previousStatus */
    $previousStatus = ProductStatusSetting::factory()->create();

    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();
    ProductStatusLink::factory()->withStatuses($previousStatus, $currentStatus)->create();

    postJson("/api/v1/classifiers/product-statuses:search", ["filter" => ["id" => $currentStatus->id], "include" => [
        'next_statuses', 'previous_statuses',
    ]])->assertStatus(200)
        ->assertJsonPath('data.0.next_statuses.0.id', $nextStatus->id)
        ->assertJsonCount(1, 'data.0.next_statuses')
        ->assertJsonPath('data.0.previous_statuses.0.id', $previousStatus->id)
        ->assertJsonCount(1, 'data.0.previous_statuses');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/classifiers/product-statuses 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductStatusRequestFactory::new()->make();

    $id = postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(ProductStatusSetting::class, ['id' => $id, 'code' => $request['code']]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/classifiers/product-statuses some statuses with is_publication=false', function () {
    $request = ProductStatusRequestFactory::new()->make(['code' => null, 'is_publication' => false]);

    postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(201);
    postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(201);
});

test('POST /api/v1/classifiers/product-statuses only one status with is_publication=true', function () {
    $request = ProductStatusRequestFactory::new()->make(['code' => null, 'is_publication' => true]);

    postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(201);
    postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(400);
});

test('POST /api/v1/classifiers/product-statuses creation with the same names 201', function () {
    $name = 'name';
    /** @var ProductStatusSetting $productStatusSetting */
    $productStatusSetting = ProductStatusSetting::factory()->create(['name' => $name, 'code' => null]);

    $request = ProductStatusRequestFactory::new()->make(['name' => $name, 'code' => null]);

    $id = postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(201)
        ->json('data.id');

    /** @var ProductStatusSetting $newProductStatusSetting */
    $newProductStatusSetting = ProductStatusSetting::query()->find($id);

    Assert::assertEquals($productStatusSetting->name, $productStatusSetting->code);
    Assert::assertNotNull($newProductStatusSetting);
    Assert::assertNotNull($newProductStatusSetting->code);
});

test('POST /api/v1/classifiers/product-statuses 400', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->skipNextOpenApiRequestValidation();

    $request = ProductStatusRequestFactory::new()->make([
        'type' => ProductStatusTypeEnum::AUTOMATIC->value,
        'events' => null,
    ]);

    postJson('/api/v1/classifiers/product-statuses', $request)
        ->assertStatus(400);
});

test('GET /api/v1/classifiers/product-statuses/{id} 200', function () {
    /** @var ProductStatusSetting $productStatusSetting */
    $productStatusSetting = ProductStatusSetting::factory()->create();

    getJson("/api/v1/classifiers/product-statuses/{$productStatusSetting->id}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $productStatusSetting->id);
});

test('GET /api/v1/classifiers/product-statuses/{id} 404', function () {
    getJson('/api/v1/classifiers/product-statuses/404')
        ->assertStatus(404);
});

test('DELETE /api/v1/classifiers/product-statuses/{id} 200', function () {
    /** @var ProductStatusSetting $productStatusSetting */
    $productStatusSetting = ProductStatusSetting::factory()->create();

    deleteJson("/api/v1/classifiers/product-statuses/{$productStatusSetting->id}")
        ->assertStatus(200);

    assertModelMissing($productStatusSetting);
});

test('DELETE /api/v1/classifiers/product-statuses/{id} 400', function () {
    /** @var ProductStatusSetting $productStatusSetting */
    $productStatusSetting = ProductStatusSetting::factory()->create();
    Product::factory()->inStatus($productStatusSetting)->create();

    deleteJson("/api/v1/classifiers/product-statuses/{$productStatusSetting->id}")
        ->assertStatus(400);

    assertModelExists($productStatusSetting);
});

test('PATCH /api/v1/classifiers/product-statuses/{id} 200', function () {
    /** @var ProductStatusSetting $productStatusSetting */
    $productStatusSetting = ProductStatusSetting::factory()->create();

    $request = ProductStatusRequestFactory::new()->make();

    patchJson("/api/v1/classifiers/product-statuses/{$productStatusSetting->id}", $request)
        ->assertStatus(200);
});

test('PATCH /api/v1/classifiers/product-statuses/{id} 400', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->skipNextOpenApiRequestValidation();

    $request = ProductStatusRequestFactory::new()->make([
        'type' => ProductStatusTypeEnum::AUTOMATIC->value,
        'events' => null,
    ]);

    patchJson('/api/v1/classifiers/product-statuses/400', $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/classifiers/product-statuses/{id} 404', function () {
    $request = ProductStatusRequestFactory::new()->make();
    patchJson('/api/v1/classifiers/product-statuses/404', $request)
        ->assertStatus(404);
});

test('POST /api/v1/classifiers/product-statuses:next 200 next status', function () {
    /** @var ProductStatusSetting $currentStatus */
    $currentStatus = ProductStatusSetting::factory()->activeManual()->create();

    /** @var ProductStatusSetting $nextStatus */
    $nextStatus = ProductStatusSetting::factory()->activeManual()->create();
    /** @var ProductStatusSetting $previousStatus */
    $previousStatus = ProductStatusSetting::factory()->activeManual()->create();

    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();
    ProductStatusLink::factory()->withStatuses($previousStatus, $currentStatus)->create();

    $data = postJson("/api/v1/classifiers/product-statuses:next", ["id" => $currentStatus->id])
        ->assertStatus(200)
        ->json('data');

    $availableNextStatuses = Arr::pluck($data, 'available', 'id');

    Assert::assertTrue($availableNextStatuses[$nextStatus->id]);
    Assert::assertFalse($availableNextStatuses[$previousStatus->id]);
});

test('POST /api/v1/classifiers/product-statuses:next 200 first status', function () {
    /** @var ProductStatusSetting $currentStatus */
    $linkStatus = ProductStatusSetting::factory()->activeManual()->create();

    /** @var ProductStatusSetting $nextStatus */
    $firstStatus = ProductStatusSetting::factory()->activeManual()->create();
    /** @var ProductStatusSetting $previousStatus */
    $statusWithLink = ProductStatusSetting::factory()->activeManual()->create();

    ProductStatusLink::factory()->withStatuses($linkStatus, $statusWithLink)->create();

    $data = postJson("/api/v1/classifiers/product-statuses:next", ["id" => null])
        ->assertStatus(200)
        ->json('data');

    $availableNextStatuses = Arr::pluck($data, 'available', 'id');

    Assert::assertTrue($availableNextStatuses[$firstStatus->id]);
    Assert::assertFalse($availableNextStatuses[$statusWithLink->id]);
});

test('POST /api/v1/classifiers/product-statuses/{id}:set-previous 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    /** @var ProductStatusSetting $currentStatus */
    $currentStatus = ProductStatusSetting::factory()->create();

    /** @var ProductStatusSetting $previousStatus */
    $previousStatus = ProductStatusSetting::factory()->create();
    /** @var ProductStatusSetting $deletePreviousStatus */
    $deletePreviousStatus = ProductStatusSetting::factory()->create();
    /** @var ProductStatusSetting $futurePreviousStatus */
    $futurePreviousStatus = ProductStatusSetting::factory()->create();

    /** @var ProductStatusSetting $nextStatus */
    $nextStatus = ProductStatusSetting::factory()->create();

    /** @var ProductStatusLink $linkWithPrevious */
    $linkWithPrevious = ProductStatusLink::factory()->withStatuses($previousStatus, $currentStatus)->create();
    /** @var ProductStatusLink $linkWithDeletePrevious */
    $linkWithDeletePrevious = ProductStatusLink::factory()->withStatuses($deletePreviousStatus, $currentStatus)->create();
    /** @var ProductStatusLink $linkWithNext */
    $linkWithNext = ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $previousStatusIds = [$previousStatus->id, $futurePreviousStatus->id];

    $currentPreviousStatuses = postJson(
        "/api/v1/classifiers/product-statuses/$currentStatus->id:set-previous",
        ['ids' => $previousStatusIds]
    )
        ->assertStatus(200)
        ->assertJsonPath('data.id', $currentStatus->id)
        ->json('data.previous_statuses');

    Assert::assertEquals(Arr::pluck($currentPreviousStatuses, 'id'), $previousStatusIds);

    assertModelExists($linkWithPrevious);
    assertDatabaseHas(ProductStatusLink::class, ['last_status_id' => $futurePreviousStatus->id, 'new_status_id' => $currentStatus->id]);
    assertModelMissing($linkWithDeletePrevious);
    assertModelExists($deletePreviousStatus);

    assertModelExists($linkWithNext);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/classifiers/product-statuses/{id}:set-previous 400', function () {
    postJson('/api/v1/classifiers/product-statuses/400:set-previous', ['ids' => [400]])
        ->assertStatus(400);
});

test('POST /api/v1/classifiers/product-statuses/{id}:set-previous 404', function () {
    /** @var ProductStatusSetting $productStatus */
    $productStatus = ProductStatusSetting::factory()->activeManual()->create();

    postJson('/api/v1/classifiers/product-statuses/404:set-previous', ['ids' => [$productStatus->id]])
        ->assertStatus(404);
});
