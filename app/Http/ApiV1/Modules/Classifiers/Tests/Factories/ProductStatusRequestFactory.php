<?php

namespace App\Http\ApiV1\Modules\Classifiers\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\EventOperationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductStatusRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $type = $this->faker->randomEnum(ProductStatusTypeEnum::cases());
        $isAutomatic = $type == ProductStatusTypeEnum::AUTOMATIC->value;

        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug(),
            'color' => $this->faker->nullable()->hexColor(),
            'type' => $type,
            'is_active' => $this->faker->boolean(),
            'is_publication' => false,
            'events' => $this->faker->nullable($isAutomatic)->exactly([
                'operation' => $this->faker->nullable()->randomEnum(EventOperationEnum::cases()),
                'events' => $this->faker->randomList(fn () => $this->faker->randomEnum(ProductEventEnum::cases()), 1, 3),
            ]),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
