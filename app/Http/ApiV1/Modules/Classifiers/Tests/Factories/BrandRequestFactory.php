<?php

namespace App\Http\ApiV1\Modules\Classifiers\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class BrandRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'code' => $this->faker->unique()->text,
            'name' => $this->faker->unique()->company,
            'is_active' => true,
            'description' => $this->faker->optional()->sentence,
            'logo_url' => $this->faker->optional()->url,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
