<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Http\ApiV1\Modules\Common\Queries\BulkOperationsQuery;
use App\Http\ApiV1\Modules\Common\Resources\BulkOperationsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BulkOperationsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, BulkOperationsQuery $query): AnonymousResourceCollection
    {
        return BulkOperationsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
