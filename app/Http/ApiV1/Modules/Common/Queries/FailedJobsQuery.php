<?php

namespace App\Http\ApiV1\Modules\Common\Queries;

use App\Domain\Common\Models\FailedJob;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class FailedJobsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(FailedJob::query());

        $this->allowedSorts(['id', 'connection', 'queue', 'failed_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('uuid'),
            AllowedFilter::exact('connection'),
            AllowedFilter::exact('queue'),
            ...StringFilter::make('payload')->contain(),
            ...StringFilter::make('exception')->contain(),
            ...DateFilter::make('failed_at')->lte()->gte(),
        ]);
    }
}
