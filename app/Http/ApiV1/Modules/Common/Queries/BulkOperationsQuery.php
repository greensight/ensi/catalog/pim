<?php

namespace App\Http\ApiV1\Modules\Common\Queries;

use App\Domain\Common\Models\BulkOperation;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BulkOperationsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(BulkOperation::query());

        $this->allowedSorts([
            'id',
            'created_at',
            'updated_at',
        ]);

        $this->defaultSort('-id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('action'),
            AllowedFilter::exact('entity'),
            AllowedFilter::exact('status'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->allowedIncludes(['errors']);
    }
}
