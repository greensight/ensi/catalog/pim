<?php

namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Domain\Common\Models\BulkOperationError;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BulkOperationError */
class BulkOperationErrorsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'operation_id' => $this->operation_id,
            'entity_id' => $this->entity_id,
            'message' => $this->message,
        ];
    }
}
