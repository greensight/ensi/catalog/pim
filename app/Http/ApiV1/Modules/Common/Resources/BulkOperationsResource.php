<?php

namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Domain\Common\Models\BulkOperation;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BulkOperation */
class BulkOperationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'action' => $this->action,
            'entity' => $this->entity,
            'ids' => $this->ids,
            'status' => $this->status,
            'error_message' => $this->error_message,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'errors' => BulkOperationErrorsResource::collection($this->whenLoaded('errors')),
        ];
    }
}
