<?php

use App\Domain\Common\Models\BulkOperation;
use App\Domain\Common\Models\BulkOperationError;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationActionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationEntityEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'common');

test('POST /api/v1/common/bulk-operations:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $operation = BulkOperation::factory()->count(2)->create()->last();
    $request = ['filter' => ['id' => $operation->id]];

    postJson('/api/v1/common/bulk-operations:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment(['id' => $operation->id, 'status' => $operation->status]);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/common/bulk-operations:search filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
) {
    /** @var BulkOperation $bulkOperation */
    $bulkOperation = BulkOperation::factory()->create($value ? [$fieldKey => $value] : []);
    BulkOperation::factory()->create();

    postJson("/api/v1/common/bulk-operations:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $bulkOperation->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $bulkOperation->id);
})->with([
    ['id', null, null, null],
    ['action', BulkOperationActionTypeEnum::PATCH->value, null, null],
    ['entity', BulkOperationEntityEnum::PRODUCT->value, null, null],
    ['status', BulkOperationStatusEnum::NEW->value, null, null],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/common/bulk-operations:search sort success", function (string $sort) {
    BulkOperation::factory()->create();
    postJson("/api/v1/common/bulk-operations:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'created_at', 'updated_at',
]);

test('POST /api/v1/common/bulk-operations:search include errors 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var BulkOperation $operation */
    $operation = BulkOperation::factory()
        ->has(BulkOperationError::factory(), 'errors')
        ->createOne();
    /** @var BulkOperationError $error */
    $error = $operation->errors->first();
    $request = ['include' => ['errors']];

    postJson('/api/v1/common/bulk-operations:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $operation->id)
        ->assertJsonCount(1, 'data.0.errors')
        ->assertJsonPath('data.0.errors.0.entity_id', $error->entity_id);
})->with(FakerProvider::$optionalDataset);
