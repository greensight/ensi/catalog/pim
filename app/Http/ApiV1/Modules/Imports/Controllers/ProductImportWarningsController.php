<?php

namespace App\Http\ApiV1\Modules\Imports\Controllers;

use App\Http\ApiV1\Modules\Imports\Queries\ProductImportWarningsQuery;
use App\Http\ApiV1\Modules\Imports\Resources\ProductImportWarningsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ProductImportWarningsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductImportWarningsQuery $query): Responsable
    {
        return ProductImportWarningsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build(),
        );
    }
}
