<?php

namespace App\Http\ApiV1\Modules\Imports\Controllers;

use App\Domain\Imports\Actions\CreateProductImportAction;
use App\Domain\Imports\Actions\PreloadImportFileAction;
use App\Domain\Imports\Actions\StartProductImportAction;
use App\Http\ApiV1\Modules\Imports\Queries\ProductImportsQuery;
use App\Http\ApiV1\Modules\Imports\Requests\CreateProductImportRequest;
use App\Http\ApiV1\Modules\Imports\Requests\PreloadImportFileRequest;
use App\Http\ApiV1\Modules\Imports\Resources\ProductImportsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\PreloadProtectedFileResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductImportsController
{
    public function create(
        CreateProductImportRequest $request,
        CreateProductImportAction $createImportAction,
        StartProductImportAction $importAction,
    ): ProductImportsResource {
        $import = $createImportAction->execute(
            $request->getType(),
            $request->getPreloadFileId()
        );

        $importAction->onQueue()->execute($import->id);

        return new ProductImportsResource($import);
    }

    public function get(int $id, ProductImportsQuery $query): ProductImportsResource
    {
        return new ProductImportsResource($query->findOrFail($id));
    }

    public function search(
        PageBuilderFactory $pageBuilderFactory,
        ProductImportsQuery $query
    ): AnonymousResourceCollection {
        return ProductImportsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build(),
        );
    }

    public function preloadFile(
        PreloadImportFileRequest $request,
        PreloadImportFileAction $action
    ): PreloadProtectedFileResource {
        return new PreloadProtectedFileResource($action->execute($request->uploadedFile()));
    }
}
