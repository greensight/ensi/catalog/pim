<?php

namespace App\Http\ApiV1\Modules\Imports\Requests;

use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateProductImportRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'preload_file_id' => ['required', Rule::exists(TempFile::class, 'id')],
            'type' => ['required', Rule::enum(ProductImportTypeEnum::class)],
        ];
    }

    public function getPreloadFileId(): int
    {
        return $this->integer('preload_file_id');
    }

    public function getType(): ProductImportTypeEnum
    {
        return ProductImportTypeEnum::from($this->integer('type'));
    }
}
