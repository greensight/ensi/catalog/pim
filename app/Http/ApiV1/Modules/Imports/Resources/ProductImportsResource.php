<?php

namespace App\Http\ApiV1\Modules\Imports\Resources;

use App\Domain\Imports\Models\ProductImport;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin ProductImport */
class ProductImportsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'type' => $this->type,
            'status' => $this->status,
            'file' => $this->mapProtectedFileToResponse($this->file),
            'chunks_count' => $this->chunks_count,
            'chunks_finished' => $this->chunks_finished,

            'warnings' => ProductImportWarningsResource::collection($this->whenLoaded('warnings')),
        ];
    }
}
