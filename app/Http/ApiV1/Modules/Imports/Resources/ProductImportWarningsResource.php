<?php

namespace App\Http\ApiV1\Modules\Imports\Resources;

use App\Domain\Imports\Models\ProductImportWarning;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin ProductImportWarning */
class ProductImportWarningsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'import_id' => $this->import_id,
            'vendor_code' => $this->vendor_code,
            'import_type' => $this->import_type,
            'message' => $this->message,

            'import' => ProductImportsResource::make($this->whenLoaded('import')),
        ];
    }
}
