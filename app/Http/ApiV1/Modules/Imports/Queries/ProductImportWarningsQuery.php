<?php

namespace App\Http\ApiV1\Modules\Imports\Queries;

use App\Domain\Imports\Models\ProductImportWarning;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductImportWarningsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductImportWarning::query());

        $this->allowedSorts([
            'id',
            'import_type',
            'vendor_code',
            'message',
            'created_at',
        ]);
        $this->defaultSort('-id');

        $this->allowedFilters([
            AllowedFilter::exact('import_id'),
            AllowedFilter::exact('import_type'),

            ...StringFilter::make('vendor_code')->contain(),

            ...DateFilter::make('created_at')->exact()->lte()->gte(),
            ...DateFilter::make('updated_at')->exact()->lte()->gte(),
        ]);

        $this->allowedIncludes(['import']);
    }
}
