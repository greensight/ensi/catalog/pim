<?php

namespace App\Http\ApiV1\Modules\Imports\Queries;

use App\Domain\Imports\Models\ProductImport;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductImportsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductImport::query());

        $this->allowedSorts([
            'id',
            'created_at',
            'updated_at',
            'status',
            'type',
        ]);
        $this->defaultSort('-id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('status'),

            ...DateFilter::make('created_at')->exact()->lte()->gte(),
            ...DateFilter::make('updated_at')->exact()->lte()->gte(),
        ]);

        $this->allowedIncludes(['warnings']);
    }
}
