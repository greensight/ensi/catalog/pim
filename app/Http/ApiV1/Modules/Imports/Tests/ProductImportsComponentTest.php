<?php

use App\Domain\Imports\Models\ProductImport;
use App\Domain\Imports\Models\ProductImportWarning;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelTestFactories\PaginationFactory;

use Illuminate\Http\UploadedFile;

use function Pest\Laravel\assertDatabaseHas;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

use Symfony\Component\HttpFoundation\Response;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'product-imports');

test('POST /api/v1/imports/products:search 200', function () {
    $imports = ProductImport::factory()
        ->count(3)
        ->create(['type' => ProductImportTypeEnum::PRODUCT]);

    postJson('/api/v1/imports/products:search', [
        'filter' => ['type' => ProductImportTypeEnum::PRODUCT],
        'sort' => ['-id'],
    ])
        ->assertStatus(200)
        ->assertJsonCount(3, 'data')
        ->assertJsonPath('data.0.id', $imports->last()->id)
        ->assertJsonPath('data.0.type', ProductImportTypeEnum::PRODUCT->value);
});

test('POST /api/v1/imports/products:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    $import = ProductImport::factory()->createOne([$fieldKey => $value]);

    postJson('/api/v1/imports/products:search', [
        'filter' => [
            ($filterKey ?: $fieldKey) => ($filterValue ?: $import->{$fieldKey}),
        ],
        'sort' => ['id'],
        'pagination' => PaginationFactory::new()->makeRequestOffset(['offset' => 0]),
    ])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $import->id);
})->with([
    ['id', 12],
    ['type', ProductImportTypeEnum::PRODUCT],
    ['status', ProductImportStatusEnum::NEW],
    ['created_at', now(), 'created_at_gte', now()->subDay()->toIso8601String()],
    ['created_at', now(), 'created_at_lte', now()->addDay()->toIso8601String()],
    ['updated_at', now(), 'updated_at_gte', now()->toIso8601String()],
    ['updated_at', now(), 'updated_at_lte', now()->addDay()->toIso8601String()],
]);

test('POST /api/v1/imports/products:search sort success', function (string $sort) {
    ProductImport::factory()->create();
    postJson('/api/v1/imports/products:search', ['sort' => [$sort]])->assertStatus(200);
})->with([
    'id',
    'status',
    'type',
    'created_at',
    'updated_at',
]);

test('POST /api/v1/imports/products:search include success', function () {
    $import = ProductImport::factory()->createOne();
    $warning = ProductImportWarning::factory()
        ->for($import, 'import')
        ->count(3)
        ->create();


    postJson('/api/v1/imports/products:search', ['include' => [
        'warnings',
    ]])
        ->assertStatus(200)
        ->assertJsonCount($warning->count(), 'data.0.warnings');
});

test('GET /api/v1/imports/products/{id} 200', function () {
    $import = ProductImport::factory()->createOne();

    getJson("/api/v1/imports/products/{$import->id}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $import->id);
});

test('GET /api/v1/imports/products/{id} 404', function () {
    getJson('/api/v1/imports/products/123456')
        ->assertStatus(404);
});

test('POST /api/v1/imports/products:preload-file success', function () {
    $diskName = resolve(EnsiFilesystemManager::class)->protectedDiskName();
    Storage::fake($diskName);

    $file = UploadedFile::fake()->create('file.csv', kilobytes: 20);

    $fileId = postFile('/api/v1/imports/products:preload-file', $file)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'preload_file_id',
                'file' => ['path', 'root_path', 'url'],
            ],
        ])
        ->json('data.preload_file_id');

    assertDatabaseHas(TempFile::class, ['id' => $fileId]);

    $tempFile = TempFile::find($fileId);

    $disk = Storage::disk($diskName);
    $disk->assertExists($tempFile->path);
});

test('POST /api/v1/imports/products:preload-file validates mimetypes', function (string $extension, bool $success) {
    $diskName = resolve(EnsiFilesystemManager::class)->protectedDiskName();
    Storage::fake($diskName);

    $file = UploadedFile::fake()->create("file.$extension", kilobytes: 20);
    $status = $success ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST;

    postFile('/api/v1/imports/products:preload-file', $file)->assertStatus($status);
})->with([
    ['csv', true],
    ['xls', true],
    ['xlsx', true],
    ['pdf', false],
    ['jpeg', false],
    ['mp3', false],
]);
