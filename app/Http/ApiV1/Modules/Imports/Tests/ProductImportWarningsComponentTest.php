<?php

use App\Domain\Imports\Models\ProductImport;
use App\Domain\Imports\Models\ProductImportWarning;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'product-import-warnings');

test('POST /api/v1/imports/product-warnings:search 200', function () {
    $filteredVendorCode = 'BAC-101200';

    ProductImportWarning::factory()->createMany([
        ['vendor_code' => $filteredVendorCode],
        ['vendor_code' => 'TAS-102344'],
    ]);

    postJson('/api/v1/imports/product-warnings:search', [
        'filter' => ['vendor_code_like' => $filteredVendorCode],
    ])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data');
});

test('POST /api/v1/imports/product-warnings:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    $warning = ProductImportWarning::factory()->createOne([$fieldKey => $value]);

    postJson('/api/v1/imports/product-warnings:search', [
        'filter' => [
            ($filterKey ?: $fieldKey) => ($filterValue ?: $warning->{$fieldKey}),
        ],
        'sort' => ['id'],
        'pagination' => PaginationFactory::new()->makeRequestOffset(['offset' => 0]),
    ])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $warning->id);
})->with([
    ['import_id', fn () => ProductImport::factory()->createOne()->id],
    ['import_type', ProductImportTypeEnum::PRODUCT],
    ['vendor_code', 'BAC-107889', 'vendor_code_like', '107889'],
    ['created_at', now(), 'created_at_gte', now()->subDay()->toIso8601String()],
    ['created_at', now(), 'created_at_lte', now()->addDay()->toIso8601String()],
]);

test('POST /api/v1/imports/product-warnings:search sort success', function (string $sort) {
    ProductImportWarning::factory()->create();
    postJson('/api/v1/imports/product-warnings:search', ['sort' => [$sort]])->assertStatus(200);
})->with([
    'id',
    'vendor_code',
    'message',
    'import_type',
    'created_at',
]);

test('POST /api/v1/imports/product-warnings:search include success', function () {
    $import = ProductImport::factory()->createOne();
    ProductImportWarning::factory()
        ->for($import, 'import')
        ->count(3)
        ->create();


    postJson('/api/v1/imports/product-warnings:search', ['include' => [
        'import',
    ]])
        ->assertStatus(200)
        ->assertJsonStructure(['data' => [['import']]]);
});
