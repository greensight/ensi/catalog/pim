<?php

namespace App\Domain\Categories\Events;

use Illuminate\Foundation\Events\Dispatchable;

class PropertyInvalidated
{
    use Dispatchable;

    public function __construct(public int $propertyId)
    {
    }
}
