<?php

namespace App\Domain\Categories\Events;

use Illuminate\Foundation\Events\Dispatchable;

class CategoryInvalidated
{
    use Dispatchable;

    public function __construct(public int $categoryId)
    {
    }
}
