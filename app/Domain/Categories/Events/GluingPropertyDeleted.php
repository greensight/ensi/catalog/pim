<?php

namespace App\Domain\Categories\Events;

use App\Domain\Categories\Models\CategoryPropertyLink;
use Illuminate\Foundation\Events\Dispatchable;

class GluingPropertyDeleted
{
    use Dispatchable;

    public function __construct(public CategoryPropertyLink $link)
    {
    }
}
