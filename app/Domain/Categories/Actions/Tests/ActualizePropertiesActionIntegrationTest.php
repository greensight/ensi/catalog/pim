<?php

use App\Domain\Categories\Actions\Categories\ActualizePropertiesAction;
use App\Domain\Categories\Core\PropertiesResolver;
use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;

use Mockery\MockInterface;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\travel;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

function executeActualizePropertiesAction(Category $category, array $resolvedProperties = []): int
{
    /** @var PropertiesResolver|MockInterface $resolver */
    $resolver = Mockery::mock(PropertiesResolver::class, ['resolve' => collect($resolvedProperties)]);

    return (new ActualizePropertiesAction($resolver))->execute($category);
}

test('delete irrelevant properties', function () {
    $existing = ActualCategoryProperty::factory()->createOne();

    expect(executeActualizePropertiesAction($existing->category))->toBe(1);

    assertModelMissing($existing);
});

test('add missing properties', function () {
    $category = Category::factory()->createOne();
    $property = Property::factory()->createOne();
    $resolvedProperties = [CategoryPropertySample::own($property->id, true)];

    expect(executeActualizePropertiesAction($category, $resolvedProperties))->toBe(1);

    assertDatabaseHas(
        ActualCategoryProperty::class,
        ['category_id' => $category->id, 'property_id' => $property->id, 'is_required' => true]
    );
});

test('change existing properties', function () {
    $existing = ActualCategoryProperty::factory()->inherited()->createOne();
    $resolvedProperties = [CategoryPropertySample::own($existing->property_id, $existing->is_required)];

    expect(executeActualizePropertiesAction($existing->category, $resolvedProperties))->toBe(1);

    assertDatabaseHas($existing->getTable(), ['id' => $existing->id, 'is_inherited' => false]);
});

test('ignore unmodified properties', function () {
    $category = Category::factory()->createOne();
    $existing = ActualCategoryProperty::factory()->forCategory($category)->createOne();
    $resolvedProperties = [CategoryPropertySample::own($existing->property_id, $existing->is_required)];

    travel(1)->minutes();
    expect(executeActualizePropertiesAction($existing->category, $resolvedProperties))->toBe(0);

    assertDatabaseHas($existing->getTable(), ['id' => $existing->id, 'updated_at' => $existing->updated_at]);
});
