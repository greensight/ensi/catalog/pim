<?php

use App\Domain\Categories\Actions\Categories\BindPropertiesAction;
use App\Domain\Categories\Actions\Categories\Data\CategoryPropertyLinkData;
use App\Domain\Categories\Events\CategoryInvalidated;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\travel;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

beforeEach(function () {
    Event::fake();
});

test('create link', function (bool $replace) {
    $category = Category::factory()->createOne();
    $property = Property::factory()->createOne();

    (new BindPropertiesAction())->execute(
        $category->id,
        $replace,
        [new CategoryPropertyLinkData($property->id, true, false)]
    );

    Event::assertDispatched(CategoryInvalidated::class);
    assertDatabaseHas(
        CategoryPropertyLink::tableName(),
        ['category_id' => $category->id, 'property_id' => $property->id]
    );
})->with([
    'replace' => [true],
    'patch' => [false],
]);

test('modify link', function (bool $replace) {
    $link = CategoryPropertyLink::factory()->required()->createOne();

    (new BindPropertiesAction())->execute(
        $link->category_id,
        $replace,
        [new CategoryPropertyLinkData($link->property_id, false, false)]
    );

    Event::assertDispatched(CategoryInvalidated::class);
    assertDatabaseHas($link->getTable(), ['id' => $link->id, 'is_required' => false]);
})->with([
    'replace' => [true],
    'patch' => [false],
]);

test('delete unchanged links when replace=true', function () {
    $link = CategoryPropertyLink::factory()->required()->createOne();

    (new BindPropertiesAction())->execute($link->category_id, true, []);

    Event::assertDispatched(CategoryInvalidated::class);
    assertModelMissing($link);
});

test('not delete unchanged links when replace=false', function () {
    $link = CategoryPropertyLink::factory()->required()->createOne();

    (new BindPropertiesAction())->execute($link->category_id, false, []);

    Event::assertNotDispatched(CategoryInvalidated::class);
    assertDatabaseHas($link->getTable(), ['id' => $link->id]);
});

test('does nothing when no changes', function () {
    $link = CategoryPropertyLink::factory()->required()->createOne();
    travel(1)->minutes();

    (new BindPropertiesAction())->execute(
        $link->category_id,
        true,
        [new CategoryPropertyLinkData($link->property_id, true, false)]
    );

    Event::assertNotDispatched(CategoryInvalidated::class);
    assertDatabaseHas($link->getTable(), ['id' => $link->id, 'updated_at' => $link->updated_at]);
});
