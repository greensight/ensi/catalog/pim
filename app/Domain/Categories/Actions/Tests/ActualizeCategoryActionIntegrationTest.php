<?php

use App\Domain\Categories\Actions\Categories\ActualizeCategoryAction;
use App\Domain\Categories\Actions\Categories\ActualizePropertiesAction;
use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Categories\Models\Category;
use Illuminate\Support\Facades\Event;
use Mockery\MockInterface;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\travel;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

function executeActualizeCategoryAction(Category $category, int $propertiesChanged = 0): void
{
    /** @var ActualizePropertiesAction|MockInterface $propertiesActualizer */
    $propertiesActualizer = Mockery::mock(ActualizePropertiesAction::class, ['execute' => $propertiesChanged]);

    (new ActualizeCategoryAction($propertiesActualizer))->execute($category->id);
}

test('actualize status', function (bool $activeParent, bool $active, bool $expected) {
    $parent = Category::factory()->actualized($activeParent)->createOne();
    $category = Category::factory()->withParent($parent)->createOne(['is_active' => $active]);

    Event::fake();

    executeActualizeCategoryAction($category);

    assertDatabaseHas($category->getTable(), ['id' => $category->id, 'is_real_active' => $expected]);
})->with([
    'both active' => [true, true, true],
    'parent inactive' => [false, true, false],
    'self inactive' => [true, false, false],
]);

test('actualize full code', function () {
    $parent = Category::factory()->actualized()->createOne(['full_code' => ['foo']]);
    $category = Category::factory()
        ->withParent($parent)
        ->actualized()
        ->createOne();

    Event::fake();

    executeActualizeCategoryAction($category);

    assertDatabaseHas($category->getTable(), [
        'id' => $category->id,
        'full_code' => json_encode(['foo', $category->code]),
    ]);
});

test('not modify standard timestamps', function () {
    $category = Category::factory()->createOne(['is_active' => false, 'is_real_active' => true]);

    Event::fake();

    travel(2)->minutes();
    executeActualizeCategoryAction($category);

    assertDatabaseHas($category->getTable(), ['id' => $category->id, 'updated_at' => $category->updated_at]);
});

test('update actualizing timestamp', function () {
    $category = Category::factory()->createOne(['is_active' => false, 'is_real_active' => true]);

    Event::fake();

    travel(2)->minutes();
    executeActualizeCategoryAction($category);

    assertDatabaseHas($category->getTable(), ['id' => $category->id, 'actualized_at' => now()]);
});

test('actualize properties', function () {
    $category = Category::factory()->actualized()->createOne();

    Event::fake();

    travel(2)->minutes();
    executeActualizeCategoryAction($category, 1);

    assertDatabaseHas($category->getTable(), [
        'id' => $category->id,
        'actualized_at' => now(),
        'updated_at' => $category->updated_at,
    ]);
    Event::assertDispatched(CategoryActualized::class);
});

test('ignore not changed', function () {
    $category = Category::factory()->actualized()->createOne();

    Event::fake();

    travel(2)->minutes();
    executeActualizeCategoryAction($category);

    assertDatabaseHas($category->getTable(), ['id' => $category->id, 'actualized_at' => $category->actualized_at]);
    Event::assertNotDispatched(CategoryActualized::class);
});
