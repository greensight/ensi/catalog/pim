<?php

namespace App\Domain\Categories\Actions\Properties;

use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Exceptions\IllegalOperationException;

class DeletePropertyAction
{
    use AppliesToAggregate;

    public function execute(int $propertyId): void
    {
        $this->delete($propertyId, function (Property $property) {
            $name = $property->name;
            $id = $property->id;

            throw_if(
                $property->is_system,
                IllegalOperationException::class,
                "Свойство [$id] \"$name\" является системным. Удаление запрещено."
            );

            throw_if(
                $property->categoryPropertyLinks()->exists(),
                IllegalOperationException::class,
                "Свойство [$id] \"$name\" привязано к категориям. Удаление запрещено."
            );

            $linkProductId = ProductPropertyValue::query()
                ->where('property_id', $property->id)
                ->get()
                ->pluck('product_id');

            throw_if(
                count($linkProductId) > 0,
                IllegalOperationException::class,
                "Свойство [$id] \"$name\" привязано к товарам $linkProductId. Удаление запрещено."
            );
        });
    }

    protected function createModel(): Model
    {
        return new Property();
    }
}
