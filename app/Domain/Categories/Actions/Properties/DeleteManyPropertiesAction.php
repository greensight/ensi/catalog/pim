<?php

namespace App\Domain\Categories\Actions\Properties;

use App\Domain\Support\Concerns\HandlesMassOperation;
use App\Domain\Support\MassOperationResult;

class DeleteManyPropertiesAction
{
    use HandlesMassOperation;

    public function __construct(private DeletePropertyAction $deleteAction)
    {
    }

    public function execute(array $propertyIds): MassOperationResult
    {
        return $this->each($propertyIds, function (int $propertyId) {
            $this->deleteAction->execute($propertyId);
        });
    }
}
