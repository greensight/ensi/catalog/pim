<?php

namespace App\Domain\Categories\Actions\Properties;

use App\Domain\Categories\Events\PropertyInvalidated;
use App\Domain\Categories\Models\Property;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class PatchPropertyAction
{
    use AppliesToAggregate;

    public function execute(int $propertyId, array $fields): Property
    {
        $property = $this->patch($propertyId, $fields);

        if ($property->invalidated()) {
            PropertyInvalidated::dispatch($property->id);
        }

        return $property;
    }

    protected function createModel(): Model
    {
        return new Property();
    }
}
