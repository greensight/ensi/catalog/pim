<?php

namespace App\Domain\Categories\Actions\Categories\Data;

class CategoryPropertyLinkData
{
    public function __construct(
        public int  $propertyId,
        public bool $isRequired,
        public bool $isGluing,
    ) {
    }
}
