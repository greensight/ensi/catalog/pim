<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Core\PropertiesResolver;
use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Support\Concerns\InteractsWithModels;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ActualizePropertiesAction
{
    use InteractsWithModels;

    public function __construct(private PropertiesResolver $resolver)
    {
    }

    public function execute(Category $category, ?Category $parent = null): int
    {
        $parent ??= $category->parent;

        $expectedProperties = $this->resolver->resolve($category, $parent);

        $relation = $category->actualAllProperties();
        $existingProperties = $relation->get()->keyBy('property_id');

        $changes = 0;

        foreach ($expectedProperties as $property) {
            /** @var ActualCategoryProperty|null $existing */
            $existing = $existingProperties->get($property->propertyId);

            if ($existing === null) {
                $this->addProperty($relation, $property);
                $changes++;

                continue;
            }

            if ($this->updateProperty($property, $existing)) {
                $changes++;
            }

            $existingProperties->forget($property->propertyId);
        }

        foreach ($existingProperties as $existing) {
            $this->deleteOrThrow($existing);
            $changes++;
        }

        return $changes;
    }

    private function addProperty(HasMany $relation, CategoryPropertySample $source): void
    {
        $property = ActualCategoryProperty::fromSample($source);

        $this->saveRelatedOrThrow($relation, $property);
    }

    private function updateProperty(CategoryPropertySample $source, ActualCategoryProperty $target): bool
    {
        if ($target->toSample() == $source) {
            return false;
        }

        $target->fillFromSample($source);

        $this->saveOrThrow($target);

        return true;
    }
}
