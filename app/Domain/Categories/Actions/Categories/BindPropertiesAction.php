<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Actions\Categories\Data\CategoryPropertyLinkData;
use App\Domain\Categories\Events\CategoryInvalidated;
use App\Domain\Categories\Events\GluingPropertyDeleted;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Exceptions\IllegalOperationException;
use App\Support\Actions\ActionDecorator;
use App\Support\Actions\DecoratesAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Illuminate\Support\Collection;

class BindPropertiesAction
{
    use AppliesToAggregate;
    use DecoratesAction;

    /**
     * @param int $categoryId
     * @param bool $replace
     * @param CategoryPropertyLinkData[] $categoryPropertyLinks
     * @return Category
     */
    public function execute(int $categoryId, bool $replace, array $categoryPropertyLinks): Category
    {
        [$category, $hasChanges] = $this->apply($categoryId, function (Category $category) use ($replace, $categoryPropertyLinks) {
            $hasChanges = $this->bind($category, $categoryPropertyLinks, $replace) > 0;

            return [$category, $hasChanges];
        });

        if ($hasChanges) {
            CategoryInvalidated::dispatch($category->id);
        }

        return $category;
    }

    public function enrichResult(): static|ActionDecorator
    {
        return $this->decorateResult(fn (Category $category) => $category->load('properties'));
    }

    protected function createModel(): Model
    {
        return new Category();
    }

    /**
     * @param Category $category
     * @param CategoryPropertyLinkData[] $categoryPropertyLinks
     * @param bool $replace
     * @return int
     */
    private function bind(Category $category, array $categoryPropertyLinks, bool $replace): int
    {
        $relation = $category->propertyLinks();
        $existing = $relation->get()->keyBy('property_id');

        return $this->updateLinks($existing, $categoryPropertyLinks, $relation)
            + $this->processUnchangedLinks($existing, $replace);
    }

    /**
     * @param Collection $existing
     * @param CategoryPropertyLinkData[] $categoryPropertyLinks
     * @param HasOneOrMany $relation
     * @return int
     */
    private function updateLinks(Collection $existing, array $categoryPropertyLinks, HasOneOrMany $relation): int
    {
        return collect($categoryPropertyLinks)
            ->reduce(function (int $carry, CategoryPropertyLinkData $categoryPropertyLink) use ($relation, $existing) {
                $propertyId = $categoryPropertyLink->propertyId;
                $link = $existing->pull($propertyId) ?? CategoryPropertyLink::new($propertyId);

                if ($link->exists &&
                    $link->is_required === $categoryPropertyLink->isRequired &&
                    $link->is_gluing === $categoryPropertyLink->isGluing
                ) {
                    return $carry;
                }

                $link->is_required = $categoryPropertyLink->isRequired;
                $link->is_gluing = $categoryPropertyLink->isGluing;
                $this->saveRelatedOrThrow($relation, $link);

                if ($link->wasChanged('is_gluing') && !$link->is_gluing) {
                    GluingPropertyDeleted::dispatch($link);
                }

                return $carry + 1;
            }, 0);
    }

    private function processUnchangedLinks(Collection $links, bool $replace): int
    {
        return $replace
            ? $links->each(function (CategoryPropertyLink $link) {
                $this->canDelete($link);
                $this->deleteOrThrow($link);

                if ($link->is_gluing) {
                    GluingPropertyDeleted::dispatch($link);
                }
            })->count()
            : 0;
    }

    private function canDelete(CategoryPropertyLink $link): void
    {
        $categoryIds = Category::descendantsAndSelf($link->category_id, ['id'])
            ->pluck('id')
            ->all();

        $linkProductId = ProductPropertyValue::query()
            ->where('property_id', $link->property_id)
            ->whereHas('categoryProductLinks', function (Builder $query) use ($categoryIds) {
                $query->whereIn('category_id', $categoryIds);
            })
            ->get()
            ->pluck('product_id');

        throw_if(
            count($linkProductId) > 0,
            IllegalOperationException::class,
            "Свойство [$link->property_id] содержит привязанные товары
            $linkProductId в данной категории или дочерней. Удаление запрещено."
        );
    }
}
