<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Products\Models\Product;
use Illuminate\Support\Collection;

class SetCategoryProductLinksAction
{
    /**
     * @param Product $product
     * @param array|int[] $categoryIds
     * @param bool $replace
     * @return int
     */
    public function execute(Product $product, array $categoryIds, bool $replace = true): int
    {
        $newIds = collect($categoryIds)
            ->unique()
            ->values();

        $currentIds = $product->getCategoryIds();

        return $replace
            ? $this->replace($product, $newIds)
            : $this->patch($product, $currentIds, $newIds);
    }

    protected function replace(Product $product, Collection $newIds): int
    {
        $result = $product->categories()->sync($newIds);

        return count($result['attached']) + count($result['detached']);
    }

    protected function patch(Product $product, Collection $currentIds, Collection $newIds): int
    {
        $diff = $newIds->diff($currentIds);
        $product->categories()->attach($diff);

        return $diff->count();
    }
}
