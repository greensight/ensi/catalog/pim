<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Categories\Models\Category;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class ActualizeCategoryAction
{
    use AppliesToAggregate;

    public function __construct(private ActualizePropertiesAction $actualizePropertiesAction)
    {
    }

    public function execute(int $categoryId): void
    {
        $this->apply($categoryId, function (Category $category) {
            $parent = $category->parent;

            $this->actualizeState(
                $category,
                $parent,
                $this->actualizeProperties($category, $parent)
            );
        });
    }

    private function actualizeProperties(Category $category, ?Category $parent): bool
    {
        return $this->actualizePropertiesAction->execute($category, $parent) > 0;
    }

    private function actualizeState(Category $category, ?Category $parent, bool $forceSave): void
    {
        $parentActive = $parent?->is_real_active ?? true;
        $fullCode = $parent?->full_code ?? [];
        $fullCode[] = $category->code;

        $category->is_real_active = $category->is_active && $parentActive;
        $category->full_code = $fullCode;

        $this->saveState($category, $forceSave);
    }

    private function saveState(Category $category, bool $force): void
    {
        $hasChanges = $category->isDirty();

        if (!$hasChanges && !$force) {
            return;
        }

        $category->actualized_at = now();
        $category->timestamps = false;
        $category->save();
        $category->timestamps = true;

        CategoryActualized::dispatch($category->id);
    }

    protected function createModel(): Model
    {
        return new Category();
    }
}
