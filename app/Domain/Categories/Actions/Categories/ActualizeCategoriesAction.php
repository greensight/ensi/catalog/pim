<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Models\Category;
use App\Domain\Support\Concerns\HandlesMassOperation;

class ActualizeCategoriesAction
{
    use HandlesMassOperation;

    public function __construct(private ActualizeCategoryAction $action)
    {
    }

    public function execute(): void
    {
        $categoryIds = Category::query()
            ->orderBy('id')
            ->pluck('id')
            ->toArray();

        $this->each($categoryIds, function (int $categoryId) {
            $this->action->execute($categoryId);
        });
    }
}
