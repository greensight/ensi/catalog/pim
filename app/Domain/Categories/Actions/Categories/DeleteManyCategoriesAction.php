<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Support\Concerns\HandlesMassOperation;

class DeleteManyCategoriesAction
{
    use HandlesMassOperation;

    public function __construct(private DeleteCategoryAction $deleteAction)
    {
    }

    public function execute(array $categoryIds): void
    {
        $this->each($categoryIds, function (int $categoryId) {
            $this->deleteAction->execute($categoryId);
        });
    }
}
