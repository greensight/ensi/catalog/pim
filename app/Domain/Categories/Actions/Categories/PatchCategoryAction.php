<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Events\CategoryInvalidated;
use App\Domain\Categories\Models\Category;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Support\Actions\ActionDecorator;
use App\Support\Actions\DecoratesAction;

class PatchCategoryAction
{
    use AppliesToAggregate;
    use DecoratesAction;

    public function execute(int $categoryId, array $fields): Category
    {
        $category = $this->patch($categoryId, $fields);

        CategoryInvalidated::dispatch($category->id);

        return $category;
    }

    public function refreshResult(): static|ActionDecorator
    {
        return $this->decorateResult(fn (Category $category) => $category->refresh());
    }

    protected function createModel(): Model
    {
        return new Category();
    }
}
