<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Models\Category;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Exceptions\IllegalOperationException;

class DeleteCategoryAction
{
    use AppliesToAggregate;

    public function execute(int $categoryId): void
    {
        $this->delete($categoryId, function (Category $category) {
            $name = $category->name;
            $id = $category->id;

            throw_if(
                $category->propertyLinks()->exists(),
                IllegalOperationException::class,
                "Категория [$id] \"$name\" содержит привязанные атрибуты. Удаление запрещено."
            );

            throw_if(
                $category->children()->exists(),
                IllegalOperationException::class,
                "Категория [$id] \"$name\" содержит дочерние категории. Удаление запрещено."
            );

            throw_if(
                $category->products()->exists(),
                IllegalOperationException::class,
                "Категория [$id] \"$name\" содержит товары. Удаление запрещено."
            );
        });
    }

    protected function createModel(): Model
    {
        return new Category();
    }
}
