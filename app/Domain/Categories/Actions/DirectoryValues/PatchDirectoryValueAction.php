<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;

class PatchDirectoryValueAction
{
    use AppliesToDirectory;

    public function execute(int $valueId, array $fields): PropertyDirectoryValue
    {
        return $this->updateOrCreate(
            $valueId,
            null,
            function (PropertyDirectoryValue $value, Property $property) use ($fields) {
                $this->checkProperty($property);

                if ($this->containsValue($property, $fields)) {
                    $value->setValue($this->extractValue($property, $fields));
                }

                $value->fill($fields);
            }
        );
    }
}
