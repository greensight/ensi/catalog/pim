<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Support\Concerns\AppliesToDrivenEntity;
use App\Domain\Support\Models\Model;
use App\Domain\Support\Models\TempFile;
use App\Exceptions\IllegalOperationException;

trait AppliesToDirectory
{
    use AppliesToDrivenEntity;

    protected function createModel(): Model
    {
        return new PropertyDirectoryValue();
    }

    protected function createRootModel(): Model
    {
        return new Property();
    }

    protected function getRelationNameInRoot(): string
    {
        return 'directory';
    }

    protected function checkProperty(Property $property): void
    {
        throw_unless(
            $property->has_directory,
            IllegalOperationException::class,
            "Свойство $property->name не является справочником"
        );
    }

    protected function extractValue(Property $property, array $fields): PropertyValue
    {
        $value = $this->containsPreloadFile($property, $fields)
            ? TempFile::grab($fields['preload_file_id'])
            : $fields['value'];

        return $this->validateValue($property, $value);
    }

    protected function validateValue(Property $property, mixed $value): PropertyValue
    {
        return PropertyValue::validate($property->type, $value);
    }

    protected function containsValue(Property $property, array $fields): bool
    {
        return $this->containsPreloadFile($property, $fields) || isset($fields['value']);
    }

    protected function containsPreloadFile(Property $property, array $fields): bool
    {
        return (
            $property->type === PropertyType::IMAGE || $property->type === PropertyType::FILE
        ) && isset($fields['preload_file_id']);
    }
}
