<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Exceptions\IllegalOperationException;

class DeleteDirectoryValueAction
{
    use AppliesToDirectory;

    public function execute(int $valueId): void
    {
        $this->delete($valueId, $this->checkConstraints(...));
    }

    private function checkConstraints(PropertyDirectoryValue $value, Property $property): void
    {
        $this->checkProperty($property);

        $used = ProductPropertyValue::whereDirectoryValueId($value->id)->exists()
            || PublishedPropertyValue::whereDirectoryValueId($value->id)->exists();

        throw_if(
            $used,
            IllegalOperationException::class,
            "Значение справочника используется в атрибутах товаров"
        );
    }
}
