<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;

class CreateDirectoriesValueAction
{
    use AppliesToDirectory;

    public function execute(int $propertyId, array $fields): array
    {
        return $this->transaction(function () use ($propertyId, $fields) {
            $property = Property::findOrFail($propertyId);
            $this->checkProperty($property);

            $props = [];
            foreach ($fields['items'] as $item) {
                $props[] = $this->updateOrCreate(
                    null,
                    $propertyId,
                    function (PropertyDirectoryValue $value, Property $property) use ($item) {
                        $value->setValue($this->extractValue($property, $item));
                        $value->fill($item);
                    }
                );
            }

            return $props;
        });
    }
}
