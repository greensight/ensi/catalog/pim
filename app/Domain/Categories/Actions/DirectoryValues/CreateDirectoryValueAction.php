<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;

class CreateDirectoryValueAction
{
    use AppliesToDirectory;

    public function execute(int $propertyId, array $fields): PropertyDirectoryValue
    {
        return $this->updateOrCreate(
            null,
            $propertyId,
            function (PropertyDirectoryValue $value, Property $property) use ($fields) {
                $this->checkProperty($property);

                $value->setValue($this->extractValue($property, $fields));
                $value->fill($fields);
            }
        );
    }
}
