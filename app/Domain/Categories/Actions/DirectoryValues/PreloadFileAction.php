<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Support\Actions\UploadFileAction;
use App\Domain\Support\Models\TempFile;
use Illuminate\Http\UploadedFile;

class PreloadFileAction
{
    public function __construct(private UploadFileAction $action)
    {
    }

    public function execute(UploadedFile $uploadedFile): TempFile
    {
        return $this->action->execute($uploadedFile, 'directory_values');
    }
}
