<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\Tests\Factories\CategoryPropertyLinkFactory;
use App\Domain\Support\Models\Pivot;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

/**
 * Класс-модель для сущности "Привязки атрибутов товаров к категориям"
 *
 * @property int $property_id Идентификатор свойства
 * @property int $category_id Идентификатор категории
 * @property bool $is_required Свойство обязательно для заполнения
 * @property bool $is_gluing Свойство используется для склейки товаров
 *
 * @property-read Property $property - свойство
 * @property-read Category $category - категория
 *
 * @method static Builder|self forCategories(array $categoryIds)
 */
class CategoryPropertyLink extends Pivot
{
    protected $fillable = ['property_id', 'category_id', 'is_required', 'is_gluing'];
    protected $table = 'category_property_links';

    protected $casts = [
        'property_id' => 'int',
        'category_id' => 'int',
        'is_required' => 'bool',
        'is_gluing' => 'bool',
    ];

    /**
     * @return BelongsTo
     */
    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeForCategories(Builder $query, array $categoryIds): Builder
    {
        return $query->whereIn('category_id', $categoryIds);
    }

    public function toSample(): CategoryPropertySample
    {
        return new CategoryPropertySample(
            $this->property_id,
            $this->is_required,
            $this->property->is_common,
            isGluing: $this->is_gluing
        );
    }

    /**
     * Возвращает массив идентификаторов привязок действующих для категории свойств.
     *
     * @param int $categoryId
     * @return array|int[]
     * @deprecated
     */
    public static function selectEffectiveIds(int $categoryId): array
    {
        return Category::whereAncestorOf($categoryId, true)
            ->join(self::tableName() . ' as link', 'categories.id', '=', 'link.category_id')
            ->select([
                'link.id as link_id',
                'link.property_id as property_id',
                DB::raw('CASE WHEN link.is_required THEN 0 else categories._rgt END as priority'),
            ])
            ->orderBy('priority')
            ->get()
            ->unique('property_id')
            ->pluck('link_id')
            ->toArray();
    }

    public static function new(?int $propertyId = null): static
    {
        $instance = new static();

        if ($propertyId !== null) {
            $instance->property_id = $propertyId;
        }

        return $instance;
    }

    public static function factory(): CategoryPropertyLinkFactory
    {
        return CategoryPropertyLinkFactory::new();
    }
}
