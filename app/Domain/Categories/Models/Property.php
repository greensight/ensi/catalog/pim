<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Models\Tests\Factories\PropertyFactory;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Support\Models\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Класс-модель для сущности "Атрибут товара".
 *
 * @property string $name Название в системе
 * @property string $display_name Название на витрине
 * @property string $code Код атрибута
 * @property PropertyType $type Тип (string, integer, double, datetime, color)
 * @property string $hint_value Подсказка при заполнении для значения
 * @property string $hint_value_name Подсказка при заполнении для названия значения
 *
 * @property bool $is_active Активность свойства
 * @property bool $is_multiple Поддерживает множественные значения
 * @property bool $is_filterable Показывать в фильтре на витрине
 * @property bool $is_system Признак системного атрибута
 * @property bool $is_public Отображать на витрине
 * @property bool $is_common Общий для всех товаров, привязывается автоматически ко всем категориям
 * @property bool $is_required Обязательность для общего атрибута
 * @property bool $has_directory Признак наличия справочника значений
 * @property bool $is_moderated Признак модерируемого атрибута
 *
 * @property-read Collection<int,PropertyDirectoryValue> $directory
 * @property-read Collection<int,CategoryPropertyLink> $categoryPropertyLinks
 * @property-read Collection<int,ActualCategoryProperty> $actualLinks
 *
 * @method static static|Builder common()
 * @method static static|Builder active()
 */
class Property extends Model implements Auditable
{
    use Sluggable;
    use SupportsAudit;

    /**
     * @var array Заполняемые поля модели
     */
    public const FILLABLE = [
        'name',
        'display_name',
        'code',
        'type',
        'hint_value',
        'hint_value_name',
        'is_active',
        'is_multiple',
        'is_filterable',
        'is_public',
        'is_required',
        'has_directory',
        'is_moderated',
    ];

    public const SYSTEM_FILLABLE = [
        'name',
        'display_name',
        'hint_value',
        'hint_value_name',
        'is_active',
        'is_multiple',
        'is_filterable',
        'is_public',
        'is_required',
        'is_moderated',
    ];

    protected $fillable = self::FILLABLE;
    protected $table = 'properties';

    protected $casts = [
        'type' => PropertyType::class,
        'is_multiple' => 'bool',
        'is_filterable' => 'bool',
        'is_active' => 'bool',
        'is_system' => 'bool',
        'is_common' => 'bool',
        'is_required' => 'bool',
        'is_public' => 'bool',
        'has_directory' => 'bool',
        'is_moderated' => 'bool',
    ];

    protected $attributes = [
        'is_system' => false,
        'is_required' => false,
        'is_common' => false,
        'is_active' => false,
        'is_filterable' => false,
        'is_public' => false,
        'has_directory' => false,
        'is_moderated' => false,
    ];

    public function categoryPropertyLinks(): HasMany
    {
        return $this->hasMany(CategoryPropertyLink::class);
    }

    public function directory(): HasMany
    {
        return $this->hasMany(PropertyDirectoryValue::class);
    }

    public function actualLinks(): HasMany
    {
        return $this->hasMany(ActualCategoryProperty::class);
    }

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'display_name',
                'separator' => '_',
                'unique' => true,
                'maxLength' => 25,
            ],
        ];
    }

    public function getFillable(): array
    {
        return $this->is_system ? self::SYSTEM_FILLABLE : self::FILLABLE;
    }

    public function invalidated(): bool
    {
        $fields = ['is_active', 'type', 'has_directory'];

        if ($this->is_common) {
            $fields[] = 'is_required';
        }

        return $this->wasChanged($fields);
    }

    public function scopeCommon(Builder $query): Builder
    {
        return $query->where('is_common', true);
    }

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', true);
    }

    public static function factory(): PropertyFactory
    {
        return PropertyFactory::new();
    }
}
