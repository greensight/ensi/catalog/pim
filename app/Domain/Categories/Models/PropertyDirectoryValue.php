<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Data\PropertyValueHolder;
use App\Domain\Categories\Models\Tests\Factories\DirectoryValueFactory;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Support\Models\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Webmozart\Assert\Assert;

/**
 * Класс-модель для сущности "Варианты значений для свойств-справочников".
 *
 * @property int $property_id ID свойства
 * @property PropertyType $type Тип хранимого значения
 * @property string|null $name Название значения
 * @property string $value Значение
 * @property string|null $code Символьный код
 *
 * @property Property $property
 */
class PropertyDirectoryValue extends Model implements PropertyValueHolder
{
    use Sluggable;

    protected $fillable = ['name', 'code'];
    protected $table = 'property_directory_values';

    protected $casts = [
        'type' => PropertyType::class,
        'property_id' => 'int',
        'is_system' => 'bool',
    ];

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => $this->type === PropertyType::STRING ? 'value' : 'name',
                'separator' => '_',
                'unique' => true,
                'maxLength' => 20,
            ],
        ];
    }

    public function getValue(): PropertyValue
    {
        return PropertyValue::make($this->type ?? PropertyType::STRING, $this->value);
    }

    public function setValue(PropertyValue $value): static
    {
        Assert::false($value->isNull(), 'Требуется ненулевое значение');

        $this->type = $value->type();
        $this->value = (string)$value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getOriginalValue(): ?PropertyValue
    {
        $original = $this->getOriginal();

        if (!isset($original['type']) || !isset($original['value'])) {
            return null;
        }

        return PropertyValue::make($original['type'], $original['value']);
    }

    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    public static function factory(): DirectoryValueFactory
    {
        return DirectoryValueFactory::new();
    }

    protected static function boot(): void
    {
        parent::boot();

        self::registerModelEvent('slugging', static function (PropertyDirectoryValue $model) {
            return $model->type === PropertyType::STRING
                || ($model->type === PropertyType::COLOR && !blank($model->name));
        });
    }
}
