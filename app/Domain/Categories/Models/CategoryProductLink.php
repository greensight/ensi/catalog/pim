<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Models\Tests\Factories\CategoryProductLinkFactory;
use App\Domain\Products\Models\Product;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $category_id
 * @property int $product_id
 *
 * @property-read Category $category
 * @property-read Product $product
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class CategoryProductLink extends Pivot
{
    protected $fillable = [
        'category_id',
        'product_id',
    ];

    protected $table = 'category_product_links';
    public $incrementing = true;

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public static function factory(): CategoryProductLinkFactory
    {
        return CategoryProductLinkFactory::new();
    }
}
