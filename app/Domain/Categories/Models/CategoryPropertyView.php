<?php

namespace App\Domain\Categories\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property-read Collection|PropertyDirectoryValue[] $directory
 */
class CategoryPropertyView extends BoundPropertyView
{
    protected $table = 'v_actual_category_properties';

    public function directory(): HasMany
    {
        return $this->hasMany(PropertyDirectoryValue::class, 'property_id', 'property_id');
    }
}
