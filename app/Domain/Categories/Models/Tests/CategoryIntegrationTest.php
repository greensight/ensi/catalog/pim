<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

test('creates audit records', function () {
    $category = Category::factory()->createOne();

    $category->is_active = !$category->is_active;
    $category->save();

    expect($category->audits()->count())->toBe(2);
});

test('bind property', function () {
    $category = Category::factory()->createOne();
    $link = ActualCategoryProperty::factory()
        ->forCategory($category)
        ->createOne();

    expect($category->properties->pluck('id'))
        ->toHaveCount(1)
        ->toContain($link->id);
});
