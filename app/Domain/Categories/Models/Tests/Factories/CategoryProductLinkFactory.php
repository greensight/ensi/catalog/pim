<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Products\Models\Product;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method CategoryProductLink createOne(array $fields = [])
 * @method CategoryProductLink makeOne(array $fields = [])
 * @method CategoryProductLink|CategoryProductLink[]|Collection create(array $fields = [], $parent = null)
 */
class CategoryProductLinkFactory extends BaseModelFactory
{
    protected $model = CategoryProductLink::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'product_id' => Product::factory(),
            'category_id' => Category::factory(),
        ];
    }

    public function forCategory(Category $category): self
    {
        return $this->state(['category_id' => $category->id]);
    }

    public function forProduct(Product $product): self
    {
        return $this->state(['product_id' => $product->id]);
    }

    public function createFast(
        Category $category,
        Product $product,
    ): CategoryProductLink {
        $fields = [
            'category_id' => $category->id,
            'product_id' => $product->id,
        ];

        return $this->create($fields);
    }
}
