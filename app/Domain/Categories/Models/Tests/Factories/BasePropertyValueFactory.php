<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\PropertyType;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Facades\Date;

/**
 * @template TModel of \Illuminate\Database\Eloquent\Model
 * @extends BaseModelFactory<TModel>
 */
class BasePropertyValueFactory extends BaseModelFactory
{
    public function definition(): array
    {
        $type = $this->faker->randomElement(PropertyType::cases());

        return [
            'property_id' => Property::factory()->withType($type),
            'value' => $this->generateValue($type),
            'name' => $this->faker->sentence(3),
            'type' => $type,
        ];
    }

    public function withType(PropertyType $type): static
    {
        return $this->state(function (array $fields) use ($type) {
            $result = [
                'type' => $type->value,
                'value' => $this->generateValue($type),
            ];

            $property = $fields['property_id'] ?? null;
            if ($property instanceof PropertyFactory) {
                $result['property_id'] = $property->withType($type);
            }

            return $result;
        });
    }

    public function forProperty(Property $property): static
    {
        return $this->state(function () use ($property) {
            return [
                'property_id' => $property->id,
                'type' => $property->type->value,
                'value' => $this->generateValue($property->type),
            ];
        });
    }

    protected function generateValue(PropertyType $type): mixed
    {
        return match ($type) {
            PropertyType::STRING => $this->faker->word,
            PropertyType::TEXT => $this->faker->text,
            PropertyType::BOOLEAN => $this->faker->boolean === true ? 'true' : 'false',
            PropertyType::COLOR => '#' . dechex($this->faker->numberBetween(0x100000, 0xFFFFFF)),
            PropertyType::DATETIME => Date::make($this->faker->dateTime)->toJSON(),
            PropertyType::DOUBLE => $this->faker->randomFloat(4),
            PropertyType::INTEGER => $this->faker->randomNumber(),
            PropertyType::IMAGE => EnsiFile::factory()->fileName($this->faker->word())->fileExt('jpg')->make()['path'],
            PropertyType::FILE => EnsiFile::factory()->fileName($this->faker->word())->fileExt('txt')->make()['path'],
        };
    }
}
