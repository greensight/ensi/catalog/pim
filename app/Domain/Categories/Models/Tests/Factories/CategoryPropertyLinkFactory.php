<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method CategoryPropertyLink createOne(array $fields = [])
 * @method CategoryPropertyLink makeOne(array $fields = [])
 * @method CategoryPropertyLink|CategoryPropertyLink[]|Collection create(array $fields = [], $parent = null)
 */
class CategoryPropertyLinkFactory extends BaseModelFactory
{
    protected $model = CategoryPropertyLink::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'property_id' => Property::factory(),
            'category_id' => Category::factory(),
            'is_required' => $this->faker->boolean,
            'is_gluing' => false,
        ];
    }

    public function required(bool $value = true): self
    {
        return $this->state(['is_required' => $value]);
    }

    public function glued(bool $value = true): self
    {
        return $this->state(['is_gluing' => $value]);
    }

    public function forCategory(Category $category): self
    {
        return $this->state(['category_id' => $category->id]);
    }

    public function forProperty(Property $property): self
    {
        return $this->state(['property_id' => $property->id]);
    }

    public function createFast(
        Category $category,
        Property $property,
        ?bool $isRequired = null,
        ?bool $isGluing = null
    ): CategoryPropertyLink {
        $fields = [
            'category_id' => $category->id,
            'property_id' => $property->id,
        ];

        if ($isRequired !== null) {
            $fields['is_required'] = $isRequired;
        }

        if ($isGluing !== null) {
            $fields['is_gluing'] = $isGluing;
        }

        return $this->create($fields);
    }
}
