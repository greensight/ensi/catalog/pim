<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\PropertyType;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method Property createOne(array $fields = [])
 * @method Property makeOne(array $fields = [])
 * @method Property|Property[]|Collection create(array $fields = [], $parent = null)
 *
 * @extends BaseModelFactory<Property>
 */
class PropertyFactory extends BaseModelFactory
{
    protected $model = Property::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'display_name' => $this->faker->word,
            'code' => $this->faker->unique()->slug(2),
            'type' => $this->faker->randomEnum(PropertyType::cases()),
            'is_multiple' => $this->faker->boolean,
            'is_filterable' => $this->faker->boolean,
            'is_active' => true,
            'hint_value' => $this->faker->sentence,
            'hint_value_name' => $this->faker->sentence,
            'is_common' => false,
            'is_required' => $this->faker->boolean,
            'is_system' => false,
            'has_directory' => false,
            'is_moderated' => false,
        ];
    }

    public function directory(): self
    {
        return $this->state(['has_directory' => true]);
    }

    public function withType(PropertyType $type): self
    {
        return $this->state(['type' => $type->value]);
    }

    public function common(): self
    {
        return $this->state(['is_common' => true]);
    }

    public function inactive(): self
    {
        return $this->state(['is_active' => false]);
    }

    public function required(bool $required = true): self
    {
        return $this->state(['is_required' => $required]);
    }

    public function moderated(): self
    {
        return $this->state(['is_moderated' => true]);
    }

    public function actual(Category $category, bool $isGluing = null, bool $isInherited = null): self
    {
        return $this->afterCreating(function (Property $property) use ($category, $isGluing, $isInherited) {
            ActualCategoryProperty::factory()
                ->createFast(
                    $category,
                    $property,
                    isRequired: $property->is_required,
                    isGluing: $isGluing,
                    isInherited: $isInherited
                );
        });
    }
}
