<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use Illuminate\Support\Collection;

/**
 * @method PropertyDirectoryValue createOne(array $fields = [])
 * @method PropertyDirectoryValue makeOne(array $fields = [])
 * @method PropertyDirectoryValue|PropertyDirectoryValue[]|Collection create(array $fields = [], $parent = null)
 * @method PropertyDirectoryValue|PropertyDirectoryValue[]|Collection make(array $fields = [], $parent = null)
 */
class DirectoryValueFactory extends BasePropertyValueFactory
{
    protected $model = PropertyDirectoryValue::class;

    public function definition(): array
    {
        return array_merge(parent::definition(), [
            'property_id' => Property::factory()->directory(),
            'code' => $this->faker->unique()->slug(2),
        ]);
    }
}
