<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\Category;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Webmozart\Assert\Assert;

/**
 * @method Category createOne(array $fields = [])
 * @method Category makeOne(array $fields = [])
 * @method Category|Category[]|Collection<Category> create(array $fields = [], $parent = null)
 */
class CategoriesFactory extends BaseModelFactory
{
    protected $model = Category::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'code' => $this->faker->unique()->regexify('[A-Za-z0-9_]{30}'),
            'parent_id' => null,
            'is_active' => true,
            'is_inherits_properties' => true,
            'is_real_active' => false,
            'full_code' => null,
            'actualized_at' => null,
        ];
    }

    public function withParent(Category $category): self
    {
        return $this->state(['parent_id' => $category->id]);
    }

    public function inheritProperties(bool $inherit): self
    {
        return $this->state(['is_inherits_properties' => $inherit]);
    }

    public function actualized(?bool $realActive = null, ?array $parentCode = null): self
    {
        return $this->state(function (array $attributes) use ($realActive, $parentCode) {
            $fullCode = $parentCode ?? [];
            if (isset($attributes['code'])) {
                $fullCode[] = $attributes['code'];
            }

            return [
                'is_real_active' => $realActive ?? $attributes['is_active'] ?? false,
                'full_code' => $fullCode,
                'actualized_at' => now(),
            ];
        });
    }

    public function createTree(int $depth = 2): Collection
    {
        Assert::greaterThan($depth, 0);

        $ids = [];

        for ($i = 0; $i < $depth; $i++) {
            $ids[] = $this->createOne(['parent_id' => Arr::last($ids)])->id;
        }

        // Загружаем из БД, т.к. у родителей изменились _lft, _rgt
        return Category::query()->whereIn('id', $ids)
            ->orderBy('id')
            ->get();
    }
}
