<?php

use App\Domain\Categories\Models\Property;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

test('getFillable ordinary property', function () {
    $property = new Property();

    expect($property->getFillable())->not->toContain('is_system');
});

test('getFillable system property', function () {
    $property = new Property();
    $property->is_system = true;

    expect($property->getFillable())->not->toContain('code', 'has_directory', 'type');
});

test('check invalidated', function (bool $isCommon, string $field, bool $expected) {
    $property = Property::factory()->createOne(['is_common' => $isCommon]);

    $property->{$field} = !$property->{$field};
    $property->save();

    expect($property->invalidated())->toBe($expected);
})->with([
    'common required' => [true, 'is_required', true],
    'ignorable' => [true, 'is_public', false],
    'is_active' => [false, 'is_active', true],
]);
