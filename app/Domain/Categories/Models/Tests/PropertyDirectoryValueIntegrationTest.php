<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Support\Events\FileReleased;
use Illuminate\Support\Facades\Event;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

beforeEach(function () {
    Event::fake(FileReleased::class);
});

test('generate slug for string value', function () {
    $model = PropertyDirectoryValue::factory()
        ->createOne(['value' => 'foo', 'type' => PropertyType::STRING, 'code' => null]);

    expect($model->code)->toBe('foo');
});

test('generate slug for color name', function () {
    $model = PropertyDirectoryValue::factory()
        ->createOne(['value' => '000000', 'name' => 'foo', 'type' => PropertyType::COLOR, 'code' => null]);

    expect($model->code)->toBe('foo');
});

test('skip slug for other types', function () {
    $model = PropertyDirectoryValue::factory()
        ->createOne(['value' => '125', 'type' => PropertyType::INTEGER, 'code' => null]);

    expect($model->code)->toBeEmpty();
});

test('get value', function (?PropertyType $type, ?string $value, PropertyValue $expected) {
    $model = new PropertyDirectoryValue();
    $model->type = $type;
    $model->value = $value;

    expect($model->getValue()->equalTo($expected))->toBeTrue();
})->with([
    'null type null value' => [null, null, PropertyValue::STRING()],
    'null value' => [PropertyType::INTEGER, null, PropertyValue::INTEGER()],
    'same value' => [
        PropertyType::DATETIME,
        '2021-08-14T00:00:00.000Z',
        PropertyValue::DATETIME('2021-08-14T00:00:00.000Z'),
    ],
]);

test('set value', function (PropertyValue $value) {
    $model = PropertyDirectoryValue::factory()->make();
    $model->setValue($value);
    $model->save();

    expect(PropertyDirectoryValue::find($model->id)->getValue()->equalTo($value))
        ->toBeTrue();
})->with([
    'integer' => [PropertyValue::INTEGER(261)],
    'double' => [PropertyValue::DOUBLE(555.48)],
    'string' => [PropertyValue::STRING('foo')],
    'text' => [PropertyValue::TEXT('foo')],
    'datetime' => [PropertyValue::DATETIME('2021-08-14T00:00:00.000Z')],
    'color' => [PropertyValue::COLOR('#574A2E')],
    'boolean' => [PropertyValue::BOOLEAN(false)],
]);

test('get original value image', function () {
    $model = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE)->createOne();
    $oldValue = $model->getValue();
    $newValue = PropertyDirectoryValue::factory()->make()->getValue();

    $model->setValue($newValue);

    expect($model->getOriginalValue())->toEqual($oldValue);
});

test('change image raises FileReleased', function () {
    $model = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE)->createOne();
    $newValue = PropertyDirectoryValue::factory()
        ->withType(PropertyType::IMAGE)
        ->make(['value' => '/001/003/new_image.png'])
        ->getValue();

    $model->setValue($newValue);
    $model->save();

    Event::assertDispatched(FileReleased::class);
});

test('change external image not raises FileReleased', function () {
    $model = PropertyDirectoryValue::factory()
        ->withType(PropertyType::IMAGE)
        ->createOne(['value' => 'https://ya.ru/images?key=001']);

    $newValue = PropertyDirectoryValue::factory()
        ->withType(PropertyType::IMAGE)
        ->make(['value' => '/001/003/new_image.png'])
        ->getValue();

    $model->setValue($newValue);
    $model->save();

    Event::assertNotDispatched(FileReleased::class);
});

test('delete value with image raises FileReleased', function () {
    $model = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE)->createOne();

    $model->delete();

    Event::assertDispatched(FileReleased::class);
});

test('get original value file', function () {
    $model = PropertyDirectoryValue::factory()->withType(PropertyType::FILE)->createOne();
    $oldValue = $model->getValue();
    $newValue = PropertyDirectoryValue::factory()->make()->getValue();

    $model->setValue($newValue);

    expect($model->getOriginalValue())->toEqual($oldValue);
});

test('change file raises FileReleased', function () {
    $model = PropertyDirectoryValue::factory()->withType(PropertyType::FILE)->createOne();
    $newValue = PropertyDirectoryValue::factory()
        ->withType(PropertyType::FILE)
        ->make(['value' => '/001/003/new_text.txt'])
        ->getValue();

    $model->setValue($newValue);
    $model->save();

    Event::assertDispatched(FileReleased::class);
});

test('change external file not raises FileReleased', function () {
    $model = PropertyDirectoryValue::factory()
        ->withType(PropertyType::FILE)
        ->createOne(['value' => 'https://www.dundeecity.gov.uk/sites/default/files/publications/civic_renewal_forms.zip']);

    $newValue = PropertyDirectoryValue::factory()
        ->withType(PropertyType::FILE)
        ->make(['value' => '/001/003/new_file.txt'])
        ->getValue();

    $model->setValue($newValue);
    $model->save();

    Event::assertNotDispatched(FileReleased::class);
});

test('delete value with file raises FileReleased', function () {
    $model = PropertyDirectoryValue::factory()->withType(PropertyType::FILE)->createOne();

    $model->delete();

    Event::assertDispatched(FileReleased::class);
});
