<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\Tests\Factories\ActualCategoryPropertyFactory;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * The model for the attribute category (taking into account inheritance).
 *
 * @property int $category_id Category ID
 * @property int $property_id Property ID
 * @property bool $is_required The attribute must be filled in
 * @property bool $is_gluing The property is used for gluing products
 * @property bool $is_inherited The property is inherited from parent category
 * @property bool $is_common The property is common to all categories
 *
 * @property-read Category $category
 * @property-read Property $property
 */
class ActualCategoryProperty extends Model
{
    protected $fillable = ['property_id', 'category_id', 'is_required', 'is_gluing', 'is_inherited', 'is_common'];
    protected $table = 'actual_category_properties';

    protected $casts = [
        'property_id' => 'int',
        'category_id' => 'int',
        'is_required' => 'bool',
        'is_gluing' => 'bool',
        'is_inherited' => 'bool',
        'is_common' => 'bool',
    ];

    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function toSample(?bool $isInherited = null): CategoryPropertySample
    {
        return new CategoryPropertySample(
            $this->property_id,
            $this->is_required,
            $isInherited ?? $this->is_inherited,
            $this->is_common,
            $this->is_gluing
        );
    }

    public function fillFromSample(CategoryPropertySample $source): static
    {
        $this->fill([
            'property_id' => $source->propertyId,
            'is_required' => $source->isRequired,
            'is_inherited' => $source->isInherited,
            'is_common' => $source->isCommon,
            'is_gluing' => $source->isGluing,
        ]);

        return $this;
    }

    public static function fromSample(CategoryPropertySample $source): static
    {
        return (new static())->fillFromSample($source);
    }

    public static function factory(): ActualCategoryPropertyFactory
    {
        return ActualCategoryPropertyFactory::new();
    }
}
