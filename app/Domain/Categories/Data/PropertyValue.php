<?php

namespace App\Domain\Categories\Data;

use App\Domain\Classifiers\Enums\PropertyType;
use Carbon\CarbonInterface;
use Exception;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;
use Webmozart\Assert\Assert;

/**
 * @method static static STRING(mixed $value = null)
 * @method static static TEXT(mixed $value = null)
 * @method static static INTEGER(mixed $value = null)
 * @method static static DOUBLE(mixed $value = null)
 * @method static static BOOLEAN(mixed $value = null)
 * @method static static DATETIME(mixed $value = null)
 * @method static static IMAGE(mixed $value = null)
 * @method static static FILE(mixed $value = null)
 * @method static static COLOR(mixed $value = null)
 */
class PropertyValue
{
    private const MIN_DATETIME_STRING_LENGTH = 8;
    private const PATTERN_COLOR = '/^#([a-f0-9]{3}|[a-f0-9]{6})$/i';

    protected mixed $value = null;

    public function __construct(protected PropertyType $type, mixed $value = null)
    {
        $this->value = $this->convert($value);
    }

    public function native(): mixed
    {
        return $this->value;
    }

    public function type(): PropertyType
    {
        return $this->type;
    }

    public function isNull(): bool
    {
        return $this->value === null;
    }

    public function isFile(): bool
    {
        return ($this->type === PropertyType::IMAGE || $this->type === PropertyType::FILE)
            && !blank($this->value)
            && !is_absolute_url($this->value);
    }

    public function equalTo(?PropertyValue $other): bool
    {
        if ($other === null) {
            return false;
        }

        if ($other === $this) {
            return true;
        }

        return $this->type() === $other->type() && $this->value == $other->value;
    }

    public function __toString(): string
    {
        return match (true) {
            $this->type === PropertyType::DATETIME => $this->value->toJSON(),
            $this->type === PropertyType::BOOLEAN => $this->value === true ? 'true' : 'false',
            default => (string)$this->value
        };
    }

    public static function make(PropertyType $type, mixed $value = null): static
    {
        return new static($type, $value);
    }

    public static function validate(PropertyType $type, mixed $value, string $attribute = 'value'): static
    {
        $rules = match ($type) {
            PropertyType::INTEGER => ['integer'],
            PropertyType::DOUBLE => ['numeric'],
            PropertyType::BOOLEAN => ['boolean'],
            PropertyType::DATETIME => ['date'],
            PropertyType::COLOR => ['regex:' . self::PATTERN_COLOR],
            default => ['string'],
        };

        Validator::validate([$attribute => $value], [$attribute => $rules]);

        return static::make($type, $value);
    }

    private function convert(mixed $source): mixed
    {
        if ($source === null) {
            return null;
        }

        return match ($this->type) {
            PropertyType::INTEGER => is_numeric($source) ? (int)$source : null,
            PropertyType::DOUBLE => is_numeric($source) ? (float)$source : null,
            PropertyType::BOOLEAN => is_bool($source) ? $source : parse_bool((string)$source),
            PropertyType::DATETIME => $this->convertToDateTime($source),
            PropertyType::COLOR => $this->convertToColor($source),
            default => (string)$source,
        };
    }

    private function convertToDateTime(mixed $source): ?CarbonInterface
    {
        if ($source instanceof CarbonInterface) {
            return $source;
        }

        if (!is_string($source) || strlen($source) < self::MIN_DATETIME_STRING_LENGTH) {
            return null;
        }

        try {
            return Date::make($source);
        } catch (Exception) {
            return null;
        }
    }

    private function convertToColor(mixed $value): ?string
    {
        if (!is_string($value)) {
            return null;
        }

        return preg_match(self::PATTERN_COLOR, $value) === 1 ? $value : null;
    }

    public static function __callStatic(string $name, array $arguments)
    {
        Assert::countBetween($arguments, 0, 1);

        $type = PropertyType::from(strtolower($name));

        return static::make($type, head($arguments));
    }
}
