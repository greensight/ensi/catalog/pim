<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Classifiers\Enums\PropertyType;
use Carbon\CarbonInterface;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\ValidationException;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('category', 'unit');

test('create primitive type', function (PropertyType $type, mixed $value, mixed $expected) {
    expect(PropertyValue::make($type, $value)->native())->toBe($expected);
})->with(function () {
    yield 'integer' => [PropertyType::INTEGER, '1540', 1540];
    yield 'double' => [PropertyType::DOUBLE, '254.45', 254.45];
    yield 'string' => [PropertyType::STRING, 'foo', 'foo'];
    yield 'text' => [PropertyType::TEXT, 'foo', 'foo'];
    yield 'true' => [PropertyType::BOOLEAN, 'true', true];
    yield 'false' => [PropertyType::BOOLEAN, 'false', false];
    yield 'image file' => [PropertyType::IMAGE, EnsiFile::factory()->fileName('main')->fileExt('jpg')->make()['path'], EnsiFile::factory()->fileName('main')->fileExt('jpg')->make()['path']];
    yield 'common file' => [PropertyType::FILE, EnsiFile::factory()->fileName('main')->fileExt('txt')->make()['path'], EnsiFile::factory()->fileName('main')->fileExt('txt')->make()['path']];
});

test('create datetime type', function (mixed $value, ?CarbonInterface $expected) {
    expect(PropertyValue::make(PropertyType::DATETIME, $value)->native())->toEqual($expected);
})->with([
    'valid date' => ['2021-05-09', Date::make('2021-05-09')],
    'valid formatted date' => ['9.8.2021', Date::make('2021-08-09')],
    'valid datetime' => ['2021-05-09T15:47:11.000Z', Date::make('2021-05-09T15:47:11.000Z')],
    'valid instance' => [Date::make('2021-09-01T20:00:08'), Date::make('2021-09-01T20:00:08')],
    'invalid integer' => [19_000_524, null],
    'invalid string' => ['foo', null],
]);

test('create color type', function (mixed $value, ?string $expected) {
    expect(PropertyValue::make(PropertyType::COLOR, $value)->native())->toEqual($expected);
})->with([
    'valid color' => ['#45454A', '#45454A'],
    'valid short' => ['#fff', '#fff'],
    'invalid value' => ['foo', null],
    'invalid character' => ['#0000S8', null],
]);

test('equal', function (PropertyValue $first, PropertyValue $second) {
    expect($first->equalTo($second))->toBeTrue();
})->with([
    'same type both nulls' => [PropertyValue::INTEGER(), PropertyValue::INTEGER()],
    'same type same value' => [PropertyValue::DOUBLE(50), PropertyValue::DOUBLE(50)],
    'datetime' => [PropertyValue::DATETIME('2021-09-01'), PropertyValue::DATETIME('2021-09-01')],
]);

test('not equal', function (PropertyValue $first, ?PropertyValue $second) {
    expect($first->equalTo($second))->toBeFalse();
})->with([
    'null value' => [PropertyValue::STRING('foo'), PropertyValue::STRING()],
    'null instance' => [PropertyValue::STRING('foo'), null],
    'different types' => [PropertyValue::COLOR('#aaa'), PropertyValue::STRING('#aaa')],
    'different values' => [PropertyValue::DATETIME('2021-09-01'), PropertyValue::DATETIME('2021-09-02')],
]);

test('validate success', function (PropertyType $type, mixed $value) {
    expect(PropertyValue::validate($type, $value)->equalTo(PropertyValue::make($type, $value)))->toBeTrue();
})->with(function () {
    yield 'integer' => [PropertyType::INTEGER, 597];
    yield 'double' => [PropertyType::DOUBLE, 1024.14];
    yield 'string' => [PropertyType::STRING, 'foo'];
    yield 'text' => [PropertyType::TEXT, 'foo'];
    yield 'boolean' => [PropertyType::BOOLEAN, false];
    yield 'datetime' => [PropertyType::DATETIME, '2021-09-24T19:34:11.000Z'];
    yield 'image' => [PropertyType::IMAGE, EnsiFile::factory()->fileName('image1')->fileExt('gif')->make()['path']];
    yield 'file' => [PropertyType::FILE, EnsiFile::factory()->fileName('main')->fileExt('txt')->make()['path']];
    yield 'color' => [PropertyType::COLOR, '#888888'];
});

test('validate failed', function (PropertyType $type, mixed $value) {
    expect(fn () => PropertyValue::validate($type, $value))
        ->toThrow(ValidationException::class);
})->with([
    'integer' => [PropertyType::INTEGER, 'foo'],
    'double' => [PropertyType::DOUBLE, 'foo'],
    'boolean' => [PropertyType::BOOLEAN, 15],
    'datetime' => [PropertyType::DATETIME, 25],
    'color' => [PropertyType::COLOR, '#19R'],
]);
