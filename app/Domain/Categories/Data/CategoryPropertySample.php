<?php

namespace App\Domain\Categories\Data;

class CategoryPropertySample
{
    public const PRIORITY_INHERITED = 1;
    public const PRIORITY_SELF = 2;
    public const PRIORITY_REQUIRED = 3;
    public const PRIORITY_COMMON = 4;

    public function __construct(
        public int $propertyId,
        public bool $isRequired,
        public bool $isInherited = false,
        public bool $isCommon = false,
        public bool $isGluing = false
    ) {
    }

    public function priority(): int
    {
        return match (true) {
            $this->isCommon => self::PRIORITY_COMMON,
            $this->isRequired => self::PRIORITY_REQUIRED,
            $this->isInherited => self::PRIORITY_INHERITED,
            default => self::PRIORITY_SELF,
        };
    }

    public static function own(int $propertyId, bool $isRequired): self
    {
        return new self($propertyId, $isRequired);
    }

    public static function common(int $propertyId, bool $isRequired): self
    {
        return new self($propertyId, $isRequired, false, true);
    }

    public static function inherited(int $propertyId, bool $isRequired): self
    {
        return new self($propertyId, $isRequired, true);
    }
}
