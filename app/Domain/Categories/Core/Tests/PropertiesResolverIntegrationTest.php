<?php

use App\Domain\Categories\Core\PropertiesResolver;
use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

test('resolve self properties', function () {
    $category = Category::factory()->createOne();
    $link = CategoryPropertyLink::factory()->forCategory($category)->createOne();

    expect((new PropertiesResolver())->resolve($category)->all())
        ->toEqual([CategoryPropertySample::own($link->property_id, $link->is_required)]);
});

test('not ignores inactive self properties', function () {
    $category = Category::factory()->createOne();
    $property = Property::factory()->inactive()->createOne();
    CategoryPropertyLink::factory()->createFast($category, $property);

    expect((new PropertiesResolver())->resolve($category))->toHaveCount(1);
});

test('resolve common properties', function () {
    $category = Category::factory()->createOne();
    $property = Property::factory()->common()->createOne();

    expect((new PropertiesResolver())->resolve($category)->all())
        ->toEqual([CategoryPropertySample::common($property->id, $property->is_required)]);
});

test('ignores inactive common properties', function () {
    $category = Category::factory()->createOne();
    Property::factory()->common()->inactive()->createOne();

    expect((new PropertiesResolver())->resolve($category))->toBeEmpty();
});

test('resolve parent properties', function () {
    $parent = Category::factory()->createOne();
    $category = Category::factory()->withParent($parent)->createOne();
    $actual = ActualCategoryProperty::factory()->forCategory($parent)->createOne();

    expect((new PropertiesResolver())->resolve($category, $parent)->all())
        ->toEqual([CategoryPropertySample::inherited($actual->property_id, $actual->is_required)]);
});

test('ignore parent properties when inheritance disabled', function () {
    $parent = Category::factory()->createOne();
    $category = Category::factory()->withParent($parent)->createOne(['is_inherits_properties' => false]);
    ActualCategoryProperty::factory()->forCategory($parent)->createOne();

    expect((new PropertiesResolver())->resolve($category, $parent))->toBeEmpty();
});

test('ignore common properties from parent', function () {
    $parent = Category::factory()->createOne();
    $category = Category::factory()->withParent($parent)->createOne();
    $property = Property::factory()->common()->inactive()->createOne();
    ActualCategoryProperty::factory()->common()->createFast($parent, $property);

    expect((new PropertiesResolver())->resolve($category, $parent))->toBeEmpty();
});

test('parent required properties replaces self not required', function () {
    $parent = Category::factory()->createOne();
    $category = Category::factory()->withParent($parent)->createOne();
    $property = Property::factory()->createOne();

    ActualCategoryProperty::factory()->createFast($parent, $property, true);
    CategoryPropertyLink::factory()->createFast($category, $property, false);

    expect((new PropertiesResolver())->resolve($category, $parent)->all())
        ->toEqual([CategoryPropertySample::inherited($property->id, true)]);
});

test('self required properties replaces any parent properties', function () {
    $parent = Category::factory()->createOne();
    $category = Category::factory()->withParent($parent)->createOne();
    $property = Property::factory()->createOne();

    ActualCategoryProperty::factory()->createFast($parent, $property, true, false);
    CategoryPropertyLink::factory()->createFast($category, $property, true, false);

    expect((new PropertiesResolver())->resolve($category, $parent)->all())
        ->toEqual([CategoryPropertySample::own($property->id, true)]);
});
