<?php

namespace App\Domain\Categories\Core;

use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use App\Support\Lazy;
use Illuminate\Support\Collection;

class PropertiesResolver
{
    /** @var Lazy|Collection|CategoryPropertySample[] */
    private Lazy|Collection $commonProperties;

    public function __construct()
    {
        $this->commonProperties = new Lazy(fn () => $this->loadCommonProperties());
    }

    /**
     * @return Collection|CategoryPropertySample[]
     */
    public function resolve(Category $category, ?Category $parent = null): Collection
    {
        return $this->getSelfProperties($category)
            ->when($category->is_inherits_properties, fn ($target) => $this->mergeInheritedProperties($target, $parent))
            ->pipe(fn ($target) => $this->mergeCommonProperties($target))
            ->values();
    }

    private function loadCommonProperties(): Collection
    {
        return Property::common()->active()->get()->toBase();
    }

    private function mergeInheritedProperties(Collection $target, ?Category $parent): Collection
    {
        if ($parent === null) {
            return $target;
        }

        $inherited = $this->getInheritableProperties($parent);

        foreach ($inherited as $item) {
            $target[$item->propertyId] = $this->resolveProperty($item, $target->get($item->propertyId));
        }

        return $target;
    }

    private function mergeCommonProperties(Collection $target): Collection
    {
        /** @var Property $property */
        foreach ($this->commonProperties->value() as $property) {
            $target[$property->id] = CategoryPropertySample::common($property->id, $property->is_required);
        }

        return $target;
    }

    private function getSelfProperties(Category $category): Collection
    {
        return $category->propertyLinks()
            ->with('property')
            ->get()
            ->mapWithKeys(fn (CategoryPropertyLink $link) => [$link->property_id => $link->toSample()])
            ->toBase();
    }

    /**
     * @return Collection|CategoryPropertySample[]
     */
    private function getInheritableProperties(Category $parent): Collection
    {
        return $parent->actualAllProperties
            ->where('is_common', false)
            ->map(fn (ActualCategoryProperty $prop) => $prop->toSample(true));
    }

    private function resolveProperty(CategoryPropertySample $candidate, ?CategoryPropertySample $existing): CategoryPropertySample
    {
        return $candidate->priority() > ($existing?->priority() ?? 0) ? $candidate : $existing;
    }
}
