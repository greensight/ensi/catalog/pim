<?php

namespace App\Domain\Categories\Listeners;

use App\Domain\Categories\Actions\Categories\ActualizeCategoryAction;
use App\Domain\Categories\Events\CategoryInvalidated;

class ActualizeCategoryListener
{
    public function __construct(private ActualizeCategoryAction $action)
    {
    }

    public function handle(CategoryInvalidated $event): void
    {
        $this->action->execute($event->categoryId);
    }
}
