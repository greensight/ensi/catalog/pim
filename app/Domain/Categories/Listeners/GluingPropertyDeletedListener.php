<?php

namespace App\Domain\Categories\Listeners;

use App\Domain\Categories\Events\GluingPropertyDeleted;
use App\Domain\Products\Actions\ProductGroups\DeleteProductGroupAction;
use App\Domain\Products\Models\ProductGroup;

class GluingPropertyDeletedListener
{
    public function __construct(private DeleteProductGroupAction $action)
    {
    }

    public function handle(GluingPropertyDeleted $event): void
    {
        $productGroups = ProductGroup::query()->where('category_id', $event->link->category_id)->get();
        foreach ($productGroups as $productGroup) {
            $this->action->execute($productGroup->id);
        }
    }
}
