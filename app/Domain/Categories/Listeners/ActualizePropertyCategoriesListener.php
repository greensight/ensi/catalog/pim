<?php

namespace App\Domain\Categories\Listeners;

use App\Domain\Categories\Actions\Categories\ActualizeCategoryAction;
use App\Domain\Categories\Events\PropertyInvalidated;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Support\Log\CategoriesLoggerAwareInterface;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Psr\Log\LoggerAwareTrait;

class ActualizePropertyCategoriesListener implements ShouldQueue, CategoriesLoggerAwareInterface
{
    use LoggerAwareTrait;

    public bool $afterCommit = true;

    public function __construct(private ActualizeCategoryAction $action)
    {
    }

    public function handle(PropertyInvalidated $event): void
    {
        $property = Property::find($event->propertyId);

        if (!$property) {
            return;
        }

        foreach ($this->loadCategories($property) as $categoryId) {
            try {
                $this->action->execute($categoryId);
            } catch (Exception $e) {
                $message = "Не удалось актуализировать категорию: \"{$e->getMessage()}\"";
                $this->logger?->error($message, ['id' => $categoryId]);
            }
        }
    }

    private function loadCategories(Property $property): array
    {
        return $property->is_common
            ? $this->queryRootCategories()
            : $this->queryLinkedCategories($property);
    }

    private function queryRootCategories(): array
    {
        return Category::whereIsRoot()
            ->pluck('id')
            ->all();
    }

    private function queryLinkedCategories(Property $property): array
    {
        return $property->categoryPropertyLinks()
            ->pluck('category_id')
            ->all();
    }
}
