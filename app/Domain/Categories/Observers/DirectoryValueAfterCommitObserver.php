<?php

namespace App\Domain\Categories\Observers;

use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Kafka\Actions\Send\SendPropertyDirectoryValueEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class DirectoryValueAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendPropertyDirectoryValueEventAction $sendPropertyDirectoryValueEventAction
    ) {
    }

    public function created(PropertyDirectoryValue $model): void
    {
        $this->sendPropertyDirectoryValueEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(PropertyDirectoryValue $model): void
    {
        $this->sendPropertyDirectoryValueEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(PropertyDirectoryValue $model): void
    {
        $this->sendPropertyDirectoryValueEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
