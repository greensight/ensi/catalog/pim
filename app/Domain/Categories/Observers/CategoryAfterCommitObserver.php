<?php

namespace App\Domain\Categories\Observers;

use App\Domain\Categories\Models\Category;
use App\Domain\Kafka\Actions\Send\SendCategoryEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class CategoryAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendCategoryEventAction $sendCategoryEventAction
    ) {
    }

    public function created(Category $model): void
    {
        $this->sendCategoryEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(Category $model): void
    {
        $this->sendCategoryEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(Category $model): void
    {
        $this->sendCategoryEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
