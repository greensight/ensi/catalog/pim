<?php

namespace App\Domain\Categories\Observers;

use App\Domain\Categories\Models\Property;
use App\Domain\Kafka\Actions\Send\SendPropertyEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class PropertyAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendPropertyEventAction $sendPropertyEventAction
    ) {
    }

    public function created(Property $model): void
    {
        $this->sendPropertyEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(Property $model): void
    {
        $this->sendPropertyEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(Property $model): void
    {
        $this->sendPropertyEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
