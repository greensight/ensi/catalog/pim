<?php

namespace App\Domain\Categories\Observers;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Kafka\Actions\Send\SendActualCategoryPropertyEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class ActualCategoryPropertyAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendActualCategoryPropertyEventAction $sendActualCategoryPropertyEventAction
    ) {
    }

    public function created(ActualCategoryProperty $model): void
    {
        $this->sendActualCategoryPropertyEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(ActualCategoryProperty $model): void
    {
        $this->sendActualCategoryPropertyEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(ActualCategoryProperty $model): void
    {
        $this->sendActualCategoryPropertyEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
