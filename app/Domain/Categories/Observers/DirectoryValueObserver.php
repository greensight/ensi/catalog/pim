<?php

namespace App\Domain\Categories\Observers;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Support\Events\FileReleased;

class DirectoryValueObserver
{
    public function updated(PropertyDirectoryValue $model): void
    {
        $oldValue = $model->getOriginalValue();
        if (!$this->isFileChanged($oldValue, $model->getValue())) {
            return;
        }

        $this->raiseEvent($oldValue->native());
    }

    public function deleted(PropertyDirectoryValue $model): void
    {
        $oldValue = $model->getOriginalValue();
        if (!$this->isFileChanged($oldValue)) {
            return;
        }

        $this->raiseEvent($oldValue->native());
    }

    private function raiseEvent(string $fileName): void
    {
        if (is_absolute_url($fileName)) {
            return;
        }

        FileReleased::dispatch($fileName);
    }

    private function isFileChanged(?PropertyValue $oldValue, ?PropertyValue $newValue = null): bool
    {
        if ($oldValue === null || $oldValue->isNull()) {
            return false;
        }

        if ($oldValue->type() !== PropertyType::IMAGE && $oldValue->type() !== PropertyType::FILE) {
            return false;
        }

        if ($newValue !== null && $oldValue->equalTo($newValue)) {
            return false;
        }

        return true;
    }
}
