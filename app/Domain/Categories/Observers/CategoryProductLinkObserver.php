<?php

namespace App\Domain\Categories\Observers;

use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Kafka\Actions\Send\SendCategoryProductLinksEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class CategoryProductLinkObserver
{
    public function __construct(
        protected SendCategoryProductLinksEventAction $eventAction
    ) {
    }

    public function created(CategoryProductLink $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(CategoryProductLink $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(CategoryProductLink $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::DELETE);
    }
}
