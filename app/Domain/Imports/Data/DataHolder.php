<?php

namespace App\Domain\Imports\Data;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use Illuminate\Container\Container;
use Illuminate\Support\Collection;

class DataHolder extends Container
{
    public Collection $brands;
    public Collection $categories;
    public Collection $vendorCodes;
    public Collection $codes;
    public Collection $barcodes;

    public function setData(array $brandIds, array $categoryIds, array $vendorCodes, array $codes, array $barcodes): void
    {
        $this->brands = Brand::query()->whereIn('id', $brandIds)->get('id')->keyBy('id');
        $this->categories = Category::query()->whereIn('id', $categoryIds)->get('id')->keyBy('id');
        $this->vendorCodes = Product::query()->whereIn('vendor_code', $vendorCodes)->get('vendor_code')->keyBy('vendor_code');
        $this->codes = Product::query()->whereIn('code', $codes)->get('code')->keyBy('code');
        $this->barcodes = Product::query()->whereIn('barcode', $barcodes)->get('barcode')->keyBy('barcode');
    }
}
