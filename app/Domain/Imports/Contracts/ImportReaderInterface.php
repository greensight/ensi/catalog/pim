<?php

namespace App\Domain\Imports\Contracts;

use App\Domain\Imports\Models\ProductImport;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

interface ImportReaderInterface extends
    ToArray,
    WithChunkReading,
    WithEvents,
    WithStartRow,
    WithHeadingRow,
    SkipsOnError,
    SkipsOnFailure
{
    public function getImport(): ProductImport;
}
