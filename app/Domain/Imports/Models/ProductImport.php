<?php

namespace App\Domain\Imports\Models;

use App\Domain\Imports\Models\Tests\Factories\ProductImportFactory;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property ProductImportTypeEnum $type Тип импорта
 * @property ProductImportStatusEnum $status Статус импорта
 * @property string $file Путь к файлу
 * @property int|null $chunks_count Количество запланированных чанков
 * @property int $chunks_finished Количество обработанных чанков
 *
 * @property-read Collection<ProductImportWarning> $warnings
 */
class ProductImport extends Model
{
    protected $table = 'product_imports';

    protected $fillable = ['type', 'status', 'file'];

    protected $casts = [
        'type' => ProductImportTypeEnum::class,
        'status' => ProductImportStatusEnum::class,
    ];

    public function warnings(): HasMany
    {
        return $this->hasMany(ProductImportWarning::class, 'import_id', 'id');
    }

    public static function factory(): ProductImportFactory
    {
        return ProductImportFactory::new();
    }
}
