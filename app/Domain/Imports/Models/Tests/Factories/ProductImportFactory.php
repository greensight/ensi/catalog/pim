<?php

namespace App\Domain\Imports\Models\Tests\Factories;

use App\Domain\Imports\Models\ProductImport;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<ProductImport>
 */
class ProductImportFactory extends BaseModelFactory
{
    protected $model = ProductImport::class;

    public function definition(): array
    {
        $extension = $this->faker->randomElement(['.csv', '.xls', '.xlsx']);
        $filepath = $this->faker->filePath() . $extension;

        return [
            'type' => $this->faker->randomEnum(ProductImportTypeEnum::cases()),
            'status' => $this->faker->randomEnum(ProductImportStatusEnum::cases()),
            'file' => $filepath,
            'chunks_count' => $this->faker->nullable()->numberBetween(0, 1000),
            'chunks_finished' => $this->faker->numberBetween(0, 1000),
        ];
    }

    public function withStatus(ProductImportStatusEnum $status): static
    {
        return $this->state(['status' => $status]);
    }

    public function withType(ProductImportTypeEnum $type): static
    {
        return $this->state(['type' => $type]);
    }
}
