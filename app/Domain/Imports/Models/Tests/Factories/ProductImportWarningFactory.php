<?php

namespace App\Domain\Imports\Models\Tests\Factories;

use App\Domain\Imports\Models\ProductImport;
use App\Domain\Imports\Models\ProductImportWarning;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<ProductImportWarning>
 */
class ProductImportWarningFactory extends BaseModelFactory
{
    protected $model = ProductImportWarning::class;

    public function definition(): array
    {
        return [
            'import_id' => ProductImport::factory(),
            'vendor_code' => $this->faker->nullable()->uuid(),
            'import_type' => $this->faker->randomEnum(ProductImportTypeEnum::cases()),
            'message' => $this->faker->sentence(),
        ];
    }
}
