<?php

namespace App\Domain\Imports\Models;

use App\Domain\Imports\Models\Tests\Factories\ProductImportWarningFactory;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $import_id ID импорта
 * @property ProductImportTypeEnum $import_type Тип импорта
 * @property string|null $vendor_code Артикул сущности
 * @property string $message Сообщение
 *
 * @property-read ProductImport $import
 */
class ProductImportWarning extends Model
{
    protected $table = 'product_import_warnings';

    protected $casts = [
        'import_type' => ProductImportTypeEnum::class,
    ];

    public function import(): BelongsTo
    {
        return $this->belongsTo(ProductImport::class, 'import_id', 'id');
    }

    public static function factory(): ProductImportWarningFactory
    {
        return ProductImportWarningFactory::new();
    }
}
