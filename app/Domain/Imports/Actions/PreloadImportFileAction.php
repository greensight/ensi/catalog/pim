<?php

namespace App\Domain\Imports\Actions;

use App\Domain\Support\Actions\UploadFileAction;
use App\Domain\Support\Models\TempFile;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;

class PreloadImportFileAction
{
    public function __construct(
        protected readonly UploadFileAction $action,
    ) {
    }

    public function execute(UploadedFile $uploadedFile): TempFile
    {
        return $this->action
            ->setDiskType(Filesystem::VISIBILITY_PRIVATE)
            ->execute($uploadedFile, 'import/products');
    }
}
