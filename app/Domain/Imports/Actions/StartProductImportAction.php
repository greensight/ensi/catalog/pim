<?php

namespace App\Domain\Imports\Actions;

use App\Domain\Imports\Models\ProductImport;
use App\Domain\Imports\Readers\ImportReaderFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueueableAction\QueueableAction;
use Throwable;

class StartProductImportAction
{
    use QueueableAction;

    public function __construct(
        private readonly ImportReaderFactory $readerFactory,
        private readonly EnsiFilesystemManager $fs,
    ) {
    }

    public function execute(int $importId): void
    {
        $import = ProductImport::findOrFail($importId);

        if ($import->status !== ProductImportStatusEnum::NEW) {
            return;
        }

        try {
            $reader = $this->readerFactory->get($import);
            $disk = Storage::disk($this->fs->protectedDiskName());

            Excel::import(
                $reader,
                $disk->path($import->file),
            );
        } catch (Throwable $e) {
            logger()->channel('products_import')->error(
                'Error occurred during product import:' . $e->getMessage(),
            );

            $import->status = ProductImportStatusEnum::FAILED;
            $import->save();
        }
    }
}
