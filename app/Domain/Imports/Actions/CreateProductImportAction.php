<?php

namespace App\Domain\Imports\Actions;

use App\Domain\Imports\Models\ProductImport;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;

class CreateProductImportAction
{
    use AppliesToAggregate;

    public function execute(ProductImportTypeEnum $type, int $preloadFileId): ProductImport
    {
        return $this->create([
            'file' => TempFile::grab($preloadFileId),
            'type' => $type,
            'status' => ProductImportStatusEnum::NEW,
        ]);
    }

    protected function createModel(): Model
    {
        return new ProductImport();
    }
}
