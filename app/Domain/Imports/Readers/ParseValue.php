<?php

namespace App\Domain\Imports\Readers;

trait ParseValue
{
    protected function parseBool(mixed $value): ?bool
    {
        return match (true) {
            is_bool($value) => $value,
            strcasecmp($value, 'true') == 0 || $value === 1 || $value === '1' => true,
            strcasecmp($value, 'false') == 0 || $value === 0 || $value === '0' => false,
            default => null,
        };
    }
}
