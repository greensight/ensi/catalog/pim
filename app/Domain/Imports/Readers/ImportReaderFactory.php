<?php

namespace App\Domain\Imports\Readers;

use App\Domain\Imports\Contracts\ImportReaderInterface;
use App\Domain\Imports\Models\ProductImport;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;

class ImportReaderFactory
{
    public function get(ProductImport $import): ImportReaderInterface
    {
        return resolve(
            match ($import->type) {
                ProductImportTypeEnum::PRODUCT => ProductReader::class,
            },
            ['import' => $import],
        );
    }
}
