<?php

namespace App\Domain\Imports\Readers;

use App\Domain\Imports\Contracts\ImportReaderInterface;
use App\Domain\Imports\Data\DataHolder;
use App\Domain\Imports\Models\ProductImport;
use App\Domain\Imports\Models\ProductImportWarning;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;

abstract class ImportReader implements ImportReaderInterface
{
    use Importable;
    use RegistersEventListeners;
    use ParseValue;

    abstract protected function handle(array $rows): void;

    public function __construct(
        protected readonly ProductImport $import,
    ) {
    }

    public function getImport(): ProductImport
    {
        return $this->import;
    }

    public function array(array $array): void
    {
        $this->handle($array);

        $this->import->chunks_finished++;
        $this->import->save();
    }

    public function chunkSize(): int
    {
        return 500;
    }

    public function startRow(): int
    {
        return 2;
    }

    public function onFailure(Failure ...$failures): void
    {
        foreach ($failures as $failure) {
            $vendorCode = data_get($failure->values(), 'vendor_code');

            foreach ($failure->errors() as $message) {
                $this->createWarning(
                    $this->import->id,
                    $vendorCode,
                    $message,
                    $this->import->type,
                );
            }
        }
    }

    public function onError(Throwable $e): void
    {
        $this->import->status = ProductImportStatusEnum::FAILED;
        $this->import->save();
    }

    protected function createWarning(
        int $importId,
        ?string $vendorCode,
        string $message,
        ProductImportTypeEnum $type,
    ): void {
        $warning = new ProductImportWarning();
        $warning->import_id = $importId;
        $warning->vendor_code = $vendorCode;
        $warning->message = $message;
        $warning->import_type = $type;

        $warning->save();
    }

    public static function beforeImport(BeforeImport $event): void
    {
        /** @var static $importReader */
        $importReader = $event->getConcernable();

        $sheetReader = $event->getReader();
        $sheetReader->readSpreadsheet();

        $totalRows = iterator_count(
            $sheetReader
                ->getActiveSheet()
                ->getRowIterator()
        );

        $totalRows -= $importReader->startRow() - 1;

        if ($totalRows > 0) {
            $importModel = $importReader->getImport();

            $importModel->chunks_count = (int) ceil($totalRows / $importReader->chunkSize());
            $importModel->chunks_finished = 0;
            $importModel->status = ProductImportStatusEnum::IN_PROCESS;
            $importModel->save();
        }
    }

    public static function beforeSheet(BeforeSheet $event): void
    {
        /** @var static $importReader */
        $importReader = $event->getConcernable();
        $firstRow = $importReader->startRow();

        $sheet = $event->getSheet();
        $totalRows = iterator_count($sheet->getRowIterator());

        $brandIds = [];
        $categoryIds = [];
        $codes = [];
        $vendorCodes = [];
        $barcodes = [];
        foreach ($sheet->getColumnIterator() as $column) {
            $columnIndex = $column->getColumnIndex();
            $columnName = $sheet->getCell("{$columnIndex}1");

            if ($columnName == 'brand_id') {
                $brandIds = $sheet->getDelegate()->rangeToArray("{$columnIndex}{$firstRow}:{$columnIndex}{$totalRows}");
                $brandIds = collect($brandIds)->flatten()->filter(fn (mixed $brandId) => (string)(int)$brandId === $brandId)->toArray();

                continue;
            }

            if ($columnName == 'category_ids') {
                $categoryIds = $sheet->getDelegate()->rangeToArray("{$columnIndex}{$firstRow}:{$columnIndex}{$totalRows}");
                $categoryIds = collect($categoryIds)
                    // Parsing categories into individual values
                    ->flatten()
                    ->filter()
                    ->map(fn (string $categoryIds) => explode(';', $categoryIds))
                    ->flatten()
                    // Preparing values for SQL query
                    ->map(fn (mixed $categoryId) => is_string($categoryId) ? trim($categoryId) : $categoryId)
                    ->filter(fn (mixed $categoryId) => (string)(int)$categoryId === $categoryId)
                    ->unique()
                    ->toArray();

                continue;
            }

            if ($columnName == 'barcode') {
                $barcodes = $sheet->getDelegate()->rangeToArray("{$columnIndex}{$firstRow}:{$columnIndex}{$totalRows}");

                continue;
            }

            if ($columnName == 'code') {
                $codes = $sheet->getDelegate()->rangeToArray("{$columnIndex}{$firstRow}:{$columnIndex}{$totalRows}");

                continue;
            }

            if ($columnName == 'vendor_code') {
                $vendorCodes = $sheet->getDelegate()->rangeToArray("{$columnIndex}{$firstRow}:{$columnIndex}{$totalRows}");
            }
        }

        app(DataHolder::class)->setData(
            brandIds: $brandIds,
            categoryIds: $categoryIds,
            vendorCodes: $vendorCodes,
            codes: $codes,
            barcodes: $barcodes,
        );
    }

    public static function afterImport(AfterImport $event): void
    {
        /** @var static $importReader */
        $importReader = $event->getConcernable();
        $importModel = $importReader->getImport();

        $importModel->status = ProductImportStatusEnum::DONE;
        $importModel->save();
    }
}
