<?php

use App\Domain\Imports\Models\ProductImport;
use App\Domain\Imports\Readers\ProductReader;
use App\Domain\Imports\Readers\Tests\Factories\ProductImportFileFactory;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Downloader;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductImportTypeEnum;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('import', 'product-import', 'integration');

it('imports products without warnings', function () {
    /** @var IntegrationTestCase $this */

    /** @var EnsiFilesystemManager $fileManager */
    $fileManager = resolve(EnsiFilesystemManager::class);

    Storage::fake($fileManager->publicDiskName());

    $downloader = $this->mock(Downloader::class);
    $downloader->expects('downloadFile')->andReturnUsing(function (string $url) {
        $filePath = '/tmp/' . uniqid();
        file_put_contents($filePath, basename($url));

        return $filePath;
    });

    /** @var ProductImport $import */
    $import = ProductImport::factory()
        ->withStatus(ProductImportStatusEnum::NEW)
        ->withType(ProductImportTypeEnum::PRODUCT)
        ->createOne([
        'file' => 'storage/products.csv',
    ]);

    $file = UploadedFile::fake()
        ->createWithContent('products.csv', ProductImportFileFactory::new()->make());

    Excel::import(resolve(ProductReader::class, ['import' => $import]), $file);

    $import->refresh();

    expect($import->chunks_count)->toBe(1)
        ->and($import->chunks_finished)->toBe(1)
        ->and($import->status)->toEqual(ProductImportStatusEnum::DONE)
        ->and($import->warnings)->toBeEmpty();
});

it('creates warnings when product exists', function (): void {
    $vendorCode = 'BAC-100165';

    $commonAttributes = [
        'vendor_code' => $vendorCode,
        'code' => 'maika',
        'barcode' => '100012314234',
    ];

    $product = Product::factory()->create($commonAttributes);

    /** @var ProductImport $import */
    $import = ProductImport::factory()
        ->withStatus(ProductImportStatusEnum::NEW)
        ->withType(ProductImportTypeEnum::PRODUCT)
        ->createOne([
            'file' => 'storage/products.csv',
        ]);

    $contents = ProductImportFileFactory::new()->make($commonAttributes);

    $file = UploadedFile::fake()
        ->createWithContent('products.csv', $contents);

    Excel::import(resolve(ProductReader::class, ['import' => $import]), $file);

    $import->refresh();

    expect($import->chunks_count)->toBe(1)
        ->and($import->chunks_finished)->toBe(1)
        ->and($import->status)->toEqual(ProductImportStatusEnum::DONE);

    $warnings = $import->warnings;

    expect($warnings)->toHaveCount(3)
        ->and($warnings->where('message', __('validation.unique', ['attribute' => 'vendor_code'])))
        ->toHaveCount(1)
        ->and($warnings->where('vendor_code', $vendorCode))
        ->toHaveCount(3)
        ->and($warnings->where('import_type', ProductImportTypeEnum::PRODUCT))
        ->toHaveCount(3);

    assertDatabaseHas(Product::class, [
        'id' => $product->id,
        'vendor_code' => $product->vendor_code,
        'name' => $product->name,
        'code' => $product->code,
    ]);
});

it('creates warnings without vendor_code', function (): void {
    $commonAttributes = [
        'vendor_code' => null,
    ];

    /** @var ProductImport $import */
    $import = ProductImport::factory()
        ->withStatus(ProductImportStatusEnum::NEW)
        ->withType(ProductImportTypeEnum::PRODUCT)
        ->createOne([
            'file' => 'storage/products.csv',
        ]);

    $contents = ProductImportFileFactory::new()->make($commonAttributes);

    $file = UploadedFile::fake()
        ->createWithContent('products.csv', $contents);

    Excel::import(resolve(ProductReader::class, ['import' => $import]), $file);

    $import->refresh();

    expect($import->chunks_count)->toBe(1)
        ->and($import->status)->toEqual(ProductImportStatusEnum::DONE);

    $warnings = $import->warnings;

    expect($warnings)
        ->toHaveCount(1)
        ->and($warnings->where('message', __('validation.required', ['attribute' => 'vendor_code'])))
        ->toHaveCount(1)
        ->and($warnings->where('vendor_code', null))
        ->toHaveCount(1)
        ->and($warnings->where('import_type', ProductImportTypeEnum::PRODUCT))
        ->toHaveCount(1);
});

it('creates warnings when category doesn\'t exist', function (): void {
    $commonAttributes = [
        'category_ids' => '404',
    ];

    /** @var ProductImport $import */
    $import = ProductImport::factory()
        ->withStatus(ProductImportStatusEnum::NEW)
        ->withType(ProductImportTypeEnum::PRODUCT)
        ->createOne([
            'file' => 'storage/products.csv',
        ]);

    $contents = ProductImportFileFactory::new()->make($commonAttributes);

    $file = UploadedFile::fake()
        ->createWithContent('products.csv', $contents);

    Excel::import(resolve(ProductReader::class, ['import' => $import]), $file);

    $import->refresh();

    expect($import->chunks_count)->toBe(1)
        ->and($import->status)->toEqual(ProductImportStatusEnum::DONE);

    $warnings = $import->warnings;

    expect($warnings)
        ->toHaveCount(1)
        ->and($warnings->where('message', __('validation.exists', ['attribute' => 'category_ids.0'])))
        ->toHaveCount(1)
        ->and($warnings->where('import_type', ProductImportTypeEnum::PRODUCT))
        ->toHaveCount(1);
});
