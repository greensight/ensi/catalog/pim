<?php

namespace App\Domain\Imports\Readers\Tests\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductImportFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'brand_id' => Brand::factory()->create()->id,
            'category_ids' => '"' . implode(';', $this->faker->randomList(fn () => Category::factory()->create()->id, 1)) . '"',
            'barcode' => $this->faker->nullable()->ean13(),
            'name' => $this->faker->word(),
            'code' => $this->faker->unique()->nullable()->slug(),
            'description' => $this->faker->nullable()->text(),
            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->nullable()->randomElement(['true', 'false']),
            'allow_publish' => $this->faker->nullable()->randomElement(['true', 'false']),
            'vendor_code' => $this->faker->numerify('###-###-###'),
            'type' => $this->faker->randomEnum(ProductType::cases()),
            'image' => $this->faker->imageUrl(),
        ];
    }

    public function make(array $extra = []): string
    {
        $headingRow = join(';', array_keys($this->definition()));
        $values = join(';', $this->makeArray($extra));

        return $headingRow . PHP_EOL . $values;
    }
}
