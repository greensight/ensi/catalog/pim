<?php

namespace App\Domain\Imports\Readers;

use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Imports\Models\ProductImport;
use App\Domain\Products\Actions\Products\CreateProductAction;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Actions\DownloadFileAction;
use App\Domain\Support\Rules\BarcodeUnique;
use App\Domain\Support\Rules\BrandExists;
use App\Domain\Support\Rules\CategoryExists;
use App\Domain\Support\Rules\CodeUnique;
use App\Domain\Support\Rules\VendorCodeUnique;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductReader extends ImportReader implements WithValidation
{
    public function __construct(
        ProductImport $import,
        protected readonly CreateProductAction $createProductAction,
        protected readonly DownloadFileAction $downloadAction,
    ) {
        parent::__construct($import);
    }

    protected function handle(array $rows): void
    {
        foreach ($rows as $key => $row) {
            $product = $this->createProductAction->execute(array_filter($row));

            if ($row['image'] !== null) {
                $this->downloadAndSetImages($product->id, $row);
            }
        }
    }

    private function downloadAndSetImages(int $productId, array $row): void
    {
        $imageUrls = new Collection(
            explode(',', $row['image'])
        );

        if ($imageUrls->isEmpty()) {
            return;
        }

        $imageUrls
            ->transform(fn (string $url) => $this->downloadAction->execute(Str::random(20), $url, 'products'))
            ->each(fn (string $path, int $i) => $this->setImage($productId, $path, $i + 1))
            ->filter();

        if ($imageUrls->isNotEmpty()) {
            ProductInvalidated::dispatch($productId);
        }
    }

    private function setImage(int $productId, string $path, int $sort): void
    {
        $image = new ProductImage();
        $image->product_id = $productId;
        $image->file = $path;
        $image->sort = $sort;

        $image->save();
    }

    public function prepareForValidation(array $data, int $index): array
    {
        $data['allow_publish'] = $this->parseBool(data_get($data, 'allow_publish'));
        $data['is_adult'] = $this->parseBool(data_get($data, 'is_adult'));
        $data['category_ids'] = explode(';', data_get($data, 'category_ids'));

        return $data;
    }

    public function rules(): array
    {
        return [
            'brand_id' => ['nullable', 'integer', new BrandExists()],
            'category_ids' => ['required', 'array'],
            'category_ids.*' => ['required_with:category_ids.*', 'integer', new CategoryExists()],
            'vendor_code' => ['required', 'distinct', new VendorCodeUnique()],
            'name' => ['required'],
            'type' => ['required', 'integer', Rule::enum(ProductType::class)],
            'allow_publish' => ['nullable', 'boolean'],
            'code' => ['nullable', 'distinct', new CodeUnique()],
            'description' => ['nullable'],
            'barcode' => ['nullable', 'distinct', new BarcodeUnique()],
            'weight' => ['nullable', 'numeric'],
            'weight_gross' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'is_adult' => ['nullable', 'boolean'],
        ];
    }
}
