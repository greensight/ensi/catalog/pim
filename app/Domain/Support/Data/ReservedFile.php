<?php

namespace App\Domain\Support\Data;

use App\Domain\Support\Models\TempFile;

class ReservedFile
{
    public function __construct(public TempFile $copy, public TempFile $prototype)
    {
    }
}
