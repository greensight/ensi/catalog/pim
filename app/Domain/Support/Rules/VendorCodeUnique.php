<?php

namespace App\Domain\Support\Rules;

use App\Domain\Imports\Data\DataHolder;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class VendorCodeUnique implements ValidationRule
{
    /**
     * @param string $attribute
     * @param int|null $value
     * @param Closure $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($value === null) {
            return;
        }

        if (app(DataHolder::class)->vendorCodes->get($value) === null) {
            return;
        }

        $fail(__('validation.unique'));
    }
}
