<?php

namespace App\Domain\Support\Rules;

use App\Domain\Imports\Data\DataHolder;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CategoryExists implements ValidationRule
{
    /**
     * @param string $attribute
     * @param int|string|null $value
     * @param Closure $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($value === null) {
            return;
        }

        $value = is_string($value) ? trim($value) : $value;
        if (app(DataHolder::class)->categories->get($value) !== null) {
            return;
        }

        $fail(__('validation.exists'));
    }
}
