<?php

namespace App\Domain\Support\Jobs;

use App\Domain\Support\Data\ReservedFile;
use App\Support\Log\DefaultLoggerAwareInterface;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Psr\Log\LoggerAwareTrait;

class DistributeFileContentsJob implements ShouldQueue, DefaultLoggerAwareInterface
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use LoggerAwareTrait;

    public function __construct(private readonly array|Collection $files)
    {
    }

    public function handle(EnsiFilesystemManager $fileManager): void
    {
        $disk = Storage::disk($fileManager->publicDiskName());

        foreach ($this->files as $file) {
            if (!$file instanceof ReservedFile) {
                continue;
            }
            $result = $disk->copy($file->prototype->path, $file->copy->path);

            if (!$result) {
                $this->logger?->warning("Не удалось копировать из {$file->prototype->path} в {$file->copy->path}");
            }
        }
    }
}
