<?php

use App\Domain\Support\Data\ReservedFile;
use App\Domain\Support\Jobs\DistributeFileContentsJob;
use App\Domain\Support\Models\TempFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('support', 'integration');

test('Job DistributeFileContents success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    $diskName = $fs->publicDiskName();
    Storage::fake($diskName);

    $disk = Storage::disk($diskName);

    $prototypePath = '/path/to/image.png';
    $content = 'content';
    $disk->put($prototypePath, $content);

    $prototype = TempFile::create(['path' => $prototypePath]);

    $reservedPath = '/path/to/image_copy.png';
    $reservedFile = TempFile::create(['path' => $reservedPath]);

    $input = [
        new ReservedFile($reservedFile, $prototype),
    ];

    Storage::disk($diskName)->assertMissing($reservedPath);

    DistributeFileContentsJob::dispatchSync($input);

    // Файл успешно скопирован
    Storage::disk($diskName)->assertExists($prototypePath);
    Storage::disk($diskName)->assertExists($reservedPath);
    assertEquals($disk->get($reservedPath), $content);
});
