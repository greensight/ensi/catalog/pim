<?php

namespace App\Domain\Support;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class Downloader
{
    private const TMP_DIR = '/tmp/downloads';

    private ?Client $client = null;

    private function getHttpClient(): Client
    {
        return $this->client ??= new Client();
    }

    public function downloadFile(string $url): ?string
    {
        $this->createDirectory();
        $tmpFileName = self::TMP_DIR . '/' . md5($url);

        $this->getHttpClient()->get($url, [
            RequestOptions::VERIFY => false,
            RequestOptions::SINK => $tmpFileName,
        ]);

        if (file_exists($tmpFileName) && filesize($tmpFileName) > 0) {
            return $tmpFileName;
        }

        return null;
    }

    private function createDirectory(): void
    {
        if (!file_exists(self::TMP_DIR)) {
            mkdir(self::TMP_DIR, 0777, true);
        }
    }
}
