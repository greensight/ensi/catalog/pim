<?php

namespace App\Domain\Support\Models\Tests\Factories;

use App\Domain\Support\Models\TempFile;
use Ensi\LaravelTestFactories\BaseModelFactory;

class TempFileFactory extends BaseModelFactory
{
    protected $model = TempFile::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'path' => $this->faker->filePath(),
            'created_at' => $this->faker->dateTime(),
        ];
    }
}
