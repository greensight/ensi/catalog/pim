<?php

namespace App\Domain\Support\Observers;

use App\Domain\Support\Concerns\SetsUserAttributesInterface;
use Ensi\InitialEventPropagation\InitialEventDTO;
use Ensi\LaravelInitialEventPropagation\InitialEventHolderFacade;

class CurrentUserObserver
{
    protected function getCurrentUser(): ?string
    {
        /** @var InitialEventDTO|null $event */
        $event = InitialEventHolderFacade::getInitialEvent();

        if ($event === null) {
            return null;
        }

        if (blank($event->userId)) {
            return null;
        }

        return $event->userId;
    }

    public function creating(SetsUserAttributesInterface $model)
    {
        $user = $this->getCurrentUser();

        $model->setCreatedBy($user);
    }
}
