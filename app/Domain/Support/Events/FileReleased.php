<?php

namespace App\Domain\Support\Events;

use Illuminate\Foundation\Events\Dispatchable;

class FileReleased
{
    use Dispatchable;

    public function __construct(public string $path)
    {
    }
}
