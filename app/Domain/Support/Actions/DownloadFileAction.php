<?php

namespace App\Domain\Support\Actions;

use App\Domain\Support\Downloader;
use App\Exceptions\DownloadFileException;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;

class DownloadFileAction
{
    public function __construct(
        protected Downloader $downloader,
        protected EnsiFilesystemManager $fileManager,
    ) {
    }

    public function execute(string $name, ?string $url, string $folder): string|bool|null
    {
        if (!$url) {
            return null;
        }

        $tmpFile = $this->downloader->downloadFile($url);

        if (!$tmpFile) {
            throw new DownloadFileException("Временный файл не существует или пуст: {$url}");
        }

        $tail = pathinfo($url, PATHINFO_EXTENSION);
        $pair = explode('?', $tail);
        $ext = current($pair);

        $filename = "{$name}.{$ext}";

        $newFileDir = "$folder/" . $this->fileManager->getHashedDirsForFileName($filename);

        $disk = Storage::disk($this->fileManager->publicDiskName());

        return $disk->putFileAs($newFileDir, $tmpFile, $filename);
    }
}
