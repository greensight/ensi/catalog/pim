<?php

namespace App\Domain\Classifiers\Observers;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Kafka\Actions\Send\SendBrandEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class BrandAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendBrandEventAction $sendBrandEventAction
    ) {
    }

    public function created(Brand $model): void
    {
        $this->sendBrandEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(Brand $model): void
    {
        $this->sendBrandEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(Brand $model): void
    {
        $this->sendBrandEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
