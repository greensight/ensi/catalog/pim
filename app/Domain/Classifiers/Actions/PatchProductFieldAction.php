<?php

namespace App\Domain\Classifiers\Actions;

use App\Domain\Classifiers\Models\ProductFieldSettings;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class PatchProductFieldAction
{
    use AppliesToAggregate;

    public function execute(int $id, array $fields): ProductFieldSettings
    {
        return $this->patch($id, $fields);
    }

    protected function createModel(): Model
    {
        return new ProductFieldSettings();
    }
}
