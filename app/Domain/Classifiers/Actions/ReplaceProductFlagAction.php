<?php

namespace App\Domain\Classifiers\Actions;

use App\Domain\Classifiers\Models\ProductFlagSettings;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class ReplaceProductFlagAction
{
    use AppliesToAggregate;

    public function execute(int $id, array $fields): ProductFlagSettings
    {
        return $this->replace($id, $fields);
    }

    protected function createModel(): Model
    {
        return new ProductFlagSettings();
    }
}
