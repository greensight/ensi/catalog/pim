<?php

namespace App\Domain\Classifiers\Actions\ProductStatuses;

use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Support\Concerns\InteractsWithModels;

class SetPreviousProductStatusesAction
{
    use InteractsWithModels;

    public function execute(int $statusId, array $previousStatusIds): ProductStatusSetting
    {
        /** @var ProductStatusSetting $productStatus */
        $productStatus = ProductStatusSetting::query()
            ->with(['previousStatuses'])
            ->findOrFail($statusId);


        $this->transaction(fn () => $this->updateLinks($productStatus, $previousStatusIds));
        $productStatus->load('previousStatuses');

        return $productStatus;
    }

    private function updateLinks(ProductStatusSetting $productStatus, array $previousStatusIds): void
    {
        $existing = $productStatus->previousStatuses->keyBy('id');

        foreach ($previousStatusIds as $previousStatusId) {
            if ($existing->has($previousStatusId)) {
                continue;
            }

            ProductStatusLink::makeLink($previousStatusId, $productStatus->id);
        }

        $deleteStatusIds = array_diff($existing->keys()->all(), $previousStatusIds);

        foreach ($deleteStatusIds as $deleteStatusId) {
            ProductStatusLink::dropLink($deleteStatusId, $productStatus->id);
        }
    }
}
