<?php

namespace App\Domain\Classifiers\Actions\ProductStatuses\Data;

class ProductStatusData
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly bool $available,
    ) {
    }
}
