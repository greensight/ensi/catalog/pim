<?php

namespace App\Domain\Classifiers\Actions\ProductStatuses;

use App\Domain\Classifiers\Models\ProductStatusSetting;

class CreateProductStatusSettingAction
{
    public function execute(array $fields): ProductStatusSetting
    {
        $productStatusSetting = new ProductStatusSetting();
        $productStatusSetting->fill($fields);
        $productStatusSetting->save();

        return $productStatusSetting;
    }
}
