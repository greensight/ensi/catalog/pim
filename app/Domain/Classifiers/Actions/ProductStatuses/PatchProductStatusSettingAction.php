<?php

namespace App\Domain\Classifiers\Actions\ProductStatuses;

use App\Domain\Classifiers\Models\ProductStatusSetting;

class PatchProductStatusSettingAction
{
    public function execute(int $id, array $fields): ProductStatusSetting
    {
        /** @var ProductStatusSetting $productStatusSetting */
        $productStatusSetting = ProductStatusSetting::query()->findOrFail($id);
        $productStatusSetting->update($fields);

        return $productStatusSetting;
    }
}
