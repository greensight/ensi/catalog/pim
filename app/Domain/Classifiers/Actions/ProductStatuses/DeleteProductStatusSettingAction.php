<?php

namespace App\Domain\Classifiers\Actions\ProductStatuses;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Models\Product;
use App\Exceptions\IllegalOperationException;

class DeleteProductStatusSettingAction
{
    public function execute(int $id): void
    {
        $countProduct = Product::query()->where('status_id', $id)->count();

        if ($countProduct > 0) {
            throw new IllegalOperationException('Нельзя удалить статус, так как он установлен у продуктов');
        }

        ProductStatusSetting::destroy($id);
    }
}
