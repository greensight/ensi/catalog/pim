<?php

namespace App\Domain\Classifiers\Actions\ProductStatuses;

use App\Domain\Classifiers\Actions\ProductStatuses\Data\ProductStatusData;
use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use Illuminate\Support\Collection;

class GetNextProductStatusesAction
{
    public function execute(?int $currentStatusId = null, ProductStatusTypeEnum $type = ProductStatusTypeEnum::MANUAL): Collection
    {
        return $currentStatusId == null ? $this->loadFirstStatus($type) : $this->loadNextStatus($currentStatusId, $type);
    }

    protected function loadFirstStatus(ProductStatusTypeEnum $type): Collection
    {
        $unavailableStatus = ProductStatusLink::query()
            ->get()
            ->keyBy('new_status_id');

        $productStatus = $this->loadProductStatusSettings($type);

        return $productStatus->map(
            fn (ProductStatusSetting $s) => new ProductStatusData($s->id, $s->name, !$unavailableStatus->has($s->id))
        );
    }

    protected function loadNextStatus(int $currentStatusId, ProductStatusTypeEnum $type): Collection
    {
        $availableStatus = ProductStatusLink::query()
            ->where('last_status_id', $currentStatusId)
            ->get()
            ->keyBy('new_status_id');

        $productStatus = $this->loadProductStatusSettings($type);

        return $productStatus->map(
            fn (ProductStatusSetting $s) => new ProductStatusData($s->id, $s->name, $availableStatus->has($s->id))
        );
    }

    protected function loadProductStatusSettings(ProductStatusTypeEnum $type): Collection
    {
        return ProductStatusSetting::query()
            ->where('is_active', '=', true)
            ->where('type', '=', $type)
            ->get();
    }
}
