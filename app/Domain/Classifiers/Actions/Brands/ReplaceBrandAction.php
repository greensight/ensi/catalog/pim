<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;

class ReplaceBrandAction
{
    use AppliesToBrand;

    public function execute(int $brandId, array $fields): Brand
    {
        return $this->updateOrCreate($brandId, function (Brand $brand) use ($fields) {
            $fields = $this->checkImageId($fields);

            if (!isset($fields['logo_file']) && !isset($fields['logo_url'])) {
                $fields['logo_file'] = $brand->logo_file;
            }

            $brand->fill(
                data_combine_assoc($brand->getFillable(), $fields)
            );
        });
    }
}
