<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Support\Actions\UploadFileAction;
use App\Domain\Support\Models\TempFile;
use Illuminate\Http\UploadedFile;

class PreloadBrandImageAction
{
    public function __construct(
        private readonly UploadFileAction $action
    ) {
    }

    public function execute(UploadedFile $uploadedFile): TempFile
    {
        return $this->action->execute($uploadedFile, 'brands');
    }
}
