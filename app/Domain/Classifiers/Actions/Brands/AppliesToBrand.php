<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Domain\Support\Models\TempFile;

trait AppliesToBrand
{
    use AppliesToAggregate;

    protected function checkImageId(array $fields): array
    {
        if (isset($fields['preload_file_id'])) {
            $fields['logo_file'] = TempFile::grab($fields['preload_file_id']);
        }

        return $fields;
    }

    protected function createModel(): Model
    {
        return new Brand();
    }
}
