<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedProduct;
use App\Exceptions\IllegalOperationException;

class DeleteBrandAction
{
    use AppliesToBrand;

    public function execute(int $brandId): void
    {
        $this->delete($brandId, function (Brand $brand) {
            $hasProducts = Product::whereBrandId($brand->id)->exists()
                || PublishedProduct::whereBrandId($brand->id)->exists();

            throw_if(
                $hasProducts,
                IllegalOperationException::class,
                "Бренд [{$brand->id}] \"{$brand->name}\" используется в товарах. Удаление запрещено."
            );
        });
    }
}
