<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;

class DeleteBrandImageAction
{
    use AppliesToBrand;

    public function execute(int $brandId): Brand
    {
        return $this->updateOrCreate($brandId, function (Brand $brand) {
            $brand->logo_file = null;
        });
    }
}
