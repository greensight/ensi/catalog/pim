<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;

class CreateBrandAction
{
    use AppliesToBrand;

    public function execute(array $fields): Brand
    {
        return $this->create($this->checkImageId($fields));
    }
}
