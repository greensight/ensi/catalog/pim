<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Support\Concerns\HandlesMassOperation;

class DeleteManyBrandsAction
{
    use HandlesMassOperation;

    public function __construct(private DeleteBrandAction $deleteAction)
    {
    }

    public function execute(array $brandIds): void
    {
        $this->each($brandIds, function (int $brandId) {
            $this->deleteAction->execute($brandId);
        });
    }
}
