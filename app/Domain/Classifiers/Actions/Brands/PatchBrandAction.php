<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;

class PatchBrandAction
{
    use AppliesToBrand;

    public function execute(int $brandId, array $fields): Brand
    {
        return $this->patch($brandId, $this->checkImageId($fields));
    }
}
