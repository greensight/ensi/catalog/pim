<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Actions\UploadFileAction;
use Illuminate\Http\UploadedFile;

class SaveBrandImageAction
{
    use AppliesToBrand;

    public function __construct(private UploadFileAction $fileAction)
    {
    }

    public function execute(int $brandId, UploadedFile $file): Brand
    {
        $tempFile = $this->fileAction->execute($file, 'brands', "brand_{$brandId}");

        return $this->updateOrCreate(
            $brandId,
            function (Brand $brand) use ($tempFile) {
                $brand->logo_file = $tempFile->evict();
            }
        );
    }
}
