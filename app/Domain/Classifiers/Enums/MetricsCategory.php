<?php

namespace App\Domain\Classifiers\Enums;

enum MetricsCategory: string
{
    case MASTER = 'master';
    case CONTENT = 'content';
    case ATTRIBUTES = 'attributes';
}
