<?php

namespace App\Domain\Classifiers\Enums;

/**
 * Поддерживаемые типы данных свойств.
 */
enum PropertyType: string
{
    case STRING = 'string';
    case TEXT = 'text';
    case INTEGER = 'integer';
    case DOUBLE = 'double';
    case BOOLEAN = 'boolean';
    case DATETIME = 'datetime';
    case IMAGE = 'image';
    case FILE = 'file';
    case COLOR = 'color';
}
