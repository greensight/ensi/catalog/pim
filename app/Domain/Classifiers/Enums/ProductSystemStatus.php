<?php

namespace App\Domain\Classifiers\Enums;

use Illuminate\Support\Arr;

enum ProductSystemStatus: int
{
    case REJECTED = 1;
    case CLOSED = 2;
    case DELETED = 3;

    public static function hiddenStatus(): array
    {
        return [
            self::DELETED,
        ];
    }

    public static function notHiddenStatuses(): array
    {
        return Arr::where(self::cases(), fn (self $status) => !in_array($status, self::hiddenStatus()));
    }
}
