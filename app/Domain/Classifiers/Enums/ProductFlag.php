<?php

namespace App\Domain\Classifiers\Enums;

enum ProductFlag: int
{
    case MODERATION = 1;
    case CONTENT = 2;
    case NOT_SENT_FOR_MODERATION = 3;
    case NOT_AGREED = 4;
    case REQUIRED_ATTRIBUTES = 5;
    case REQUIRED_MASTER_DATA = 6;
    case IN_SALE = 7;
    case NOT_SALE = 8;
}
