<?php

namespace App\Domain\Classifiers\Enums;

enum ProductType: int
{
    case PIECE = 1;
    case WEIGHT = 2;
    case PACKED = 3;
}
