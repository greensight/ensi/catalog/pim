<?php

namespace App\Domain\Classifiers\Models;

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Support\Models\Model;

/**
 * @property string $code Код поля
 * @property int $edit_mask Флаги разрешения редактирования отдельных настроек
 * @property string $name Имя поля
 * @property bool $is_moderated Признак модерируемого поля
 * @property bool $is_required Признак обязательности заполнения
 * @property MetricsCategory $metrics_category Категория метрик
 */
class ProductFieldSettings extends Model
{
    public const EDIT_NAME = 0b00000001;
    public const EDIT_MODERATED = 0b00000010;
    public const EDIT_REQUIRED = 0b00000100;
    public const EDIT_METRICS = 0b00001000;

    private static $maskMap = [
        self::EDIT_NAME => 'name',
        self::EDIT_MODERATED => 'is_moderated',
        self::EDIT_REQUIRED => 'is_required',
        self::EDIT_METRICS => 'metrics_category',
    ];

    protected $table = 'product_fields';

    protected $casts = [
        'edit_mask' => 'int',
        'is_moderated' => 'bool',
        'is_required' => 'bool',
        'metrics_category' => MetricsCategory::class,
    ];

    public function getFillable(): array
    {
        $result = [];
        $mask = $this->edit_mask ?? 0;

        foreach (self::$maskMap as $bit => $name) {
            if ($mask & $bit) {
                $result[] = $name;
            }
        }

        return $result;
    }

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public static function findByCode(string $code): ?self
    {
        return self::query()->where('code', $code)->first();
    }
}
