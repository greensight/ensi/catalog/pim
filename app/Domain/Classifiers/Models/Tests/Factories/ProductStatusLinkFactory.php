<?php

namespace App\Domain\Classifiers\Models\Tests\Factories;

use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<ProductStatusLink>
 */
class ProductStatusLinkFactory extends BaseModelFactory
{
    protected $model = ProductStatusLink::class;

    public function definition(): array
    {
        return [
            'last_status_id' => ProductStatusSetting::factory(),
            'new_status_id' => ProductStatusSetting::factory(),
        ];
    }

    public function withStatuses(ProductStatusSetting $current, ProductStatusSetting $next): static
    {
        return $this->state([
            'last_status_id' => $current->id,
            'new_status_id' => $next->id,
        ]);
    }
}
