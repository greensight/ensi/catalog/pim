<?php

namespace App\Domain\Classifiers\Models\Tests\Factories;

use App\Domain\Classifiers\Models\Brand;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method Brand createOne(array $fields = [])
 * @method Brand makeOne(array $fields = [])
 * @method Brand|Brand[]|Collection create(array $fields = [])
 */
class BrandFactory extends BaseModelFactory
{
    protected $model = Brand::class;

    public function definition(): array
    {
        $fileId = $this->faker->unique()->numberBetween(100, 999);

        return [
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'is_active' => true,
            'description' => $this->faker->optional()->sentence,
            'logo_file' => "catalog/brands/logo_{$fileId}.png",
        ];
    }

    public function withExternalLogo(): self
    {
        return $this->state(fn () => [
            'logo_url' => $this->faker->url,
            'logo_file' => null,
        ]);
    }
}
