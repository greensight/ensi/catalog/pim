<?php

namespace App\Domain\Classifiers\Models\Tests\Factories;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\EventOperationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<ProductStatusSetting>
 */
class ProductStatusSettingFactory extends BaseModelFactory
{
    protected $model = ProductStatusSetting::class;

    public function definition(): array
    {
        $type = $this->faker->randomEnum(ProductStatusTypeEnum::cases());
        $isAutomatic = $type == ProductStatusTypeEnum::AUTOMATIC->value;

        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug(),
            'color' => $this->faker->nullable()->hexColor(),
            'type' => $type,
            'is_active' => $this->faker->boolean(),
            'is_publication' => false,
            'events' => $this->faker->nullable($isAutomatic)->exactly([
                'operation' => $this->faker->nullable()->randomEnum(EventOperationEnum::cases()),
                'events' => $this->faker->randomList(fn () => $this->faker->randomEnum(ProductEventEnum::cases()), 1, 3),
            ]),
        ];
    }

    /**
     * @param ProductEventEnum[] $productEvents
     */
    public function activeAutomaticEvents(array $productEvents, ?EventOperationEnum $operation = null): static
    {
        return $this->activeAutomatic()->events($productEvents, $operation);
    }

    /**
     * @param ProductEventEnum[] $productEvents
     */
    public function events(array $productEvents, ?EventOperationEnum $operation = null): static
    {
        return $this->state([
            'events' => [
                'operation' => $operation,
                'events' => $productEvents,
            ],
        ]);
    }

    public function publication(): static
    {
        return $this->state(['is_publication' => true]);
    }

    public function active(bool $active = true): static
    {
        return $this->state(['is_active' => $active]);
    }

    public function manual(): static
    {
        return $this->state([
            'type' => ProductStatusTypeEnum::MANUAL->value,
        ]);
    }

    public function automatic(): static
    {
        return $this->state([
            'type' => ProductStatusTypeEnum::AUTOMATIC->value,
            'events' => [
                'operation' => $this->faker->nullable()->randomEnum(EventOperationEnum::cases()),
                'events' => $this->faker->randomList(fn () => $this->faker->randomEnum(ProductEventEnum::cases()), 1, 3),
            ],
        ]);
    }

    public function activeManual(): static
    {
        return $this->manual()->active();
    }

    public function activeAutomatic(): static
    {
        return $this->automatic()->active();
    }
}
