<?php

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Events\FileReleased;
use Illuminate\Support\Facades\Event;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('classifiers', 'integration');

test('set logo url clears file', function () {
    $brand = Brand::factory()->makeOne();

    $brand->logo_url = 'https://images.com/logos/img1.png';

    expect($brand->logo_file)->toBeNull();
});

test('set logo file clears url', function () {
    $brand = Brand::factory()->withExternalLogo()->makeOne();

    $brand->logo_file = '/catalog/brands/img11.gif';

    expect($brand->logo_url)->toBeNull();
});

test('remove old logo file on update', function () {
    Event::fake(FileReleased::class);
    $brand = Brand::factory()->createOne();

    $brand->logo_file = '/brands/new_file.png';
    $brand->save();

    Event::assertDispatched(FileReleased::class);
});

test('remove logo file on delete', function () {
    Event::fake(FileReleased::class);
    $brand = Brand::factory()->createOne();

    $brand->delete();

    Event::assertDispatched(FileReleased::class);
});

test('ignore old file if empty', function () {
    Event::fake(FileReleased::class);
    $brand = Brand::factory()->withExternalLogo()->createOne();

    $brand->logo_file = '/brands/new_file.png';
    $brand->save();

    Event::assertNotDispatched(FileReleased::class);
});
