<?php

namespace App\Domain\Classifiers\Models;

use App\Domain\Support\Models\Model;

/**
 * @property string $name
 */
class ProductFlagSettings extends Model
{
    protected $table = 'product_flags';
    protected $fillable = ['name'];
    public $incrementing = false;
}
