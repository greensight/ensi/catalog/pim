<?php

namespace App\Domain\Classifiers\Models;

use App\Domain\Classifiers\Models\Tests\Factories\BrandFactory;
use App\Domain\Support\Models\Model;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Brand - бренд производителя.
 *
 * @property bool $is_active Активность бренда
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание бренда
 * @property string|null $logo_file Файл логотипа
 * @property string|null $logo_url URL логотипа
 */
class Brand extends Model
{
    use Sluggable;

    public const FILLABLE = ['name', 'code', 'description', 'logo_file', 'logo_url', 'is_active'];

    protected $table = 'brands';
    protected $fillable = self::FILLABLE;

    protected $casts = [
        'is_active' => 'bool',
    ];

    protected $attributes = [
        'is_active' => false,
    ];

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'name',
                'separator' => '_',
                'unique' => true,
                'maxLength' => 30,
            ],
        ];
    }

    public function isExternalLogo(): bool
    {
        return filled($this->logo_url);
    }

    public function isInternalLogo(): bool
    {
        return filled($this->logo_file);
    }

    public function setLogoFileAttribute(?string $value): void
    {
        $this->attributes['logo_file'] = $value;

        if (!blank($value)) {
            $this->attributes['logo_url'] = null;
        }
    }

    public function setLogoUrlAttribute(?string $value): void
    {
        $this->attributes['logo_url'] = $value;

        if (!blank($value)) {
            $this->attributes['logo_file'] = null;
        }
    }

    public static function factory(): BrandFactory
    {
        return BrandFactory::new();
    }
}
