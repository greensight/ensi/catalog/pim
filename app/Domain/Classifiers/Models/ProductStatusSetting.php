<?php

namespace App\Domain\Classifiers\Models;

use App\Domain\Classifiers\Models\Tests\Factories\ProductStatusSettingFactory;
use App\Domain\Products\Data\EventConditionsData;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * @property string $code - код статуса
 * @property string $name - название статуса
 * @property string|null $color - цвет статуса в формате HEX
 * @property ProductStatusTypeEnum $type - тип статуса
 * @property bool $is_active - активность
 * @property bool $is_publication - флаг по которому у товара проставляется признак активности для витрины
 * @property EventConditionsData|null $events - условие перехода в данный статус
 *
 * @property-read Collection|ProductStatusSetting[] $nextStatuses - статусы доступные к переходу из текущего
 * @property-read Collection|ProductStatusSetting[] $previousStatuses - статусы из которых можно перейти в текущие
 */
class ProductStatusSetting extends Model
{
    use Sluggable;

    protected $table = 'status_settings';

    protected $fillable = ['code', 'name', 'color', 'type', 'is_active', 'is_publication', 'events'];

    protected $casts = [
        'type' => ProductStatusTypeEnum::class,
        'is_active' => 'bool',
        'events' => EventConditionsData::class,
    ];

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'name',
                'separator' => '_',
                'unique' => true,
            ],
        ];
    }

    public function nextStatuses(): BelongsToMany
    {
        return $this->belongsToMany(
            self::class,
            ProductStatusLink::class,
            'last_status_id',
            'new_status_id',
        );
    }

    public function previousStatuses(): BelongsToMany
    {
        return $this->belongsToMany(
            self::class,
            ProductStatusLink::class,
            'new_status_id',
            'last_status_id',
        );
    }

    public static function factory(): ProductStatusSettingFactory
    {
        return ProductStatusSettingFactory::new();
    }
}
