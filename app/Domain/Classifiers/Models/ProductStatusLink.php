<?php

namespace App\Domain\Classifiers\Models;

use App\Domain\Classifiers\Models\Tests\Factories\ProductStatusLinkFactory;
use App\Domain\Support\Models\Model;

/**
 * @property int $last_status_id - текущий статус
 * @property int $new_status_id - статус в который можно перейти
 */
class ProductStatusLink extends Model
{
    protected $table = 'status_links';

    protected $fillable = ['last_status_id', 'new_status_id'];

    protected $casts = [
        'last_status_id' => 'int',
        'new_status_id' => 'int',
    ];

    public static function makeLink(int $currentStatusId, int $nextStatusId): bool
    {
        $self = new self();
        $self->last_status_id = $currentStatusId;
        $self->new_status_id = $nextStatusId;

        return $self->save();
    }

    public static function dropLink(int $currentStatusId, int $nextStatusId): bool
    {
        $self = self::query()
            ->where('last_status_id', $currentStatusId)
            ->where('new_status_id', $nextStatusId);

        return $self->delete();
    }

    public static function factory(): ProductStatusLinkFactory
    {
        return ProductStatusLinkFactory::new();
    }
}
