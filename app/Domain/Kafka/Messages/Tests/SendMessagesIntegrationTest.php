<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Kafka\Messages\Send\ModelEvent\ActualCategoryPropertyPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\BrandPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\CategoryPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\CategoryProductLinksPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ProductGroupPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ProductGroupProductPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ProductPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\PropertyDirectoryValuePayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\PropertyPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\PublishedImagePayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\PublishedProductPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\PublishedPropertyValuePayload;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedProduct;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-send-messages');

test("generate BrandPayload success", function () {
    $model = Brand::factory()->create();

    $payload = new BrandPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate CategoryPayload success", function () {
    $model = Category::factory()->create();

    $payload = new CategoryPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate PublishedImagePayload success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = ProductImage::factory()->published()->create();

    $payload = new PublishedImagePayload($model->release);

    assertEquals($payload->jsonSerialize(), $model->release->attributesToArray());
})->with(FakerProvider::$optionalDataset);

test("generate PublishedProductPayload success", function () {
    $product = Product::factory()->create();
    /** @var PublishedProduct $model */
    $model = PublishedProduct::query()->find($product->id);

    $payload = new PublishedProductPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate PublishedPropertyValuePayload success", function () {
    $model = ProductPropertyValue::factory()->published()->create();

    $payload = new PublishedPropertyValuePayload($model->release);

    assertEquals($payload->jsonSerialize(), $model->release->attributesToArray());
});

test("generate PropertyPayload success", function () {
    $model = Property::factory()->create();

    $payload = new PropertyPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate PropertyDirectoryValuePayload success", function () {
    $model = PropertyDirectoryValue::factory()->create();

    $payload = new PropertyDirectoryValuePayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate ActualCategoryPropertyPayload success", function () {
    $model = ActualCategoryProperty::factory()->create();

    $payload = new ActualCategoryPropertyPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate ProductGroupPayload success", function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $model = ProductGroup::factory()
        ->withCategoryId($category->id)
        ->withMainProductId($product->id)
        ->create();

    $payload = new ProductGroupPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate ProductGroupProductPayload success", function () {
    $model = ProductGroupProduct::factory()->create();

    $payload = new ProductGroupProductPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate ProductPayload success", function () {
    $model = Product::factory()->create();

    $payload = new ProductPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});

test("generate CategoryProductLinksPayload success", function () {
    $model = CategoryProductLink::factory()->create();

    $payload = new CategoryProductLinksPayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
});
