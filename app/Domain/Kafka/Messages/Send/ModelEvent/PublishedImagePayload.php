<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Products\Models\PublishedImage;

class PublishedImagePayload extends Payload
{
    public function __construct(protected PublishedImage $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'product_id' => $this->model->product_id,
            'sort' => $this->model->sort,
            'file' => $this->model->file,
            'name' => $this->model->name,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
