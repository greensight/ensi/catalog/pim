<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Products\Models\ProductGroup;

class ProductGroupPayload extends Payload
{
    public function __construct(protected ProductGroup $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'category_id' => $this->model->category_id,
            'name' => $this->model->name,
            'main_product_id' => $this->model->main_product_id,
            'is_active' => $this->model->is_active,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
