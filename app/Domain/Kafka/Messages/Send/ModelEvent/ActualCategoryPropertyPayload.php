<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Kafka\Messages\Send\Payload;

class ActualCategoryPropertyPayload extends Payload
{
    public function __construct(protected ActualCategoryProperty $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'category_id' => $this->model->category_id,
            'property_id' => $this->model->property_id,
            'is_required' => $this->model->is_required,
            'is_inherited' => $this->model->is_inherited,
            'is_common' => $this->model->is_common,
            'is_gluing' => $this->model->is_gluing,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
