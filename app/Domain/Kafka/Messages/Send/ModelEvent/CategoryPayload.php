<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Categories\Models\Category;
use App\Domain\Kafka\Messages\Send\Payload;

class CategoryPayload extends Payload
{
    public function __construct(protected Category $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'name' => $this->model->name,
            'code' => $this->model->code,
            'parent_id' => $this->model->parent_id,
            'is_active' => $this->model->is_active,
            'is_inherits_properties' => $this->model->is_inherits_properties,
            'is_real_active' => $this->model->is_real_active,
            'full_code' => $this->model->full_code,
            'actualized_at' => $this->model->actualized_at?->toJSON(),

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
