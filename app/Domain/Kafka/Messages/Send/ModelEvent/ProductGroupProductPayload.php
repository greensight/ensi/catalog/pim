<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Products\Models\ProductGroupProduct;

class ProductGroupProductPayload extends Payload
{
    public function __construct(protected ProductGroupProduct $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'product_group_id' => $this->model->product_group_id,
            'product_id' => $this->model->product_id,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
