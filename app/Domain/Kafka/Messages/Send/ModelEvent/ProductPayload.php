<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Products\Models\Product;

class ProductPayload extends Payload
{
    public function __construct(protected Product $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'name' => $this->model->name,
            'code' => $this->model->code,
            'description' => $this->model->description,
            'brand_id' => $this->model->brand_id,
            'external_id' => $this->model->external_id,
            'type' => $this->model->type->value,
            'status_id' => $this->model->status_id,
            'status_comment' => $this->model->status_comment,
            'allow_publish' => $this->model->allow_publish,
            'barcode' => $this->model->barcode,
            'vendor_code' => $this->model->vendor_code,
            'is_adult' => $this->model->is_adult,
            'weight' => $this->model->weight,
            'weight_gross' => $this->model->weight_gross,
            'width' => $this->model->width,
            'height' => $this->model->height,
            'length' => $this->model->length,
            'uom' => $this->model->uom?->value,
            'tariffing_volume' => $this->model->tariffing_volume?->value,
            'order_step' => $this->model->order_step,
            'order_minvol' => $this->model->order_minvol,
            'picking_weight_deviation' => $this->model->picking_weight_deviation,
            'system_status_id' => $this->model->system_status_id?->value,
            'system_status_comment' => $this->model->system_status_comment,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
