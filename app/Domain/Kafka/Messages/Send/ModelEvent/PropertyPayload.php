<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Categories\Models\Property;
use App\Domain\Kafka\Messages\Send\Payload;

class PropertyPayload extends Payload
{
    public function __construct(protected Property $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'name' => $this->model->name,
            'code' => $this->model->code,
            'is_system' => $this->model->is_system,
            'is_filterable' => $this->model->is_filterable,
            'type' => $this->model->type->value,
            'is_active' => $this->model->is_active,
            'has_directory' => $this->model->has_directory,
            'hint_value_name' => $this->model->hint_value_name,
            'is_moderated' => $this->model->is_moderated,
            'hint_value' => $this->model->hint_value,
            'is_multiple' => $this->model->is_multiple,
            'is_required' => $this->model->is_required,
            'display_name' => $this->model->display_name,
            'is_public' => $this->model->is_public,
            'is_common' => $this->model->is_common,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
