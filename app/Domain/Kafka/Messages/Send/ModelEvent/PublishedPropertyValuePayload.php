<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Products\Models\PublishedPropertyValue;

class PublishedPropertyValuePayload extends Payload
{
    public function __construct(protected PublishedPropertyValue $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'deleted_at' => $this->model->deleted_at?->toJSON(),
            'product_id' => $this->model->product_id,
            'property_id' => $this->model->property_id,
            'directory_value_id' => $this->model->directory_value_id,
            'type' => $this->model->type->value,
            'value' => $this->model->value,
            'name' => $this->model->name,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
