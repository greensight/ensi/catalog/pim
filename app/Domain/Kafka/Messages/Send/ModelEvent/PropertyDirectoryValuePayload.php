<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Kafka\Messages\Send\Payload;

class PropertyDirectoryValuePayload extends Payload
{
    public function __construct(protected PropertyDirectoryValue $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'property_id' => $this->model->property_id,
            'type' => $this->model->type->value,
            'name' => $this->model->name,
            'value' => $this->model->value,
            'code' => $this->model->code,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
