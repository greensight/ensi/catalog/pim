<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Kafka\Messages\Send\Payload;

class CategoryProductLinksPayload extends Payload
{
    public function __construct(protected CategoryProductLink $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'product_id' => $this->model->product_id,
            'category_id' => $this->model->category_id,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
