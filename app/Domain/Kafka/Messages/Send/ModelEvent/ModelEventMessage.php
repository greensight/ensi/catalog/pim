<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\KafkaMessage;
use App\Domain\Kafka\Messages\Send\Payload;
use Illuminate\Database\Eloquent\Model;

class ModelEventMessage extends KafkaMessage
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public ?array $dirty;

    public function __construct(Model $model, public Payload $attributes, public string $event, public string $topicKey)
    {
        $this->dirty = $event == static::UPDATE ? array_keys($model->getChanges()) : null;
    }

    public function toArray(): array
    {
        return [
            'dirty' => $this->dirty,
            'attributes' => $this->attributes,
            'event' => $this->event,
        ];
    }

    public function topicKey(): string
    {
        return $this->topicKey;
    }
}
