<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Kafka\Messages\Send\Payload;

class BrandPayload extends Payload
{
    public function __construct(protected Brand $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,

            'name' => $this->model->name,
            'code' => $this->model->code,
            'description' => $this->model->description,
            'is_active' => $this->model->is_active,
            'logo_file' => $this->model->logo_file,
            'logo_url' => $this->model->logo_url,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}
