<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Kafka\Messages\Send\ModelEvent\CategoryProductLinksPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class SendCategoryProductLinksEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(CategoryProductLink $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new CategoryProductLinksPayload($model), $event, 'category-product-links');
        $this->sendAction->execute($modelEvent);
    }
}
