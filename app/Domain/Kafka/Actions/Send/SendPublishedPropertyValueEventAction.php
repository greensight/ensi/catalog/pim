<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\PublishedPropertyValuePayload;
use App\Domain\Products\Models\PublishedPropertyValue;

class SendPublishedPropertyValueEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(PublishedPropertyValue $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new PublishedPropertyValuePayload($model), $event, 'published-property-values');
        $this->sendAction->execute($modelEvent);
    }
}
