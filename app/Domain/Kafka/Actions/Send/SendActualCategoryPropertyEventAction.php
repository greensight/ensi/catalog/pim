<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Kafka\Messages\Send\ModelEvent\ActualCategoryPropertyPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class SendActualCategoryPropertyEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(ActualCategoryProperty $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new ActualCategoryPropertyPayload($model), $event, 'actual-category-properties');
        $this->sendAction->execute($modelEvent);
    }
}
