<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\ProductGroupPayload;
use App\Domain\Products\Models\ProductGroup;

class SendProductGroupEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(ProductGroup $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new ProductGroupPayload($model), $event, 'product-groups');
        $this->sendAction->execute($modelEvent);
    }
}
