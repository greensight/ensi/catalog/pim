<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\ProductPayload;
use App\Domain\Products\Models\Product;

class SendProductEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Product $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new ProductPayload($model), $event, 'products');
        $this->sendAction->execute($modelEvent);
    }
}
