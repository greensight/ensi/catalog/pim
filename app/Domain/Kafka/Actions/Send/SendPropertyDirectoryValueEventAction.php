<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\PropertyDirectoryValuePayload;

class SendPropertyDirectoryValueEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(PropertyDirectoryValue $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new PropertyDirectoryValuePayload($model), $event, 'property-directory-values');
        $this->sendAction->execute($modelEvent);
    }
}
