<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Kafka\Messages\Send\ModelEvent\BrandPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class SendBrandEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Brand $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new BrandPayload($model), $event, 'brands');
        $this->sendAction->execute($modelEvent);
    }
}
