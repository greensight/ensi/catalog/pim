<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\ProductGroupProductPayload;
use App\Domain\Products\Models\ProductGroupProduct;

class SendProductGroupProductEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(ProductGroupProduct $model, string $event): void
    {
        $modelEvent = new ModelEventMessage(
            $model,
            new ProductGroupProductPayload($model),
            $event,
            'product-group-products'
        );
        $this->sendAction->execute($modelEvent);
    }
}
