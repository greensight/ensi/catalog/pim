<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\PublishedImagePayload;
use App\Domain\Products\Models\PublishedImage;

class SendPublishedImageEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(PublishedImage $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new PublishedImagePayload($model), $event, 'published-images');
        $this->sendAction->execute($modelEvent);
    }
}
