<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Categories\Models\Category;
use App\Domain\Kafka\Messages\Send\ModelEvent\CategoryPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;

class SendCategoryEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Category $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new CategoryPayload($model), $event, 'categories');
        $this->sendAction->execute($modelEvent);
    }
}
