<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\PublishedProductPayload;
use App\Domain\Products\Models\PublishedProduct;

class SendPublishedProductEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(PublishedProduct $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new PublishedProductPayload($model), $event, 'published-products');
        $this->sendAction->execute($modelEvent);
    }
}
