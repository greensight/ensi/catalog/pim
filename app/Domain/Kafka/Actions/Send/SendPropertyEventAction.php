<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Categories\Models\Property;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\PropertyPayload;

class SendPropertyEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Property $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new PropertyPayload($model), $event, 'properties');
        $this->sendAction->execute($modelEvent);
    }
}
