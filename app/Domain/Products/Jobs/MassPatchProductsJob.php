<?php

namespace App\Domain\Products\Jobs;

use App\Domain\Common\Models\BulkOperation;
use App\Domain\Products\Actions\Products\PatchManyProductsAction;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MassPatchProductsJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        private readonly BulkOperation $bulk,
        private readonly array $productIds,
        private readonly array $fields,
        private readonly array $attributes,
    ) {
    }

    public function handle(PatchManyProductsAction $action): void
    {
        try {
            $action->execute(
                productIds: $this->productIds,
                fields: $this->fields,
                attributes: $this->attributes,
                bulk: $this->bulk,
            );
        } catch (Exception $e) {
            logger()->channel('products_bulk_operation')->error(
                $e->getMessage(),
                [
                    'product_ids' => $this->productIds,
                    'fields' => $this->fields,
                    'attributes' => $this->attributes,
                ]
            );
        }
    }
}
