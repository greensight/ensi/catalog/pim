<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Common\Models\BulkOperation;
use App\Domain\Products\Jobs\MassPatchProductsJob;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationStatusEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

test('Job MassPatchProducts success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $category = Category::factory()->create();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->create();
    $product = Product::factory()
        ->create(['is_adult' => false]);
    CategoryProductLink::factory()->createFast($category, $product);

    $operation = BulkOperation::factory()
        ->state(['status' => BulkOperationStatusEnum::NEW])
        ->create();

    $attributes = [
        [
            'property_id' => $property->id,
            'value' => 'foo',
        ],
    ];
    MassPatchProductsJob::dispatchSync($operation, [$product->id], ['is_adult' => true], $attributes);

    assertDatabaseHas(BulkOperation::class, ['id' => $operation->id, 'status' => BulkOperationStatusEnum::COMPLETED]);

    assertDatabaseHas(Product::class, ['id' => $product->id, 'is_adult' => true]);

    assertDatabaseHas(ProductPropertyValue::class, [
        'product_id' => $product->id,
        'property_id' => $property->id,
        'value' => 'foo',
    ]);
})->with(FakerProvider::$optionalDataset);
