<?php

namespace App\Domain\Products\Observers;

use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;

class ProductGroupObserver
{
    public function deleting(ProductGroup $model): void
    {
        $model->loadMissing('productLinks');
        $model->productLinks->each(fn (ProductGroupProduct $link) => $link->delete());
    }
}
