<?php

namespace App\Domain\Products\Observers;

use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Events\FileReleased;

class ProductImageObserver
{
    public function deleted(ProductImage $model): void
    {
        if ($model->isInternal()) {
            FileReleased::dispatch($model->file);
        }
    }
}
