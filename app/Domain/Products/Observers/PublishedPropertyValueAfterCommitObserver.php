<?php

namespace App\Domain\Products\Observers;

use App\Domain\Kafka\Actions\Send\SendPublishedPropertyValueEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Products\Models\PublishedPropertyValue;

class PublishedPropertyValueAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendPublishedPropertyValueEventAction $publishedPropertyValueEventAction
    ) {
    }

    public function created(PublishedPropertyValue $model): void
    {
        $this->publishedPropertyValueEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(PublishedPropertyValue $model): void
    {
        $this->publishedPropertyValueEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(PublishedPropertyValue $model): void
    {
        $this->publishedPropertyValueEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
