<?php

namespace App\Domain\Products\Observers;

use App\Domain\Kafka\Actions\Send\SendPublishedImageEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Products\Models\PublishedImage;

class PublishedImageAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendPublishedImageEventAction $publishedImageEventAction
    ) {
    }

    public function created(PublishedImage $model): void
    {
        $this->publishedImageEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(PublishedImage $model): void
    {
        $this->publishedImageEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(PublishedImage $model): void
    {
        $this->publishedImageEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
