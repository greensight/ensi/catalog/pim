<?php

namespace App\Domain\Products\Observers;

use App\Domain\Kafka\Actions\Send\SendProductGroupProductEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Products\Models\ProductGroupProduct;

class ProductGroupProductAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendProductGroupProductEventAction $productGroupProductEventAction
    ) {
    }

    public function created(ProductGroupProduct $model): void
    {
        $this->productGroupProductEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(ProductGroupProduct $model): void
    {
        $this->productGroupProductEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(ProductGroupProduct $model): void
    {
        $this->productGroupProductEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
