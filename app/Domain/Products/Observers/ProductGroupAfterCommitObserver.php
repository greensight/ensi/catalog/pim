<?php

namespace App\Domain\Products\Observers;

use App\Domain\Kafka\Actions\Send\SendProductGroupEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Products\Models\ProductGroup;

class ProductGroupAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendProductGroupEventAction $productGroupEventAction
    ) {
    }

    public function created(ProductGroup $model): void
    {
        $this->productGroupEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(ProductGroup $model): void
    {
        $this->productGroupEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(ProductGroup $model): void
    {
        $this->productGroupEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
