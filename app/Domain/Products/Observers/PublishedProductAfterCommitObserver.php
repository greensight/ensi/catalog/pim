<?php

namespace App\Domain\Products\Observers;

use App\Domain\Kafka\Actions\Send\SendPublishedProductEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Products\Models\PublishedProduct;

class PublishedProductAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(
        protected SendPublishedProductEventAction $sendPublishedProductEventAction
    ) {
    }

    public function created(PublishedProduct $model): void
    {
        $this->sendPublishedProductEventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(PublishedProduct $model): void
    {
        $this->sendPublishedProductEventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(PublishedProduct $model): void
    {
        $this->sendPublishedProductEventAction->execute($model, ModelEventMessage::DELETE);
    }
}
