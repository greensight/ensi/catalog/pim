<?php

namespace App\Domain\Products\Observers;

use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Events\FileReleased;

class ProductPropertyValueObserver
{
    public function deleted(ProductPropertyValue $model): void
    {
        if ($model->isDirectory()) {
            return;
        }

        $value = $model->getValue();

        if ($value->isFile()) {
            FileReleased::dispatch($value->native());
        }
    }
}
