<?php

namespace App\Domain\Products\Observers;

use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Kafka\Actions\Send\SendProductEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Support\Concerns\InteractsWithModels;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum as Event;

class ProductObserver
{
    use InteractsWithModels;

    public function __construct(
        protected SendProductEventAction $sendProductEventAction
    ) {
    }

    public function created(Product $product): void
    {
        $release = new PublishedProduct();
        $release->id = $product->id;

        $values = $product->only($product->getFillable());
        foreach ($values as $field => $value) {
            $release->{$field} = $value;
        }

        $this->saveOrThrow($release);

        $this->sendEvents($product, isCreate: true);
    }

    public function updated(Product $product): void
    {
        $this->sendEvents($product);
    }

    public function deleted(Product $product): void
    {
        $this->sendProductEventAction->execute($product, ModelEventMessage::DELETE);
    }

    protected function sendEvents(Product $product, bool $isCreate = false): void
    {
        if ($isCreate) {
            ProductEvent::dispatch($product->id, Event::PRODUCT_CREATED);
        }

        if ($product->isDirty('description') || $isCreate) {
            ProductEvent::dispatchIf(filled($product->description), $product->id, Event::ADDED_DESCRIPTION);
        }

        if ($product->isDirty('allow_publish') || $isCreate) {
            ProductEvent::dispatch($product->id, $product->allow_publish ? Event::PRODUCT_ACTIVATION : Event::PRODUCT_DEACTIVATION);
        }

        if ($product->isDirty('system_status_id') || $product->system_status_id === ProductSystemStatus::DELETED) {
            $this->sendProductEventAction->execute($product, ModelEventMessage::DELETE);
        }
    }
}
