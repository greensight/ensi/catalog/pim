<?php

use App\Domain\Products\Actions\Images\SetProductImagesAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Models\TempFile;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertSoftDeleted;
use function Pest\Laravel\travel;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

test('new image from file', function () {
    $product = Product::factory()->createOne();
    $fields = [
        'preload_file_id' => TempFile::create(['path' => '/path/to/image.png'])->id,
        'sort' => 10,
        'name' => 'foo',
    ];

    $changes = (new SetProductImagesAction())->execute($product, [$fields], false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductImage())->getTable(),
        ['file' => '/path/to/image.png', 'sort' => 10, 'name' => 'foo']
    );
});

test('existing image', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $product = Product::factory()->createOne();
    $image = ProductImage::factory()->owned($product)->createOne();

    $fields = [
        'id' => $image->id,
        'sort' => 10,
        'name' => 'foo',
    ];

    $changes = (new SetProductImagesAction())->execute($product, [$fields], false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductImage())->getTable(),
        ['id' => $image->id, 'sort' => 10, 'name' => 'foo']
    );
})->with(FakerProvider::$optionalDataset);

test('deleting not specified images when replace is true', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $product = Product::factory()->createOne();
    $image = ProductImage::factory()->owned($product)->createOne();

    $changes = (new SetProductImagesAction())->execute($product, [], true);

    expect($changes)->toBe(1);
    assertSoftDeleted($image);
})->with(FakerProvider::$optionalDataset);

test('skip not specified images when replace is false', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $product = Product::factory()->createOne();
    $image = ProductImage::factory()->owned($product)->createOne();

    $changes = (new SetProductImagesAction())->execute($product, [], false);

    expect($changes)->toBe(0);
    assertDatabaseHas($image->getTable(), ['id' => $image->id, 'deleted_at' => null]);
})->with(FakerProvider::$optionalDataset);

test('skip existing image when no changes', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $product = Product::factory()->createOne();
    $image = ProductImage::factory()->owned($product)->createOne();

    $fields = [
        'id' => $image->id,
        'sort' => $image->sort,
        'name' => $image->name,
    ];
    travel(1)->minutes();

    $changes = (new SetProductImagesAction())->execute($product, [$fields], false);

    expect($changes)->toBe(0);
    assertDatabaseHas(
        (new ProductImage())->getTable(),
        ['id' => $image->id, 'updated_at' => $image->updated_at]
    );
})->with(FakerProvider::$optionalDataset);
