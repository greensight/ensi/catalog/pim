<?php

use App\Domain\Products\Actions\ProductGroups\ForceDeleteProductFromProductGroupAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'product-groups', 'integration');

test('delete main product from product groups with only one item', function () {
    Event::fake();

    $product = Product::factory()->create();
    $productGroup = ProductGroup::factory()->withMainProductId($product->id)->create();
    ProductGroupProduct::factory()->for($product)->for($productGroup)->create();

    $productGroup1 = ProductGroup::factory()->withMainProductId($product->id)->create();
    ProductGroupProduct::factory()->for($product)->for($productGroup1)->create();

    $otherProduct = Product::factory()->create();
    $otherProductGroup = ProductGroup::factory()->withMainProductId($otherProduct->id)->create();
    ProductGroupProduct::factory()->for($otherProduct)->for($otherProductGroup)->create();

    resolve(ForceDeleteProductFromProductGroupAction::class)->execute($product->id);

    assertModelMissing($productGroup);
    assertModelMissing($productGroup1);

    assertModelExists($otherProductGroup);
});

test(
    'delete product from product groups with more one item',
    function (int $productId, int $otherProductId, int $currentMainProductId) {
        Event::fake();

        $product = Product::factory()->create(['id' => $productId]);
        $otherProduct = Product::factory()->create(['id' => $otherProductId]);

        $productGroup = ProductGroup::factory()->create(['main_product_id' => $currentMainProductId]);

        $productGroupProduct = ProductGroupProduct::factory()->for($product)->for($productGroup)->create();
        $otherProductGroupProduct = ProductGroupProduct::factory()->for($otherProduct)->for($productGroup)->create();

        resolve(ForceDeleteProductFromProductGroupAction::class)->execute($product->id);

        assertModelMissing($productGroupProduct);
        assertModelExists($otherProductGroupProduct);
        assertDatabaseHas(ProductGroup::class, ['id' => $productGroup->id, 'main_product_id' => $otherProductId]);
    }
)->with([
    'main product' => [1, 2, 1],
    'other product' => [1, 2, 2],
]);
