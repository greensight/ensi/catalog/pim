<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Products\Actions\ProductGroups\AutoDeleteProductsFromProductGroupAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use App\Domain\Products\Models\ProductPropertyValue;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertModelMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('product-groups', 'integration');

test('delete main product from product groups when gluing property value deleted',  function () {
    Event::fake();

    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()->directory()->actual($category, true)->create();
    $productPropertyValue = ProductPropertyValue::factory()->forProduct($product)->forProperty($property)->create();
    $productGroup = ProductGroup::factory()->withMainProductId($product->id)->create();
    ProductGroupProduct::factory()->for($product)->for($productGroup)->create();

    resolve(AutoDeleteProductsFromProductGroupAction::class)->execute($productPropertyValue);

    assertModelMissing($productGroup);
});

test('delete not main product from product groups when gluing property value deleted',  function () {
    Event::fake();

    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->create();
    $property = Property::factory()->directory()->actual($category, true)->create();
    $productPropertyValue = ProductPropertyValue::factory()->forProduct($product)->forProperty($property)->create();
    $productGroup = ProductGroup::factory()->create();
    $productGroupProduct = ProductGroupProduct::factory()->for($productGroup)->for($product)->create();

    resolve(AutoDeleteProductsFromProductGroupAction::class)->execute($productPropertyValue);

    assertModelMissing($productGroupProduct);
});
