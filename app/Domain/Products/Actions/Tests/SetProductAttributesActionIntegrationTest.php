<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Actions\Attributes\SetProductAttributesAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use Illuminate\Support\Arr;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertSoftDeleted;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

test('new directory value', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $property = Property::factory()->directory()->actual($category)->createOne();
    $dirValue = PropertyDirectoryValue::factory()->forProperty($property)->createOne();

    $data = [
        'property_id' => $property->id,
        'directory_value_id' => $dirValue->id,
    ];

    $changes = (new SetProductAttributesAction())->execute($product, [$data], false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductPropertyValue())->getTable(),
        array_merge(
            ['product_id' => $product->id, 'property_id' => $property->id],
            $dirValue->only(['type', 'value', 'name'])
        )
    );
});

test('existing directory value not changed', function () {
    $product = Product::factory()->createOne();
    $dirValue = PropertyDirectoryValue::factory()->createOne();
    $propValue = ProductPropertyValue::factory()
        ->forProduct($product)
        ->directory($dirValue)
        ->createOne();

    $data = [
        'property_id' => $propValue->property_id,
        'directory_value_id' => $propValue->directory_value_id,
        'name' => 'foo',
        'value' => 'bar',
    ];

    $changes = (new SetProductAttributesAction())->execute($product, [$data], false);

    expect($changes)->toBe(0);
    assertDatabaseHas(
        $propValue->getTable(),
        array_merge(
            ['product_id' => $product->id, 'property_id' => $propValue->property_id],
            Arr::only($dirValue->getAttributes(), ['type', 'value', 'name'])
        )
    );
});

test('new native value', function (PropertyType $type) {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $property = Property::factory()->withType($type)->actual($category)->createOne();
    $propValue = ProductPropertyValue::factory()
        ->forProperty($property)
        ->forProduct($product)
        ->makeOne();

    $data = [
        'property_id' => $property->id,
        'value' => $propValue->value,
        'name' => $propValue->name,
    ];

    $changes = (new SetProductAttributesAction())->execute($product, [$data], false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        $propValue->getTable(),
        Arr::only($propValue->getAttributes(), ['product_id', 'property_id', 'type', 'name', 'value'])
    );
})->with(PropertyType::cases());

test('existing native value', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $propValue = ProductPropertyValue::factory()->forProduct($product)->actual()->createOne();
    $data = [
        'property_id' => $propValue->property_id,
        'value' => $propValue->value,
        'name' => 'foo',
    ];

    $changes = (new SetProductAttributesAction())->execute($propValue->product, [$data], false);

    expect($changes)->toBe(1);
    assertDatabaseHas($propValue->getTable(), ['id' => $propValue->id, 'name' => 'foo']);
});

test('remove old value', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $property = Property::factory()->actual($category)->createOne();
    $factory = ProductPropertyValue::factory()
        ->forProduct($product)
        ->forProperty($property);

    $oldValue = $factory->createOne();

    // Для типа boolean набор вариантов слишком мал и может несколько раз подряд генерировать одно значение
    do {
        $newValue = $factory->makeOne();
    } while ($newValue->value === $oldValue->value);

    $data = [
        'property_id' => $oldValue->property_id,
        'value' => $newValue->value,
        'name' => $newValue->name,
    ];

    $changes = (new SetProductAttributesAction())->execute($product, [$data], false);

    expect($changes)->toBe(2);
    assertSoftDeleted($oldValue);
});

test('restore deleted value', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $propValue = ProductPropertyValue::factory()->forProduct($product)->actual()->createOne();
    $propValue->delete();

    $data = [
        'property_id' => $propValue->property_id,
        'value' => $propValue->value,
        'name' => 'foo',
    ];

    $changes = (new SetProductAttributesAction())->execute($propValue->product, [$data], false);

    expect($changes)->toBe(1);
    assertDatabaseHas($propValue->getTable(), ['id' => $propValue->id, 'name' => 'foo', 'deleted_at' => null]);
});

test('add multiple values', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->createOne(['is_multiple' => true]);

    $data = [
        ['property_id' => $property->id, 'value' => 'foo'],
        ['property_id' => $property->id, 'value' => 'bar'],
    ];

    $changes = (new SetProductAttributesAction())->execute($product, $data, false);

    expect($changes)->toBe(2);
    assertDatabaseHas((new ProductPropertyValue())->getTable(), ['product_id' => $product->id, 'value' => 'foo']);
    assertDatabaseHas((new ProductPropertyValue())->getTable(), ['product_id' => $product->id, 'value' => 'bar']);
});

test('skip unused property value', function () {
    $product = Product::factory()->createOne();
    $value = ProductPropertyValue::factory()->forProduct($product)->createOne();

    $changes = (new SetProductAttributesAction())->execute($product, [], true);

    expect($changes)->toBe(0);
    assertDatabaseHas($value->getTable(), ['id' => $value->id, 'deleted_at' => null]);
});
