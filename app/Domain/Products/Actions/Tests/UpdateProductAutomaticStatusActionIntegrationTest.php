<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Actions\Products\UpdateProductAutomaticStatusAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductEvent;
use App\Domain\Products\Models\PublishedProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\EventOperationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\travel;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

test('set allow_publish', function () {
    $event = ProductEventEnum::ADDED_DESCRIPTION;

    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->publication()->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()->inStatus($currentStatus)->create(['allow_publish' => false]);
    CategoryProductLink::factory()->createFast($category, $product);
    ProductEvent::factory()->inProduct($product)->event($event)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id, 'allow_publish' => true]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id, 'allow_publish' => true]
    );
});

test('set new automatic status', function () {
    $event = ProductEventEnum::PRODUCT_CREATED;
    $newStatus = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->create();

    $product = Product::factory()->create(['status_id' => null]);
    ProductEvent::factory()->inProduct($product)->event($event)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $newStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $newStatus->id]
    );
});

test('save status if not find next status', function () {
    $event = ProductEventEnum::ADDED_DESCRIPTION;

    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->create();
    ProductStatusLink::factory()->withStatuses($nextStatus, $currentStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    ProductEvent::factory()->inProduct($product)->event($event)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
});

test('change status by condition and', function () {
    $events = [ProductEventEnum::ADDED_DESCRIPTION, ProductEventEnum::IMAGES_UPLOADED];

    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents($events, EventOperationEnum::AND)->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    foreach ($events as $event) {
        ProductEvent::factory()->inProduct($product)->event($event)->create();
    }

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id]
    );
});

test('not change status by condition and', function () {
    $events = [ProductEventEnum::ADDED_DESCRIPTION, ProductEventEnum::IMAGES_UPLOADED];

    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents($events, EventOperationEnum::AND)->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    ProductEvent::factory()->inProduct($product)->event(current($events))->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
});

test('change status by condition or', function () {
    $events = [ProductEventEnum::ADDED_DESCRIPTION, ProductEventEnum::IMAGES_UPLOADED];

    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents($events, EventOperationEnum::OR)->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    ProductEvent::factory()->inProduct($product)->event(current($events))->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id]
    );
});

test('not change status by condition or', function () {
    $events = [ProductEventEnum::ADDED_TO_PRODUCT_GROUPS, ProductEventEnum::IMAGES_UPLOADED];

    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents($events, EventOperationEnum::OR)->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
});

test('set an earlier status', function () {
    $event = ProductEventEnum::ADDED_DESCRIPTION;

    $currentStatus = ProductStatusSetting::factory()->create();

    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    travel(1)->minutes();

    $nextStatusTwo = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatusTwo)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    ProductEvent::factory()->inProduct($product)->event($event)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $nextStatus->id]
    );
});

test('dont set inactive status', function () {
    $event = ProductEventEnum::ADDED_DESCRIPTION;
    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->create(['is_active' => false]);
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    ProductEvent::factory()->inProduct($product)->event($event)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
});

test('dont set manual status', function () {
    $event = ProductEventEnum::ADDED_DESCRIPTION;
    $currentStatus = ProductStatusSetting::factory()->create();
    $nextStatus = ProductStatusSetting::factory()->activeAutomaticEvents([$event])->create(['type' => ProductStatusTypeEnum::MANUAL]);
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->inStatus($currentStatus)->create();
    ProductEvent::factory()->inProduct($product)->event($event)->create();

    resolve(UpdateProductAutomaticStatusAction::class)->execute($product->id);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'status_id' => $currentStatus->id]
    );
});
