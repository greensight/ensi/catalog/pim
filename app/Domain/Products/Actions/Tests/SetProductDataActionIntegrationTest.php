<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Classifiers\Models\ProductStatusLink;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Actions\Products\SetProductDataAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Models\TempFile;
use App\Exceptions\IllegalOperationException;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Support\Arr;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\travel;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

test('replace fields', function () {
    $product = Product::factory()->createOne();
    $source = Product::factory()->makeOne();
    $fields = Arr::except($source->getAttributes(), ['weight', 'height', 'status_id']);

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, true);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        $product->getTable(),
        ['id' => $product->id, 'name' => $fields['name'], 'weight' => null, 'height' => null]
    );
});

test('patch fields', function () {
    $product = Product::factory()->createOne();
    $fields = ['name' => 'foo'];

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        $product->getTable(),
        ['id' => $product->id, 'name' => 'foo', 'weight' => $product->weight]
    );
});

test('skip not changed', function () {
    $product = Product::factory()->createOne();
    $fields = ['name' => $product->name, 'allow_publish' => $product->allow_publish];

    travel(1)->minutes();
    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(0);
    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'updated_at' => $product->updated_at]);
});

test('add image', function () {
    $product = Product::factory()->createOne();
    $fs = resolve(EnsiFilesystemManager::class);

    Storage::fake($fs->publicDiskName());

    $imagePath = EnsiFile::factory()->fileName('image_1024')
        ->fileExt('png')
        ->make()['path'];

    $preloadFile = TempFile::create(['path' => $imagePath]);

    $fields = ['images' => [['preload_file_id' => $preloadFile->id]]];

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductImage())->getTable(),
        ['product_id' => $product->id, 'file' => $preloadFile->path]
    );
});

test('add attribute value', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING)
        ->actual($category)
        ->createOne();

    $fields = ['attributes' => [
        ['property_id' => $property->id, 'value' => 'foo', 'name' => 'bar'],
    ]];

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductPropertyValue())->getTable(),
        ['product_id' => $product->id, 'property_id' => $property->id, 'value' => 'foo', 'name' => 'bar']
    );
});

test('set new manual status success', function () {
    $newStatus = ProductStatusSetting::factory()->activeManual()->create();

    $product = Product::factory()->createQuietly(['status_id' => null]);

    $fields = ['status_id' => $newStatus->id];

    resolve(SetProductDataAction::class)->execute($product, $fields, false);

    assertDatabaseHas(
        Product::class,
        ['status_id' => $newStatus->id]
    );
});

test('set next manual status success', function () {
    $currentStatus = ProductStatusSetting::factory()->activeManual()->create();
    $nextStatus = ProductStatusSetting::factory()->activeManual()->create();
    ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();

    $product = Product::factory()->createQuietly(['status_id' => $currentStatus->id]);

    $fields = ['status_id' => $nextStatus->id];

    resolve(SetProductDataAction::class)->execute($product, $fields, false);

    assertDatabaseHas(
        Product::class,
        ['status_id' => $nextStatus->id]
    );
});

test('set new manual status error', function () {
    $linkStatus = ProductStatusSetting::factory()->activeManual()->create();
    $nextStatus = ProductStatusSetting::factory()->activeManual()->create();
    ProductStatusLink::factory()->withStatuses($linkStatus, $nextStatus)->create();

    $product = Product::factory()->createQuietly(['status_id' => null]);

    $fields = ['status_id' => $nextStatus->id];

    $action = resolve(SetProductDataAction::class);

    expect(fn () => $action->execute($product, $fields, false))
        ->toThrow(IllegalOperationException::class);
});

test('set next manual status error', function (ProductStatusSetting $nextStatus, bool $link) {
    $currentStatus = ProductStatusSetting::factory()->activeManual()->create();
    if ($link) {
        ProductStatusLink::factory()->withStatuses($currentStatus, $nextStatus)->create();
    }

    $product = Product::factory()->createQuietly(['status_id' => $currentStatus->id]);

    $fields = ['status_id' => $nextStatus->id];

    $action = resolve(SetProductDataAction::class);

    expect(fn () => $action->execute($product, $fields, false))
        ->toThrow(IllegalOperationException::class);
})->with([
    'not active status' => [fn () => ProductStatusSetting::factory()->manual()->active(false)->create(), true],
    'automatic status' => [fn () => ProductStatusSetting::factory()->activeAutomatic()->create(), true],
    'status without link' => [fn () => ProductStatusSetting::factory()->activeManual()->create(), false],
]);
