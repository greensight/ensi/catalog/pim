<?php

namespace App\Domain\Products\Actions\Images;

use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Concerns\InteractsWithModels;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;

class UploadProductImageAction
{
    use InteractsWithModels;

    public function execute(int $productId, array $fields): ProductImage
    {
        $image = new ProductImage();
        $image->product_id = $productId;
        $image->file = TempFile::grab($fields['preload_file_id'] ?? 0);
        $image->fill($fields);
        $this->saveOrThrow($image);

        ProductEvent::dispatch($productId, ProductEventEnum::IMAGES_UPLOADED);
        ProductInvalidated::dispatch($productId);

        return $image;
    }
}
