<?php

namespace App\Domain\Products\Actions\Images;

use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Concerns\InteractsWithModels;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use Illuminate\Support\Collection;

class SetProductImagesAction
{
    use InteractsWithModels;

    public function execute(Product $product, array $images, bool $replace): int
    {
        if (!$replace && blank($images)) {
            return 0;
        }

        $changes = 0;
        $relation = $product->images();
        $existingImages = $relation->get()->keyBy('id');

        foreach ($images as $fields) {
            $image = array_key_exists('id', $fields)
                ? $this->pullExistingImage($fields['id'], $existingImages)
                : $this->createImage($fields);

            $image->fill($fields);

            if ($image->exists && !$image->isDirty()) {
                continue;
            }

            $this->saveRelatedOrThrow($relation, $image);
            $changes++;
        }

        if ($changes > 0) {
            ProductEvent::dispatch($product->id, ProductEventEnum::IMAGES_UPLOADED);
        }

        if ($replace) {
            $changes += $existingImages->each(fn (ProductImage $image) => $image->delete())->count();
        }

        return $changes;
    }

    private function pullExistingImage(int $id, Collection $images): ProductImage
    {
        $image = $images->pull($id);

        throw_if($image === null, "Изображение [$id] не найдено");

        return $image;
    }

    private function createImage(array $fields): ProductImage
    {
        $image = new ProductImage();
        $image->file = TempFile::grab($fields['preload_file_id'] ?? 0);

        return $image;
    }
}
