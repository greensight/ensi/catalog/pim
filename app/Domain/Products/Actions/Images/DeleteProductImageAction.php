<?php

namespace App\Domain\Products\Actions\Images;

use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\ProductImage;

class DeleteProductImageAction
{
    public function execute(int $productId, int $fileId): void
    {
        /** @var ProductImage $image */
        $image = ProductImage::query()->where('product_id', $productId)->findOrFail($fileId);
        ProductInvalidated::dispatch($productId);

        $image->delete();
    }
}
