<?php

namespace App\Domain\Products\Actions\Attributes;

use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use Illuminate\Support\Collection;

class ReplaceProductAttributesAction
{
    use AppliesToAggregate;

    public function __construct(protected SetProductAttributesAction $action)
    {
    }

    public function execute(int $productId, array $values): Collection
    {
        $product = $this->apply($productId, function (Product $product) use ($values) {
            $this->action->execute($product, $values, true);

            return $product;
        });

        ProductInvalidated::dispatch($product->id);

        return $product->properties()->get();
    }

    protected function createModel(): Model
    {
        return new Product();
    }
}
