<?php

namespace App\Domain\Products\Actions\Attributes;

use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class DeleteProductAttributesAction
{
    use AppliesToAggregate;

    public function execute(int $productId, array $propertyIds): void
    {
        $product = $this->apply($productId, function (Product $product) use ($propertyIds) {
            $product->properties()
                ->whereIn('property_id', $propertyIds)
                ->get()
                ->each(fn (ProductPropertyValue $p) => $this->deleteOrThrow($p));

            return $product;
        });

        ProductInvalidated::dispatch($product->id);
    }

    protected function createModel(): Model
    {
        return new Product();
    }
}
