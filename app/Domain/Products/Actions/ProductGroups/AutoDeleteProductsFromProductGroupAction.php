<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroupProduct;
use App\Domain\Products\Models\ProductPropertyValue;

class AutoDeleteProductsFromProductGroupAction
{
    public function __construct(
        private readonly DeleteProductGroupAction $deleteProductGroupAction
    ) {
    }

    public function execute(ProductPropertyValue $productPropertyValue): void
    {
        /** @var Product $product */
        $product = Product::with(['productGroups.productLinks'])->find($productPropertyValue->product_id);
        if ($product->productGroups->isEmpty()) {
            return;
        }

        $isGluingPropertyDeleted = $product->getAvailableProperties()
            ->where('property_id', $productPropertyValue->property_id)
            ->where('is_gluing', true)
            ->isNotEmpty();
        if (!$isGluingPropertyDeleted) {
            return;
        }

        foreach ($product->productGroups as $productGroup) {
            if ($productGroup->main_product_id == $product->id) {
                $this->deleteProductGroupAction->execute($productGroup->id);
            } else {
                /** @var ProductGroupProduct|null $productGroupProduct */
                $productGroupProduct = $productGroup->productLinks->where('product_id', $product->id)->first();
                $productGroupProduct?->delete();
            }
        }
    }
}
