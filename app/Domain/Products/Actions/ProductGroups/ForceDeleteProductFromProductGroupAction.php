<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\ProductGroup;
use App\Domain\Support\Concerns\InteractsWithModels;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ForceDeleteProductFromProductGroupAction
{
    use InteractsWithModels;

    public function __construct(protected readonly DeleteProductGroupAction $deleteProductGroupAction)
    {
    }

    public function execute(int $productId): void
    {
        /** @var ProductGroup $productGroups */
        $productGroups = ProductGroup::query()
            ->whereHas('productLinks', fn (Builder $query) => $query->where('product_id', $productId))
            ->with('productLinks')
            ->get();

        $this->transaction(fn () => $productGroups->each(
            fn (ProductGroup $productGroup) => $this->deleteProduct($productId, $productGroup),
        ));
    }

    protected function deleteProduct(int $productId, ProductGroup $productGroup): void
    {
        if ($productGroup->main_product_id === $productId) {
            if ($productGroup->productLinks->count() === 1) {
                $this->deleteProductGroupAction->execute($productGroup->id);

                return;
            }

            $this->setAnotherMainProduct($productId, $productGroup);
        }

        $this->unbindProduct($productId, $productGroup);
    }

    protected function setAnotherMainProduct(int $productId, ProductGroup $productGroup): void
    {
        foreach ($productGroup->productLinks as $productLink) {
            if ($productLink->product_id != $productId) {
                $productGroup->main_product_id = $productLink->product_id;
                $productGroup->save();

                break;
            }
        }
    }

    protected function unbindProduct(int $productId, ProductGroup $productGroup): void
    {
        $productLink = $productGroup->productLinks->firstWhere('product_id', $productId);
        $productLink->delete();
    }
}
