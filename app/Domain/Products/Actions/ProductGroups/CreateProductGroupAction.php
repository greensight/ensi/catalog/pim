<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\ProductGroup;

class CreateProductGroupAction
{
    public function __construct(protected readonly SaveProductGroupAction $saveAction)
    {
    }

    public function execute(array $fields): ProductGroup
    {
        $productGroup = new ProductGroup();
        $productGroup->fill($fields);

        return $this->saveAction->execute($productGroup);
    }
}
