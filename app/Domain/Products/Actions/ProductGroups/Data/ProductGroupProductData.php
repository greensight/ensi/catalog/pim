<?php

namespace App\Domain\Products\Actions\ProductGroups\Data;

class ProductGroupProductData
{
    public ?int $id = null;
    public ?string $vendorCode = null;
    public ?string $barcode = null;
}
