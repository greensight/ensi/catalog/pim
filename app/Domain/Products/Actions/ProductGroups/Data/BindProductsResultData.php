<?php

namespace App\Domain\Products\Actions\ProductGroups\Data;

use App\Domain\Products\Models\ProductGroup;

class BindProductsResultData
{
    /**
     * ID товаров, для которых не заполнены ВСЕ свойства, участвующие в склейке
     * @var int[]
     */
    public array $productErrors = [];

    public function __construct(
        public readonly ProductGroup $productGroup,
    ) {
    }

    public function isError(): bool
    {
        return filled($this->productErrors);
    }
}
