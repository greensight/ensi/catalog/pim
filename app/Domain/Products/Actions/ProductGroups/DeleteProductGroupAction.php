<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\ProductGroup;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class DeleteProductGroupAction
{
    use AppliesToAggregate;

    public function execute(int $productGroupId): void
    {
        $this->delete($productGroupId);
    }

    protected function createModel(): Model
    {
        return new ProductGroup();
    }
}
