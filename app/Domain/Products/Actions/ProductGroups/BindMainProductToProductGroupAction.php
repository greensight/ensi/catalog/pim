<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Actions\ProductGroups\Data\ProductGroupProductData;
use App\Domain\Products\Models\ProductGroup;
use App\Exceptions\IllegalOperationException;

class BindMainProductToProductGroupAction
{
    public function __construct(private BindProductsAction $bindProductsAction)
    {
    }

    public function execute(ProductGroup $productGroup): void
    {
        $productGroupProductData = new ProductGroupProductData();
        $productGroupProductData->id = $productGroup->main_product_id;

        $result = $this->bindProductsAction->execute(
            $productGroup->id,
            false,
            collect([$productGroupProductData])
        );

        if ($result->isError()) {
            throw new IllegalOperationException(
                "Не удалось привязать товар {$productGroupProductData->id} к склейке. У товара не заполнены все свойства, участвующие в склейке"
            );
        }
    }
}
