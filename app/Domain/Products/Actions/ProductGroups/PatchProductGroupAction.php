<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\ProductGroup;

class PatchProductGroupAction
{
    public function __construct(protected readonly SaveProductGroupAction $saveAction)
    {
    }

    public function execute(int $productGroupId, array $fields): ProductGroup
    {
        /** @var ProductGroup $productGroup */
        $productGroup = ProductGroup::query()->findOrFail($productGroupId);
        $productGroup->fill($fields);

        return $this->saveAction->execute($productGroup);
    }
}
