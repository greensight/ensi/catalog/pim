<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\ProductGroup;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Concerns\HandlesMassOperation;
use App\Domain\Support\MassOperationResult;
use App\Domain\Support\Models\Model;

class DeleteManyProductGroupsAction
{
    use AppliesToAggregate;
    use HandlesMassOperation;

    public function execute(array $productGroupIds): MassOperationResult
    {
        $productGroups = $this->findMany($productGroupIds);

        return $this->eachModel($productGroups, function (ProductGroup $productGroup) {
            $this->applyModel($productGroup, function (ProductGroup $productGroup) {
                $this->deleteOrThrow($productGroup);
            });
        });
    }

    protected function createModel(): Model
    {
        return new ProductGroup();
    }
}
