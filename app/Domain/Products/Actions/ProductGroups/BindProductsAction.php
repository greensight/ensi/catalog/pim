<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Actions\ProductGroups\Data\BindProductsResultData;
use App\Domain\Products\Actions\ProductGroups\Data\ProductGroupProductData;
use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Exceptions\IllegalOperationException;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Support\Actions\ActionDecorator;
use App\Support\Actions\DecoratesAction;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Illuminate\Support\Collection;

class BindProductsAction
{
    use AppliesToAggregate;
    use DecoratesAction;

    /**
     * @param Collection<ProductGroupProductData> $productLinks
     */
    public function execute(int $productGroupId, bool $replace, Collection $productLinks, bool $checkProperties = true): BindProductsResultData
    {
        return $this->apply($productGroupId, function (ProductGroup $productGroup) use ($replace, $productLinks, $checkProperties) {
            if ($checkProperties) {
                $productGroup->loadMissing('category.actualProperties');
            }

            return $this->bind($productGroup, $productLinks, $replace, $checkProperties);
        });
    }

    public function enrichResult(): static|ActionDecorator
    {
        return $this->decorateResult(
            function (BindProductsResultData $productGroup) {
                $productGroup->productGroup->load('products');

                return $productGroup;
            }
        );
    }

    protected function createModel(): Model
    {
        return new ProductGroup();
    }

    /**
     * @param Collection<ProductGroupProductData> $productLinks
     */
    private function bind(ProductGroup $productGroup, Collection $productLinks, bool $replace, bool $checkProperties = true): BindProductsResultData
    {
        $result = new BindProductsResultData($productGroup);

        $relation = $result->productGroup->productLinks();
        $existing = $relation->get()->keyBy('product_id');
        $this->updateLinks($result, $existing, $productLinks, $relation, $checkProperties);
        $this->processUnchangedLinks($existing, $replace, $productGroup->main_product_id);

        return $result;
    }

    /**
     * @param Collection<ProductGroupProduct> $existing
     * @param Collection<ProductGroupProductData> $productLinks
     */
    private function updateLinks(
        BindProductsResultData $bindData,
        Collection $existing,
        Collection $productLinks,
        HasOneOrMany $relation,
        bool $checkProperties = true
    ): int {
        $productIds = $this->formProductIds($bindData, $productLinks, $checkProperties);

        return collect($productIds)
            ->reduce(function (int $carry, int $productId) use ($relation, $existing) {
                $link = $existing->pull($productId) ?? ProductGroupProduct::new($productId);
                if ($link->exists) {
                    return $carry;
                }

                $this->saveRelatedOrThrow($relation, $link);

                ProductEvent::dispatch($link->product_id, ProductEventEnum::ADDED_TO_PRODUCT_GROUPS);

                return $carry + 1;
            }, 0);
    }

    private function processUnchangedLinks(Collection $links, bool $replace, ?int $mainProductId): int
    {
        if (!$replace) {
            return 0;
        }

        if ($mainProductId && $links->has($mainProductId)) {
            throw new IllegalOperationException("Попытка удалить главный товар");
        }

        return $links->each(function (ProductGroupProduct $link) {
            $this->deleteOrThrow($link);
        })->count();
    }

    /**
     * @param BindProductsResultData $bindData
     * @param Collection<ProductGroupProductData> $productLinks
     * @param bool $checkProperties Проверить, что у товара, который добавляется в склейку, заполнены все атрибуты склейки
     * @return array
     */
    private function formProductIds(
        BindProductsResultData $bindData,
        Collection $productLinks,
        bool $checkProperties = true
    ): array {
        $ids = $productLinks->map(fn (ProductGroupProductData $link) => $link->id)->all();
        $vendorCodes = $productLinks->map(fn (ProductGroupProductData $link) => $link->vendorCode)->all();
        $barcodes = $productLinks->map(fn (ProductGroupProductData $link) => $link->barcode)->all();

        if (!$ids && !$vendorCodes && !$barcodes) {
            return [];
        }

        $query = Product::whereHas('categoryProductLinks', function (Builder $query) use ($bindData) {
            $query->where('category_id', $bindData->productGroup->category_id);
        });
        $gluingPropertyIds = collect();
        if ($checkProperties) {
            //Получаем для товаров значения свойств, которые участвуют в склейке
            $gluingPropertyIds = $bindData->productGroup->category
                ->actualProperties
                ->where('is_gluing', true)
                ->pluck('property_id');
            $query->with('properties', function (Builder $query) use ($gluingPropertyIds) {
                $query->whereIn('property_id', $gluingPropertyIds);
            });
        }
        $query->where(function (Builder $query) use ($ids, $vendorCodes, $barcodes) {
            if ($ids) {
                $query->orWhereIn('id', $ids);
            }
            if ($vendorCodes) {
                $query->orWhereIn('vendor_code', $vendorCodes);
            }
            if ($barcodes) {
                $query->orWhereIn('barcode', $barcodes);
            }
        });
        /** @var Collection<Product> $products */
        $products = $query->get();
        if ($checkProperties) {
            //Оставляем только те товары, у которых заполнены ВСЕ свойства, участвующие в склейке
            $products = $products->where(function (Product $product) use ($bindData, $gluingPropertyIds) {
                if ($product->properties->count() != $gluingPropertyIds->count()) {
                    $bindData->productErrors[] = $product->id;

                    return false;
                }


                return true;
            });
        }

        $links = [];
        foreach ($products as $product) {
            $link = $productLinks->filter(function (ProductGroupProductData $link) use ($product) {
                return ($link->id === $product->id) ||
                    ($link->vendorCode === $product->vendor_code) ||
                    ($link->barcode === $product->barcode);
            })->first();
            if ($link) {
                $links[] = $product->id;
            }
        }

        return $links;
    }
}
