<?php

namespace App\Domain\Products\Actions\ProductGroups;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Support\Concerns\InteractsWithModels;
use App\Exceptions\IllegalOperationException;

class SaveProductGroupAction
{
    use InteractsWithModels;

    public function __construct(protected readonly BindMainProductToProductGroupAction $bindMainProductToProductGroupAction)
    {
    }

    public function execute(ProductGroup $productGroup): ProductGroup
    {
        if ($productGroup->main_product_id) {
            $this->checkCategory($productGroup->category_id, $productGroup->main_product_id);
        }

        return $this->transaction(function () use ($productGroup) {
            $isUpdateMainProductId = $productGroup->isDirty('main_product_id');

            $this->saveOrThrow($productGroup);

            if ($isUpdateMainProductId) {
                $this->bindMainProductToProductGroupAction->execute($productGroup);
            }

            return $productGroup;
        });
    }

    protected function checkCategory(int $categoryId, int $mainProductId): void
    {
        $mainProduct = Product::findOrFail($mainProductId);
        throw_if(
            !in_array($categoryId, $mainProduct->getCategoryIds()->toArray()),
            IllegalOperationException::class,
            "Категория главного товар склейки не совпадает с категорией склейки"
        );
    }
}
