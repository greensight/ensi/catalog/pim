<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Common\InteractsWithBulkOperations;
use App\Domain\Common\Models\BulkOperation;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Actions\ReserveFileAction;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Concerns\HandlesMassOperation;
use App\Domain\Support\Data\ReservedFile;
use App\Domain\Support\Jobs\DistributeFileContentsJob;
use App\Domain\Support\MassOperationResult;
use App\Domain\Support\Models\Model;
use App\Domain\Support\Models\TempFile;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;

class PatchManyProductsAction
{
    use HandlesMassOperation;
    use AppliesToAggregate;
    use InteractsWithBulkOperations;

    /** @var array<int, array<ReservedFile>> */
    private array $reservedFiles = [];

    public function __construct(
        private readonly SetProductDataAction $productAction,
        private readonly ReserveFileAction $reserveFileAction,
    ) {
    }

    public function execute(
        array $productIds,
        array $fields,
        array $attributes,
        ?BulkOperation $bulk = null,
    ): MassOperationResult {
        $bulk = $bulk ?? BulkOperation::registerPatchProducts($productIds);

        /** @var MassOperationResult $result */
        $result = $this->processBulkOperation(
            operation: $bulk,
            action: fn () => $this->processing($productIds, $fields, $attributes)
        );

        return $result;
    }

    private function processing(array $productIds, array $fields, array $attributes): MassOperationResult
    {
        $products = $this->findMany($productIds);
        $this->prepareModels($products);

        $preparedData = $this->prepareDataForEach($products, $fields, $attributes);

        $result = $this->eachModel($products, function (Product $product) use ($preparedData) {
            $data = $preparedData[$product->id];

            $this->transaction(function () use ($product, $data) {
                $this->applyModel($product, function (Product $product) use ($data) {
                    $this->productAction->execute($product, $data, false);
                });
            });

            ProductInvalidated::dispatch($product->id);
        });

        $filesToProcess = $this->getFilesToProcess(filterIds: $result->getProcessed());
        if (!empty($filesToProcess)) {
            DistributeFileContentsJob::dispatch($filesToProcess);
        }

        return $result;
    }

    private function prepareDataForEach(Collection $products, array $fields, array $attributes): array
    {
        if ($products->isEmpty()) {
            return [];
        }

        $productIds = $products->pluck('id')->all();
        $dataById = array_fill_keys($productIds, $fields);

        // Категорийные атрибуты
        [$fileBasedAttributes, $plainAttributes] = $this->classifyAttributes($products, $attributes);

        $attributesById = array_fill_keys($productIds, $plainAttributes->all());

        foreach ($fileBasedAttributes as $attribute) {
            $prototype = TempFile::findOrFail($attribute['preload_file_id']);

            $reservedFiles = $this->reserveFileAction->execute($prototype, count($products));

            foreach ($productIds as $id) {
                /** @var ReservedFile $reserved */
                $reserved = $reservedFiles->pop();
                $attribute['preload_file_id'] = $reserved->copy->id;

                $attributesById[$id][] = $attribute;
                $this->reservedFiles[$id][] = $reserved;
            }
        }

        foreach ($attributesById as $id => $attributes) {
            if (blank($attributes)) {
                continue;
            }

            $dataById[$id]['attributes'] = $attributes;
        }

        return $dataById;
    }

    private function classifyAttributes(Collection $products, array $inputAttributes): Collection
    {
        $availableByProduct = $products->map(fn (Product $p) => $p->getAvailableProperties()->pluck('property_id'));
        $common = array_intersect(...$availableByProduct->toArray());

        return collect($inputAttributes)
            ->groupBy('property_id')
            ->only($common)
            ->collapse()
            ->partition(fn (array $attribute) => array_key_exists('preload_file_id', $attribute));
    }

    private function prepareModels(EloquentCollection $products): void
    {
        $products->load('categoryProductLinks');
    }

    private function getFilesToProcess(array $filterIds): array
    {
        $filtered = array_intersect_key($this->reservedFiles, array_flip($filterIds));

        return array_merge(...array_values($filtered));
    }

    protected function createModel(): Model
    {
        return new Product();
    }
}
