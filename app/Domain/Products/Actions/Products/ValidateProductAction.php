<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Models\Product;
use App\Exceptions\IllegalOperationException;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;

class ValidateProductAction
{
    /**
     * @throws IllegalOperationException
     */
    public function execute(Product $product, array $fields): void
    {
        $newStatus = data_get($fields, 'status_id');
        if ($newStatus) {
            $this->checkNextStatus($product, $newStatus);
        }
    }

    private function checkNextStatus(Product $product, int $newStatusId): void
    {
        $product->loadMissing('status');

        if ($product->status == null) {
            $availableNextStatusQuery = ProductStatusSetting::query()->has('previousStatuses', '=', 0);
        } else {
            $availableNextStatusQuery = $product->status->nextStatuses();
        }

        $availableNextStatus = $availableNextStatusQuery
            ->where('type', ProductStatusTypeEnum::MANUAL)
            ->where('is_active', true)
            ->get()
            ->keyBy('id');

        if (!$availableNextStatus->has($newStatusId)) {
            throw new IllegalOperationException("Невозможно перейти в статус {$newStatusId} из статуса {$product->status_id}.");
        }
    }
}
