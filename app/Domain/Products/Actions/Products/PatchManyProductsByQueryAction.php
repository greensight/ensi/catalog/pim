<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Common\Models\BulkOperation;
use App\Domain\Products\Jobs\MassPatchProductsJob;
use App\Domain\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use Webmozart\Assert\Assert;

class PatchManyProductsByQueryAction
{
    private int $chunkSize = 500;

    public function execute(EloquentBuilder $query, array $fields, array $attributes): void
    {
        Assert::isInstanceOf($query->getModel(), Product::class, 'Product query expected');

        $this->prepareQuery($query)
            ->chunkById($this->getChunkSize(), function (Collection $chunk) use ($fields, $attributes) {
                $ids = $chunk->pluck('id')->all();

                MassPatchProductsJob::dispatch(
                    BulkOperation::registerPatchProducts($ids),
                    $ids,
                    $fields,
                    $attributes
                );
            });
    }

    private function prepareQuery(EloquentBuilder $initQuery): QueryBuilder
    {
        return $initQuery->getQuery()
            ->select('id');
    }

    public function getChunkSize(): int
    {
        return $this->chunkSize;
    }
}
