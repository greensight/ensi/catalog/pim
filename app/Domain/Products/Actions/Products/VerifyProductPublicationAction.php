<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\ProductPublisher;
use App\Domain\Products\Publication\ProductPublisherFactory;
use App\Domain\Support\Concerns\AppliesToAggregate;

class VerifyProductPublicationAction
{
    use AppliesToAggregate;

    public function __construct(private readonly ProductPublisherFactory $publisherFactory)
    {
    }

    public function execute(int $productId): void
    {
        $this->apply($productId, function (Product $product) {
            $this->createPublisher()
                ->verify(ProductAggregate::fromDraft($product));
        });
    }

    private function createPublisher(): ProductPublisher
    {
        return $this->publisherFactory->create();
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
