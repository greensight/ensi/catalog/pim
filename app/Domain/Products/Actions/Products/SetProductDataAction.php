<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Categories\Actions\Categories\SetCategoryProductLinksAction;
use App\Domain\Products\Actions\Attributes\SetProductAttributesAction;
use App\Domain\Products\Actions\Images\SetProductImagesAction;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\InteractsWithModels;

class SetProductDataAction
{
    use InteractsWithModels;

    public function __construct(
        private readonly ValidateProductAction $validateProductAction,
        private readonly SetProductImagesAction $imagesAction,
        private readonly SetProductAttributesAction $attributesAction,
        private readonly SetCategoryProductLinksAction $categoryLinksAction,
    ) {
    }

    public function execute(Product $product, array $fields, bool $replace, bool $replaceCategories = false): int
    {
        $changes = 0;
        $fillableFields = $replace
            ? $this->makeReplaceFields($product, $fields)
            : $fields;

        $this->validateProductAction->execute($product, $fillableFields);

        $product->fill($fillableFields);
        $product->setTariffingVolume();

        if ($product->isDirty()) {
            $this->saveOrThrow($product);
            $changes++;
        }

        if (isset($fields['category_ids'])) {
            $changes += $this->categoryLinksAction->execute($product, $fields['category_ids'], $replaceCategories);
            $product->load('categories');
        }

        if (isset($fields['images'])) {
            $changes += $this->imagesAction->execute($product, $fields['images'], $replace);
            $product->load('images');
        }

        if (isset($fields['attributes'])) {
            $changes += $this->attributesAction->execute($product, $fields['attributes'], $replace);
            $product->load('properties');
        }

        return $changes;
    }

    private function makeReplaceFields(Product $product, array $fields): array
    {
        $notNullFields = ['allow_publish', 'status_id'];

        $fillable = array_filter($product->getFillable(), function (string $name) use ($notNullFields, $fields) {
            return !in_array($name, $notNullFields) || array_key_exists($name, $fields);
        });

        return data_combine_assoc($fillable, $fields);
    }
}
