<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\AppliesToAggregate;

class CreateProductAction
{
    use AppliesToAggregate;

    public function __construct(private readonly SetProductDataAction $productAction)
    {
    }

    public function execute(array $fields): Product
    {
        if (!isset($fields['category_ids'])) {
            $fields['category_ids'] = [Category::getOrCreateDefault()->id];
        }

        $product = $this->fillAndSave($this->createModel(), $fields);

        ProductInvalidated::dispatch($product->id);

        return $product;
    }

    private function fillAndSave(Product $product, array $fields): Product
    {
        return $this->transaction(function () use ($product, $fields) {
            $this->setRootEntity($product);

            $this->productAction->execute($product, $fields, false);

            return $product;
        });
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
