<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Categories\Models\CategoryPropertyView;
use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Support\Collection;
use Webmozart\Assert\Assert;

class CalculateProductsCommonAttributesAction
{
    public function execute(EloquentBuilder $initQuery): Collection
    {
        Assert::isInstanceOf($initQuery->getModel(), Product::class, 'Product query expected');

        $overallCommonPropertyIds = [];
        $overallCommonPropertiesRequired = [];

        $this->prepareQuery($initQuery)->chunk(500, function (Collection $chunk) use (&$overallCommonPropertyIds, &$overallCommonPropertiesRequired) {
            /** @phpstan-ignore-next-line  */
            $categories = $chunk->map(fn (Product $product) => $product->categoryProductLinks)
                ->flatten()
                ->pluck('category_id')
                ->unique();

            $commonPropertiesInChunk = CategoryPropertyView::query()
                ->selectRaw('property_id, bool_or(is_required) as is_required')
                ->where('is_common', false)
                ->whereIn('category_id', $categories)
                ->groupBy('property_id')
                ->havingRaw('count(*) = ?', [count($categories)])
                ->get()
                ->pluck('is_required', 'property_id');

            $commonPropertiesRequiredInChunk = $commonPropertiesInChunk->all();
            $commonPropertyIdsInChunk = $commonPropertiesInChunk->keys()->all();

            $overallCommonPropertiesRequired = $this->mergeRequiredPropertySign($overallCommonPropertiesRequired, $commonPropertiesRequiredInChunk);
            $overallCommonPropertyIds = empty($overallCommonPropertyIds) ? $commonPropertyIdsInChunk : array_intersect($overallCommonPropertyIds, $commonPropertyIdsInChunk);

            // Выходим, если общее пересечение опустело
            if (empty($overallCommonPropertyIds)) {
                return false;
            }

            return true;
        });

        $properties = $this->loadCommonWithAdditionals($overallCommonPropertyIds);

        return $this->enrichCommonProperties($properties, $overallCommonPropertiesRequired);
    }

    private function loadCommonWithAdditionals(array $ids): Collection
    {
        return Property::common()
            ->orWhereIn('id', $ids)
            ->with('directory')
            ->get();
    }

    private function enrichCommonProperties(Collection $properties, $requiredProperties): Collection
    {
        return $properties->map(function (Property $property) use ($requiredProperties) {
            if (key_exists($property->id, $requiredProperties)) {
                $property->is_required = $property->is_required || $requiredProperties[$property->id];
            }

            return $property;
        });
    }

    private function prepareQuery(EloquentBuilder $initQuery): EloquentBuilder
    {
        return $initQuery
            ->with(['categoryProductLinks' => function ($query) {
                $query->select('category_id', 'product_id');
            }])
            ->select('id');
    }

    private function mergeRequiredPropertySign(array $common, array $current): array
    {
        foreach ($current as $key => $value) {
            if (key_exists($key, $common)) {
                $value = $common[$key] || $value;
            }
            $common[$key] = $value;
        }

        return $common;
    }
}
