<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\ProductPublisherFactory;
use App\Domain\Support\Concerns\AppliesToAggregate;
use Psr\Log\LoggerAwareTrait;

class ActualizeProductAction
{
    use AppliesToAggregate;
    use LoggerAwareTrait;

    public function __construct(private readonly ProductPublisherFactory $publisherFactory)
    {
        $this->logger = logger()->channel('products:actualize');
    }

    public function execute(int $productId): void
    {
        try {
            $this->apply($productId, function (Product $product) {
                $aggregate = ProductAggregate::fromDraft($product);

                $this->publisherFactory->create()->publish($aggregate, true);
            });
        } catch (\Exception $e) {
            $this->logger?->error(
                "Product actualization failed: \"{$e->getMessage()}\"",
                ['id' => $productId, 'exception' => $e],
            );
        }
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
