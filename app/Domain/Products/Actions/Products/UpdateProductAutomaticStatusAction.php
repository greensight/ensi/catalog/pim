<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Data\EventConditionsData;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductEvent;
use App\Http\ApiV1\OpenApiGenerated\Enums\EventOperationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusTypeEnum;
use Illuminate\Support\Collection;

class UpdateProductAutomaticStatusAction
{
    public function execute(int $productId): void
    {
        /** @var Product $product */
        $product = Product::query()->with(['events'])->findOrFail($productId);

        $queryStatus = $product->status ? $product->status->nextStatuses() : ProductStatusSetting::query();

        /** @var Collection<ProductStatusSetting> $availableNextStatus */
        $availableNextStatus = $queryStatus
            ->where('type', ProductStatusTypeEnum::AUTOMATIC)
            ->where('is_active', true)
            ->orderBy('created_at')
            ->get();

        foreach ($availableNextStatus as $status) {
            if ($this->checkingTransitionPossibility($status->events, $product->events)) {
                if ($status->is_publication) {
                    $product->allow_publish = true;
                }
                $product->status_id = $status->id;
                $product->save();

                ProductInvalidated::dispatch($product->id);

                break;
            }
        }
    }

    /**
     * @param EventConditionsData $conditions
     * @param Collection<ProductEvent> $productEvents
     * @return bool
     */
    protected function checkingTransitionPossibility(EventConditionsData $conditions, Collection $productEvents): bool
    {
        $lastEvent = $productEvents->last()?->event_id;

        return match ($conditions->operation) {
            null => current($conditions->events) == $lastEvent,
            EventOperationEnum::AND => $this->checkAndConditions($conditions->events, $productEvents),
            EventOperationEnum::OR => $this->checkOrConditions($conditions->events, $lastEvent),
        };
    }

    /**
     * @param ProductEventEnum[] $events
     * @param Collection<ProductEvent> $productEvents
     */
    private function checkAndConditions(array $events, Collection $productEvents): bool
    {
        $productEvents = $productEvents->keyBy(function (ProductEvent $item) {
            return $item->event_id->value;
        });

        foreach ($events as $event) {
            if (!$productEvents->has($event->value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param ProductEventEnum[] $events
     */
    private function checkOrConditions(array $events, ?ProductEventEnum $productEvent): bool
    {
        return in_array($productEvent, $events);
    }
}
