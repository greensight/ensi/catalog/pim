<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Products\Actions\ProductGroups\ForceDeleteProductFromProductGroupAction;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\AppliesToAggregate;

class DeleteProductAction
{
    use AppliesToAggregate;

    public function __construct(
        protected readonly ForceDeleteProductFromProductGroupAction $deleteProductFromProductGroupAction
    ) {
    }

    public function execute(int $productId): void
    {
        $this->transaction(function () use ($productId) {
            $this->updateOrCreate($productId, function (Product $product) {
                $product->status_id = null;
                $product->system_status_id = ProductSystemStatus::DELETED;
            });

            $this->deleteProductFromProductGroupAction->execute($productId);
        });

        ProductInvalidated::dispatch($productId);
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
