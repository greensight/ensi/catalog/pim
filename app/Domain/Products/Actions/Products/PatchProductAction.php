<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\AppliesToAggregate;

class PatchProductAction
{
    use AppliesToAggregate;

    public function __construct(private readonly SetProductDataAction $productAction)
    {
    }

    public function execute(int $productId, array $fields): Product
    {
        $product = $this->apply($productId, function (Product $product) use ($fields) {
            $this->productAction->execute($product, $fields, false, true);

            return $product;
        });

        ProductInvalidated::dispatch($product->id);

        return $product;
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
