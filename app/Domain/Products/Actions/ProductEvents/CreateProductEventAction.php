<?php

namespace App\Domain\Products\Actions\ProductEvents;

use App\Domain\Products\Actions\Products\UpdateProductAutomaticStatusAction;
use App\Domain\Products\Models\ProductEvent;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;

class CreateProductEventAction
{
    public function __construct(protected readonly UpdateProductAutomaticStatusAction $updateProductStatusAction)
    {
    }

    public function execute(int $productId, ProductEventEnum $event): void
    {
        $productEvent = new ProductEvent();
        $productEvent->event_id = $event;
        $productEvent->product_id = $productId;
        $productEvent->save();

        $this->updateProductStatusAction->execute($productId);
    }
}
