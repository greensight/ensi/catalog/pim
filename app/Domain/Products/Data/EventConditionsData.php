<?php

namespace App\Domain\Products\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\EventOperationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Fluent;
use Illuminate\Validation\Rule;

/**
 * @property EventOperationEnum|null $operation - операция
 * @property ProductEventEnum[] $events - список событий продукта
 */
class EventConditionsData extends Fluent implements Castable
{
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $this->formatData($key, $value);
        }
    }

    private function formatData(string $key, mixed $value): mixed
    {
        return match ($key) {
            'operation' => $value instanceof EventOperationEnum ? $value : EventOperationEnum::tryFrom($value),
            'events' => array_map(fn ($item) => $item instanceof ProductEventEnum ? $item : ProductEventEnum::tryFrom($item), $value),
            default => null,
        };
    }

    public static function rules(): array
    {
        return [
            "operation" => ['nullable', Rule::enum(EventOperationEnum::class)],
            "events" => ['required', 'array', 'min:1'],
            "events.*" => ['required', Rule::enum(ProductEventEnum::class)],
        ];
    }

    public function toArray(): array
    {
        return array_intersect_key($this->attributes, self::rules());
    }

    public static function castUsing(array $arguments): CastsAttributes
    {
        return new class () implements CastsAttributes {
            public function get($model, string $key, mixed $value, array $attributes): ?EventConditionsData
            {
                return isset($attributes[$key]) ? new EventConditionsData(json_decode($attributes[$key])) : null;
            }

            public function set($model, string $key, mixed $value, array $attributes): ?string
            {
                return match (true) {
                    is_array($value) => (new EventConditionsData($value))->toJson(),
                    $value instanceof EventConditionsData => $value->toJson(),
                    default => null,
                };
            }
        };
    }
}
