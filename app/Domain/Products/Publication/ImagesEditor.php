<?php

namespace App\Domain\Products\Publication;

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Publication\Data\Field;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\Data\ReviewResult;
use Illuminate\Support\Collection;

class ImagesEditor implements Editor
{
    public const FIELD_NAME = 'images';

    public function __construct(private readonly Field $field)
    {
    }

    public static function new(bool $required = false, bool $moderated = false): static
    {
        $field = new Field(
            self::FIELD_NAME,
            PropertyType::IMAGE,
            null,
            $required,
            $moderated,
            MetricsCategory::CONTENT
        );

        return new static($field);
    }

    public function update(ProductAggregate $aggregate): void
    {
        if (!$this->field->moderated) {
            $this->commit($aggregate);
        }
    }

    public function commit(ProductAggregate $aggregate): void
    {
        $aggregate->images->each(fn (ProductImage $image) => $image->commit());

        /** @var PublishedImage $mainImage */
        $mainImage = $aggregate->publishedImages()
            ->sortBy('sort')
            ->first();

        if (isset($mainImage->file)) {
            $aggregate->release->main_image_file = $mainImage->file;
        }
    }

    public function review(ProductAggregate $aggregate): Collection
    {
        $filled = $aggregate->images
            ->filter(fn (ProductImage $image) => $image->isValid())
            ->isNotEmpty();

        return $this->makeReviewResults($filled);
    }

    public function verify(ProductAggregate $aggregate): Collection
    {
        return $this->makeReviewResults(
            $aggregate->publishedImages()->isNotEmpty()
        );
    }

    private function makeReviewResults(bool $filled): Collection
    {
        return new Collection([
            ReviewResult::fromField($this->field, $filled),
        ]);
    }

    public function rollback(ProductAggregate $aggregate): void
    {
        $aggregate->images->each(fn (ProductImage $image) => $image->rollback());
    }
}
