<?php

namespace App\Domain\Products\Publication;

use App\Domain\Categories\Models\CategoryPropertyView;
use App\Domain\Products\Models\BasePropertyValue;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\Data\ReviewResult;
use Illuminate\Support\Collection;

class AttributesEditor implements Editor
{
    private Collection $propertySuites;

    public function __construct()
    {
        $this->propertySuites = new Collection();
    }

    public function update(ProductAggregate $aggregate): void
    {
        $unmoderated = $this->getCategoryProperties($aggregate->draft->getCategoryIds()->all())
            ->where('is_moderated', false);

        $aggregate->attributes
            ->whereIn('property_id', $unmoderated->keys())
            ->each(fn (ProductPropertyValue $value) => $value->commit());
    }

    public function commit(ProductAggregate $aggregate): void
    {
        $aggregate->attributes->each(fn (ProductPropertyValue $value) => $value->commit());
    }

    /**
     * @param int[] $categoryIds
     * @return Collection|CategoryPropertyView[]
     */
    private function getCategoryProperties(array $categoryIds): Collection
    {
        $suits = new Collection();

        $suitesToLoad = [];
        foreach ($categoryIds as $categoryId) {
            $suite = $this->propertySuites->get($categoryId);

            if ($suite !== null) {
                $suits->push($suite);

                continue;
            }

            $suitesToLoad[] = $categoryId;
        }

        CategoryPropertyView::query()
            ->whereIn('category_id', $suitesToLoad)
            ->where('is_active', true)
            ->get()
            ->groupBy('category_id')
            ->each(function (Collection $properties, int $categoryId) use ($suits) {
                $this->propertySuites->put($categoryId, $properties);
                $suits->push($properties);
            });

        return $suits->flatten()->unique()->keyBy('property_id');
    }

    public function rollback(ProductAggregate $aggregate): void
    {
        $aggregate->attributes
            ->each(fn (ProductPropertyValue $value) => $value->rollback());
    }

    public function review(ProductAggregate $aggregate): Collection
    {
        return $this->reviewProperties(
            $this->getCategoryProperties($aggregate->draft->getCategoryIds()->all()),
            $aggregate->attributes
        );
    }

    public function verify(ProductAggregate $aggregate): Collection
    {
        $this->actualize($aggregate);

        return $this->reviewProperties(
            $this->getCategoryProperties($aggregate->release->getCategoryIds()->all()),
            $aggregate->publishedAttributes()
        );
    }

    private function reviewProperties(Collection $properties, Collection $values): Collection
    {
        return $properties->map(
            fn (CategoryPropertyView $item) =>
                $this->reviewProperty($item, $values->where('property_id', $item->property_id))
        );
    }

    private function reviewProperty(CategoryPropertyView $property, ?Collection $values): ReviewResult
    {
        if ($values === null) {
            return ReviewResult::fromProperty($property, false);
        }

        $filled = $values->filter(fn (BasePropertyValue $value) => $value->isValid())->isNotEmpty();

        return ReviewResult::fromProperty($property, $filled);
    }

    private function actualize(ProductAggregate $aggregate): void
    {
        $bound = $this->getCategoryProperties($aggregate->release->getCategoryIds()->all());

        foreach ($aggregate->publishedAttributes() as $value) {
            $bound->has($value->property_id)
                ? $value->resume()
                : $value->suspend();
        }
    }
}
