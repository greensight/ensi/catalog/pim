<?php

namespace App\Domain\Products\Publication\Data;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Classifiers\Models\ProductFieldSettings;
use ArrayAccess;
use BackedEnum;
use Webmozart\Assert\Assert;

/**
 * @method static static STRING(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 * @method static static INTEGER(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 * @method static static DOUBLE(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 * @method static static BOOLEAN(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 * @method static static DATETIME(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 * @method static static IMAGE(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 * @method static static COLOR(string $code, mixed $nullValue = null, bool $required = false, bool $moderated = false)
 */
class Field
{
    public readonly string $name;

    public function __construct(
        public readonly string $code,
        public readonly PropertyType $type,
        public readonly mixed $nullValue = null,
        public readonly bool $required = false,
        public readonly bool $moderated = false,
        public readonly ?MetricsCategory $category = null,
        ?string $name = null
    ) {
        $this->name = $name ?? $this->code;
    }

    public function customize(?ProductFieldSettings $settings): static
    {
        return $settings === null
            ? $this
            : new static(
                $this->code,
                $this->type,
                $this->nullValue,
                $settings->is_required,
                $settings->is_moderated,
                $settings->metrics_category,
                $settings->name
            );
    }

    public function value(ArrayAccess $model): PropertyValue
    {
        $raw = $model[$this->code];

        if ($raw instanceof BackedEnum) {
            $raw = $raw->value;
        }

        return $raw === $this->nullValue
            ? PropertyValue::make($this->type)
            : PropertyValue::make($this->type, $raw);
    }

    public function copy(ArrayAccess $source, ArrayAccess $dest): void
    {
        $dest[$this->code] = $source[$this->code];
    }

    public static function __callStatic(string $name, array $arguments)
    {
        Assert::countBetween($arguments, 1, 4);

        $type = PropertyType::from(strtolower($name));

        $code = $arguments['code'] ?? $arguments[0];
        $nullValue = $arguments['nullValue'] ?? $arguments[1] ?? null;
        $required = $arguments['required'] ?? $arguments[2] ?? false;
        $moderated = $arguments['moderated'] ?? $arguments[3] ?? false;

        return new static($code, $type, $nullValue, $required, $moderated);
    }
}
