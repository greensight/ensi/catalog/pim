<?php

namespace App\Domain\Products\Publication\Data;

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Products\Models\ProductMetrics;
use App\Domain\Support\Concerns\InteractsWithModels;
use Illuminate\Support\Collection;

class MetricsEditor
{
    use InteractsWithModels;

    /** @var Collection<string, ProductMetrics> */
    private Collection $original;

    /** @var Collection<string, ProductMetrics> */
    private ?Collection $dirty = null;

    public function __construct(private readonly int $productId)
    {
        $this->original = ProductMetrics::whereProductId($this->productId)
            ->get()
            ->keyBy(fn (ProductMetrics $metrics) => $metrics->category->value);
    }

    public function edit(): static
    {
        if ($this->dirty === null) {
            $this->dirty = new Collection();
        }

        return $this;
    }

    public function set(MetricsCategory $category, int $total, int $filled, int $errors): static
    {
        $this->edit();

        $metrics = $this->pullOrCreate($category);

        $metrics->total = $total;
        $metrics->filled = $filled;
        $metrics->errors = $errors;

        $this->dirty[$category->value] = $metrics;

        return $this;
    }

    public function get(MetricsCategory $category): ?ProductMetrics
    {
        return $this->dirty?->get($category->value) ?? $this->original->get($category->value);
    }

    public function save(): void
    {
        if ($this->dirty === null) {
            return;
        }

        $this->dirty->each(fn (ProductMetrics $metrics) => $this->saveOrThrow($metrics));
        $this->original->each(fn (ProductMetrics $metrics) => $this->deleteOrThrow($metrics));

        $this->original = $this->dirty;
        $this->dirty = null;
    }

    private function pullOrCreate(MetricsCategory $category): ProductMetrics
    {
        $metrics = $this->original->pull($category->value);
        if ($metrics !== null) {
            return $metrics;
        }

        $metrics = new ProductMetrics();
        $metrics->product_id = $this->productId;
        $metrics->category = $category;

        return $metrics;
    }
}
