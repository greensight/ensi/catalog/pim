<?php

namespace App\Domain\Products\Publication\Data;

use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Products\Models\ProductFlagValue;
use Illuminate\Support\Collection;

class FlagSet
{
    /** @var Collection<int, ProductFlagValue> */
    private Collection $original;

    /** @var Collection<int, bool> */
    private Collection $flags;

    public function __construct(private readonly int $productId)
    {
        $this->original = ProductFlagValue::whereProductId($this->productId)
            ->get()
            ->keyBy(fn (ProductFlagValue $entry) => $entry->value->value);

        $this->flags = $this->original->map(fn () => true);
    }

    public function set(ProductFlag $flag, bool $active = true): static
    {
        if (!$this->flags->has($flag->value) || !$active) {
            $this->flags[$flag->value] = $active;
        }

        return $this;
    }

    public function has(ProductFlag $flag): bool
    {
        return $this->flags[$flag->value] ?? false;
    }

    public function all(): array
    {
        return $this->flags
            ->filter()
            ->keys()
            ->map(fn (int $key) => ProductFlag::from($key))
            ->all();
    }

    public function save(): void
    {
        $dirty = new Collection();

        foreach ($this->flags as $flag => $active) {
            if (!$active) {
                continue;
            }

            $existing = $this->pullOrCreate($flag);
            $dirty->put($flag, $existing);
        }

        $this->original->each(fn (ProductFlagValue $entry) => $entry->delete());
        $this->original = $dirty;
    }

    private function pullOrCreate(int $flagId): ProductFlagValue
    {
        return $this->original->pull($flagId)
            ?? ProductFlagValue::create(['product_id' => $this->productId, 'value' => ProductFlag::from($flagId)]);
    }
}
