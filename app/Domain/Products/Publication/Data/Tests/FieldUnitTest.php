<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Publication\Data\Field;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Support\Fluent;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('publication', 'unit');

test('convert value', function (PropertyType $type, mixed $value, PropertyValue $expected) {
    $model = new Fluent(['foo' => $value]);
    $field = new Field('foo', $type);

    expect($field->value($model))->toEqual($expected);
})->with([
    PropertyType::STRING->value => [PropertyType::STRING, 'bar', PropertyValue::STRING('bar')],
    PropertyType::INTEGER->value => [PropertyType::INTEGER, 121, PropertyValue::INTEGER(121)],
    PropertyType::DOUBLE->value => [PropertyType::DOUBLE, 52.55, PropertyValue::DOUBLE(52.55)],
    PropertyType::BOOLEAN->value => [PropertyType::BOOLEAN, true, PropertyValue::BOOLEAN(true)],
    PropertyType::IMAGE->value => [PropertyType::IMAGE, EnsiFile::factory()->fileName('image')->fileExt('png')->make()['path'], PropertyValue::IMAGE(EnsiFile::factory()->fileName('image')->fileExt('png')->make()['path'])],
    PropertyType::FILE->value => [PropertyType::FILE, EnsiFile::factory()->fileName('file')->fileExt('txt')->make()['path'], PropertyValue::FILE(EnsiFile::factory()->fileName('file')->fileExt('txt')->make()['path'])],
    PropertyType::COLOR->value => [PropertyType::COLOR, '#FF00FF', PropertyValue::COLOR('#FF00FF')],
    'null' => [PropertyType::STRING, null, PropertyValue::STRING()],
]);

test('convert value with custom null', function (PropertyType $type, mixed $value) {
    $model = new Fluent(['foo' => $value]);
    $field = new Field('foo', $type, $value);

    expect($field->value($model)->isNull())->toBeTrue();
})->with([
    PropertyType::STRING->value => [PropertyType::STRING, ''],
    PropertyType::INTEGER->value => [PropertyType::INTEGER, 0],
    PropertyType::DOUBLE->value => [PropertyType::DOUBLE, 0.0],
]);

test('construct dynamic', function () {
    $field = Field::BOOLEAN('foo', false, true);

    expect($field->type)->toEqual(PropertyType::BOOLEAN)
        ->and($field->code)->toBe('foo')
        ->and($field->nullValue)->toBeFalse()
        ->and($field->required)->toBeTrue();
});

test('construct dynamic with named argument', function () {
    $field = Field::STRING('foo', moderated: true);

    expect($field->moderated)->toBeTrue();
});
