<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Publication\Data\ProductAggregate;
use Ensi\LaravelTestFactories\FakerProvider;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('load trashed attribute', function () {
    $category = Category::factory()->create();
    $property = Property::factory()->create();
    CategoryPropertyLink::factory()->createFast($category, $property);

    $product = Product::factory()->createOne();
    CategoryProductLink::factory()->createFast($category, $product);

    $attribute = ProductPropertyValue::factory()->forProduct($product)->forProperty($property)->createOne();
    $attribute->delete();

    $aggregate = ProductAggregate::fromDraft($product);

    expect($aggregate->attributes)->toHaveCount(1);
});

test('load trashed attribute release', function () {
    $category = Category::factory()->create();
    $property = Property::factory()->create();
    CategoryPropertyLink::factory()->createFast($category, $property);

    $product = Product::factory()->createOne();
    CategoryProductLink::factory()->createFast($category, $product);

    $attribute = ProductPropertyValue::factory()
        ->forProduct($product)
        ->published()
        ->createOne();

    $attribute->release->delete();

    $aggregate = ProductAggregate::fromDraft($product);

    expect($aggregate->publishedAttributes())->toHaveCount(1);
});

test('load trashed images', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();
    $image->delete();

    $aggregate = ProductAggregate::fromDraft($image->product);

    expect($aggregate->images)->toHaveCount(1);
})->with(FakerProvider::$optionalDataset);
