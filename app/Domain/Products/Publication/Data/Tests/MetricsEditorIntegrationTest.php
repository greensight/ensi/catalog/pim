<?php

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductMetrics;
use App\Domain\Products\Publication\Data\MetricsEditor;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('add new metrics', function () {
    $product = Product::factory()->createOne();
    $editor = new MetricsEditor($product->id);

    $editor->set(MetricsCategory::MASTER, 8, 5, 1)->save();

    expect($product->metrics()->count())->toBe(1);
    assertDatabaseHas(
        ProductMetrics::class,
        [
            'product_id' => $product->id,
            'category' => MetricsCategory::MASTER,
            'total' => 8,
            'filled' => 5,
            'errors' => 1,
            'ratio' => 62.5,
        ]
    );
});

test('replace existing metrics', function () {
    $metrics = ProductMetrics::factory()->errors(1)->createOne();
    $editor = new MetricsEditor($metrics->product_id);

    $editor->set($metrics->category, 10, 5, 0)->save();

    expect(ProductMetrics::whereProductId($metrics->product_id)->count())->toBe(1);
    assertDatabaseHas(
        ProductMetrics::class,
        [
            'id' => $metrics->id,
            'total' => 10,
            'filled' => 5,
            'errors' => 0,
            'ratio' => 50.0,
        ]
    );
});

test('remove old metrics', function () {
    $metrics = ProductMetrics::factory()->errors(1)->createOne();
    $editor = new MetricsEditor($metrics->product_id);

    $editor->edit()->save();

    assertModelMissing($metrics);
});

test('multiple set', function () {
    $product = Product::factory()->createOne();
    $editor = new MetricsEditor($product->id);

    $editor->edit()
        ->set(MetricsCategory::MASTER, 10, 10, 0)
        ->set(MetricsCategory::ATTRIBUTES, 15, 8, 2)
        ->save();

    expect($product->metrics()->count())->toBe(2);
});

test('save without edit does nothing', function () {
    $metrics = ProductMetrics::factory()->createOne();
    $editor = new MetricsEditor($metrics->product_id);

    $editor->save();

    assertModelExists($metrics);
});

test('get original metrics', function () {
    $metrics = ProductMetrics::factory()->createOne();
    $editor = new MetricsEditor($metrics->product_id);

    expect($editor->get($metrics->category)?->id)->toBe($metrics->id);
});

test('get dirty metrics', function () {
    $product = Product::factory()->createOne();
    $editor = new MetricsEditor($product->id);

    $editor->set(MetricsCategory::MASTER, 3, 2, 1);

    expect($editor->get(MetricsCategory::MASTER))->not->toBeNull();
});

test('get missing metrics', function () {
    $product = Product::factory()->createOne();
    $editor = new MetricsEditor($product->id);

    expect($editor->get(MetricsCategory::MASTER))->toBeNull();
});
