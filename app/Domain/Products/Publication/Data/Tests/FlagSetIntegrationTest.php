<?php

use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductFlagValue;
use App\Domain\Products\Publication\Data\FlagSet;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('load existing flags', function () {
    $product = Product::factory()->withFlags(ProductFlag::REQUIRED_MASTER_DATA)->createOne();
    $set = new FlagSet($product->id);

    expect($set->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeTrue();
    expect($set->all())->toEqual([ProductFlag::REQUIRED_MASTER_DATA]);
});

test('activate flag', function () {
    $product = Product::factory()->createOne();
    $set = new FlagSet($product->id);

    $set->set(ProductFlag::REQUIRED_ATTRIBUTES);

    expect($set->has(ProductFlag::REQUIRED_ATTRIBUTES));
    expect($set->all())->toEqual([ProductFlag::REQUIRED_ATTRIBUTES]);
});

test('deactivate flag', function () {
    $product = Product::factory()->withFlags(ProductFlag::REQUIRED_MASTER_DATA)->createOne();
    $set = new FlagSet($product->id);

    $set->set(ProductFlag::REQUIRED_MASTER_DATA, false);

    expect($set->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeFalse();
    expect($set->all())->toEqual([]);
});

test('save new flag', function () {
    $product = Product::factory()->createOne();
    $set = new FlagSet($product->id);

    $set->set(ProductFlag::REQUIRED_ATTRIBUTES)->save();

    expect($set->has(ProductFlag::REQUIRED_ATTRIBUTES))->toBeTrue();
    assertDatabaseHas(
        ProductFlagValue::class,
        ['product_id' => $product->id, 'value' => ProductFlag::REQUIRED_ATTRIBUTES]
    );
});

test('save skips existing flags', function () {
    $product = Product::factory()
        ->withFlags(ProductFlag::REQUIRED_MASTER_DATA, ProductFlag::NOT_AGREED)
        ->createOne();

    $set = new FlagSet($product->id);
    $set->set(ProductFlag::NOT_AGREED)->save();

    expect($set->all())->toEqualCanonicalizing([ProductFlag::REQUIRED_MASTER_DATA, ProductFlag::NOT_AGREED]);
    assertDatabaseHas(
        ProductFlagValue::class,
        ['product_id' => $product->id, 'value' => ProductFlag::REQUIRED_MASTER_DATA]
    );
    assertDatabaseHas(
        ProductFlagValue::class,
        ['product_id' => $product->id, 'value' => ProductFlag::NOT_AGREED]
    );
});

test('save removes not present flags', function () {
    $product = Product::factory()
        ->withFlags(ProductFlag::REQUIRED_MASTER_DATA, ProductFlag::NOT_AGREED)
        ->createOne();

    $set = new FlagSet($product->id);
    $set->set(ProductFlag::REQUIRED_MASTER_DATA, false)->save();

    expect($set->all())->toEqual([ProductFlag::NOT_AGREED]);
    assertDatabaseHas(
        ProductFlagValue::class,
        ['product_id' => $product->id, 'value' => ProductFlag::NOT_AGREED]
    );
});
