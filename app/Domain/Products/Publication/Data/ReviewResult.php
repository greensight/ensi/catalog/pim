<?php

namespace App\Domain\Products\Publication\Data;

use App\Domain\Categories\Models\CategoryPropertyView;
use App\Domain\Classifiers\Enums\MetricsCategory;

class ReviewResult
{
    public readonly bool $error;

    public function __construct(
        public readonly string $field,
        public readonly MetricsCategory $category,
        public readonly bool $required,
        public readonly bool $filled
    ) {
        $this->error = $this->required && !$this->filled;
    }

    public static function fromProperty(CategoryPropertyView $attribute, bool $filled): self
    {
        return new self(
            $attribute->code,
            MetricsCategory::ATTRIBUTES,
            $attribute->is_required,
            $filled
        );
    }

    public static function fromField(Field $field, bool $filled): self
    {
        return new self($field->code, $field->category ?? MetricsCategory::MASTER, $field->required, $filled);
    }
}
