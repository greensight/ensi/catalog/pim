<?php

namespace App\Domain\Products\Publication;

use App\Domain\Products\Publication\Data\Field;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\Data\ReviewResult;
use App\Domain\Support\Models\Model;
use Illuminate\Support\Collection;

class FieldsEditor implements Editor
{
    public function __construct(private readonly Collection $fields)
    {
    }

    public function update(ProductAggregate $aggregate): void
    {
        $this->fields->where('moderated', false)
            ->each(fn (Field $field) => $field->copy($aggregate->draft, $aggregate->release));
    }

    public function commit(ProductAggregate $aggregate): void
    {
        $this->fields->each(fn (Field $field) => $field->copy($aggregate->draft, $aggregate->release));
    }

    public function rollback(ProductAggregate $aggregate): void
    {
        $this->fields->each(fn (Field $field) => $field->copy($aggregate->release, $aggregate->draft));
    }

    public function review(ProductAggregate $aggregate): Collection
    {
        return $this->reviewVersion($aggregate->draft);
    }

    public function verify(ProductAggregate $aggregate): Collection
    {
        return $this->reviewVersion($aggregate->release);
    }

    private function reviewVersion(Model $version): Collection
    {
        return $this->fields
            ->whereNotNull('category')
            ->map(fn (Field $field) => ReviewResult::fromField($field, !$field->value($version)->isNull()));
    }

    public static function new(Field ...$fields): static
    {
        return new static(new Collection($fields));
    }
}
