<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\Data\ReviewResult;
use App\Domain\Products\Publication\ProductPublisher;
use App\Domain\Products\Publication\ProductPublisherFactory;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;

use Tests\Cases\PublicationTestCase;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class, PublicationTestCase::class)->group('publication', 'integration');

test('publish success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()->inCategories([$category])->create();

    ProductImage::factory()->owned($product)->createOne();

    ProductPublisherFactory::new()
        ->create()
        ->publish(ProductAggregate::fromDraft($product), true);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id,  'system_status_id' => null]
    );

    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'allow_publish' => true, 'allow_publish_comment' => null]
    );
})->with(FakerProvider::$optionalDataset);

test('publish calculates metrics', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(reviews: [
            new ReviewResult('name', MetricsCategory::MASTER, true, false),
            new ReviewResult('images', MetricsCategory::CONTENT, false, true),
        ]),
    ]);
    $publisher->publish($aggregate);

    expect($aggregate->metrics->get(MetricsCategory::MASTER)?->errors)->toBe(1);
    expect($aggregate->metrics->get(MetricsCategory::CONTENT)?->filled)->toBe(1);
});

test('publish sets validation result flags', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(reviews: [
            new ReviewResult('name', MetricsCategory::MASTER, true, false),
            new ReviewResult('foo', MetricsCategory::ATTRIBUTES, false, true),
        ]),
    ]);
    $publisher->publish($aggregate);

    expect($aggregate->flags->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeTrue();
    expect($aggregate->flags->has(ProductFlag::REQUIRED_ATTRIBUTES))->toBeFalse();
});

test('publish rejects invalid release', function () {
    $product = Product::factory()->create();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(verifies: [
            new ReviewResult('foo', MetricsCategory::ATTRIBUTES, true, false),
        ]),
    ]);
    $publisher->publish($aggregate);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'system_status_id' => ProductSystemStatus::REJECTED->value]
    );

    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'allow_publish' => false, 'allow_publish_comment' => 'Не заполнен обязательный атрибут']
    );
});

test('publish closes release in inactive category', function () {
    $category = Category::factory()->actualized(false)->createOne();
    $product = Product::factory()->create();
    CategoryProductLink::factory()->createFast($category, $product);
    $aggregate = ProductAggregate::fromDraft($product);

    (new ProductPublisher([]))->publish($aggregate);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'system_status_id' => ProductSystemStatus::CLOSED->value]
    );

    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'allow_publish' => false, 'allow_publish_comment' => 'Категория неактивна']
    );
});

test('publish closes release with inactive brand', function () {
    $brand = Brand::factory()->createOne(['is_active' => false]);
    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()
        ->createOne(['brand_id' => $brand->id]);
    CategoryProductLink::factory()->createFast($category, $product);

    $aggregate = ProductAggregate::fromDraft($product);

    (new ProductPublisher([]))->publish($aggregate);

    assertDatabaseHas(
        Product::class,
        ['id' => $product->id, 'system_status_id' => ProductSystemStatus::CLOSED->value]
    );

    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'allow_publish' => false, 'allow_publish_comment' => 'Бренд неактивен']
    );
});

test('reset calculates metrics and sets validation result flags', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(reviews: [
            new ReviewResult('name', MetricsCategory::MASTER, true, false),
        ]),
    ]);
    $publisher->reset($aggregate);

    expect($aggregate->metrics->get(MetricsCategory::MASTER)?->errors)->toBe(1);
    expect($aggregate->flags->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeTrue();
});

test('verify updates allow_publish release', function () {
    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()
        ->createOne(['allow_publish' => false]);
    CategoryProductLink::factory()->createFast($category, $product);

    $aggregate = ProductAggregate::fromDraft($product);
    $aggregate->draft->allow_publish = true;

    (new ProductPublisher([]))->verify($aggregate);

    assertDatabaseHas(
        PublishedProduct::class,
        ['id' => $product->id, 'allow_publish' => true]
    );
});

test('verify reviews draft', function () {
    $product = Product::factory()->withFlags(ProductFlag::REQUIRED_MASTER_DATA)->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    (new ProductPublisher([]))->verify($aggregate);

    expect($aggregate->flags->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeFalse();
});
