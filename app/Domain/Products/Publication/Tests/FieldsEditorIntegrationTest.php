<?php

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Publication\Data\Field;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\FieldsEditor;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('commit copies moderated field', function () {
    $product = Product::factory()->createOne();
    $product->name = 'foo';

    $aggregate = ProductAggregate::fromDraft($product);
    FieldsEditor::new(Field::STRING('name', moderated: true))->commit($aggregate);

    expect($aggregate->release->name)->toBe('foo');
});

test('update copies unmoderated field', function () {
    $product = Product::factory()->createOne();
    $product->name = 'foo';

    $aggregate = ProductAggregate::fromDraft($product);
    FieldsEditor::new(Field::STRING('name'))->update($aggregate);

    expect($aggregate->release->name)->toBe('foo');
});

test('update ignores moderated field', function () {
    $product = Product::factory()->createOne();
    $product->name = 'foo';

    $aggregate = ProductAggregate::fromDraft($product);
    FieldsEditor::new(Field::STRING('name', moderated: true))->update($aggregate);

    expect($aggregate->release->name)->not->toBe('foo');
});

test('review success', function () {
    $product = Product::factory()->createOne();
    $field = new Field('vendor_code', PropertyType::STRING, required: true, category: MetricsCategory::MASTER);

    $results = FieldsEditor::new($field)
        ->review(ProductAggregate::fromDraft($product));

    expect($results->where('error', true))->toHaveCount(0);
});

test('review failed', function () {
    $product = Product::factory()->createOne(['description' => null]);
    $field = new Field('description', PropertyType::STRING, required: true, category: MetricsCategory::MASTER);

    $results = FieldsEditor::new($field)
        ->review(ProductAggregate::fromDraft($product));

    expect($results->where('error', true))->toHaveCount(1);
});

test('review skips fields without category', function () {
    $product = Product::factory()->createOne(['external_id' => null]);

    $results = FieldsEditor::new(Field::STRING('external_id'))
        ->review(ProductAggregate::fromDraft($product));

    expect($results)->toHaveCount(0);
});

test('verify skips fields without category', function () {
    $product = Product::factory()->createOne(['external_id' => null]);

    $results = FieldsEditor::new(Field::STRING('external_id'))
        ->verify(ProductAggregate::fromDraft($product));

    expect($results)->toHaveCount(0);
});

test('verity uses published version', function () {
    $product = Product::factory()->createOne();
    $field = new Field('vendor_code', PropertyType::STRING, required: true, category: MetricsCategory::MASTER);

    $aggregate = ProductAggregate::fromDraft($product);
    /** @phpstan-ignore-next-line  */
    $aggregate->release->vendor_code = null;

    $results = FieldsEditor::new($field)->verify($aggregate);

    expect($results->where('error', true))->toHaveCount(1);
});

test('rollback copies field values back', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);
    $aggregate->release->name = 'foo';

    FieldsEditor::new(Field::STRING('name'))->rollback($aggregate);

    expect($product->name)->toBe('foo');
});
