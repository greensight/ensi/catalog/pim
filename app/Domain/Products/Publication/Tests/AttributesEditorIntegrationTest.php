<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Domain\Products\Publication\AttributesEditor;
use App\Domain\Products\Publication\Data\ProductAggregate;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\assertNotSoftDeleted;
use function Pest\Laravel\assertSoftDeleted;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('commit deletes marked values', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();
    $value->delete();

    (new AttributesEditor())->commit(ProductAggregate::fromDraft($value->product));

    assertModelMissing($value);
    assertModelMissing($value->release);
});

test('commit publishes new values', function () {
    $value = ProductPropertyValue::factory()->createOne();

    (new AttributesEditor())->commit(ProductAggregate::fromDraft($value->product));

    assertDatabaseHas(PublishedPropertyValue::class, ['id' => $value->id]);
});

test('commit replaces published value', function () {
    $value = ProductPropertyValue::factory()
        ->actual()
        ->published()
        ->createOne();

    $value->update(['name' => 'foo']);

    (new AttributesEditor())->commit(ProductAggregate::fromDraft($value->product));

    assertDatabaseHas(PublishedPropertyValue::class, ['id' => $value->id, 'name' => 'foo']);
});

test('update publishes unmoderated attributes', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $value = ProductPropertyValue::factory()->forProduct($product)->actual()->createOne();

    (new AttributesEditor())->update(ProductAggregate::fromDraft($value->product));

    assertDatabaseHas(PublishedPropertyValue::class, ['id' => $value->id]);
});

test('update does not change moderated attributes', function () {
    $property = Property::factory()->moderated()->createOne();
    $value = ProductPropertyValue::factory()
        ->forProperty($property)
        ->actual()
        ->createOne();

    (new AttributesEditor())->update(ProductAggregate::fromDraft($value->product));

    assertDatabaseMissing(PublishedPropertyValue::class, ['id' => $value->id]);
});

test('review failed', function () {
    $category = Category::factory()->create();
    $property = Property::factory()->moderated()->createOne();
    $product = Product::factory()->createOne();
    CategoryProductLink::factory()->createFast($category, $product);
    ActualCategoryProperty::factory()
        ->forCategory($category)
        ->forProperty($property)
        ->required()
        ->createOne();

    $reviews = (new AttributesEditor())->review(ProductAggregate::fromDraft($product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('review success', function () {
    $property = Property::factory()->moderated()->createOne();
    $value = ProductPropertyValue::factory()
        ->forProperty($property)
        ->actual(true)
        ->createOne();

    $reviews = (new AttributesEditor())->review(ProductAggregate::fromDraft($value->product));

    expect($reviews->where('error', true))->toHaveCount(0);
});

test('review skips unbound attributes', function () {
    $value = ProductPropertyValue::factory()->createOne();

    $reviews = (new AttributesEditor())->review(ProductAggregate::fromDraft($value->product));

    expect($reviews)->toHaveCount(0);
});

test('verify success', function () {
    $value = ProductPropertyValue::factory()
        ->actual(true)
        ->published()
        ->createOne();

    $reviews = (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    expect($reviews->where('error', true))->toHaveCount(0);
});

test('verify failed', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $value = ProductPropertyValue::factory()->forProduct($product)->actual(true)->createOne();

    $reviews = (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('verify skips unbound attributes', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();

    $results = (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    expect($results)->toHaveCount(0);
});

test('verify marks for deletion missing in the category', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();

    (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    assertSoftDeleted(PublishedPropertyValue::class, ['id' => $value->id]);
});

test('verify restores available in the category', function () {
    $category = Category::factory()->create();
    $product = Product::factory()->inCategories([$category])->createOne();
    $value = ProductPropertyValue::factory()
        ->published()
        ->forProduct($product)
        ->actual()
        ->createOne();

    $value->release->delete();

    (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    assertNotSoftDeleted(PublishedPropertyValue::class, ['id' => $value->id]);
});

test('rollback deletes unpublished value', function () {
    $value = ProductPropertyValue::factory()->createOne();

    (new AttributesEditor())->rollback(ProductAggregate::fromDraft($value->product));

    assertModelMissing($value);
});

test('rollback restores published value', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();
    $value->delete();

    (new AttributesEditor())->rollback(ProductAggregate::fromDraft($value->product));

    assertNotSoftDeleted($value);
});
