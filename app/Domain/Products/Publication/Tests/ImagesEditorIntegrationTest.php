<?php

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\ImagesEditor;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\assertNotSoftDeleted;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('commit publishes image', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    ImagesEditor::new(moderated: true)->commit(ProductAggregate::fromDraft($image->product));

    assertDatabaseHas(PublishedImage::class, ['id' => $image->id]);
})->with(FakerProvider::$optionalDataset);

test('commit sets main image file', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    $aggregate = ProductAggregate::fromDraft($image->product);
    ImagesEditor::new(moderated: true)->commit($aggregate);

    expect($aggregate->release->main_image_file)->toBe($image->file);
})->with(FakerProvider::$optionalDataset);

test('commit deletes marked image', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->published()->createOne();
    $image->delete();

    ImagesEditor::new(moderated: true)->commit(ProductAggregate::fromDraft($image->product));

    assertModelMissing($image);
})->with(FakerProvider::$optionalDataset);

test('commit replaces published image attributes', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->published()->createOne();
    $image->update(['name' => 'foo', 'sort' => 1000]);

    ImagesEditor::new()->commit(ProductAggregate::fromDraft($image->product));

    assertDatabaseHas(
        PublishedImage::class,
        ['id' => $image->id, 'name' => 'foo', 'sort' => 1000]
    );
})->with(FakerProvider::$optionalDataset);

test('update publishes image in unmoderated mode', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    ImagesEditor::new()->update(ProductAggregate::fromDraft($image->product));

    assertDatabaseHas(PublishedImage::class, ['id' => $image->id]);
})->with(FakerProvider::$optionalDataset);

test('update does nothing in moderated mode', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    ImagesEditor::new(moderated: true)->update(ProductAggregate::fromDraft($image->product));

    assertDatabaseMissing(PublishedImage::class, ['id' => $image->id]);
})->with(FakerProvider::$optionalDataset);

test('review success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    $reviews = ImagesEditor::new(true)->review(ProductAggregate::fromDraft($image->product));

    expect($reviews->where('error', true))->toHaveCount(0);
})->with(FakerProvider::$optionalDataset);

test('review failed', function () {
    $product = Product::factory()->createOne();

    $reviews = ImagesEditor::new(true)->review(ProductAggregate::fromDraft($product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('verify success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->published()->createOne();

    $reviews = ImagesEditor::new(true)->verify(ProductAggregate::fromDraft($image->product));

    expect($reviews->where('error', true))->toHaveCount(0);
})->with(FakerProvider::$optionalDataset);

test('verify failed', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    $reviews = ImagesEditor::new(true)->verify(ProductAggregate::fromDraft($image->product));

    expect($reviews->where('error', true))->toHaveCount(1);
})->with(FakerProvider::$optionalDataset);

test('rollback deletes unpublished image', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    ImagesEditor::new()->rollback(ProductAggregate::fromDraft($image->product));

    assertModelMissing($image);
})->with(FakerProvider::$optionalDataset);

test('rollback restores published image', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->published()->createOne();
    $image->delete();

    ImagesEditor::new()->rollback(ProductAggregate::fromDraft($image->product));

    assertNotSoftDeleted($image);
})->with(FakerProvider::$optionalDataset);
