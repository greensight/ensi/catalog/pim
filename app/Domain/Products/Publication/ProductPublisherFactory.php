<?php

namespace App\Domain\Products\Publication;

use App\Domain\Classifiers\Models\ProductFieldSettings;
use App\Domain\Products\Publication\Data\Field;
use Illuminate\Support\Collection;

class ProductPublisherFactory
{
    public function __construct(private readonly Collection $fields)
    {
    }

    public function create(): ProductPublisher
    {
        return new ProductPublisher(new Collection([
            new FieldsEditor($this->fields->except([ImagesEditor::FIELD_NAME])),
            new ImagesEditor($this->fields->get(ImagesEditor::FIELD_NAME)),
            new AttributesEditor(),
        ]));
    }

    public static function new(): static
    {
        $settings = ProductFieldSettings::all()->keyBy('code');

        $fields = static::makeFields()
            ->keyBy('code')
            ->map(fn (Field $field) => $field->customize($settings->get($field->code)));

        return new static($fields);
    }

    public static function makeFields(): Collection
    {
        return new Collection([
            Field::STRING('code'),
            Field::STRING('name', required: true),

            Field::BOOLEAN('allow_publish', required: true),
            Field::INTEGER('status_id', required: true),
            Field::STRING('status_comment'),

            Field::STRING('external_id'),
            Field::STRING('vendor_code', required: true),
            Field::INTEGER('type', required: true),

            Field::STRING('barcode'),
            Field::INTEGER('brand_id'),
            Field::BOOLEAN('is_adult'),

            Field::DOUBLE('weight'),
            Field::DOUBLE('weight_gross'),
            Field::DOUBLE('width'),
            Field::DOUBLE('height'),
            Field::DOUBLE('length'),

            Field::INTEGER('uom'),
            Field::INTEGER('tariffing_volume'),
            Field::DOUBLE('order_step'),
            Field::DOUBLE('order_minvol'),
            Field::DOUBLE('picking_weight_deviation'),

            Field::STRING('description'),
            Field::IMAGE('images'),
        ]);
    }
}
