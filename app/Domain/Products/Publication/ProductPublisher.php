<?php

namespace App\Domain\Products\Publication;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Products\Publication\Data\ProductAggregate;
use Illuminate\Support\Collection;
use Webmozart\Assert\Assert;

class ProductPublisher
{
    /** @var Collection<int, Editor> */
    private readonly Collection $editors;

    public function __construct(Collection|array $editors)
    {
        Assert::allIsInstanceOf($editors, Editor::class);

        $this->editors = Collection::wrap($editors);
    }

    /**
     * Validate and transfer data from draft to clean copy.
     *
     * @param ProductAggregate $aggregate
     * @param bool $moderated transfer moderated attributes
     * @return void
     */
    public function publish(ProductAggregate $aggregate, bool $moderated = false): void
    {
        $this->review($aggregate);

        /** @var Editor $editor */
        foreach ($this->editors as $editor) {
            $moderated ? $editor->commit($aggregate) : $editor->update($aggregate);
        }

        $this->updateStatus($aggregate);

        $aggregate->save();
    }

    /**
     * Reset the changes in the draft that are not in the release.
     *
     * @param ProductAggregate $aggregate
     * @return void
     */
    public function reset(ProductAggregate $aggregate): void
    {
        foreach ($this->editors as $editor) {
            $editor->rollback($aggregate);
        }

        $this->review($aggregate);

        $aggregate->save();
    }

    /**
     * Verify the final state of the published product.
     *
     * @param ProductAggregate $aggregate
     * @return void
     */
    public function verify(ProductAggregate $aggregate): void
    {
        $this->review($aggregate);
        $this->updateStatus($aggregate);

        $aggregate->save();
    }

    private function review(ProductAggregate $aggregate): void
    {
        $reviews = $this->editors->flatMap(fn (Editor $editor) => $editor->review($aggregate));

        foreach (MetricsCategory::cases() as $category) {
            $this->calculateMetrics($aggregate, $category, $reviews->where('category', $category));
        }

        $aggregate->flags->set(
            ProductFlag::REQUIRED_MASTER_DATA,
            !($aggregate->metrics->get(MetricsCategory::MASTER)?->errors === 0)
        );

        $aggregate->flags->set(
            ProductFlag::REQUIRED_ATTRIBUTES,
            !($aggregate->metrics->get(MetricsCategory::ATTRIBUTES)?->errors === 0)
        );
    }

    private function calculateMetrics(ProductAggregate $aggregate, MetricsCategory $category, Collection $reviews): void
    {
        $aggregate->metrics->set(
            $category,
            $reviews->count(),
            $reviews->where('filled', true)->count(),
            $reviews->where('error', true)->count()
        );
    }

    private function updateStatus(ProductAggregate $aggregate): void
    {
        [$status, $comment] = $this->determineSystemStatus($aggregate);

        // Changes to the draft are recorded in the audit, while the release is not audited
        $aggregate->draft->system_status_id = $status;
        $aggregate->draft->system_status_comment = $comment;
    }

    private function determineSystemStatus(ProductAggregate $aggregate): array
    {
        if ($aggregate->draft->system_status_id === ProductSystemStatus::DELETED) {
            return [$aggregate->draft->system_status_id, 'Товар удален'];
        }

        // Editors need to update the data in any status
        $reviews = $this->editors->flatMap(fn (Editor $editor) => $editor->verify($aggregate));
        $aggregate->release->load(['categories', 'brand']);

        return match (true) {
            $reviews->where('error', true)->isNotEmpty() =>
                [ProductSystemStatus::REJECTED, 'Не заполнен обязательный атрибут'],
            $aggregate->release->categories->filter(fn (Category $c) => $c->is_real_active === false)->count() > 0 =>
                [ProductSystemStatus::CLOSED, 'Категория неактивна'],
            $aggregate->release->brand?->is_active === false =>
                [ProductSystemStatus::CLOSED, 'Бренд неактивен'],
            default => [null, null],
        };
    }
}
