<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;

/**
 * @mixin Model
 *
 * @property-read Collection|Category[] $categories
 * @property-read Brand|null $brand
 * @property-read ProductStatusSetting|null $status
 * @property-read Collection|ProductGroup[] $productGroups
 * @property-read Collection|ProductFlagValue[] $flagValues
 * @property-read Collection|ProductFlagValue[] $flagRequiredValues Values for a flag "Product has not filled required attributes"
 * @property-read Collection|CategoryProductLink[] $categoryProductLinks
 *
 * @method static Builder|self whereBrandId(int $brandId)
 */
trait ExpandsProduct
{
    public function status(): BelongsTo
    {
        return $this->belongsTo(ProductStatusSetting::class, 'status_id');
    }

    public function categories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Category::class, 'category_product_links', 'product_id')
            ->using(CategoryProductLink::class)
            ->withTimestamps();
    }

    public function categoryProductLinks(): HasMany
    {
        return $this->hasMany(CategoryProductLink::class, 'product_id');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function productGroups(): HasManyThrough
    {
        return $this->hasManyThrough(
            ProductGroup::class,
            ProductGroupProduct::class,
            'product_id',
            'id',
            'id',
            'product_group_id'
        );
    }

    public function flagValues(): HasMany
    {
        return $this->hasMany(ProductFlagValue::class, 'product_id');
    }

    public function flagRequiredValues(): HasMany
    {
        return $this->flagValues()->requiredAttributes();
    }

    public function scopeHasProductGroups(Builder $query, bool $has): Builder
    {
        return $has ? $query->has('productGroups') : $query->doesntHave('productGroups');
    }

    public function scopeCategoryHasIsGluing(Builder $query, bool $has): Builder
    {
        return $query->whereRelation('category', function (Builder $query) use ($has) {
            /** @phpstan-ignore-next-line  */
            return $query->hasIsGluing($has);
        });
    }

    public function scopeHasNoFilledRequiredAttributes(Builder $query, bool $has): Builder
    {
        return $has ? $query->has('flagRequiredValues') : $query->doesntHave('flagRequiredValues');
    }
}
