<?php

namespace App\Domain\Products\Models;

use App\Domain\Products\Models\Tests\Factories\ProductGroupProductFactory;
use App\Domain\Support\Models\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Привязки товаров к товарным склейкам"
 *
 * @property int $product_group_id Идентификатор товарной склейки
 * @property int $product_id Идентификатор товара
 *
 * @property-read ProductGroup $productGroup Товарная склейка
 * @property-read Product $product Товар
 */
class ProductGroupProduct extends Pivot
{
    protected $fillable = ['product_group_id', 'product_id'];
    protected $table = 'product_group_product';

    protected $casts = [
        'product_group_id' => 'int',
        'product_id' => 'int',
    ];

    public function productGroup(): BelongsTo
    {
        return $this->belongsTo(ProductGroup::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public static function new(?int $productId = null): static
    {
        $instance = new static();

        if ($productId !== null) {
            $instance->product_id = $productId;
        }

        return $instance;
    }

    public static function factory(): ProductGroupProductFactory
    {
        return ProductGroupProductFactory::new();
    }
}
