<?php

use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Events\FileReleased;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Event;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

beforeEach(function () {
    Event::fake(FileReleased::class);
});

test('deleting image with file raises FileReleased', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $image = ProductImage::factory()->createOne();

    $image->delete();

    Event::assertDispatched(FileReleased::class);
})->with(FakerProvider::$optionalDataset);
