<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @method ProductGroupProduct createOne(array $fields = [])
 * @method ProductGroupProduct makeOne(array $fields = [])
 * @method ProductGroupProduct|ProductGroupProduct[]|ProductGroupProduct create(array $fields = [], $parent = null)
 */
class ProductGroupProductFactory extends BaseModelFactory
{
    protected $model = ProductGroupProduct::class;

    public function definition(): array
    {
        return [
            'product_group_id' => ProductGroup::factory(),
            'product_id' => Product::factory(),
        ];
    }
}
