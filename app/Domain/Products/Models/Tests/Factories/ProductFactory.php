<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Classifiers\Models\ProductStatusSetting;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductFlagValue;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductTariffingVolumeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductUomEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method Product createOne(array $fields = [])
 * @method Product makeOne(array $fields = [])
 * @method Product|Product[]|Collection create(array $fields = [], $parent = null)
 *
 * @extends BaseModelFactory<Product>
 */
class ProductFactory extends BaseModelFactory
{
    protected $model = Product::class;

    public function definition(): array
    {
        $type = $this->faker->randomEnum(ProductType::cases());
        $isWeigh = $type === ProductType::WEIGHT;

        return [
            'name' => $this->faker->sentence(),
            'description' => $this->faker->text,
            'type' => $this->faker->randomEnum(ProductType::cases()),
            'status_id' => $this->faker->nullable()->exactly(ProductStatusSetting::factory()),
            'status_comment' => $this->faker->nullable()->sentence(),
            'system_status_id' => $this->faker->nullable()->randomEnum(ProductSystemStatus::notHiddenStatuses()),
            'system_status_comment' => $this->faker->nullable()->sentence(),
            'allow_publish' => true,

            'external_id' => $this->faker->unique()->numerify('######'),
            'code' => $this->faker->unique()->slug,
            'barcode' => $this->faker->unique()->ean13(),
            'vendor_code' => $this->faker->unique()->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,

            'uom' => $this->faker->nullable($isWeigh)->randomEnum(ProductUomEnum::cases()),
            'tariffing_volume' => $this->faker->nullable($isWeigh)->randomEnum(ProductTariffingVolumeEnum::cases()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'picking_weight_deviation' => $this->faker->nullable($isWeigh)->randomFloat(2, 0, 100),

            'brand_id' => Brand::factory(),
        ];
    }

    public function inCategories(array $categories): self
    {
        return $this->afterCreating(function (Product $product) use ($categories) {
            foreach ($categories as $category) {
                CategoryProductLink::factory()->createFast($category, $product);
            }
        });
    }

    public function inStatus(ProductStatusSetting $status): static
    {
        return $this->state(['status_id' => $status->id]);
    }

    public function withFlags(ProductFlag ...$flags): self
    {
        return $this->afterCreating(function (Product $product) use ($flags) {
            foreach ($flags as $flag) {
                ProductFlagValue::create(['value' => $flag->value, 'product_id' => $product->id]);
            }
        });
    }

    public function deleted(): self
    {
        return $this->state(['system_status_id' => ProductSystemStatus::DELETED]);
    }
}
