<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductEvent;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class ProductEventFactory extends BaseModelFactory
{
    protected $model = ProductEvent::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'event_id' => $this->faker->randomEnum(ProductEventEnum::cases()),
            'product_id' => Product::factory(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function inProduct(Product $product): static
    {
        return $this->state(['product_id' => $product->id]);
    }

    public function event(ProductEventEnum $event): static
    {
        return $this->state(['event_id' => $event]);
    }
}
