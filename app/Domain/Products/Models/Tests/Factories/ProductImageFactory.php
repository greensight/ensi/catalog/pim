<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method ProductImage createOne(array $fields = [])
 * @method ProductImage makeOne(array $fields = [])
 * @method ProductImage|ProductImage[]|Collection create(array $fields = [], $parent = null)
 *
 * @extends BaseModelFactory<ProductImage>
 */
class ProductImageFactory extends BaseModelFactory
{
    protected $model = ProductImage::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        $fileId = $this->faker->unique()->numberBetween(100, 999);

        return [
            'file' => "path/to/image_{$fileId}.png",
            'product_id' => Product::factory(),
            'sort' => $this->faker->numberBetween(1, 500),
            'name' => $this->faker->nullable()->sentence(3),
        ];
    }

    public function owned(Product $product): self
    {
        return $this->state(function () use ($product) {
            return [
                'product_id' => $product->getKey(),
            ];
        });
    }

    public function published(): self
    {
        return $this->afterCreating(fn (ProductImage $value) => $value->publish());
    }
}
