<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Models\Tests\Factories\BasePropertyValueFactory;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use Illuminate\Support\Collection;

/**
 * @method ProductPropertyValue createOne(array $fields = [])
 * @method ProductPropertyValue makeOne(array $fields = [])
 * @method ProductPropertyValue|ProductPropertyValue[]|Collection create(array $fields = [], $parent = null)
 * @method ProductPropertyValue|ProductPropertyValue[]|Collection make(array $fields = [], $parent = null)
 *
 * @extends BasePropertyValueFactory<ProductPropertyValue>
 */
class ProductPropertyValueFactory extends BasePropertyValueFactory
{
    protected $model = ProductPropertyValue::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return array_merge(parent::definition(), [
            'product_id' => Product::factory(),
        ]);
    }

    public function directory(?PropertyDirectoryValue $reference = null): self
    {
        return $this->state(function () use ($reference) {
            $reference ??= PropertyDirectoryValue::factory()->createOne();
            $value = $reference->getValue();

            return [
                'property_id' => $reference->property_id,
                'value' => (string)$value,
                'type' => $value->type()->value,
                'name' => $reference->name,
                'directory_value_id' => $reference->id,
            ];
        });
    }

    public function forProduct(Product $product): self
    {
        return $this->state(['product_id' => $product->id]);
    }

    public function actual(?bool $isRequired = null): self
    {
        return $this->afterMaking(function (ProductPropertyValue $value) use ($isRequired) {
            foreach ($value->product->categories as $category) {
                ActualCategoryProperty::factory()->createFast($category, $value->property, $isRequired);
            }
        });
    }

    public function published(): self
    {
        return $this->afterCreating(fn (ProductPropertyValue $value) => $value->publish());
    }
}
