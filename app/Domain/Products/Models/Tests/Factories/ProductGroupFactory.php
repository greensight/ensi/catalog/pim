<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Collection;

/**
 * @method ProductGroup createOne(array $fields = [])
 * @method ProductGroup makeOne(array $fields = [])
 * @method ProductGroup|ProductGroup[]|Collection create(array $fields = [], $parent = null)
 *
 * @extends BaseModelFactory<ProductGroup>
 */
class ProductGroupFactory extends BaseModelFactory
{
    protected $model = ProductGroup::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'category_id' => Category::factory(),
            'is_active' => $this->faker->boolean,
        ];
    }

    public function withCategoryId(int $categoryId): self
    {
        return $this->state(fn () => ['category_id' => $categoryId]);
    }

    public function withMainProductId(int $productId): self
    {
        return $this->state(fn () => ['main_product_id' => $productId]);
    }

    public function createWithProducts(?int $count = 3): self
    {
        return $this->afterCreating(function (ProductGroup $productGroup) use ($count) {
            $products = Product::factory()->count($count)->create();
            $productGroup->products()->attach($products->pluck('id'));
        });
    }
}
