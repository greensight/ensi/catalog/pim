<?php

namespace App\Domain\Products\Models;

use App\Domain\Products\Models\Tests\Factories\ProductEventFactory;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property ProductEventEnum $event_id Event ID from ProductEventEnum
 * @property int $product_id Product ID
 *
 * @property-read Product $product Product
 *
 * @property-read CarbonInterface|null $created_at
 * @property-read CarbonInterface|null $updated_at
 */
class ProductEvent extends Model
{
    protected $table = 'product_events';

    protected $casts = [
        'event_id' => ProductEventEnum::class,
        'product_id' => 'int',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public static function factory(): ProductEventFactory
    {
        return ProductEventFactory::new();
    }
}
