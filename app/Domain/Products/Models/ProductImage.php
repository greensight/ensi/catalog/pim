<?php

namespace App\Domain\Products\Models;

use App\Domain\Products\Models\Tests\Factories\ProductImageFactory;
use App\Domain\Support\Concerns\SupportsPublishing;
use App\Domain\Support\Models\Model;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * ProductImage картинка товара.
 *
 * @property CarbonInterface|null $deleted_at Время пометки на удаление
 *
 * @property PublishedImage|null $release Опубликованная картинка
 * @property Product $product Товар, которому принадлежит изображение
 *
 * @method static Builder|static whereProductId(int $id)
 */
class ProductImage extends BaseImage
{
    use SupportsPublishing;

    protected $fillable = ['sort', 'name'];

    protected $attributes = [
        'sort' => 100,
    ];

    public function isValid(): bool
    {
        return $this->exists && !$this->trashed();
    }

    protected function newRelease(): Model
    {
        $release = new PublishedImage();

        $release->product_id = $this->product_id;
        $release->file = $this->file;

        return $release;
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function release(): BelongsTo
    {
        return $this->belongsTo(PublishedImage::class, 'id', 'id');
    }

    public static function factory(array $attributes = []): ProductImageFactory
    {
        return ProductImageFactory::new($attributes);
    }
}
