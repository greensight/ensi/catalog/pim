<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Data\PropertyValueHolder;

/**
 * Опубликованное значение атрибута товара.
 */
class PublishedPropertyValue extends BasePropertyValue implements PropertyValueHolder
{
    protected $table = 'published_property_values';

    protected $attributes = [
        'deleted_at' => null,
        'directory_value_id' => null,
    ];

    public function suspend(): void
    {
        if (!$this->trashed()) {
            $this->delete();
        }
    }

    public function resume(): void
    {
        if ($this->trashed()) {
            $this->restore();
        }
    }
}
