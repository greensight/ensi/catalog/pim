<?php

namespace App\Domain\Products\Models;

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Products\Models\Tests\Factories\ProductMetricsFactory;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $product_id
 * @property MetricsCategory $category
 * @property int $total Общее количество атрибутов в категории
 * @property int $filled Количество заполненных атрибутов
 * @property int $errors Количество ошибок (незаполненных обязательных атрибутов)
 * @property float $ratio Соотношение заполненных атрибутов к общему количеству
 *
 * @method static Builder|static whereProductId(int $id)
 */
class ProductMetrics extends Model
{
    protected $table = 'product_metrics';
    protected $fillable = ['total', 'filled', 'errors', 'ratio'];
    public $timestamps = false;

    protected $casts = [
        'product_id' => 'int',
        'category' => MetricsCategory::class,
        'total' => 'int',
        'filled' => 'int',
        'errors' => 'int',
        'ratio' => 'float',
    ];

    protected static function boot(): void
    {
        parent::boot();

        self::saving(function (self $model) {
            $model->ratio = $model->total > 0
                ? round($model->filled * 100 / $model->total, 2)
                : 0;
        });
    }

    public static function factory(): ProductMetricsFactory
    {
        return ProductMetricsFactory::new();
    }
}
