<?php

namespace App\Domain\Products\Models;

/**
 * Опубликованная картинка товара.
 */
class PublishedImage extends BaseImage
{
    // Идентификатор такой же как у картинки источника
    public $incrementing = false;
}
