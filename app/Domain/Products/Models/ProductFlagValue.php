<?php

namespace App\Domain\Products\Models;

use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Models\ProductFlagSettings;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Признак товара.
 *
 * @property int $product_id Идентификатор товара
 * @property ProductFlag $value Значение флага из перечисления ProductFlagsEnum
 *
 * @property-read ProductFlagSettings $flag
 *
 * @method static static create(array $fields)
 * @method static Builder|static whereProductId(int $value)
 */
class ProductFlagValue extends Model
{
    protected $table = 'product_flag_values';
    protected $fillable = ['value', 'product_id'];
    public $timestamps = false;

    protected $casts = [
        'product_id' => 'int',
        'value' => ProductFlag::class,
    ];

    public function flag(): BelongsTo
    {
        return $this->belongsTo(ProductFlagSettings::class, 'value');
    }

    public function scopeRequiredAttributes(Builder $query): Builder
    {
        return $query->where('value', ProductFlag::REQUIRED_ATTRIBUTES);
    }
}
