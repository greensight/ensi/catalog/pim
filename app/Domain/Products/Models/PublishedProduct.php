<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductTariffingVolumeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductUomEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property bool $allow_publish
 * @property string|null $allow_publish_comment
 * @property string|null $external_id
 * @property ProductType $type Product type from ProductTypeEnum
 * @property int|null $status_id
 * @property string|null $status_comment
 * @property string|null $barcode EAN
 * @property string $vendor_code Article
 * @property string $name
 * @property string $code Code to use in URL
 * @property string|null $description
 * @property float|null $weight Net weight in kg
 * @property float|null $weight_gross Gross weight in kg
 * @property float|null $width Width in mm
 * @property float|null $height Height in mm
 * @property float|null $length Length in mm
 * @property int|null $brand_id
 * @property bool $is_adult Is product 18+
 * @property string|null $main_image_file
 *
 * @property ProductUomEnum|null $uom Unit of measurement
 * @property ProductTariffingVolumeEnum|null $tariffing_volume Unit of tariffication
 * @property float|null $order_step
 * @property float|null $order_minvol Minimum quantity for order
 * @property float|null $picking_weight_deviation Weight deviation limit in percent
 *
 * @property-read Collection|PublishedPropertyValue[] $properties Published property values
 * @property-read Collection<PublishedImage> $images Published images
 * @property-read Collection|CategoryProductLink[] $categoryProductLinks
 */
class PublishedProduct extends Model
{
    use ExpandsProduct;

    protected $table = 'published_products';

    // id must be the same as in Product
    public $incrementing = false;

    protected $casts = [
        'weight' => 'float',
        'weight_gross' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'brand_id' => 'int',
        'status_id' => 'int',
        'type' => ProductType::class,
        'is_adult' => 'bool',
        'allow_publish' => 'bool',
        'uom' => ProductUomEnum::class,
        'tariffing_volume' => ProductTariffingVolumeEnum::class,
    ];

    public function properties(): HasMany
    {
        return $this->hasMany(PublishedPropertyValue::class, 'product_id');
    }

    public function images(): HasMany
    {
        return $this->hasMany(PublishedImage::class, 'product_id')
            ->orderBy('product_id')
            ->orderBy('sort');
    }

    public function categoryProductLinks(): HasMany
    {
        return $this->hasMany(CategoryProductLink::class, 'product_id', 'id');
    }

    public static function whereCategoryId(int $categoryId): Collection
    {
        return self::query()
            ->whereHas('categoryProductLinks', function (Builder $query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            })
            ->get();
    }

    public function getCategoryIds(): Collection
    {
        return $this
            ->loadMissing('categoryProductLinks')
            ->categoryProductLinks
            ->pluck('category_id');
    }
}
