<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\CategoryPropertyView;
use App\Domain\Classifiers\Enums\ProductSystemStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\ProductFlagSettings;
use App\Domain\Products\Models\Tests\Factories\ProductFactory;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductTariffingVolumeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductUomEnum;
use Cviebrock\EloquentSluggable\Sluggable;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;

/**
 * @property bool $allow_publish
 * @property string|null $external_id
 * @property ProductType $type Product type from ProductTypeEnum
 * @property int|null $status_id
 * @property string|null $status_comment
 * @property string|null $barcode EAN
 * @property string $vendor_code Article
 * @property string $name
 * @property string $code Code to use in URL
 * @property string|null $description
 * @property float|null $weight Net weight in kg
 * @property float|null $weight_gross Gross weight in kg
 * @property float|null $width Width in mm
 * @property float|null $height Height in mm
 * @property float|null $length Length in mm
 * @property int|null $brand_id
 * @property bool $is_adult Is product 18+
 *
 * @property ProductUomEnum|null $uom Unit of measurement
 * @property ProductTariffingVolumeEnum|null $tariffing_volume Unit of tariffication
 * @property float|null $order_step
 * @property float|null $order_minvol Minimum quantity for order
 * @property float|null $picking_weight_deviation Weight deviation limit in percent
 *
 * @property ProductSystemStatus|null $system_status_id Product status determined by the system
 * @property string|null $system_status_comment
 *
 * @property-read Collection|ProductEvent[] $events Product events history
 * @property-read Collection|ProductPropertyValue[] $properties Product property values
 * @property-read Collection|ProductImage[] $images Product images
 * @property-read Collection|ProductFlagSettings[] $flags Established characteristics of the product
 * @property-read Collection|ProductMetrics[] $metrics Validation metrics
 * @property-read Collection|CategoryPropertyView[] $availableProperties Available product properties
 * @property-read Collection|ProductGroup[] $productGroups Product groups that include the product
 */
class Product extends Model implements Auditable
{
    use Sluggable;
    use SupportsAudit;
    use ExpandsProduct;

    protected $fillable = [
        'barcode', 'vendor_code', 'external_id',
        'name', 'code', 'description', 'type', 'status_id', 'status_comment',
        'weight', 'width', 'height', 'length', 'weight_gross',
        'brand_id', 'is_adult', 'allow_publish',
        'uom', 'order_step', 'order_minvol', 'picking_weight_deviation',
    ];

    protected $casts = [
        'weight' => 'float',
        'weight_gross' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'brand_id' => 'int',
        'status_id' => 'int',
        'type' => ProductType::class,
        'is_adult' => 'bool',
        'allow_publish' => 'bool',
        'system_status_id' => ProductSystemStatus::class,
        'uom' => ProductUomEnum::class,
        'tariffing_volume' => ProductTariffingVolumeEnum::class,
    ];

    protected $attributes = [
        'is_adult' => false,
        'allow_publish' => false,
    ];

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'name',
                'unique' => true,
            ],
        ];
    }

    public function setTariffingVolume(): void
    {
        $this->tariffing_volume = match ($this->uom) {
            ProductUomEnum::GRAM => ProductTariffingVolumeEnum::PRICE_PER_ONE_HUNDRED_GRAMS,
            ProductUomEnum::KILOGRAM => ProductTariffingVolumeEnum::PRICE_PER_KILOGRAM,
            null => null,
        };
    }

    public function properties(): HasMany
    {
        return $this->hasMany(ProductPropertyValue::class, 'product_id');
    }

    public function images(): HasMany
    {
        return $this->hasMany(ProductImage::class)
            ->orderBy('product_id')
            ->orderBy('sort');
    }

    public function flags(): HasManyThrough
    {
        return $this->hasManyThrough(
            ProductFlagSettings::class,
            ProductFlagValue::class,
            'product_id',
            'id',
            'id',
            'value'
        );
    }

    public function metrics(): HasMany
    {
        return $this->hasMany(ProductMetrics::class);
    }

    public function getAvailableProperties(): Collection
    {
        $this->loadMissing('categories.properties');

        return $this->categories->pluck('properties')->flatten();
    }

    public function getCategoryIds(): Collection
    {
        return $this
            ->fresh()
            ->loadMissing('categoryProductLinks')
            ->categoryProductLinks
            ->pluck('category_id');
    }

    public function productGroups(): BelongsToMany
    {
        return $this->belongsToMany(ProductGroup::class, 'product_group_product');
    }

    public function events(): HasMany
    {
        return $this->hasMany(ProductEvent::class, 'product_id', 'id');
    }

    public static function factory(): ProductFactory
    {
        return ProductFactory::new();
    }
}
