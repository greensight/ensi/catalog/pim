<?php

namespace App\Domain\Products\Models;

use App\Domain\Support\Models\Model;

/**
 * @property int $id Идентификатор картинки
 * @property int $product_id Идентификатор товара
 * @property int $sort Порядок сортировки
 * @property string|null $file Файл картинки
 * @property string|null $name Описание картинки
 */
abstract class BaseImage extends Model
{
    protected $casts = [
        'product_id' => 'int',
        'sort' => 'int',
    ];

    public function isInternal(): bool
    {
        return filled($this->file);
    }
}
