<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Tests\Factories\ProductGroupFactory;
use App\Domain\Support\Models\Model;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Товарная склейка (группа)
 * @property int $category_id Id категории
 * @property string $name Название
 * @property int|null $main_product_id Id главного товара
 * @property bool $is_active Активность
 * @property-read int $products_count Кол-во товаров в склейке
 *
 * @property-read Category $category Категория
 * @property-read Product $mainProduct Главный товар
 * @property-read Collection|Product[] $products Товары склейки
 * @property-read Collection|ProductGroupProduct[] $productLinks Привязки товаров к склейке
 */
class ProductGroup extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = ['category_id', 'name', 'main_product_id', 'is_active'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function mainProduct(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_group_product');
    }

    public function productLinks(): HasMany
    {
        return $this->hasMany(ProductGroupProduct::class);
    }

    public function scopeProductNameLike(Builder $query, $productName): Builder
    {
        return $query->whereHas('products', function (Builder $query) use ($productName) {
            $query->where('name', 'ilike', "%{$productName}%");
        });
    }

    public function scopeProductBarcodeLike(Builder $query, $productBarcode): Builder
    {
        return $query->whereHas('products', function (Builder $query) use ($productBarcode) {
            $query->where('barcode', 'ilike', "%{$productBarcode}%");
        });
    }

    public function scopeProductVendorCodeLike(Builder $query, $productVendorCode): Builder
    {
        return $query->whereHas('products', function (Builder $query) use ($productVendorCode) {
            $query->where('vendor_code', 'ilike', "%{$productVendorCode}%");
        });
    }

    public static function factory(): ProductGroupFactory
    {
        return ProductGroupFactory::new();
    }
}
