<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Classifiers\Enums\PropertyType;
use App\Domain\Support\Models\Model;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use LogicException;

/**
 * @property int $property_id ID свойства
 * @property int $product_id ID товара
 * @property PropertyType $type Тип значения
 * @property string $value Значение
 * @property string|null $name Название значения
 * @property int|null $directory_value_id ID значения справочника
 * @property CarbonInterface|null $deleted_at Время пометки на удаление
 *
 * @method static Builder|static whereProductId(int $id)
 * @method static Builder|static whereDirectoryValueId(int $value)
 */
abstract class BasePropertyValue extends Model
{
    use SoftDeletes;

    protected $casts = [
        'product_id' => 'int',
        'property_id' => 'int',
        'directory_value_id' => 'int',
        'type' => PropertyType::class,
    ];

    public function getValue(): PropertyValue
    {
        $result = PropertyValue::make($this->type ?? PropertyType::STRING->value, $this->value);

        if ($result->isNull()) {
            throw new LogicException('Значение не задано');
        }

        return $result;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function isValid(): bool
    {
        return $this->exists && !$this->trashed();
    }

    public function isDirectory(): bool
    {
        return $this->directory_value_id !== null;
    }

    public function directoryValue(): BelongsTo
    {
        return $this->belongsTo(PropertyDirectoryValue::class, 'directory_value_id');
    }
}
