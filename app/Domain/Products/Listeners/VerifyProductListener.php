<?php

namespace App\Domain\Products\Listeners;

use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Products\Actions\Products\VerifyProductPublicationAction;
use App\Domain\Products\Models\PublishedProduct;
use App\Support\Log\DefaultLoggerAwareInterface;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Psr\Log\LoggerAwareTrait;

class VerifyProductListener implements ShouldQueue, DefaultLoggerAwareInterface
{
    use LoggerAwareTrait;

    public bool $afterCommit = true;

    public function __construct(private readonly VerifyProductPublicationAction $action)
    {
    }

    public function handle(CategoryActualized $event): void
    {
        PublishedProduct::whereCategoryId($event->categoryId)
            ->pluck('id')
            ->each($this->verify(...));
    }

    private function verify(int $productId): void
    {
        try {
            $this->action->execute($productId);
        } catch (Exception $e) {
            $this->logFaultyProduct($productId, $e->getMessage());
        }
    }

    private function logFaultyProduct(int $id, string $reason): void
    {
        $this->logger?->error(
            "Не удалось проверить публикацию товара: \"{$reason}\"",
            ['id' => $id]
        );
    }
}
