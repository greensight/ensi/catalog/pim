<?php

namespace App\Domain\Products\Listeners;

use App\Domain\Products\Actions\Products\ActualizeProductAction;
use App\Domain\Products\Events\ProductInvalidated;

class ActualizeProductListener
{
    public function __construct(private readonly ActualizeProductAction $action)
    {
    }

    public function handle(ProductInvalidated $event): void
    {
        $this->action->execute($event->productId);
    }
}
