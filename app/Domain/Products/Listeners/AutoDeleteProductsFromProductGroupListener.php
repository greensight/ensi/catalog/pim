<?php

namespace App\Domain\Products\Listeners;

use App\Domain\Products\Actions\ProductGroups\AutoDeleteProductsFromProductGroupAction;
use App\Domain\Products\Events\ProductPropertyValueDeleted;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutoDeleteProductsFromProductGroupListener implements ShouldQueue
{
    public function __construct(private readonly AutoDeleteProductsFromProductGroupAction $action)
    {
    }

    public function handle(ProductPropertyValueDeleted $event): void
    {
        $this->action->execute($event->productPropertyValue);
    }
}
