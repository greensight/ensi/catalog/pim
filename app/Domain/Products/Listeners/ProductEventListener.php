<?php

namespace App\Domain\Products\Listeners;

use App\Domain\Products\Actions\ProductEvents\CreateProductEventAction;
use App\Domain\Products\Events\ProductEvent;

class ProductEventListener
{
    public function __construct(protected readonly CreateProductEventAction $createProductEventAction)
    {
    }

    public function handle(ProductEvent $event): void
    {
        $this->createProductEventAction->execute($event->productId, $event->event);
    }
}
