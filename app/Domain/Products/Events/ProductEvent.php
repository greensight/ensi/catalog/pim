<?php

namespace App\Domain\Products\Events;

use App\Http\ApiV1\OpenApiGenerated\Enums\ProductEventEnum;
use Illuminate\Foundation\Events\Dispatchable;

class ProductEvent
{
    use Dispatchable;

    public function __construct(public int $productId, public ProductEventEnum $event)
    {
    }
}
