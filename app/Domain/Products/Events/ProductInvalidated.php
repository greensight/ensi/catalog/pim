<?php

namespace App\Domain\Products\Events;

use Illuminate\Foundation\Events\Dispatchable;

class ProductInvalidated
{
    use Dispatchable;

    public function __construct(public int $productId)
    {
    }
}
