<?php

namespace App\Domain\Products\Events;

use App\Domain\Products\Models\ProductPropertyValue;
use Illuminate\Foundation\Events\Dispatchable;

class ProductPropertyValueDeleted
{
    use Dispatchable;

    public function __construct(public ProductPropertyValue $productPropertyValue)
    {
    }
}
