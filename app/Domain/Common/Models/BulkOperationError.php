<?php

namespace App\Domain\Common\Models;

use App\Domain\Common\Models\Tests\Factories\BulkOperationErrorFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $operation_id Id массовой операции
 * @property int $entity_id Id сущности
 * @property string $message Сообщение об ошибке
 */
class BulkOperationError extends Model
{
    protected $table = 'bulk_operation_errors';

    protected $fillable = ['operation_id', 'entity_id', 'message'];

    public $timestamps = false;

    public static function factory(): BulkOperationErrorFactory
    {
        return BulkOperationErrorFactory::new();
    }
}
