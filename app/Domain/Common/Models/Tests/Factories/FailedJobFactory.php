<?php

namespace App\Domain\Common\Models\Tests\Factories;

use App\Domain\Common\Models\FailedJob;
use Ensi\LaravelTestFactories\BaseModelFactory;

class FailedJobFactory extends BaseModelFactory
{
    protected $model = FailedJob::class;

    public function definition(): array
    {
        return [
            'uuid' => $this->faker->unique()->uuid(),
            'connection' => $this->faker->word(),
            'queue' => $this->faker->word(),
            'payload' => $this->faker->text(),
            'exception' => $this->faker->text(),
            'failed_at' => $this->faker->dateTime(),
        ];
    }
}
