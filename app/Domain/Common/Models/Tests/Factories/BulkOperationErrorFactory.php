<?php

namespace App\Domain\Common\Models\Tests\Factories;

use App\Domain\Common\Models\BulkOperation;
use App\Domain\Common\Models\BulkOperationError;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @template-extends BaseModelFactory<BulkOperationError>
 */
class BulkOperationErrorFactory extends BaseModelFactory
{
    protected $model = BulkOperationError::class;

    public function definition(): array
    {
        return [
            'operation_id' => BulkOperation::factory(),
            'entity_id' => $this->faker->modelId(),
            'message' => $this->faker->text(100),
        ];
    }
}
