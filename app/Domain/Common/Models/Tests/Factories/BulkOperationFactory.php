<?php

namespace App\Domain\Common\Models\Tests\Factories;

use App\Domain\Common\Models\BulkOperation;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationActionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationEntityEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @template-extends BaseModelFactory<BulkOperation>
 */
class BulkOperationFactory extends BaseModelFactory
{
    protected $model = BulkOperation::class;

    public function definition(): array
    {
        return [
            'action' => $this->faker->randomEnum(BulkOperationActionTypeEnum::cases()),
            'entity' => $this->faker->randomEnum(BulkOperationEntityEnum::cases()),
            'status' => $this->faker->randomEnum(BulkOperationStatusEnum::cases()),
            'ids' => range($this->faker->randomDigit(), $this->faker->numberBetween(10, 20)),
            'error_message' => $this->faker->nullable()->text(),
            'created_by' => $this->faker->nullable()->uuid,
        ];
    }
}
