<?php

namespace App\Domain\Common\Models;

use App\Domain\Common\Models\Tests\Factories\BulkOperationFactory;
use App\Domain\Support\Concerns\SetsUserAttributesInterface;
use App\Domain\Support\MassOperationResult;
use App\Domain\Support\Models\Model;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationActionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationEntityEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BulkOperationStatusEnum;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string|null $created_by Пользователь, инициировавший операцию
 * @property BulkOperationActionTypeEnum $action Действие
 * @property BulkOperationEntityEnum $entity Сущность
 * @property array $ids Идентификаторы для обработки
 * @property BulkOperationStatusEnum $status Статус обработки
 * @property string|null $error_message Сообщение об ошибке
 *
 * @property-read Collection<BulkOperationError> $errors Ошибки при выполнении
 */
class BulkOperation extends Model implements SetsUserAttributesInterface
{
    protected $table = 'bulk_operations';

    protected $casts = [
        'action' => BulkOperationActionTypeEnum::class,
        'entity' => BulkOperationEntityEnum::class,
        'status' => BulkOperationStatusEnum::class,
        'ids' => 'array',
    ];

    public static function register(BulkOperationActionTypeEnum $action, BulkOperationEntityEnum $entity, array $ids): static
    {
        $model = new static();
        $model->status = BulkOperationStatusEnum::NEW;
        $model->action = $action;
        $model->entity = $entity;
        $model->ids = $ids;
        $model->save();

        return $model;
    }

    public static function registerPatchProducts(array $ids): static
    {
        return self::register(
            action: BulkOperationActionTypeEnum::PATCH,
            entity: BulkOperationEntityEnum::PRODUCT,
            ids: $ids,
        );
    }

    public function setInProgress(): self
    {
        $this->status = BulkOperationStatusEnum::IN_PROGRESS;
        $this->save();

        return $this;
    }

    public function setCompleted(?MassOperationResult $result): void
    {
        $this->status = BulkOperationStatusEnum::COMPLETED;

        foreach ($result?->getErrors() ?? [] as $id => $message) {
            $bulkOperationError = new BulkOperationError();
            $bulkOperationError->entity_id = $id;
            $bulkOperationError->message = $message;

            $this->errors()->save($bulkOperationError);
        }

        $this->save();
    }

    public function setError(string $message = 'Неизвестная ошибка'): void
    {
        $this->status = BulkOperationStatusEnum::ERROR;
        $this->error_message = $message;
        $this->save();
    }

    public function errors(): HasMany
    {
        return $this->hasMany(BulkOperationError::class, 'operation_id');
    }

    public function setCreatedBy(?string $value): void
    {
        $this->attributes['created_by'] = $value;
    }

    public static function factory(): BulkOperationFactory
    {
        return BulkOperationFactory::new();
    }
}
