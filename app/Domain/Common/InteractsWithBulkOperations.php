<?php

namespace App\Domain\Common;

use App\Domain\Common\Models\BulkOperation;
use App\Domain\Support\MassOperationResult;
use Closure;

trait InteractsWithBulkOperations
{
    private function processBulkOperation(BulkOperation $operation, Closure $action): mixed
    {
        $operation->setInProgress();

        try {
            $result = $action();
        } catch (\Exception $e) {
            $operation->setError($e->getMessage());

            throw $e;
        }

        $operation->setCompleted($result instanceof MassOperationResult ? $result : null);

        return $result;
    }
}
