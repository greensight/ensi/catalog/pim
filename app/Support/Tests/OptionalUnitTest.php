<?php

use App\Support\Optional;
use App\Support\Tests\OptionalTestMapStub;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

dataset('factories', function () {
    $factories = [
        'static create' => fn ($value = null) => Optional::create($value),
        '"optional" extension' => fn ($value = null) => optional($value),
    ];

    foreach ($factories as $key => $factory) {
        yield $key => $factory;
    }
});

test('value', function (Closure $factory) {
    expect($factory('foo')->value())->toBe('foo');
})->with('factories');

test('get not empty', function (Closure $factory) {
    expect($factory('foo')->get())->toBe('foo');
})->with('factories');

test('get empty throws exception', function (Closure $factory) {
    $factory()->get();
})
    ->with('factories')
    ->throws(RuntimeException::class, Optional::EXCEPTION_MESSAGE);

test('isEmpty', function (Closure $factory) {
    expect($factory()->isEmpty())->toBeTrue();
    expect($factory(1)->isEmpty())->toBeFalse();
})->with('factories');

test('orElse', function (Closure $factory) {
    expect($factory()->orElse(100)->value())->toBe(100);
})->with('factories');

test('orElse on not null', function (Closure $factory) {
    expect($factory('foo')->orElse('bar')->value())->toBe('foo');
})->with('factories');

test('orElse with callable', function (Closure $factory) {
    expect($factory()->orElse(fn () => 'foo')->value())->toBe('foo');
})->with('factories');

test('orElseThrow', function (Closure $factory) {
    $factory()->orElseThrow();
})
    ->with('factories')
    ->throws(RuntimeException::class);

test('set default exception class', function (Closure $factory) {
    $factory()
        ->withDefaultException(LogicException::class)
        ->getOrThrow();
})
    ->with('factories')
    ->throws(LogicException::class);

test('set default exception class with args', function (Closure $factory) {
    $factory()
        ->withDefaultException(LogicException::class, 'Error message', 500)
        ->getOrThrow();
})
    ->with('factories')
    ->throws(LogicException::class, 'Error message', 500);

test('set default exception instance', function (Closure $factory) {
    $factory()
        ->withDefaultException(new LogicException('Error message', 500))
        ->getOrThrow();
})
    ->with('factories')
    ->throws(LogicException::class, 'Error message', 500);

test('map to class', function (Closure $factory) {
    $result = $factory('foo')
        ->map(OptionalTestMapStub::class)
        ->get();

    expect($result)->toEqual(new OptionalTestMapStub('foo'));
})->with('factories');

test('map to class with additional args', function (Closure $factory) {
    $result = $factory('foo')
        ->map(OptionalTestMapStub::class, 45)
        ->get();

    expect($result)->toEqual(new OptionalTestMapStub('foo', 45));
})->with('factories');

test('map with callable', function (Closure $factory) {
    $result = $factory('foo')
        ->map(fn (string $source) => "bar {$source}")
        ->get();

    expect($result)->toBe('bar foo');
})->with('factories');

test('map empty does nothing', function (Closure $factory) {
    $result = $factory()->map(OptionalTestMapStub::class);

    expect($result->value())->toBeNull();
})->with('factories');
