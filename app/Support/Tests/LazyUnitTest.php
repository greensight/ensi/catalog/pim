<?php

use App\Support\Lazy;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

beforeEach(fn () => LazyUnitTestTarget::$creatingCount = 0);

test('construct not creating target', function () {
    new Lazy([LazyUnitTestTarget::class, 'new']);

    expect(LazyUnitTestTarget::$creatingCount)->toBe(0);
});

test('method call results in the creation of target', function () {
    $lazy = LazyUnitTestTarget::lazy([LazyUnitTestTarget::class, 'new']);

    $lazy->name();

    expect(LazyUnitTestTarget::$creatingCount)->toBe(1);
});

test('target created only once', function () {
    $lazy = LazyUnitTestTarget::lazy();

    $lazy->setName('bar')->name();

    expect(LazyUnitTestTarget::$creatingCount)->toBe(1);
});

test('method call forwards to target', function () {
    $lazy = LazyUnitTestTarget::lazy(fn () => LazyUnitTestTarget::new());

    expect($lazy->name())->toBe('foo');
});

test('set target field', function () {
    $lazy = LazyUnitTestTarget::lazy();

    /** @phpstan-ignore-next-line  */
    $lazy->value = 'bar';

    /** @phpstan-ignore-next-line  */
    expect($lazy->value)->toBe('bar');
});

test('get target field', function () {
    /** @phpstan-ignore-next-line  */
    expect(LazyUnitTestTarget::lazy()->value)->toBe('foo');
});

test('isset target field', function () {
    expect(isset(LazyUnitTestTarget::lazy()->value))->toBeTrue();
});

test('unset target field', function () {
    $lazy = LazyUnitTestTarget::lazy(fn () => (object)['value' => 'foo']);

    unset($lazy->value);

    expect(isset($lazy->value))->toBeFalse();
});

class LazyUnitTestTarget
{
    public static int $creatingCount = 0;

    public string $value = 'foo';

    public static function lazy(?callable $factory = null): self|Lazy
    {
        return new Lazy($factory ?? fn () => self::new());
    }

    public static function new(): self
    {
        self::$creatingCount++;

        return new self();
    }

    public function name(): string
    {
        return $this->value;
    }

    public function setName(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
