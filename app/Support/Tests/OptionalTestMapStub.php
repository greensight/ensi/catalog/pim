<?php

namespace App\Support\Tests;

class OptionalTestMapStub
{
    public function __construct(public string $name, public int $age = 30)
    {
        $this->name = $name;
        $this->age = $age;
    }
}
