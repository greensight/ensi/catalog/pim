<?php

namespace App\Support\Extensions;

use App\Support\Optional;
use Closure;

/**
 * Определяет расширения для {@see \Illuminate\Support\Optional}
 *
 * @property mixed $value
 */
class OptionalMacros
{
    protected mixed $value;

    /**
     * Возвращает значение, или выбрасывает исключение, если значение не задано.
     * @return Closure
     */
    public function get(): Closure
    {
        return $this->createClosure('get');
    }

    /**
     * Возвращает истину, если значение не задано.
     * @return Closure
     */
    public function isEmpty(): Closure
    {
        return function () {
            return $this->value === null;
        };
    }

    /**
     * Возвращает текущее значение или null.
     * @return Closure
     */
    public function value(): Closure
    {
        return function () {
            return $this->value;
        };
    }

    /**
     * Преобразует значение в заданный класс или с помощью заданной функции.
     *
     * @return Closure
     */
    public function map(): Closure
    {
        return $this->createClosure('map');
    }

    /**
     * Реализует логику альтернативного получения значения.
     *
     * @return Closure
     */
    public function orElse(): Closure
    {
        return $this->createClosure('orElse');
    }

    /**
     * Выполняет заданный $action, если значение присутствует.
     *
     * @return Closure
     */
    public function ifPresent(): Closure
    {
        return $this->createClosure('ifPresent');
    }

    /**
     * Выбрасывает исключение, если значение не было установлено.
     * @return Closure
     */
    public function orElseThrow(): Closure
    {
        return $this->createClosure('orElseThrow');
    }

    /**
     * Выбрасывает HttpException, если значение не заполнено.
     * @return Closure
     */
    public function orElseAbort(): Closure
    {
        return $this->createClosure('orElseAbort');
    }

    /**
     * Возвращает значение, если оно задано, иначе выбрасывает исключение.
     * @return Closure
     */
    public function getOrThrow(): Closure
    {
        return $this->createClosure('getOrThrow');
    }

    /**
     * Устанавливает имя класса или объект исключения по умолчанию.
     * @return Closure
     */
    public function withDefaultException(): Closure
    {
        return $this->createClosure('withDefaultException');
    }

    /**
     * Возвращает замыкание для макроса, соответствующего заданному имени.
     *
     * @param string $method
     * @return Closure
     */
    private function createClosure(string $method): Closure
    {
        return function (...$args) use ($method) {
            $wrapper = new Optional($this);

            return $wrapper->$method(...$args);
        };
    }
}
