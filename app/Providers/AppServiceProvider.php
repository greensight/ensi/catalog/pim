<?php

namespace App\Providers;

use App\Domain\Imports\Data\DataHolder;
use App\Domain\Products\Publication\ProductPublisher;
use App\Domain\Products\Publication\ProductPublisherFactory;
use App\Support\Extensions\OptionalMacros;
use App\Support\Log\CategoriesLoggerAwareInterface;
use App\Support\Log\DefaultLoggerAwareInterface;
use Carbon\CarbonImmutable;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\DateFactory;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Optional;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        DateFactory::useClass(CarbonImmutable::class);

        $this->app->bind(ProductPublisherFactory::class, fn (Application $app) => ProductPublisherFactory::new());
        $this->app->bind(
            ProductPublisher::class,
            fn (Application $app) => $app->make(ProductPublisherFactory::class)->create()
        );
        $this->app->singleton(DataHolder::class, fn (Application $app) => new DataHolder());

        $this->attachLoggers();
    }

    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);
        Optional::mixin(new OptionalMacros());

        QueryBuilderRequest::setFilterArrayValueDelimiter('');
    }

    private function attachLoggers(): void
    {
        $this->app->afterResolving(
            DefaultLoggerAwareInterface::class,
            fn (DefaultLoggerAwareInterface $resolved) => $resolved->setLogger(Log::channel())
        );

        $this->app->afterResolving(
            CategoriesLoggerAwareInterface::class,
            fn (CategoriesLoggerAwareInterface $resolved) => $resolved->setLogger(Log::channel('categories'))
        );
    }
}
