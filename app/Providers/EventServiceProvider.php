<?php

namespace App\Providers;

use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Categories\Events\CategoryInvalidated;
use App\Domain\Categories\Events\GluingPropertyDeleted;
use App\Domain\Categories\Events\PropertyInvalidated;
use App\Domain\Categories\Listeners\ActualizeCategoryListener;
use App\Domain\Categories\Listeners\ActualizePropertyCategoriesListener;
use App\Domain\Categories\Listeners\ActualizeSubCategoriesListener;
use App\Domain\Categories\Listeners\GluingPropertyDeletedListener;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryProductLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Observers\ActualCategoryPropertyAfterCommitObserver;
use App\Domain\Categories\Observers\CategoryAfterCommitObserver;
use App\Domain\Categories\Observers\CategoryProductLinkObserver;
use App\Domain\Categories\Observers\DirectoryValueAfterCommitObserver;
use App\Domain\Categories\Observers\DirectoryValueObserver;
use App\Domain\Categories\Observers\PropertyAfterCommitObserver;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Classifiers\Observers\BrandAfterCommitObserver;
use App\Domain\Classifiers\Observers\BrandObserver;
use App\Domain\Common\Models\BulkOperation;
use App\Domain\Products\Events\ProductEvent;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Events\ProductPropertyValueDeleted;
use App\Domain\Products\Listeners\ActualizeProductListener;
use App\Domain\Products\Listeners\AutoDeleteProductsFromProductGroupListener;
use App\Domain\Products\Listeners\ProductEventListener;
use App\Domain\Products\Listeners\VerifyProductListener;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductGroup;
use App\Domain\Products\Models\ProductGroupProduct;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Domain\Products\Observers\ProductGroupAfterCommitObserver;
use App\Domain\Products\Observers\ProductGroupObserver;
use App\Domain\Products\Observers\ProductGroupProductAfterCommitObserver;
use App\Domain\Products\Observers\ProductImageObserver;
use App\Domain\Products\Observers\ProductObserver;
use App\Domain\Products\Observers\ProductPropertyValueObserver;
use App\Domain\Products\Observers\PublishedImageAfterCommitObserver;
use App\Domain\Products\Observers\PublishedProductAfterCommitObserver;
use App\Domain\Products\Observers\PublishedPropertyValueAfterCommitObserver;
use App\Domain\Support\Events\FileReleased;
use App\Domain\Support\Listeners\ReleaseFileListener;
use App\Domain\Support\Observers\CurrentUserObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        FileReleased::class => [ReleaseFileListener::class],
        CategoryInvalidated::class => [ActualizeCategoryListener::class],
        CategoryActualized::class => [
            ActualizeSubCategoriesListener::class,
            VerifyProductListener::class,
        ],
        PropertyInvalidated::class => [ActualizePropertyCategoriesListener::class],
        ProductInvalidated::class => [ActualizeProductListener::class],
        ProductPropertyValueDeleted::class => [AutoDeleteProductsFromProductGroupListener::class],
        GluingPropertyDeleted::class => [GluingPropertyDeletedListener::class],
        ProductEvent::class => [ProductEventListener::class],
    ];

    public function boot(): void
    {
        parent::boot();

        PropertyDirectoryValue::observe([DirectoryValueObserver::class, DirectoryValueAfterCommitObserver::class]);
        ProductPropertyValue::observe(ProductPropertyValueObserver::class);
        ProductImage::observe(ProductImageObserver::class);
        Product::observe(ProductObserver::class);
        Brand::observe([BrandObserver::class, BrandAfterCommitObserver::class]);
        Category::observe(CategoryAfterCommitObserver::class);
        PublishedImage::observe(PublishedImageAfterCommitObserver::class);
        PublishedProduct::observe(PublishedProductAfterCommitObserver::class);
        PublishedPropertyValue::observe(PublishedPropertyValueAfterCommitObserver::class);
        Property::observe(PropertyAfterCommitObserver::class);
        ActualCategoryProperty::observe(ActualCategoryPropertyAfterCommitObserver::class);
        ProductGroup::observe(ProductGroupObserver::class);
        ProductGroup::observe(ProductGroupAfterCommitObserver::class);
        ProductGroupProduct::observe(ProductGroupProductAfterCommitObserver::class);
        BulkOperation::observe(CurrentUserObserver::class);
        CategoryProductLink::observe(CategoryProductLinkObserver::class);
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
